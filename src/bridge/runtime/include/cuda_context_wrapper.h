#pragma once

extern thread_local bool g_hasContextBeenCreated;

#define ACTIVE_CONTEXT()   g_hasContextBeenCreated = true;
#define DEACTIVE_CONTEXT() g_hasContextBeenCreated = false;
