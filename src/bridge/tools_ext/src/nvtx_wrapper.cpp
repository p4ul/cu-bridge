#include "nvtx_wrapper.h"

/* ====================== mcToolsExt.h ============================== */
void wnvtxInitialize_v3(const void *reserved)
{
    /* TODO:mctx-bwang1 */
    mctxInitialize(reserved);
    return;
}

int wnvtxInitialize_v2(const mctxInitializationAttributes_t* initAttrib)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

void wnvtxDomainMarkEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxMarkEx(const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxMarkA(const char *message)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxMarkW(const wchar_t *message)
{
    /* TODO:mctx-bwang1 */
    return;
}

mctxRangeId_t wnvtxDomainRangeStartEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

mctxRangeId_t wnvtxRangeStartEx(const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

mctxRangeId_t wnvtxRangeStartA(const char *message)
{
    /* TODO:mctx-bwang1 */
    return -1;
}
mctxRangeId_t wnvtxRangeStartW(const wchar_t *message)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

void wnvtxDomainRangeEnd(mctxDomainHandle_t domain, mctxRangeId_t id)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxRangeEnd(mctxRangeId_t id)
{
    /* TODO:mctx-bwang1 */
    return;
}

int wnvtxDomainRangePushEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

int wnvtxRangePushEx(const mctxEventAttributes_t *eventAttrib)
{
    /* TODO:mctx-bwang1 */
    return mctxRangePushEx(eventAttrib);
}

int wnvtxRangePushA(const char *message)
{
    /* TODO:mctx-bwang1 */
    return mctxRangePushA(message);
}
int wnvtxRangePushW(const wchar_t *message)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

int wnvtxDomainRangePop(mctxDomainHandle_t domain)
{
    /* TODO:mctx-bwang1 */
    return -1;
}

int wnvtxRangePop(void)
{
   return mctxRangePop();
}

mctxResourceHandle_t wnvtxDomainResourceCreate(mctxDomainHandle_t domain, mctxResourceAttributes_t *attribs)
{
    /* TODO:mctx-bwang1 */
    return 0;
}

void wnvtxDomainResourceDestroy(mctxResourceHandle_t resource)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxDomainNameCategoryA(mctxDomainHandle_t domain, uint32_t category, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxDomainNameCategoryW(mctxDomainHandle_t domain, uint32_t category, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCategoryA(uint32_t category, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCategoryW(uint32_t category, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameOsThreadA(uint32_t threadId, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameOsThreadW(uint32_t threadId, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

mctxStringHandle_t wnvtxDomainRegisterStringA(mctxDomainHandle_t domain, const char *string)
{
    /* TODO:mctx-bwang1 */
    return 0;
}
mctxStringHandle_t wnvtxDomainRegisterStringW(mctxDomainHandle_t domain, const wchar_t *string)
{
    /* TODO:mctx-bwang1 */
    return 0;
}

mctxDomainHandle_t wnvtxDomainCreateA(const char *name)
{
    /* TODO:mctx-bwang1 */
    return 0;
}
mctxDomainHandle_t wnvtxDomainCreateW(const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return 0;
}

void wnvtxDomainDestroy(mctxDomainHandle_t domain)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* ====================== mcToolsExt.h end ============================== */

/* ====================== mcToolsExtCuda.h ================================ */
void wnvtxNameCuDeviceA(mcDevice_t device, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCuDeviceW(mcDevice_t device, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCuContextA(mcCtx_t context, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCuContextW(mcCtx_t context, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCuStreamA(mcStream_t stream, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCuStreamW(mcStream_t stream, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCuEventA(mcEvent_t event, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCuEventW(mcEvent_t event, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* ====================== mcToolsExtCuda.h end ============================ */

/* ====================== mcToolsExtCudaRt.h ============================== */
void wnvtxNameCudaDeviceA(int device, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCudaDeviceW(int device, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCudaStreamA(mcStream_t stream, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCudaStreamW(mcStream_t stream, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameCudaEventA(mcEvent_t event, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameCudaEventW(mcEvent_t event, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* ====================== mcToolsExtCudaRt.h end ============================== */

/* ====================== mcToolsExtOpenCL.h ============================== */
void wnvtxNameClDeviceA(cl_device_id device, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClDeviceW(cl_device_id device, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* Annotates an OpenCL context. */
void wnvtxNameClContextA(cl_context context, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClContextW(cl_context context, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameClCommandQueueA(cl_command_queue command_queue, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClCommandQueueW(cl_command_queue command_queue, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameClMemObjectA(cl_mem memobj, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClMemObjectW(cl_mem memobj, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameClSamplerA(cl_sampler sampler, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClSamplerW(cl_sampler sampler, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameClProgramA(cl_program program, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClProgramW(cl_program program, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxNameClEventA(cl_event evnt, const char *name)
{
    /* TODO:mctx-bwang1 */
    return;
}
void wnvtxNameClEventW(cl_event evnt, const wchar_t *name)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* ====================== mcToolsExtOpenCL.h end ============================== */

/* ====================== mcToolsExtSync.h ============================== */
mctxSyncUser_t wnvtxDomainSyncUserCreate(mctxDomainHandle_t domain, const mctxSyncUserAttributes_t *attribs)
{
    /* TODO:mctx-bwang1 */
    return 0;
}

void wnvtxDomainSyncUserDestroy(mctxSyncUser_t handle)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxDomainSyncUserAcquireStart(mctxSyncUser_t handle)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxDomainSyncUserAcquireFailed(mctxSyncUser_t handle)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxDomainSyncUserAcquireSuccess(mctxSyncUser_t handle)
{
    /* TODO:mctx-bwang1 */
    return;
}

void wnvtxDomainSyncUserReleasing(mctxSyncUser_t handle)
{
    /* TODO:mctx-bwang1 */
    return;
}

/* ====================== mcToolsExtSync.h end ============================== */


/* ====================== mcToolsExtPayload.h ============================== */
uint64_t wnvtxPayloadSchemaRegister(mctxDomainHandle_t domain, const mctxPayloadSchemaAttr_t *attr)
{
    return mctxPayloadSchemaRegister(domain, attr);
}

uint64_t wnvtxPayloadEnumRegister(mctxDomainHandle_t domain, const mctxPayloadEnumAttr_t *attr)
{
    return mctxPayloadEnumRegister(domain, attr);
}

/* ====================== mcToolsExtPayload.h end ============================== */
