#include "symbol_cu.h"

#ifdef __cplusplus
extern "C"
{
#endif

    mcDrvError_t
    cuGetErrorString(mcDrvError_t
                         error,
                     const char **pStr)
    {
        return wcuGetErrorString(error, pStr);
    }

    mcDrvError_t
    cuGetErrorName(mcDrvError_t
                       error,
                   const char **pStr)
    {
        return wcuGetErrorName(error, pStr);
    }

    mcDrvError_t
    cuInit(unsigned int Flags)
    {
        return wcuInit(Flags);
    }

    mcDrvError_t
    cuDriverGetVersion(int *driverVersion)
    {
        return wcuDriverGetVersion(driverVersion);
    }

    mcDrvError_t
    cuDeviceGet(MCdevice *device, int ordinal)
    {
        return wcuDeviceGet(device, ordinal);
    }

    mcDrvError_t
    cuDeviceGetCount(int *count)
    {
        return wcuDeviceGetCount(count);
    }

    mcDrvError_t
    cuDeviceGetName(char *name, int len, MCdevice dev)
    {
        return wcuDeviceGetName(name, len, dev);
    }

    mcDrvError_t
    cuDeviceGetUuid(MCuuid *uuid, MCdevice dev)
    {
        return wcuDeviceGetUuid(uuid, dev);
    }

    mcDrvError_t
    cuDeviceGetUuid_v2(MCuuid *uuid, MCdevice dev)
    {
        return wcuDeviceGetUuid_v2(uuid, dev);
    }

    mcDrvError_t
    cuDeviceGetLuid(char *luid, unsigned int *deviceNodeMask,
                    MCdevice dev)
    {
        return wcuDeviceGetLuid(luid, deviceNodeMask, dev);
    }

    mcDrvError_t
    cuDeviceTotalMem(size_t *bytes, MCdevice dev)
    {
        return wcuDeviceTotalMem(bytes, dev);
    }

    mcDrvError_t
    cuDeviceGetTexture1DLinearMaxWidth(size_t *maxWidthInElements,
                                       mcArray_Format format,
                                       unsigned numChannels,
                                       MCdevice dev)
    {
        return wcuDeviceGetTexture1DLinearMaxWidth(maxWidthInElements, format, numChannels, dev);
    }

    mcDrvError_t
    cuDeviceGetAttribute(int *pi, mcDrvDeviceAttribute_t attrib,
                         MCdevice dev)
    {
        return wcuDeviceGetAttribute(pi, attrib, dev);
    }

    mcDrvError_t
    cuDeviceGetNvSciSyncAttributes(void *nvSciSyncAttrList,
                                   MCdevice dev, int flags)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuDeviceSetMemPool(MCdevice dev, mcDrvMemPool_t pool)
    {
        return wcuDeviceSetMemPool(dev, pool);
    }

    mcDrvError_t
    cuDeviceGetMemPool(mcDrvMemPool_t *pool, MCdevice dev)
    {
        return wcuDeviceGetMemPool(pool, dev);
    }

    mcDrvError_t
    cuDeviceGetDefaultMemPool(mcDrvMemPool_t *pool_out,
                              MCdevice dev)
    {
        return wcuDeviceGetDefaultMemPool(pool_out, dev);
    }

    mcDrvError_t
    cuDeviceGetExecAffinitySupport(int *pi,
                                   mcDrvexecAffinityType type,
                                   MCdevice dev)
    {
        return wcuDeviceGetExecAffinitySupport(pi, type, dev);
    }

    mcDrvError_t
    cuFlushGPUDirectRDMAWrites(mcDrvFlushGPUDirectRDMAWritesTarget target,
                               mcDrvFlushGPUDirectRDMAWritesScope scope)
    {
        return wcuFlushGPUDirectRDMAWrites(target, scope);
    }

    mcDrvError_t
    cuDeviceGetProperties(mcDrvDevprop *prop,
                          MCdevice dev)
    {
        return wcuDeviceGetProperties(prop, dev);
    }

    mcDrvError_t
    cuDeviceComputeCapability(int *major,
                              int *minor,
                              MCdevice dev)
    {
        return wcuDeviceComputeCapability(major, minor, dev);
    }

    mcDrvError_t
    cuDevicePrimaryCtxRetain(MCcontext *pctx, MCdevice dev)
    {
        return wcuDevicePrimaryCtxRetain(pctx, dev);
    }

    mcDrvError_t
    cuDevicePrimaryCtxRelease(MCdevice dev)
    {
        return wcuDevicePrimaryCtxRelease(dev);
    }

    mcDrvError_t
    cuDevicePrimaryCtxSetFlags(MCdevice dev, unsigned int flags)
    {
        return wcuDevicePrimaryCtxSetFlags(dev, flags);
    }

    mcDrvError_t
    cuDevicePrimaryCtxGetState(MCdevice dev, unsigned int *flags,
                               int *active)
    {
        return wcuDevicePrimaryCtxGetState(dev, flags, active);
    }

    mcDrvError_t
    cuDevicePrimaryCtxReset(MCdevice dev)
    {
        return wcuDevicePrimaryCtxReset(dev);
    }

    mcDrvError_t
    cuCtxCreate(MCcontext *pctx, unsigned int flags,
                MCdevice dev)
    {
        return wcuCtxCreate(pctx, flags, dev);
    }

    mcDrvError_t
    cuCtxCreate_v3(MCcontext *pctx,
                   mcDrvexecAffinityParam_t *paramsArray, int numParams,
                   unsigned int flags, MCdevice dev)
    {
        return wcuCtxCreate_v3(pctx, paramsArray, numParams, flags, dev);
    }

    mcDrvError_t
    cuCtxDestroy(MCcontext ctx)
    {
        return wcuCtxDestroy(ctx);
    }

    mcDrvError_t
    cuCtxPushCurrent(MCcontext ctx)
    {
        return wcuCtxPushCurrent(ctx);
    }

    mcDrvError_t
    cuCtxPopCurrent(MCcontext *pctx)
    {

        return wcuCtxPopCurrent(pctx);
    }

    mcDrvError_t
    cuCtxSetCurrent(MCcontext ctx)
    {
        return wcuCtxSetCurrent(ctx);
    }

    mcDrvError_t
    cuCtxGetCurrent(MCcontext *pctx)
    {
        return wcuCtxGetCurrent(pctx);
    }

    mcDrvError_t
    cuCtxGetDevice(MCdevice *device)
    {
        return wcuCtxGetDevice(device);
    }

    mcDrvError_t
    cuCtxGetFlags(unsigned int *flags)
    {
        return wcuCtxGetFlags(flags);
    }

    mcDrvError_t
    cuCtxGetId(MCcontext ctx, unsigned long long *ctxId)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuCtxSynchronize(void)
    {
        return wcuCtxSynchronize();
    }

    mcDrvError_t
    cuCtxSetLimit(mcDrvlimit_t limit, size_t value)
    {
        return wcuCtxSetLimit(limit, value);
    }

    mcDrvError_t
    cuCtxGetLimit(size_t *pvalue, mcDrvlimit_t limit)
    {
        return wcuCtxGetLimit(pvalue, limit);
    }

    mcDrvError_t
    cuCtxGetCacheConfig(mcDrvfunc_cache_t *pconfig)
    {
        return wcuCtxGetCacheConfig(pconfig);
    }

    mcDrvError_t
    cuCtxSetCacheConfig(mcDrvfunc_cache_t config)
    {
        return wcuCtxSetCacheConfig(config);
    }

    mcDrvError_t
    cuCtxGetSharedMemConfig(mcDrvsharedconfig_t *pConfig)
    {
        return wcuCtxGetSharedMemConfig(pConfig);
    }

    mcDrvError_t
    cuCtxSetSharedMemConfig(mcDrvsharedconfig_t config)
    {
        return wcuCtxSetSharedMemConfig(config);
    }

    mcDrvError_t
    cuCtxGetApiVersion(MCcontext ctx, unsigned int *version)
    {
        return wcuCtxGetApiVersion(ctx, version);
    }

    mcDrvError_t
    cuCtxGetStreamPriorityRange(int *leastPriority,
                                int *greatestPriority)
    {
        return wcuCtxGetStreamPriorityRange(leastPriority, greatestPriority);
    }

    mcDrvError_t
    cuCtxResetPersistingL2Cache(void)
    {
        return wcuCtxResetPersistingL2Cache();
    }

    mcDrvError_t
    cuCtxGetExecAffinity(mcDrvexecAffinityParam_t *pExecAffinity,
                         mcDrvexecAffinityType type)
    {

        return wcuCtxGetExecAffinity(pExecAffinity, type);
    }

    mcDrvError_t
    cuCtxAttach(MCcontext *pctx,
                unsigned int flags)
    {
        return wcuCtxAttach(pctx, flags);
    }

    mcDrvError_t
    cuCtxDetach(MCcontext ctx) { return wcuCtxDetach(ctx); }

    mcDrvError_t
    cuModuleLoad(MCmodule *module, const char *fname)
    {
        return wcuModuleLoad(module, fname);
    }

    mcDrvError_t
    cuModuleLoadData(MCmodule *module, const void *image)
    {
        return wcuModuleLoadData(module, image);
    }

    mcDrvError_t
    cuModuleLoadDataEx(MCmodule *module, const void *image,
                       unsigned int numOptions,
                       mcDrvjit_option_t *options,
                       void **optionValues)
    {
        return wcuModuleLoadDataEx(module, image, numOptions, options, optionValues);
    }

    mcDrvError_t
    cuModuleLoadFatBinary(MCmodule *module, const void *fatCubin)
    {
        return wcuModuleLoadFatBinary(module, fatCubin);
    }

    mcDrvError_t
    cuModuleUnload(MCmodule hmod)
    {
        return wcuModuleUnload(hmod);
    }

    mcDrvError_t
    cuModuleGetLoadingMode(int *mode)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuModuleGetFunction(MCfunction *hfunc, MCmodule hmod,
                        const char *name)
    {
        return wcuModuleGetFunction(hfunc, hmod, name);
    }

    mcDrvError_t
    cuModuleGetGlobal(mcDrvDeviceptr_t *dptr, size_t *bytes,
                      MCmodule hmod, const char *name)
    {
        return wcuModuleGetGlobal(dptr, bytes, hmod, name);
    }

    mcDrvError_t
    cuLinkCreate(unsigned int numOptions, mcDrvjit_option_t *options,
                 void **optionValues, mcDrvlinkState_t *stateOut)
    {
        return wcuLinkCreate(numOptions, options, optionValues, stateOut);
    }

    mcDrvError_t
    cuLinkAddData(mcDrvlinkState_t state, mcDrvJitInputType_t type,
                  void *data, size_t size, const char *name,
                  unsigned int numOptions, mcDrvjit_option_t *options,
                  void **optionValues)
    {
        return wcuLinkAddData(state, type, data, size, name, numOptions, options, optionValues);
    }

    mcDrvError_t
    cuLinkAddFile(mcDrvlinkState_t state, mcDrvJitInputType_t type,
                  const char *path, unsigned int numOptions,
                  mcDrvjit_option_t *options, void **optionValues)
    {
        return wcuLinkAddFile(state, type, path, numOptions, options, optionValues);
    }

    mcDrvError_t
    cuLinkComplete(mcDrvlinkState_t state, void **cubinOut,
                   size_t *sizeOut)
    {
        return wcuLinkComplete(state, cubinOut, sizeOut);
    }

    mcDrvError_t
    cuLinkDestroy(mcDrvlinkState_t state)
    {
        return wcuLinkDestroy(state);
    }

    mcDrvError_t
    cuModuleGetTexRef(mcDrvTexref *pTexRef,
                      MCmodule hmod,
                      const char *name)
    {
        return wcuModuleGetTexRef(pTexRef, hmod, name);
    }

    mcDrvError_t
    cuModuleGetSurfRef(mcDrvSurfref *pSurfRef,
                       MCmodule hmod,
                       const char *name)
    {
        return wcuModuleGetSurfRef(pSurfRef, hmod, name);
    }

    mcDrvError_t
    cuLibraryLoadData(MClibrary *library, const void *code,
                      mcDrvjit_option_t *jitOptions,
                      void **jitOptionsValues,
                      unsigned int numJitOptions,
                      void *libraryOptions,
                      void **libraryOptionValues,
                      unsigned int numLibraryOptions)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryLoadFromFile(MClibrary *library, const char *fileName,
                          mcDrvjit_option_t *jitOptions,
                          void **jitOptionsValues,
                          unsigned int numJitOptions,
                          void *libraryOptions,
                          void **libraryOptionValues,
                          unsigned int numLibraryOptions)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryUnload(MClibrary library)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryGetKernel(MCkernel *pKernel, MClibrary library,
                       const char *name)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryGetModule(MCmodule *pMod, MClibrary library)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuKernelGetFunction(MCfunction *pFunc, MCkernel kernel)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryGetGlobal(mcDrvDeviceptr_t *dptr, size_t *bytes,
                       MClibrary library, const char *name)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryGetManaged(mcDrvDeviceptr_t *dptr, size_t *bytes,
                        MClibrary library, const char *name)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLibraryGetUnifiedFunction(void **fptr, MClibrary library,
                                const char *symbol)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuKernelGetAttribute(int *pi, mcFunction_attribute attrib,
                         MCkernel kernel, MCdevice dev)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuKernelSetAttribute(mcFunction_attribute attrib, int val,
                         MCkernel kernel, MCdevice dev)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }
    mcDrvError_t
    cuKernelSetCacheConfig(MCkernel kernel, mcDrvfunc_cache_t config,
                           MCdevice dev)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemGetInfo(size_t *free, size_t *total)
    {
        return wcuMemGetInfo(free, total);
    }

    mcDrvError_t
    cuMemAlloc(mcDrvDeviceptr_t *dptr, size_t bytesize)
    {
        return wcuMemAlloc(dptr, bytesize);
    }

    mcDrvError_t
    cuMemAllocPitch(mcDrvDeviceptr_t *dptr, size_t *pPitch,
                    size_t WidthInBytes, size_t Height,
                    unsigned int ElementSizeBytes)
    {
        return wcuMemAllocPitch(dptr, pPitch, WidthInBytes, Height, ElementSizeBytes);
    }

    mcDrvError_t
    cuMemFree(mcDrvDeviceptr_t dptr)
    {
        return wcuMemFree(dptr);
    }

    mcDrvError_t
    cuMemGetAddressRange(mcDrvDeviceptr_t *pbase, size_t *psize,
                         mcDrvDeviceptr_t dptr)
    {
        return wcuMemGetAddressRange(pbase, psize, dptr);
    }

    mcDrvError_t
    cuMemAllocHost(void **pp, size_t bytesize)
    {
        return wcuMemAllocHost(pp, bytesize);
    }

    mcDrvError_t
    cuMemFreeHost(void *p)
    {
        return cuMemFreeHost(p);
    }

    mcDrvError_t
    cuMemHostAlloc(void **pp, size_t bytesize,
                   unsigned int Flags)
    {
        return wcuMemHostAlloc(pp, bytesize, Flags);
    }

    mcDrvError_t
    cuMemHostGetDevicePointer(mcDrvDeviceptr_t *pdptr, void *p,
                              unsigned int Flags)
    {
        return wcuMemHostGetDevicePointer(pdptr, p, Flags);
    }

    mcDrvError_t
    cuMemHostGetFlags(unsigned int *pFlags, void *p)
    {
        return wcuMemHostGetFlags(pFlags, p);
    }

    mcDrvError_t
    cuMemAllocManaged(mcDrvDeviceptr_t *dptr, size_t bytesize,
                      unsigned int flags)
    {
        return wcuMemAllocManaged(dptr, bytesize, flags);
    }

    mcDrvError_t
    cuDeviceGetByPCIBusId(MCdevice *dev, const char *pciBusId)
    {
        return wcuDeviceGetByPCIBusId(dev, pciBusId);
    }

    mcDrvError_t
    cuDeviceGetPCIBusId(char *pciBusId, int len, MCdevice dev)
    {
        return wcuDeviceGetPCIBusId(pciBusId, len, dev);
    }

    mcDrvError_t
    cuIpcGetEventHandle(mcDrvIpcEventHandle_t *pHandle, MCevent event)
    {
        return wcuIpcGetEventHandle(pHandle, event);
    }

    mcDrvError_t
    cuIpcOpenEventHandle(MCevent *phEvent,
                         mcDrvIpcEventHandle_t handle)
    {
        return wcuIpcOpenEventHandle(phEvent, handle);
    }

    mcDrvError_t
    cuIpcGetMemHandle(mcDrvIpcMemHandle_t *pHandle, mcDrvDeviceptr_t dptr)
    {
        return wcuIpcGetMemHandle(pHandle, dptr);
    }

    mcDrvError_t
    cuIpcOpenMemHandle(mcDrvDeviceptr_t *pdptr, mcDrvIpcMemHandle_t handle,
                       unsigned int Flags)
    {
        return wcuIpcOpenMemHandle(pdptr, handle, Flags);
    }

    mcDrvError_t
    cuIpcCloseMemHandle(mcDrvDeviceptr_t dptr)
    {
        return wcuIpcCloseMemHandle(dptr);
    }

    mcDrvError_t
    cuMemHostRegister(void *p, size_t bytesize,
                      unsigned int Flags)
    {
        return wcuMemHostRegister(p, bytesize, Flags);
    }

    mcDrvError_t
    cuMemHostUnregister(void *p)
    {
        return wcuMemHostUnregister(p);
    }

    mcDrvError_t
    cuMemcpy(mcDrvDeviceptr_t dst, mcDrvDeviceptr_t src, size_t ByteCount)
    {
        return wcuMemcpy(dst, src, ByteCount);
    }

    mcDrvError_t
    cuMemcpyPeer(mcDrvDeviceptr_t dstDevice, MCcontext dstContext,
                 mcDrvDeviceptr_t srcDevice, MCcontext srcContext,
                 size_t ByteCount)
    {

        return wcuMemcpyPeer(dstDevice, dstContext, srcDevice, srcContext, ByteCount);
    }

    mcDrvError_t
    cuMemcpyHtoD(mcDrvDeviceptr_t dstDevice, const void *srcHost,
                 size_t ByteCount)
    {
        return wcuMemcpyHtoD(dstDevice, srcHost, ByteCount);
    }

    mcDrvError_t
    cuMemcpyDtoH(void *dstHost, mcDrvDeviceptr_t srcDevice,
                 size_t ByteCount)
    {
        return wcuMemcpyDtoH(dstHost, srcDevice, ByteCount);
    }
    mcDrvError_t
    cuMemcpyDtoD(mcDrvDeviceptr_t dstDevice, mcDrvDeviceptr_t srcDevice,
                 size_t ByteCount)
    {
        return wcuMemcpyDtoD(dstDevice, srcDevice, ByteCount);
    }

    mcDrvError_t
    cuMemcpyDtoA(MCarray dstArray, size_t dstOffset,
                 mcDrvDeviceptr_t srcDevice, size_t ByteCount)
    {
        return wcuMemcpyDtoA(dstArray, dstOffset, srcDevice, ByteCount);
    }

    mcDrvError_t
    cuMemcpyAtoD(mcDrvDeviceptr_t dstDevice, MCarray srcArray,
                 size_t srcOffset, size_t ByteCount)
    {
        return wcuMemcpyAtoD(dstDevice, srcArray, srcOffset, ByteCount);
    }

    mcDrvError_t
    cuMemcpyHtoA(MCarray dstArray, size_t dstOffset,
                 const void *srcHost, size_t ByteCount)
    {
        return wcuMemcpyHtoA(dstArray, dstOffset, srcHost, ByteCount);
    }

    mcDrvError_t
    cuMemcpyAtoH(void *dstHost, MCarray srcArray, size_t srcOffset,
                 size_t ByteCount)
    {
        return wcuMemcpyAtoH(dstHost, srcArray, srcOffset, ByteCount);
    }

    mcDrvError_t
    cuMemcpyAtoA(MCarray dstArray, size_t dstOffset,
                 MCarray srcArray, size_t srcOffset,
                 size_t ByteCount)
    {
        return wcuMemcpyAtoA(dstArray, dstOffset, srcArray, srcOffset, ByteCount);
    }

    mcDrvError_t
    cuMemcpy2D(const mcDrvMemcpy2D *pCopy)
    {
        return wcuMemcpy2D(pCopy);
    }

    mcDrvError_t
    cuMemcpy2DUnaligned(const mcDrvMemcpy2D *pCopy)
    {
        return wcuMemcpy2DUnaligned(pCopy);
    }

    mcDrvError_t
    cuMemcpy3D(const mcDrvMemcpy3D *pCopy)
    {
        return wcuMemcpy3D(pCopy);
    }

    mcDrvError_t
    cuMemcpy3DPeer(const mcDrvMemcpy3DPeer *pCopy)
    {
        return wcuMemcpy3DPeer(pCopy);
    }

    mcDrvError_t
    cuMemcpyAsync(mcDrvDeviceptr_t dst, mcDrvDeviceptr_t src,
                  size_t ByteCount, MCstream hStream)
    {
        return wcuMemcpyAsync(dst, src, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyPeerAsync(mcDrvDeviceptr_t dstDevice, MCcontext dstContext,
                      mcDrvDeviceptr_t srcDevice, MCcontext srcContext,
                      size_t ByteCount, MCstream hStream)
    {
        return wcuMemcpyPeerAsync(dstDevice, dstContext, srcDevice, srcContext, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyHtoDAsync(mcDrvDeviceptr_t dstDevice, const void *srcHost,
                      size_t ByteCount, MCstream hStream)
    {
        return wcuMemcpyHtoDAsync(dstDevice, srcHost, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyDtoHAsync(void *dstHost, mcDrvDeviceptr_t srcDevice,
                      size_t ByteCount, MCstream hStream)
    {

        return wcuMemcpyDtoHAsync(dstHost, srcDevice, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyDtoDAsync(mcDrvDeviceptr_t dstDevice, mcDrvDeviceptr_t srcDevice,
                      size_t ByteCount, MCstream hStream)
    {
        return wcuMemcpyDtoDAsync(dstDevice, srcDevice, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyHtoAAsync(MCarray dstArray, size_t dstOffset,
                      const void *srcHost, size_t ByteCount,
                      MCstream hStream)
    {
        return wcuMemcpyHtoAAsync(dstArray, dstOffset, srcHost, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpyAtoHAsync(void *dstHost, MCarray srcArray,
                      size_t srcOffset, size_t ByteCount,
                      MCstream hStream)
    {
        return wcuMemcpyAtoHAsync(dstHost, srcArray, srcOffset, ByteCount, hStream);
    }

    mcDrvError_t
    cuMemcpy2DAsync(const mcDrvMemcpy2D *pCopy, MCstream hStream)
    {
        return wcuMemcpy2DAsync(pCopy, hStream);
    }

    mcDrvError_t
    cuMemcpy3DAsync(const mcDrvMemcpy3D *pCopy, MCstream hStream)
    {
        return wcuMemcpy3DAsync(pCopy, hStream);
    }

    mcDrvError_t
    cuMemcpy3DPeerAsync(const mcDrvMemcpy3DPeer *pCopy,
                        MCstream hStream)
    {
        return wcuMemcpy3DPeerAsync(pCopy, hStream);
    }

    mcDrvError_t
    cuMemsetD8(mcDrvDeviceptr_t dstDevice, unsigned char uc, size_t N)
    {
        return wcuMemsetD8(dstDevice, uc, N);
    }

    mcDrvError_t
    cuMemsetD16(mcDrvDeviceptr_t dstDevice, unsigned short us,
                size_t N)
    {
        return wcuMemsetD16(dstDevice, us, N);
    }

    mcDrvError_t
    cuMemsetD32(mcDrvDeviceptr_t dstDevice, unsigned int ui, size_t N)
    {
        return wcuMemsetD32(dstDevice, ui, N);
    }

    mcDrvError_t
    cuMemsetD2D8(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                 unsigned char uc, size_t Width, size_t Height)
    {
        return wcuMemsetD2D8(dstDevice, dstPitch, uc, Width, Height);
    }

    mcDrvError_t
    cuMemsetD2D16(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                  unsigned short us, size_t Width, size_t Height)
    {
        return wcuMemsetD2D16(dstDevice, dstPitch, us, Width, Height);
    }

    mcDrvError_t
    cuMemsetD2D32(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                  unsigned int ui, size_t Width, size_t Height)
    {
        return wcuMemsetD2D32(dstDevice, dstPitch, ui, Width, Height);
    }

    mcDrvError_t
    cuMemsetD8Async(mcDrvDeviceptr_t dstDevice, unsigned char uc,
                    size_t N, MCstream hStream)
    {
        return wcuMemsetD8Async(dstDevice, uc, N, hStream);
    }

    mcDrvError_t
    cuMemsetD16Async(mcDrvDeviceptr_t dstDevice, unsigned short us,
                     size_t N, MCstream hStream)
    {
        return wcuMemsetD16Async(dstDevice, us, N, hStream);
    }

    mcDrvError_t
    cuMemsetD32Async(mcDrvDeviceptr_t dstDevice, unsigned int ui,
                     size_t N, MCstream hStream)
    {
        return wcuMemsetD32Async(dstDevice, ui, N, hStream);
    }

    mcDrvError_t
    cuMemsetD2D8Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                      unsigned char uc, size_t Width,
                      size_t Height, MCstream hStream)
    {
        return wcuMemsetD2D8Async(dstDevice, dstPitch, uc, Width, Height, hStream);
    }

    mcDrvError_t
    cuMemsetD2D16Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                       unsigned short us, size_t Width,
                       size_t Height, MCstream hStream)
    {
        return wcuMemsetD2D16Async(dstDevice, dstPitch, us, Width, Height, hStream);
    }

    mcDrvError_t
    cuMemsetD2D32Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                       unsigned int ui, size_t Width,
                       size_t Height, MCstream hStream)
    {
        return wcuMemsetD2D32Async(dstDevice, dstPitch, ui, Width, Height, hStream);
    }

    mcDrvError_t
    cuArrayCreate(MCarray *pHandle,
                  const mcDrvArrayDescriptor *pAllocateArray)
    {
        return wcuArrayCreate(pHandle, pAllocateArray);
    }

    mcDrvError_t
    cuArrayGetDescriptor(mcDrvArrayDescriptor *pArrayDescriptor,
                         MCarray hArray)
    {
        return wcuArrayGetDescriptor(pArrayDescriptor, hArray);
    }

    mcDrvError_t
    cuArrayGetSparseProperties(
        mcDrvArraySparseProperties *sparseProperties, MCarray array)
    {
        return wcuArrayGetSparseProperties(sparseProperties, array);
    }

    mcDrvError_t
    cuMipmappedArrayGetSparseProperties(
        mcDrvArraySparseProperties *sparseProperties, mcDrvMipmappedArray mipmap)
    {
        return wcuMipmappedArrayGetSparseProperties(sparseProperties, mipmap);
    }

    mcDrvError_t
    cuArrayGetMemoryRequirements(mcDrvArrayMemoryRequirements *memoryRequirements,
                                 MCarray array, MCdevice device)
    {
        return wcuArrayGetMemoryRequirements(memoryRequirements, array, device);
    }

    mcDrvError_t
    cuMipmappedArrayGetMemoryRequirements(
        mcDrvArrayMemoryRequirements *memoryRequirements, mcDrvMipmappedArray mipmap,
        MCdevice device)
    {
        return wcuMipmappedArrayGetMemoryRequirements(memoryRequirements, mipmap, device);
    }

    mcDrvError_t
    cuArrayGetPlane(MCarray *pPlaneArray, MCarray hArray,
                    unsigned int planeIdx)
    {
        return wcuArrayGetPlane(pPlaneArray, hArray, planeIdx);
    }

    mcDrvError_t
    cuArrayDestroy(MCarray hArray)
    {
        return wcuArrayDestroy(hArray);
    }

    mcDrvError_t
    cuArray3DCreate(
        MCarray *pHandle, const mcDrvArray3DDescriptor *pAllocateArray)
    {
        return wcuArray3DCreate(pHandle, pAllocateArray);
    }

    mcDrvError_t
    cuArray3DGetDescriptor(
        mcDrvArray3DDescriptor *pArrayDescriptor, MCarray hArray)
    {
        return wcuArray3DGetDescriptor(pArrayDescriptor, hArray);
    }

    mcDrvError_t
    cuMipmappedArrayCreate(mcDrvMipmappedArray *pHandle,
                           const mcDrvArray3DDescriptor *pMipmappedArrayDesc,
                           unsigned int numMipmapLevels)
    {
        return wcuMipmappedArrayCreate(pHandle, pMipmappedArrayDesc, numMipmapLevels);
    }

    mcDrvError_t
    cuMipmappedArrayGetLevel(MCarray *pLevelArray,
                             mcDrvMipmappedArray hMipmappedArray,
                             unsigned int level)
    {
        return wcuMipmappedArrayGetLevel(pLevelArray, hMipmappedArray, level);
    }

    mcDrvError_t
    cuMipmappedArrayDestroy(mcDrvMipmappedArray hMipmappedArray)
    {
        return wcuMipmappedArrayDestroy(hMipmappedArray);
    }

    mcDrvError_t
    cuMemGetHandleForAddressRange(void *handle, mcDrvDeviceptr_t dptr,
                                  size_t size,
                                  int handleType,
                                  unsigned long long flags)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemAddressReserve(mcDrvDeviceptr_t *ptr, size_t size,
                        size_t alignment, mcDrvDeviceptr_t addr,
                        unsigned long long flags)
    {
        return wcuMemAddressReserve(ptr, size, alignment, addr, flags);
    }

    mcDrvError_t
    cuMemAddressFree(mcDrvDeviceptr_t ptr, size_t size)
    {
        return wcuMemAddressFree(ptr, size);
    }

    mcDrvError_t
    cuMemCreate(mcDrvMemGenericAllocationHandle_t *handle, size_t size,
                const mcDrvMemAllocationProp_t *prop,
                unsigned long long flags)
    {
        return wcuMemCreate(handle, size, prop, flags);
    }
    mcDrvError_t
    cuMemRelease(mcDrvMemGenericAllocationHandle_t handle)
    {
        return wcuMemRelease(handle);
    }

    mcDrvError_t
    cuMemMap(mcDrvDeviceptr_t ptr, size_t size, size_t offset,
             mcDrvMemGenericAllocationHandle_t handle,
             unsigned long long flags)
    {
        return wcuMemMap(ptr, size, offset, handle, flags);
    }

    mcDrvError_t
    cuMemMapArrayAsync(mcDrvArrayMapInfo *mapInfoList,
                       unsigned int count, MCstream hStream)
    {
        return wcuMemMapArrayAsync(mapInfoList, count, hStream);
    }
    mcDrvError_t
    cuMemUnmap(mcDrvDeviceptr_t ptr, size_t size)
    {
        return wcuMemUnmap(ptr, size);
    }

    mcDrvError_t
    cuMemSetAccess(mcDrvDeviceptr_t ptr, size_t size,
                   const mcDrvMemAccessDesc_t *desc, size_t count)
    {
        return wcuMemSetAccess(ptr, size, desc, count);
    }

    mcDrvError_t
    cuMemGetAccess(unsigned long long *flags,
                   const mcDrvMemLocation_t *location,
                   mcDrvDeviceptr_t ptr)
    {
        return wcuMemGetAccess(flags, location, ptr);
    }

    mcDrvError_t cuMulticastCreate(mcDrvMemGenericAllocationHandle_t *mcHandle,
                                   const mcDrvMulticastObjectProp *prop)
    {
        return wcuMulticastCreate(mcHandle, prop);
    }

    mcDrvError_t cuMulticastAddDevice(mcDrvMemGenericAllocationHandle_t mcHandle, mcDrvDevice_t dev)
    {
        return wcuMulticastAddDevice(mcHandle, dev);
    }

    mcDrvError_t cuMulticastBindMem(mcDrvMemGenericAllocationHandle_t mcHandle, size_t mcOffset,
                                    mcDrvMemGenericAllocationHandle_t memHandle, size_t memOffset,
                                    size_t size, unsigned long long flags)
    {
        return wcuMulticastBindMem(mcHandle, mcOffset, memHandle, memOffset, size, flags);
    }

    mcDrvError_t cuMulticastBindAddr(mcDrvMemGenericAllocationHandle_t mcHandle, size_t mcOffset,
                                     mcDrvDeviceptr_t memptr, size_t size, unsigned long long flags)
    {
        return wcuMulticastBindAddr(mcHandle, mcOffset, memptr, size, flags);
    }

    mcDrvError_t cuMulticastUnbind(mcDrvMemGenericAllocationHandle_t mcHandle, mcDrvDevice_t dev,
                                   size_t mcOffset, size_t size)
    {
        return wcuMulticastUnbind(mcHandle, dev, mcOffset, size);
    }

    mcDrvError_t cuMulticastGetGranularity(size_t *granularity,
                                           const mcDrvMulticastObjectProp *prop,
                                           mcDrvMulticastGranularity_flags option)
    {
        return wcuMulticastGetGranularity(granularity, prop, option);
    }

    mcDrvError_t
    cuMemExportToShareableHandle(
        void *shareableHandle, mcDrvMemGenericAllocationHandle_t handle,
        mcDrvMemAllocationHandleType_t handleType, unsigned long long flags)
    {
        return wcuMemExportToShareableHandle(shareableHandle, handle, handleType, flags);
    }

    mcDrvError_t
    cuMemImportFromShareableHandle(
        mcDrvMemGenericAllocationHandle_t *handle, void *osHandle,
        mcDrvMemAllocationHandleType_t shHandleType)
    {
        return wcuMemImportFromShareableHandle(handle, osHandle, shHandleType);
    }

    mcDrvError_t
    cuMemGetAllocationGranularity(
        size_t *granularity, const mcDrvMemAllocationProp_t *prop,
        mcMemAllocationGranularity_flags option)
    {
        return wcuMemGetAllocationGranularity(granularity, prop, option);
    }

    mcDrvError_t
    cuMemGetAllocationPropertiesFromHandle(
        mcDrvMemAllocationProp_t *prop, mcDrvMemGenericAllocationHandle_t handle)
    {
        return wcuMemGetAllocationPropertiesFromHandle(prop, handle);
    }

    mcDrvError_t
    cuMemRetainAllocationHandle(mcDrvMemGenericAllocationHandle_t *handle, void *addr)
    {
        return wcuMemRetainAllocationHandle(handle, addr);
    }

    mcDrvError_t
    cuMemFreeAsync(mcDrvDeviceptr_t dptr, MCstream hStream)
    {
        return wcuMemFreeAsync(dptr, hStream);
    }

    mcDrvError_t
    cuMemAllocAsync(mcDrvDeviceptr_t *dptr, size_t bytesize,
                    MCstream hStream)
    {
        return wcuMemAllocAsync(dptr, bytesize, hStream);
    }

    mcDrvError_t
    cuMemPoolTrimTo(mcDrvMemPool_t pool, size_t minBytesToKeep)
    {
        return wcuMemPoolTrimTo(pool, minBytesToKeep);
    }

    mcDrvError_t
    cuMemPoolSetAttribute(mcDrvMemPool_t pool,
                          mcDrvMemPoolAttr_t attr, void *value)
    {
        return wcuMemPoolSetAttribute(pool, attr, value);
    }

    mcDrvError_t
    cuMemPoolGetAttribute(mcDrvMemPool_t pool,
                          mcDrvMemPoolAttr_t attr, void *value)
    {
        return wcuMemPoolGetAttribute(pool, attr, value);
    }

    mcDrvError_t
    cuMemPoolSetAccess(mcDrvMemPool_t pool,
                       const mcDrvMemAccessDesc_t *map, size_t count)
    {
        return wcuMemPoolSetAccess(pool, map, count);
    }

    mcDrvError_t
    cuMemPoolGetAccess(mcDrvMemAccess_flags_t *flags,
                       mcDrvMemPool_t memPool,
                       mcDrvMemLocation_t *location)
    {
        return wcuMemPoolGetAccess(flags, memPool, location);
    }

    mcDrvError_t
    cuMemPoolCreate(mcDrvMemPool_t *pool,
                    const mcDrvMemPoolProps_t *poolProps)
    {
        return wcuMemPoolCreate(pool, poolProps);
    }

    mcDrvError_t
    cuMemPoolDestroy(mcDrvMemPool_t pool)
    {
        return wcuMemPoolDestroy(pool);
    }

    mcDrvError_t
    cuMemAllocFromPoolAsync(mcDrvDeviceptr_t *dptr, size_t bytesize,
                            mcDrvMemPool_t pool, MCstream hStream)
    {
        return wcuMemAllocFromPoolAsync(dptr, bytesize, pool, hStream);
    }

    mcDrvError_t
    cuMemPoolExportToShareableHandle(
        void *handle_out, mcDrvMemPool_t pool, mcDrvMemAllocationHandleType_t handleType,
        unsigned long long flags)
    {
        return wcuMemPoolExportToShareableHandle(handle_out, pool, handleType, flags);
    }

    mcDrvError_t
    cuMemPoolImportFromShareableHandle(
        mcDrvMemPool_t *pool_out, void *handle, mcDrvMemAllocationHandleType_t handleType,
        unsigned long long flags)
    {
        return wcuMemPoolImportFromShareableHandle(pool_out, handle, handleType, flags);
    }

    mcDrvError_t
    cuMemPoolExportPointer(mcDrvMemPoolPtrExportData_t *shareData_out,
                           mcDrvDeviceptr_t ptr)
    {
        return wcuMemPoolExportPointer(shareData_out, ptr);
    }

    mcDrvError_t
    cuMemPoolImportPointer(mcDrvDeviceptr_t *ptr_out, mcDrvMemPool_t pool,
                           mcDrvMemPoolPtrExportData_t *shareData)
    {
        return wcuMemPoolImportPointer(ptr_out, pool, shareData);
    }

    mcDrvError_t
    cuPointerGetAttribute(void *data,
                          mcDrvPointerAttributeType attribute,
                          mcDrvDeviceptr_t ptr)
    {
        return wcuPointerGetAttribute(data, attribute, ptr);
    }

    mcDrvError_t
    cuMemPrefetchAsync(mcDrvDeviceptr_t devPtr, size_t count,
                       MCdevice dstDevice, MCstream hStream)
    {
        return wcuMemPrefetchAsync(devPtr, count, dstDevice, hStream);
    }

    mcDrvError_t
    cuMemAdvise(mcDrvDeviceptr_t devPtr, size_t count,
                mcDrvMemoryAdvise advice, MCdevice device)
    {
        return wcuMemAdvise(devPtr, count, advice, device);
    }

    mcDrvError_t
    cuMemRangeGetAttribute(void *data, size_t dataSize,
                           mcDrvMemoryAttributeType_t attribute,
                           mcDrvDeviceptr_t devPtr, size_t count)
    {
        return wcuMemRangeGetAttribute(data, dataSize, attribute, devPtr, count);
    }

    mcDrvError_t
    cuMemRangeGetAttributes(void **data, size_t *dataSizes,
                            mcDrvMemoryAttributeType_t *attributes,
                            size_t numAttributes,
                            mcDrvDeviceptr_t devPtr, size_t count)
    {
        return wcuMemRangeGetAttributes(data, dataSizes, attributes, numAttributes, devPtr, count);
    }

    mcDrvError_t
    cuPointerSetAttribute(const void *value,
                          mcDrvPointerAttributeType attribute,
                          mcDrvDeviceptr_t ptr)
    {
        return wcuPointerSetAttribute(value, attribute, ptr);
    }

    mcDrvError_t
    cuPointerGetAttributes(unsigned int numAttributes,
                           mcDrvPointerAttributeType *attributes,
                           void **data, mcDrvDeviceptr_t ptr)
    {
        return wcuPointerGetAttributes(numAttributes, attributes, data, ptr);
    }

    mcDrvError_t
    cuStreamCreate(MCstream *phStream, unsigned int Flags)
    {
        return wcuStreamCreate(phStream, Flags);
    }

    mcDrvError_t
    cuStreamCreateWithPriority(MCstream *phStream,
                               unsigned int flags, int priority)
    {
        return wcuStreamCreateWithPriority(phStream, flags, priority);
    }

    mcDrvError_t
    cuStreamGetPriority(MCstream hStream, int *priority)
    {
        return wcuStreamGetPriority(hStream, priority);
    }

    mcDrvError_t
    cuStreamGetFlags(MCstream hStream, unsigned int *flags)
    {
        return wcuStreamGetFlags(hStream, flags);
    }

    mcDrvError_t
    cuStreamGetId(MCstream hStream, unsigned long long *streamId)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuStreamGetCtx(MCstream hStream, MCcontext *pctx)
    {
        return wcuStreamGetCtx(hStream, pctx);
    }

    mcDrvError_t
    cuStreamWaitEvent(MCstream hStream, MCevent hEvent,
                      unsigned int Flags)
    {
        return wcuStreamWaitEvent(hStream, hEvent, Flags);
    }

    mcDrvError_t
    cuStreamAddCallback(MCstream hStream,
                        mcDrvStreamCallback_t callback, void *userData,
                        unsigned int flags)
    {
        return wcuStreamAddCallback(hStream, callback, userData, flags);
    }

    mcDrvError_t
    cuStreamBeginCapture(MCstream hStream,
                         mcStreamCaptureMode mode)
    {
        return wcuStreamBeginCapture(hStream, mode);
    }

    mcDrvError_t
    cuThreadExchangeStreamCaptureMode(mcStreamCaptureMode *mode)
    {
        return wcuThreadExchangeStreamCaptureMode(mode);
    }

    mcDrvError_t
    cuStreamEndCapture(MCstream hStream, mcDrvGraph *phGraph)
    {
        return wcuStreamEndCapture(hStream, phGraph);
    }

    mcDrvError_t
    cuStreamIsCapturing(MCstream hStream,
                        mcStreamCaptureStatus *captureStatus)
    {
        return wcuStreamIsCapturing(hStream, captureStatus);
    }

    mcDrvError_t
    cuStreamGetCaptureInfo(
        MCstream hStream, mcStreamCaptureStatus *captureStatus_out,
        mcuint64_t *id_out, mcDrvGraph *graph_out,
        const mcDrvGraphNode **dependencies_out, size_t *numDependencies_out)
    {
        return wcuStreamGetCaptureInfo_v2(hStream, captureStatus_out, id_out, graph_out, dependencies_out, numDependencies_out);
    }

    mcDrvError_t
    cuStreamUpdateCaptureDependencies(MCstream hStream,
                                      mcDrvGraphNode *dependencies,
                                      size_t numDependencies,
                                      unsigned int flags)
    {
        return wcuStreamUpdateCaptureDependencies(hStream, dependencies, numDependencies, flags);
    }

    mcDrvError_t
    cuStreamAttachMemAsync(MCstream hStream, mcDrvDeviceptr_t dptr,
                           size_t length, unsigned int flags)
    {
        return wcuStreamAttachMemAsync(hStream, dptr, length, flags);
    }

    mcDrvError_t
    cuStreamQuery(MCstream hStream)
    {
        return wcuStreamQuery(hStream);
    }

    mcDrvError_t
    cuStreamSynchronize(MCstream hStream)
    {
        return wcuStreamSynchronize(hStream);
    }

    mcDrvError_t
    cuStreamDestroy(MCstream hStream)
    {
        return wcuStreamDestroy(hStream);
    }

    mcDrvError_t
    cuStreamCopyAttributes(MCstream dst, MCstream src)
    {
        return wcuStreamCopyAttributes(dst, src);
    }

    mcDrvError_t
    cuStreamGetAttribute(MCstream hStream, mcStreamAttrID attr,
                         mcDrvStreamAttrValue *value_out)
    {
        return wcuStreamGetAttribute(hStream, attr, value_out);
    }

    mcDrvError_t
    cuStreamSetAttribute(MCstream hStream, mcStreamAttrID attr,
                         const mcDrvStreamAttrValue *value)
    {
        return wcuStreamSetAttribute(hStream, attr, value);
    }

    mcDrvError_t
    cuEventCreate(MCevent *phEvent, unsigned int Flags)
    {
        return wcuEventCreate(phEvent, Flags);
    }

    mcDrvError_t
    cuEventRecord(MCevent hEvent, MCstream hStream)
    {
        return wcuEventRecord(hEvent, hStream);
    }

    mcDrvError_t
    cuEventRecordWithFlags(MCevent hEvent, MCstream hStream,
                           unsigned int flags)
    {
        return wcuEventRecordWithFlags(hEvent, hStream, flags);
    }

    mcDrvError_t
    cuEventQuery(MCevent hEvent)
    {
        return wcuEventQuery(hEvent);
    }

    mcDrvError_t
    cuEventSynchronize(MCevent hEvent)
    {
        return wcuEventSynchronize(hEvent);
    }

    mcDrvError_t
    cuEventDestroy(MCevent hEvent)
    {
        return wcuEventDestroy(hEvent);
    }

    mcDrvError_t
    cuEventElapsedTime(float *pMilliseconds, MCevent hStart,
                       MCevent hEnd)
    {
        return wcuEventElapsedTime(pMilliseconds, hStart, hEnd);
    }

    mcDrvError_t
    cuImportExternalMemory(mcDrvExternalMemory *extMem_out,
                           const mcDrvExternalMemoryHandleDesc *memHandleDesc)
    {
        return wcuImportExternalMemory(extMem_out, memHandleDesc);
    }

    mcDrvError_t
    cuExternalMemoryGetMappedBuffer(
        mcDrvDeviceptr_t *devPtr, mcDrvExternalMemory extMem,
        const mcDrvExternalMemoryBufferDesc *bufferDesc)
    {
        return wcuExternalMemoryGetMappedBuffer(devPtr, extMem, bufferDesc);
    }

    mcDrvError_t
    cuExternalMemoryGetMappedMipmappedArray(
        mcDrvMipmappedArray *mipmap, mcDrvExternalMemory extMem,
        const mcDrvExternalMemoryMipmappedArrayDesc *mipmapDesc)
    {
        return wcuExternalMemoryGetMappedMipmappedArray(mipmap, extMem, mipmapDesc);
    }

    mcDrvError_t
    cuDestroyExternalMemory(mcDrvExternalMemory extMem)
    {
        return wcuDestroyExternalMemory(extMem);
    }

    mcDrvError_t
    cuImportExternalSemaphore(
        mcDrvExternalSemaphore *extSem_out,
        const mcDrvExternalSemaphorHandleDesc *semHandleDesc)
    {
        return wcuImportExternalSemaphore(extSem_out, semHandleDesc);
    }

    mcDrvError_t
    cuSignalExternalSemaphoresAsync(
        const mcDrvExternalSemaphore *extSemArray,
        const mcDrvExternalSemaphoreSignalParams *paramsArray,
        unsigned int numExtSems, MCstream stream)
    {
        return wcuSignalExternalSemaphoresAsync(extSemArray, paramsArray, numExtSems, stream);
    }

    mcDrvError_t
    cuWaitExternalSemaphoresAsync(
        const mcDrvExternalSemaphore *extSemArray,
        const mcDrvExternalSemaphoreWaitParams *paramsArray,
        unsigned int numExtSems, MCstream stream)
    {
        return wcuWaitExternalSemaphoresAsync(extSemArray, paramsArray, numExtSems, stream);
    }

    mcDrvError_t
    cuDestroyExternalSemaphore(mcDrvExternalSemaphore extSem)
    {
        return wcuDestroyExternalSemaphore(extSem);
    }

    mcDrvError_t
    cuStreamWaitValue32(MCstream stream, mcDrvDeviceptr_t addr,
                        mcuint32_t value, unsigned int flags)
    {
        return wcuStreamWaitValue32(stream, addr, value, flags);
    }

    mcDrvError_t
    cuStreamWaitValue64(MCstream stream, mcDrvDeviceptr_t addr,
                        mcuint64_t value, unsigned int flags)
    {
        return wcuStreamWaitValue64(stream, addr, value, flags);
    }

    mcDrvError_t
    cuStreamWriteValue32(MCstream stream, mcDrvDeviceptr_t addr,
                         mcuint32_t value, unsigned int flags)
    {
        return wcuStreamWriteValue32(stream, addr, value, flags);
    }

    mcDrvError_t
    cuStreamWriteValue64(MCstream stream, mcDrvDeviceptr_t addr,
                         mcuint64_t value, unsigned int flags)
    {
        return wcuStreamWriteValue64(stream, addr, value, flags);
    }

    mcDrvError_t
    cuStreamBatchMemOp(MCstream stream, unsigned int count,
                       mcDrvStreamBatchMemOpParams *paramArray,
                       unsigned int flags)
    {
        return wcuStreamBatchMemOp(stream, count, paramArray, flags);
    }

    mcDrvError_t
    cuFuncGetAttribute(int *pi, mcFunction_attribute attrib,
                       MCfunction hfunc)
    {
        return wcuFuncGetAttribute(pi, attrib, hfunc);
    }

    mcDrvError_t
    cuFuncSetAttribute(MCfunction hfunc,
                       mcFunction_attribute attrib, int value)
    {

        return wcuFuncSetAttribute(hfunc, attrib, value);
    }

    mcDrvError_t
    cuFuncSetCacheConfig(MCfunction hfunc, mcDrvfunc_cache_t config)
    {
        return wcuFuncSetCacheConfig(hfunc, config);
    }

    mcDrvError_t
    cuFuncSetSharedMemConfig(MCfunction hfunc,
                             mcDrvsharedconfig_t config)
    {
        return wcuFuncSetSharedMemConfig(hfunc, config);
    }

    mcDrvError_t
    cuFuncGetModule(MCmodule *hmod, MCfunction hfunc)
    {
        return wcuFuncGetModule(hmod, hfunc);
    }

    mcDrvError_t
    cuLaunchKernel(MCfunction f, unsigned int gridDimX,
                   unsigned int gridDimY, unsigned int gridDimZ,
                   unsigned int blockDimX, unsigned int blockDimY,
                   unsigned int blockDimZ,
                   unsigned int sharedMemBytes, MCstream hStream,
                   void **kernelParams, void **extra)
    {
        return wcuLaunchKernel(f, gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ, sharedMemBytes, hStream, kernelParams, extra);
    }

    mcDrvError_t
    cuLaunchKernelEx(const void *config, MCfunction f,
                     void **kernelParams, void **extra)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuLaunchCooperativeKernel(
        MCfunction f, unsigned int gridDimX, unsigned int gridDimY,
        unsigned int gridDimZ, unsigned int blockDimX, unsigned int blockDimY,
        unsigned int blockDimZ, unsigned int sharedMemBytes, MCstream hStream,
        void **kernelParams)
    {
        return wcuLaunchCooperativeKernel(f, gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ, sharedMemBytes, hStream, kernelParams);
    }

    mcDrvError_t
    cuLaunchCooperativeKernelMultiDevice(
        mcDrvLaunchParams *launchParamsList, unsigned int numDevices,
        unsigned int flags)
    {
        return wcuLaunchCooperativeKernelMultiDevice(launchParamsList, numDevices, flags);
    }

    mcDrvError_t
    cuLaunchHostFunc(MCstream hStream, mcHostFn_t fn,
                     void *userData)
    {
        return wcuLaunchHostFunc(hStream, fn, userData);
    }

    mcDrvError_t
    cuFuncSetBlockShape(MCfunction hfunc, int x,
                        int y, int z)
    {
        return wcuFuncSetBlockShape(hfunc, x, y, z);
    }

    mcDrvError_t
    cuFuncSetSharedSize(MCfunction hfunc,
                        unsigned int bytes)
    {
        return wcuFuncSetSharedSize(hfunc, bytes);
    }
    mcDrvError_t
    cuParamSetSize(MCfunction hfunc,
                   unsigned int numbytes)
    {
        return wcuParamSetSize(hfunc, numbytes);
    }

    mcDrvError_t
    cuParamSeti(MCfunction hfunc, int offset,
                unsigned int value)
    {
        wcuParamSeti(hfunc, offset, value);
    }

    mcDrvError_t
    cuParamSetf(MCfunction hfunc, int offset,
                float value)
    {
        return wcuParamSetf(hfunc, offset, value);
    }

    mcDrvError_t
    cuParamSetv(MCfunction hfunc, int offset,
                void *ptr,
                unsigned int numbytes)
    {
        return wcuParamSetv(hfunc, offset, ptr, numbytes);
    }

    mcDrvError_t
    cuLaunch(MCfunction f)
    {
        return wcuLaunch(f);
    }

    mcDrvError_t
    cuLaunchGrid(MCfunction f, int grid_width,
                 int grid_height)
    {
        return wcuLaunchGrid(f, grid_width, grid_height);
    }

    mcDrvError_t
    cuLaunchGridAsync(MCfunction f,
                      int grid_width,
                      int grid_height,
                      MCstream hStream)
    {
        return wcuLaunchGridAsync(f, grid_width, grid_height, hStream);
    }

    mcDrvError_t
    cuParamSetTexRef(MCfunction hfunc,
                     int texunit,
                     mcDrvTexref hTexRef)
    {
        return wcuParamSetTexRef(hfunc, texunit, hTexRef);
    }

    mcDrvError_t
    cuGraphCreate(mcDrvGraph *phGraph, unsigned int flags)
    {
        return wcuGraphCreate(phGraph, flags);
    }

    mcDrvError_t
    cuGraphAddKernelNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, const mcDrvKernelNodeParams *nodeParams)
    {
        return wcuGraphAddKernelNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphKernelNodeGetParams(
        mcDrvGraphNode hNode, mcDrvKernelNodeParams *nodeParams)
    {
        return wcuGraphKernelNodeGetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphKernelNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvKernelNodeParams *nodeParams)
    {
        return wcuGraphKernelNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddMemcpyNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                         const mcDrvGraphNode *dependencies,
                         size_t numDependencies,
                         const mcDrvMemcpy3D *copyParams,
                         MCcontext ctx)
    {
        return wcuGraphAddMemcpyNode(phGraphNode, hGraph, dependencies, numDependencies, copyParams, ctx);
    }

    mcDrvError_t
    cuGraphMemcpyNodeGetParams(mcDrvGraphNode hNode,
                               mcDrvMemcpy3D *nodeParams)
    {
        return wcuGraphMemcpyNodeGetParams(hNode, nodeParams);
    }
    mcDrvError_t
    cuGraphMemcpyNodeSetParams(mcDrvGraphNode hNode,
                               const mcDrvMemcpy3D *nodeParams)
    {
        return wcuGraphMemcpyNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddMemsetNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, const mcDrvMemsetNodeParams *memsetParams,
        MCcontext ctx)
    {
        return wcuGraphAddMemsetNode(phGraphNode, hGraph, dependencies, numDependencies, memsetParams, ctx);
    }

    mcDrvError_t
    cuGraphMemsetNodeGetParams(
        mcDrvGraphNode hNode, mcDrvMemsetNodeParams *nodeParams)
    {
        return wcuGraphMemsetNodeGetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphMemsetNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvMemsetNodeParams *nodeParams)
    {
        return wcuGraphMemsetNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddHostNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                       const mcDrvGraphNode *dependencies,
                       size_t numDependencies,
                       const mcDrvHostNodeParams *nodeParams)
    {
        return wcuGraphAddHostNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphHostNodeGetParams(mcDrvGraphNode hNode,
                             mcDrvHostNodeParams *nodeParams)
    {
        return wcuGraphHostNodeGetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphHostNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvHostNodeParams *nodeParams)
    {
        return wcuGraphHostNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddChildGraphNode(mcDrvGraphNode *phGraphNode,
                             mcDrvGraph hGraph,
                             const mcDrvGraphNode *dependencies,
                             size_t numDependencies,
                             mcDrvGraph childGraph)
    {
        return wcuGraphAddChildGraphNode(phGraphNode, hGraph, dependencies, numDependencies, childGraph);
    }

    mcDrvError_t
    cuGraphChildGraphNodeGetGraph(mcDrvGraphNode hNode,
                                  mcDrvGraph *phGraph)
    {
        return wcuGraphChildGraphNodeGetGraph(hNode, phGraph);
    }

    mcDrvError_t
    cuGraphAddEmptyNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                        const mcDrvGraphNode *dependencies,
                        size_t numDependencies)
    {
        return wcuGraphAddEmptyNode(phGraphNode, hGraph, dependencies, numDependencies);
    }

    mcDrvError_t
    cuGraphAddEventRecordNode(mcDrvGraphNode *phGraphNode,
                              mcDrvGraph hGraph,
                              const mcDrvGraphNode *dependencies,
                              size_t numDependencies,
                              MCevent event)
    {
        return wcuGraphAddEventRecordNode(phGraphNode, hGraph, dependencies, numDependencies, event);
    }

    mcDrvError_t
    cuGraphEventRecordNodeGetEvent(mcDrvGraphNode hNode,
                                   MCevent *event_out)
    {
        return wcuGraphEventRecordNodeGetEvent(hNode, event_out);
    }

    mcDrvError_t
    cuGraphEventRecordNodeSetEvent(mcDrvGraphNode hNode,
                                   MCevent event)
    {
        return wcuGraphEventRecordNodeSetEvent(hNode, event);
    }

    mcDrvError_t
    cuGraphAddEventWaitNode(mcDrvGraphNode *phGraphNode,
                            mcDrvGraph hGraph,
                            const mcDrvGraphNode *dependencies,
                            size_t numDependencies,
                            MCevent event)
    {
        return wcuGraphAddEventWaitNode(phGraphNode, hGraph, dependencies, numDependencies, event);
    }

    mcDrvError_t
    cuGraphEventWaitNodeGetEvent(mcDrvGraphNode hNode,
                                 MCevent *event_out)
    {
        return wcuGraphEventWaitNodeGetEvent(hNode, event_out);
    }

    mcDrvError_t
    cuGraphEventWaitNodeSetEvent(mcDrvGraphNode hNode,
                                 MCevent event)
    {
        return wcuGraphEventWaitNodeSetEvent(hNode, event);
    }

    mcDrvError_t
    cuGraphAddExternalSemaphoresSignalNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, const mcDrvExtSemSignalNodeParams *nodeParams)
    {
        return wcuGraphAddExternalSemaphoresSignalNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphExternalSemaphoresSignalNodeGetParams(
        mcDrvGraphNode hNode, mcDrvExtSemSignalNodeParams *params_out)
    {
        return wcuGraphExternalSemaphoresSignalNodeGetParams(hNode, params_out);
    }

    mcDrvError_t
    cuGraphExternalSemaphoresSignalNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvExtSemSignalNodeParams *nodeParams)
    {
        return wcuGraphExternalSemaphoresSignalNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddExternalSemaphoresWaitNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, const mcDrvExtSemWaitNodeParams *nodeParams)
    {
        return wcuGraphAddExternalSemaphoresWaitNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphExternalSemaphoresWaitNodeGetParams(
        mcDrvGraphNode hNode, mcDrvExtSemWaitNodeParams *params_out)
    {
        return wcuGraphExternalSemaphoresWaitNodeGetParams(hNode, params_out);
    }

    mcDrvError_t
    cuGraphExternalSemaphoresWaitNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvExtSemWaitNodeParams *nodeParams)
    {
        return wcuGraphExternalSemaphoresWaitNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddBatchMemOpNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, const mcDrvBatchMemOpNodeParams *nodeParams)
    {
        return wcuGraphAddBatchMemOpNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphBatchMemOpNodeGetParams(
        mcDrvGraphNode hNode, mcDrvBatchMemOpNodeParams *nodeParams_out)
    {
        return wcuGraphBatchMemOpNodeGetParams(hNode, nodeParams_out);
    }

    mcDrvError_t
    cuGraphBatchMemOpNodeSetParams(
        mcDrvGraphNode hNode, const mcDrvBatchMemOpNodeParams *nodeParams)
    {
        return wcuGraphBatchMemOpNodeSetParams(hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphExecBatchMemOpNodeSetParams(
        mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
        const mcDrvBatchMemOpNodeParams *nodeParams)
    {
        return wcuGraphExecBatchMemOpNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphAddMemAllocNode(
        mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph, const mcDrvGraphNode *dependencies,
        size_t numDependencies, mcDrvMemAllocNodeParams *nodeParams)
    {
        return wcuGraphAddMemAllocNode(phGraphNode, hGraph, dependencies, numDependencies, nodeParams);
    }

    mcDrvError_t
    cuGraphMemAllocNodeGetParams(
        mcDrvGraphNode hNode, mcDrvMemAllocNodeParams *params_out)
    {
        return wcuGraphMemAllocNodeGetParams(hNode, params_out);
    }

    mcDrvError_t
    cuGraphAddMemFreeNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                          const mcDrvGraphNode *dependencies,
                          size_t numDependencies,
                          mcDrvDeviceptr_t dptr)
    {
        return wcuGraphAddMemFreeNode(phGraphNode, hGraph, dependencies, numDependencies, dptr);
    }

    mcDrvError_t
    cuGraphMemFreeNodeGetParams(mcDrvGraphNode hNode,
                                mcDrvDeviceptr_t *dptr_out)
    {
        return wcuGraphMemFreeNodeGetParams(hNode, dptr_out);
    }

    mcDrvError_t
    cuDeviceGraphMemTrim(MCdevice device)
    {
        return wcuDeviceGraphMemTrim(device);
    }

    mcDrvError_t
    cuDeviceGetGraphMemAttribute(MCdevice device,
                                 mcDrvGraphMem_attribute attr,
                                 void *value)
    {
        return wcuDeviceGetGraphMemAttribute(device, attr, value);
    }
    mcDrvError_t
    cuDeviceSetGraphMemAttribute(MCdevice device,
                                 mcDrvGraphMem_attribute attr,
                                 void *value)
    {
        return wcuDeviceSetGraphMemAttribute(device, attr, value);
    }

    mcDrvError_t
    cuGraphClone(mcDrvGraph *phGraphClone, mcDrvGraph originalGraph)
    {
        return wcuGraphClone(phGraphClone, originalGraph);
    }

    mcDrvError_t
    cuGraphNodeFindInClone(mcDrvGraphNode *phNode,
                           mcDrvGraphNode hOriginalNode,
                           mcDrvGraph hClonedGraph)
    {
        return wcuGraphNodeFindInClone(phNode, hOriginalNode, hClonedGraph);
    }

    mcDrvError_t
    cuGraphNodeGetType(mcDrvGraphNode hNode, mcDrvGraphNodeType *type)
    {
        return wcuGraphNodeGetType(hNode, type);
    }

    mcDrvError_t
    cuGraphGetNodes(mcDrvGraph hGraph, mcDrvGraphNode *nodes,
                    size_t *numNodes)
    {
        return wcuGraphGetNodes(hGraph, nodes, numNodes);
    }

    mcDrvError_t
    cuGraphGetRootNodes(mcDrvGraph hGraph, mcDrvGraphNode *rootNodes,
                        size_t *numRootNodes)
    {
        return wcuGraphGetRootNodes(hGraph, rootNodes, numRootNodes);
    }

    mcDrvError_t
    cuGraphGetEdges(mcDrvGraph hGraph, mcDrvGraphNode *from,
                    mcDrvGraphNode *to, size_t *numEdges)
    {
        return wcuGraphGetEdges(hGraph, from, to, numEdges);
    }

    mcDrvError_t
    cuGraphNodeGetDependencies(mcDrvGraphNode hNode,
                               mcDrvGraphNode *dependencies,
                               size_t *numDependencies)
    {
        return wcuGraphNodeGetDependencies(hNode, dependencies, numDependencies);
    }

    mcDrvError_t
    cuGraphNodeGetDependentNodes(mcDrvGraphNode hNode,
                                 mcDrvGraphNode *dependentNodes,
                                 size_t *numDependentNodes)
    {
        return wcuGraphNodeGetDependentNodes(hNode, dependentNodes, numDependentNodes);
    }

    mcDrvError_t
    cuGraphAddDependencies(mcDrvGraph hGraph, const mcDrvGraphNode *from,
                           const mcDrvGraphNode *to,
                           size_t numDependencies)
    {
        return wcuGraphAddDependencies(hGraph, from, to, numDependencies);
    }

    mcDrvError_t
    cuGraphRemoveDependencies(mcDrvGraph hGraph,
                              const mcDrvGraphNode *from,
                              const mcDrvGraphNode *to,
                              size_t numDependencies)
    {
        return wcuGraphRemoveDependencies(hGraph, from, to, numDependencies);
    }

    mcDrvError_t
    cuGraphDestroyNode(mcDrvGraphNode hNode)
    {
        return wcuGraphDestroyNode(hNode);
    }

    mcDrvError_t
    cuGraphInstantiate(mcDrvGraphExec *phGraphExec, mcDrvGraph hGraph,
                       mcDrvGraphNode *phErrorNode, char *logBuffer, size_t bufferSize)
    {
        return wcuGraphInstantiate(phGraphExec, hGraph, phErrorNode, logBuffer, bufferSize);
    }

    mcDrvError_t
    cuGraphInstantiateWithParams(mcDrvGraphExec *phGraphExec, mcDrvGraph hGraph,
                                 mcDrvGraphInstantiateParams *instantiateParams)
    {
        return wcuGraphInstantiateWithParams(phGraphExec, hGraph, instantiateParams);
    }

    mcDrvError_t
    cuGraphExecGetFlags(mcDrvGraphExec hGraphExec,
                        mcuint64_t *flags)
    {
        return wcuGraphExecGetFlags(hGraphExec, flags);
    }

    mcDrvError_t
    cuGraphExecKernelNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                   const mcDrvKernelNodeParams *nodeParams)
    {
        return wcuGraphExecKernelNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphExecMemcpyNodeSetParams(mcDrvGraphExec hGraphExec,
                                   mcDrvGraphNode hNode,
                                   const mcDrvMemcpy3D *copyParams,
                                   MCcontext ctx)
    {
        return wcuGraphExecMemcpyNodeSetParams(hGraphExec, hNode, copyParams, ctx);
    }

    mcDrvError_t
    cuGraphExecMemsetNodeSetParams(
        mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
        const mcDrvMemsetNodeParams *memsetParams, MCcontext ctx)
    {
        return wcuGraphExecMemsetNodeSetParams(hGraphExec, hNode, memsetParams, ctx);
    }

    mcDrvError_t
    cuGraphExecHostNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                 const mcDrvHostNodeParams *nodeParams)
    {
        return wcuGraphExecHostNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphExecChildGraphNodeSetParams(mcDrvGraphExec hGraphExec,
                                       mcDrvGraphNode hNode,
                                       mcDrvGraph childGraph)
    {
        return wcuGraphExecChildGraphNodeSetParams(hGraphExec, hNode, childGraph);
    }

    mcDrvError_t
    cuGraphExecEventRecordNodeSetEvent(mcDrvGraphExec hGraphExec,
                                       mcDrvGraphNode hNode,
                                       MCevent event)
    {
        return wcuGraphExecEventRecordNodeSetEvent(hGraphExec, hNode, event);
    }

    mcDrvError_t
    cuGraphExecEventWaitNodeSetEvent(mcDrvGraphExec hGraphExec,
                                     mcDrvGraphNode hNode,
                                     MCevent event)
    {
        return wcuGraphExecEventWaitNodeSetEvent(hGraphExec, hNode, event);
    }

    mcDrvError_t
    cuGraphExecExternalSemaphoresSignalNodeSetParams(
        mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
        const mcDrvExtSemSignalNodeParams *nodeParams)
    {
        return wcuGraphExecExternalSemaphoresSignalNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphExecExternalSemaphoresWaitNodeSetParams(
        mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
        const mcDrvExtSemWaitNodeParams *nodeParams)
    {
        return wcuGraphExecExternalSemaphoresWaitNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcDrvError_t
    cuGraphNodeSetEnabled(mcDrvGraphExec hGraphExec,
                          mcDrvGraphNode hNode,
                          unsigned int isEnabled)
    {
        return wcuGraphNodeSetEnabled(hGraphExec, hNode, isEnabled);
    }

    mcDrvError_t
    cuGraphNodeGetEnabled(mcDrvGraphExec hGraphExec,
                          mcDrvGraphNode hNode,
                          unsigned int *isEnabled)
    {
        return wcuGraphNodeGetEnabled(hGraphExec, hNode, isEnabled);
    }

    mcDrvError_t
    cuGraphUpload(mcDrvGraphExec hGraphExec, MCstream hStream)
    {
        return wcuGraphUpload(hGraphExec, hStream);
    }

    mcDrvError_t
    cuGraphLaunch(mcDrvGraphExec hGraphExec, MCstream hStream)
    {
        return wcuGraphLaunch(hGraphExec, hStream);
    }

    mcDrvError_t
    cuGraphExecDestroy(mcDrvGraphExec hGraphExec)
    {
        return wcuGraphExecDestroy(hGraphExec);
    }
    mcDrvError_t
    cuGraphDestroy(mcDrvGraph hGraph)
    {
        return wcuGraphDestroy(hGraph);
    }

    mcDrvError_t
    cuGraphExecUpdate(mcDrvGraphExec hGraphExec, mcDrvGraph hGraph,
                      mcDrvGraphNode *hErrorNode_out,
                      mcGraphExecUpdateResult *updateResult_out)
    {
        return wcuGraphExecUpdate(hGraphExec, hGraph, hErrorNode_out, updateResult_out);
    }

    mcDrvError_t
    cuGraphKernelNodeCopyAttributes(mcDrvGraphNode dst,
                                    mcDrvGraphNode src)
    {
        return wcuGraphKernelNodeCopyAttributes(dst, src);
    }

    mcDrvError_t
    cuGraphKernelNodeGetAttribute(mcDrvGraphNode hNode, mcDrvkernelNodeAttrID attr,
                                  mcDrvKernelNodeAttrValue *value_out)
    {
        return wcuGraphKernelNodeGetAttribute(hNode, attr, value_out);
    }

    mcDrvError_t
    cuGraphKernelNodeSetAttribute(mcDrvGraphNode hNode, mcDrvkernelNodeAttrID attr,
                                  const mcDrvKernelNodeAttrValue *value)
    {
        return wcuGraphKernelNodeSetAttribute(hNode, attr, value);
    }

    mcDrvError_t
    cuGraphDebugDotPrint(mcDrvGraph hGraph, const char *path,
                         unsigned int flags)
    {
        return wcuGraphDebugDotPrint(hGraph, path, flags);
    }

    mcDrvError_t
    cuUserObjectCreate(mcDrvUserObject *object_out, void *ptr,
                       mcHostFn_t destroy,
                       unsigned int initialRefcount,
                       unsigned int flags)
    {
        return wcuUserObjectCreate(object_out, ptr, destroy, initialRefcount, flags);
    }

    mcDrvError_t
    cuUserObjectRetain(mcDrvUserObject object, unsigned int count)
    {
        return wcuUserObjectRetain(object, count);
    }

    mcDrvError_t
    cuUserObjectRelease(mcDrvUserObject object, unsigned int count)
    {
        return wcuUserObjectRelease(object, count);
    }

    mcDrvError_t
    cuGraphRetainUserObject(mcDrvGraph graph, mcDrvUserObject object,
                            unsigned int count,
                            unsigned int flags)
    {
        return wcuGraphRetainUserObject(graph, object, count, flags);
    }

    mcDrvError_t
    cuGraphReleaseUserObject(mcDrvGraph graph, mcDrvUserObject object,
                             unsigned int count)
    {
        return wcuGraphReleaseUserObject(graph, object, count);
    }

    mcDrvError_t
    cuOccupancyMaxActiveBlocksPerMultiprocessor(
        int *numBlocks, MCfunction func, int blockSize, size_t dynamicSMemSize)
    {
        return wcuOccupancyMaxActiveBlocksPerMultiprocessor(numBlocks, func, blockSize, dynamicSMemSize);
    }

    mcDrvError_t
    cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(
        int *numBlocks, MCfunction func, int blockSize, size_t dynamicSMemSize,
        unsigned int flags)
    {
        return wcuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(numBlocks, func, blockSize, dynamicSMemSize, flags);
    }

    mcDrvError_t
    cuOccupancyMaxPotentialBlockSize(
        int *minGridSize, int *blockSize, MCfunction func,
        mcOccupancyB2DSize blockSizeToDynamicSMemSize, size_t dynamicSMemSize,
        int blockSizeLimit)
    {
        return wcuOccupancyMaxPotentialBlockSize(minGridSize, blockSize, func, blockSizeToDynamicSMemSize, dynamicSMemSize, blockSizeLimit);
    }

    mcDrvError_t
    cuOccupancyMaxPotentialBlockSizeWithFlags(
        int *minGridSize, int *blockSize, MCfunction func,
        mcOccupancyB2DSize blockSizeToDynamicSMemSize, size_t dynamicSMemSize,
        int blockSizeLimit, unsigned int flags)
    {
        return wcuOccupancyMaxPotentialBlockSizeWithFlags(minGridSize, blockSize, func, blockSizeToDynamicSMemSize, dynamicSMemSize, blockSizeLimit, flags);
    }

    mcDrvError_t
    cuOccupancyAvailableDynamicSMemPerBlock(
        size_t *dynamicSmemSize, MCfunction func, int numBlocks, int blockSize)
    {
        return wcuOccupancyAvailableDynamicSMemPerBlock(dynamicSmemSize, func, numBlocks, blockSize);
    }

    mcDrvError_t
    cuOccupancyMaxPotentialClusterSize(
        int *clusterSize, MCfunction func, const void *config)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuOccupancyMaxActiveClusters(int *numClusters, MCfunction func,
                                 const void *config)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }
    
    mcDrvError_t
    cuTexRefSetArray(mcDrvTexref hTexRef,
                     MCarray hArray,
                     unsigned int Flags)
    {
        return wcuTexRefSetArray(hTexRef, hArray, Flags);
    }

    mcDrvError_t
    cuTexRefSetMipmappedArray(
        mcDrvTexref hTexRef, mcDrvMipmappedArray hMipmappedArray, unsigned int Flags)
    {
        return wcuTexRefSetMipmappedArray(hTexRef, hMipmappedArray, Flags);
    }

    mcDrvError_t
    cuTexRefSetAddress(size_t *ByteOffset,
                       mcDrvTexref hTexRef,
                       mcDrvDeviceptr_t dptr,
                       size_t bytes)
    {
        return wcuTexRefSetAddress(ByteOffset, hTexRef, dptr, bytes);
    }

    mcDrvError_t
    cuTexRefSetAddress2D(mcDrvTexref hTexRef, const mcDrvArrayDescriptor *desc,
                         mcDrvDeviceptr_t dptr, size_t Pitch)
    {
        return wcuTexRefSetAddress2D(hTexRef, desc, dptr, Pitch);
    }

    mcDrvError_t
    cuTexRefSetFormat(mcDrvTexref hTexRef,
                      mcArray_Format fmt,
                      int NumPackedComponents)
    {
        return wcuTexRefSetFormat(hTexRef, fmt, NumPackedComponents);
    }

    mcDrvError_t
    cuTexRefSetAddressMode(mcDrvTexref hTexRef,
                           int dim,
                           mcDrvAddress_mode am)
    {
        return wcuTexRefSetAddressMode(hTexRef, dim, am);
    }

    mcDrvError_t
    cuTexRefSetFilterMode(mcDrvTexref hTexRef,
                          mcDrvFilter_mode fm)
    {
        return wcuTexRefSetFilterMode(hTexRef, fm);
    }

    mcDrvError_t
    cuTexRefSetMipmapFilterMode(mcDrvTexref hTexRef, mcDrvFilter_mode fm)
    {
        return wcuTexRefSetMipmapFilterMode(hTexRef, fm);
    }

    mcDrvError_t
    cuTexRefSetMipmapLevelBias(mcDrvTexref hTexRef,
                               float bias)
    {
        return wcuTexRefSetMipmapLevelBias(hTexRef, bias);
    }
    mcDrvError_t
    cuTexRefSetMipmapLevelClamp(
        mcDrvTexref hTexRef, float minMipmapLevelClamp, float maxMipmapLevelClamp)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuTexRefSetMaxAnisotropy(mcDrvTexref hTexRef, unsigned int maxAniso)
    {
        return wcuTexRefSetMaxAnisotropy(hTexRef, maxAniso);
    }

    mcDrvError_t
    cuTexRefSetBorderColor(mcDrvTexref hTexRef,
                           float *pBorderColor)
    {
        return wcuTexRefSetBorderColor(hTexRef, pBorderColor);
    }

    mcDrvError_t
    cuTexRefSetFlags(mcDrvTexref hTexRef,
                     unsigned int Flags)
    {
        return wcuTexRefSetFlags(hTexRef, Flags);
    }

    mcDrvError_t
    cuTexRefGetAddress(mcDrvDeviceptr_t *pdptr,
                       mcDrvTexref hTexRef)
    {
        return wcuTexRefGetAddress(pdptr, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetArray(MCarray *phArray,
                     mcDrvTexref hTexRef)
    {
        return wcuTexRefGetArray(phArray, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetMipmappedArray(
        mcDrvMipmappedArray *phMipmappedArray, mcDrvTexref hTexRef)
    {
        return wcuTexRefGetMipmappedArray(phMipmappedArray, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetAddressMode(mcDrvAddress_mode *pam,
                           mcDrvTexref hTexRef,
                           int dim)
    {
        return wcuTexRefGetAddressMode(pam, hTexRef, dim);
    }

    mcDrvError_t
    cuTexRefGetFilterMode(mcDrvFilter_mode *pfm,
                          mcDrvTexref hTexRef)
    {
        return wcuTexRefGetFilterMode(pfm, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetFormat(mcArray_Format *pFormat,
                      int *pNumChannels,
                      mcDrvTexref hTexRef)
    {
        return wcuTexRefGetFormat(pFormat, pNumChannels, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetMipmapFilterMode(mcDrvFilter_mode *pfm, mcDrvTexref hTexRef)
    {
        return wcuTexRefGetMipmapFilterMode(pfm, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetMipmapLevelBias(float *pbias, mcDrvTexref hTexRef)
    {
        return wcuTexRefGetMipmapLevelBias(pbias, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetMipmapLevelClamp(float *pminMipmapLevelClamp,
                                float *pmaxMipmapLevelClamp, mcDrvTexref hTexRef)
    {
        return wcuTexRefGetMipmapLevelClamp(pminMipmapLevelClamp, pmaxMipmapLevelClamp, hTexRef);
    }
    mcDrvError_t
    cuTexRefGetMaxAnisotropy(int *pmaxAniso,
                             mcDrvTexref hTexRef)
    {
        return wcuTexRefGetMaxAnisotropy(pmaxAniso, hTexRef);
    }

    mcDrvError_t
    cuTexRefGetBorderColor(float *pBorderColor,
                           mcDrvTexref hTexRef)
    {
        return wcuTexRefGetBorderColor(pBorderColor, hTexRef);
    }
    mcDrvError_t
    cuTexRefGetFlags(unsigned int *pFlags,
                     mcDrvTexref hTexRef)
    {

        return wcuTexRefGetFlags(pFlags, hTexRef);
    }
    mcDrvError_t
    cuTexRefCreate(mcDrvTexref *pTexRef)
    {

        return wcuTexRefCreate(pTexRef);
    }

    mcDrvError_t
    cuTexRefDestroy(mcDrvTexref hTexRef)
    {

        return wcuTexRefDestroy(hTexRef);
    }

    mcDrvError_t
    cuSurfRefSetArray(mcDrvSurfref hSurfRef,
                      MCarray hArray,
                      unsigned int Flags)
    {
        return wcuSurfRefSetArray(hSurfRef, hArray, Flags);
    }

    mcDrvError_t
    cuSurfRefGetArray(MCarray *phArray,
                      mcDrvSurfref hSurfRef)
    {
        return wcuSurfRefGetArray(phArray, hSurfRef);
    }

    mcDrvError_t
    cuTexObjectCreate(mcDrvTexObject *pTexObject, const mcDrvResourceDesc *pResDesc,
                      const mcDrvTextureDesc *pTexDesc,
                      const mcDrvResourceViewDesc *pResViewDesc)
    {
        return wcuTexObjectCreate(pTexObject, pResDesc, pTexDesc, pResViewDesc);
    }

    mcDrvError_t
    cuTexObjectDestroy(mcDrvTexObject texObject)
    {
        return wcuTexObjectDestroy(texObject);
    }

    mcDrvError_t
    cuTexObjectGetResourceDesc(mcDrvResourceDesc *pResDesc,
                               mcDrvTexObject texObject)
    {
        return wcuTexObjectGetResourceDesc(pResDesc, texObject);
    }

    mcDrvError_t
    cuTexObjectGetTextureDesc(mcDrvTextureDesc *pTexDesc,
                              mcDrvTexObject texObject)
    {
        return wcuTexObjectGetTextureDesc(pTexDesc, texObject);
    }

    mcDrvError_t
    cuTexObjectGetResourceViewDesc(
        mcDrvResourceViewDesc *pResViewDesc, mcDrvTexObject texObject)
    {
        return wcuTexObjectGetResourceViewDesc(pResViewDesc, texObject);
    }

    mcDrvError_t
    cuSurfObjectCreate(mcDrvSurfObject *pSurfObject,
                       const mcDrvResourceDesc *pResDesc)
    {
        return wcuSurfObjectCreate(pSurfObject, pResDesc);
    }

    mcDrvError_t
    cuSurfObjectDestroy(mcDrvSurfObject surfObject)
    {
        return cuSurfObjectDestroy(surfObject);
    }

    mcDrvError_t
    cuSurfObjectGetResourceDesc(mcDrvResourceDesc *pResDesc,
                                mcDrvSurfObject surfObject)
    {
        return wcuSurfObjectGetResourceDesc(pResDesc, surfObject);
    }

    mcDrvError_t
    cuDeviceCanAccessPeer(int *canAccessPeer, MCdevice dev,
                          MCdevice peerDev)
    {
        return wcuDeviceCanAccessPeer(canAccessPeer, dev, peerDev);
    }

    mcDrvError_t
    cuCtxEnablePeerAccess(MCcontext peerContext,
                          unsigned int Flags)
    {
        return wcuCtxEnablePeerAccess(peerContext, Flags);
    }

    mcDrvError_t
    cuCtxDisablePeerAccess(MCcontext peerContext)
    {
        return wcuCtxDisablePeerAccess(peerContext);
    }

    mcDrvError_t
    cuDeviceGetP2PAttribute(int *value,
                            mcDrvDeviceP2PAttribute attrib,
                            MCdevice srcDevice,
                            MCdevice dstDevice)
    {
        return wcuDeviceGetP2PAttribute(value, attrib, srcDevice, dstDevice);
    }

    mcDrvError_t
    cuGraphicsUnregisterResource(mcDrvGraphicsResource resource)
    {
        return wcuGraphicsUnregisterResource(resource);
    }

    mcDrvError_t
    cuGraphicsSubResourceGetMappedArray(
        MCarray *pArray, mcDrvGraphicsResource resource, unsigned int arrayIndex,
        unsigned int mipLevel)
    {
        return wcuGraphicsSubResourceGetMappedArray(pArray, resource, arrayIndex, mipLevel);
    }

    mcDrvError_t
    cuGraphicsResourceGetMappedMipmappedArray(
        mcDrvMipmappedArray *pMipmappedArray, mcDrvGraphicsResource resource)
    {
        return wcuGraphicsResourceGetMappedMipmappedArray(pMipmappedArray, resource);
    }

    mcDrvError_t
    cuGraphicsResourceGetMappedPointer(
        mcDrvDeviceptr_t *pDevPtr, size_t *pSize, mcDrvGraphicsResource resource)
    {
        return wcuGraphicsResourceGetMappedPointer(pDevPtr, pSize, resource);
    }

    mcDrvError_t
    cuGraphicsResourceSetMapFlags(mcDrvGraphicsResource resource,
                                  unsigned int flags)
    {
        return wcuGraphicsResourceSetMapFlags(resource, flags);
    }

    mcDrvError_t
    cuGraphicsMapResources(unsigned int count,
                           mcDrvGraphicsResource *resources,
                           MCstream hStream)
    {
        return wcuGraphicsMapResources(count, resources, hStream);
    }

    mcDrvError_t
    cuGraphicsUnmapResources(unsigned int count,
                             mcDrvGraphicsResource *resources,
                             MCstream hStream)
    {
        return wcuGraphicsUnmapResources(count, resources, hStream);
    }

    mcDrvError_t
    cuGetProcAddress(const char *symbol, void **pfn, int mcVersion, uint64_t flags)
    {
        return wcuGetProcAddress(symbol, pfn, mcVersion, flags);
    }

    mcDrvError_t
    cuGetExportTable(const void **ppExportTable,
                     const MCuuid *pExportTableId)
    {
        return wcuGetExportTable(ppExportTable, pExportTableId);
    }

    mcDrvError_t
    cuCtxDestroy_v2(MCcontext ctx)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuCtxPopCurrent_v2(MCcontext *pctx)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuCtxPushCurrent_v2(MCcontext ctx)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuStreamDestroy_v2(MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuEventDestroy_v2(MCevent hEvent)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuDeviceTotalMem_v2(size_t *bytes, MCdevice dev)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuCtxCreate_v2(MCcontext *pctx, unsigned int flags, MCdevice dev)
    {
        return wcuCtxCreate(pctx, flags, dev);
    }

    mcDrvError_t
    cuModuleGetGlobal_v2(mcDrvDeviceptr_t *dptr, size_t *bytes,
                      MCmodule hmod, const char *name)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemGetInfo_v2(size_t *free, size_t *total)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemAlloc_v2(mcDrvDeviceptr_t *dptr, size_t bytesize)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemAllocPitch_v2(mcDrvDeviceptr_t *dptr, size_t *pPitch,
                    size_t WidthInBytes, size_t Height,
                    unsigned int ElementSizeBytes)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemFree_v2(mcDrvDeviceptr_t dptr)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemGetAddressRange_v2(mcDrvDeviceptr_t *pbase, size_t *psize,
                         mcDrvDeviceptr_t dptr)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemAllocHost_v2(void **pp, size_t bytesize)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemHostGetDevicePointer_v2(mcDrvDeviceptr_t *pdptr, void *p,
                              unsigned int Flags)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyHtoD_v2(mcDrvDeviceptr_t dstDevice, const void *srcHost,
                 size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyDtoH_v2(void *dstHost, mcDrvDeviceptr_t srcDevice,
                 size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyDtoD_v2(mcDrvDeviceptr_t dstDevice, mcDrvDeviceptr_t srcDevice,
                 size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyDtoA_v2(MCarray dstArray, size_t dstOffset,
                 mcDrvDeviceptr_t srcDevice, size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyAtoD_v2(mcDrvDeviceptr_t dstDevice, MCarray srcArray,
                 size_t srcOffset, size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyHtoA_v2(MCarray dstArray, size_t dstOffset,
                 const void *srcHost, size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyAtoH_v2(void *dstHost, MCarray srcArray, size_t srcOffset,
                 size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyAtoA_v2(MCarray dstArray, size_t dstOffset,
                 MCarray srcArray, size_t srcOffset,
                 size_t ByteCount)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpy2D_v2(const mcDrvMemcpy2D *pCopy)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpy2DUnaligned_v2(const mcDrvMemcpy2D *pCopy)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpy3D_v2(const mcDrvMemcpy3D *pCopy)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyHtoDAsync_v2(mcDrvDeviceptr_t dstDevice, const void *srcHost,
                      size_t ByteCount, MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyDtoHAsync_v2(void *dstHost, mcDrvDeviceptr_t srcDevice,
                      size_t ByteCount, MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyHtoAAsync_v2(MCarray dstArray, size_t dstOffset,
                      const void *srcHost, size_t ByteCount,
                      MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpyAtoHAsync_v2(void *dstHost, MCarray srcArray,
                      size_t srcOffset, size_t ByteCount,
                      MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpy2DAsync_v2(const mcDrvMemcpy2D *pCopy, MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemcpy3DAsync_v2(const mcDrvMemcpy3D *pCopy, MCstream hStream)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD8_v2(mcDrvDeviceptr_t dstDevice, unsigned char uc, size_t N)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD16_v2(mcDrvDeviceptr_t dstDevice, unsigned short us,
                size_t N)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD32_v2(mcDrvDeviceptr_t dstDevice, unsigned int ui, size_t N)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD2D8_v2(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                 unsigned char uc, size_t Width, size_t Height)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD2D16_v2(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                  unsigned short us, size_t Width, size_t Height)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuMemsetD2D32_v2(mcDrvDeviceptr_t dstDevice, size_t dstPitch,
                  unsigned int ui, size_t Width, size_t Height)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuArrayCreate_v2(MCarray *pHandle,
                  const mcDrvArrayDescriptor *pAllocateArray)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuArrayGetDescriptor_v2(mcDrvArrayDescriptor *pArrayDescriptor,
                         MCarray hArray)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuArray3DCreate_v2(
        MCarray *pHandle, const mcDrvArray3DDescriptor *pAllocateArray)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuArray3DGetDescriptor_v2(
        mcDrvArray3DDescriptor *pArrayDescriptor, MCarray hArray)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuTexRefSetAddress_v2(size_t *ByteOffset,
                       mcDrvTexref hTexRef,
                       mcDrvDeviceptr_t dptr,
                       size_t bytes)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuTexRefGetAddress_v2(mcDrvDeviceptr_t *pdptr,
                       mcDrvTexref hTexRef)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuTexRefSetAddress2D_v2(mcDrvTexref hTexRef, const mcDrvArrayDescriptor *desc,
                         mcDrvDeviceptr_t dptr, size_t Pitch)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuGraphicsResourceGetMappedPointer_v2(
        mcDrvDeviceptr_t *pDevPtr, size_t *pSize, mcDrvGraphicsResource resource)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t
    cuTexRefSetAddress2D_v3(mcDrvTexref hTexRef, const mcDrvArrayDescriptor *desc,
                         mcDrvDeviceptr_t dptr, size_t Pitch)
    {
        return MC_ERROR_NOT_SUPPORTED;
    }

    mcDrvError_t cuProfilerStop(void) { return wcuProfilerStop(); }

#ifdef __cplusplus
}
#endif
