#include "symbol_cu.h"

#ifdef __cplusplus
extern "C"
{
#endif
    mcError_t
    cudaDeviceReset(void)
    {
        return wcudaDeviceReset();
    }

    mcError_t
    cudaDeviceSynchronize(void)
    {
        return wcudaDeviceSynchronize();
    }

    mcError_t
    cudaDeviceSetLimit(enum mcLimit_t limit, size_t value)
    {
        return wcudaDeviceSetLimit(limit, value);
    }

    mcError_t
    cudaDeviceGetLimit(size_t *pValue, enum mcLimit_t limit)
    {
        return wcudaDeviceGetLimit(pValue, limit);
    }

    mcError_t
    cudaDeviceGetTexture1DLinearMaxWidth(size_t *maxWidthInElements, const struct mcChannelFormatDesc_enum *fmtDesc, int device)
    {
        return wcudaDeviceGetTexture1DLinearMaxWidth(maxWidthInElements, fmtDesc, device);
    }

    mcError_t
    cudaDeviceGetCacheConfig(enum mcFuncCache_t *pCacheConfig)
    {
        return wcudaDeviceGetCacheConfig(pCacheConfig);
    }

    mcError_t
    cudaDeviceGetStreamPriorityRange(int *leastPriority, int *greatestPriority)
    {
        return wcudaDeviceGetStreamPriorityRange(leastPriority, greatestPriority);
    }

    mcError_t
    cudaDeviceSetCacheConfig(enum mcFuncCache_t cacheConfig)
    {
        return wcudaDeviceSetCacheConfig(cacheConfig);
    }

    mcError_t
    cudaDeviceGetSharedMemConfig(enum mcSharedMemConfig *pConfig)
    {
        return wcudaDeviceGetSharedMemConfig(pConfig);
    }

    mcError_t
    cudaDeviceSetSharedMemConfig(enum mcSharedMemConfig config)
    {
        return wcudaDeviceSetSharedMemConfig(config);
    }

    mcError_t
    cudaDeviceGetByPCIBusId(int *device, const char *pciBusId)
    {
        return wcudaDeviceGetByPCIBusId(device, pciBusId);
    }

    mcError_t
    cudaDeviceGetPCIBusId(char *pciBusId, int len, int device)
    {
        return wcudaDeviceGetPCIBusId(pciBusId, len, device);
    }

    mcError_t
    cudaIpcGetEventHandle(mcIpcEventHandle_t *handle, mcEvent_t event)
    {
        return wcudaIpcGetEventHandle(handle, event);
    }

    mcError_t
    cudaIpcOpenEventHandle(mcEvent_t *event, mcIpcEventHandle_t handle)
    {
        return wcudaIpcOpenEventHandle(event, handle);
    }

    mcError_t
    cudaIpcGetMemHandle(mcIpcMemHandle_t *handle, void *devPtr)
    {
        return wcudaIpcGetMemHandle(handle, devPtr);
    }

    mcError_t
    cudaIpcOpenMemHandle(void **devPtr, mcIpcMemHandle_t handle, unsigned int flags)
    {
        return wcudaIpcOpenMemHandle(devPtr, handle, flags);
    }

    mcError_t
    cudaIpcCloseMemHandle(void *devPtr)
    {
        return cudaIpcCloseMemHandle(devPtr);
    }

    mcError_t
    cudaDeviceFlushGPUDirectRDMAWrites(
        enum mcFlushGPUDirectRDMAWritesTarget target,
        enum mcFlushGPUDirectRDMAWritesScope scope)
    {
        return wcudaDeviceFlushGPUDirectRDMAWrites(target, scope);
    }

    mcError_t
    cudaThreadExit(void)
    {
        return wcudaDeviceReset();
    }

    mcError_t
    cudaThreadSynchronize(void)
    {
        return wcudaDeviceSynchronize();
    }

    mcError_t
    cudaThreadSetLimit(enum mcLimit_t limit, size_t value)
    {
        return wcudaDeviceSetLimit(limit, value);
    }

    mcError_t
    cudaThreadGetLimit(size_t *pValue, enum mcLimit_t limit)
    {
        return wcudaDeviceGetLimit(pValue, limit);
    }

    mcError_t
    cudaThreadGetCacheConfig(enum mcFuncCache_t *pCacheConfig)
    {
        return wcudaDeviceGetCacheConfig(pCacheConfig);
    }

    mcError_t
    cudaThreadSetCacheConfig(enum mcFuncCache_t cacheConfig)
    {
        return wcudaDeviceSetCacheConfig(cacheConfig);
    }

    mcError_t
    cudaGetLastError(void)
    {
        return wcudaGetLastError();
    }

    mcError_t
    cudaPeekAtLastError(void)
    {
        return wcudaPeekAtLastError();
    }

    const char *
    cudaGetErrorName(mcError_t
                         error)
    {
        return wcudaGetErrorName(error);
    }

    const char *
    cudaGetErrorString(mcError_t
                           error)
    {
        return wcudaGetErrorString(error);
    }

    mcError_t
    cudaGetDeviceCount(int *count)
    {
        return wcudaGetDeviceCount(count);
    }

    mcError_t
    cudaGetDeviceProperties(struct _mcDeviceProp_t *prop, int device)
    {
        return wcudaGetDeviceProperties(prop, device);
    }

    mcError_t
    cudaDeviceGetAttribute(int *value, enum mcDeviceAttribute_t attr, int device)
    {
        return wcudaDeviceGetAttribute(value, attr, device);
    }

    mcError_t
    cudaDeviceGetDefaultMemPool(mcMemPool_t *memPool, int device)
    {
        return wcudaDeviceGetDefaultMemPool(memPool, device);
    }

    mcError_t
    cudaDeviceSetMemPool(int device, mcMemPool_t memPool)
    {
        return wcudaDeviceSetMemPool(device, memPool);
    }

    mcError_t
    cudaDeviceGetMemPool(mcMemPool_t *memPool, int device)
    {
        return wcudaDeviceGetMemPool(memPool, device);
    }

    mcError_t
    cudaDeviceGetNvSciSyncAttributes(
        void *nvSciSyncAttrList, int device, int flags)
    {
        return wcudaDeviceGetNvSciSyncAttributes(nvSciSyncAttrList, device, flags);
    }

    mcError_t
    cudaDeviceGetP2PAttribute(int *value, enum mcDeviceP2PAttr attr,
                              int srcDevice, int dstDevice)
    {
        return wcudaDeviceGetP2PAttribute(value, attr, srcDevice, dstDevice);
    }

    mcError_t
    cudaChooseDevice(int *device, const struct _mcDeviceProp_t *prop)
    {
        return wcudaChooseDevice(device, prop);
    }

    mcError_t
    cudaInitDevice(int device,
                   unsigned int deviceFlags,
                   unsigned int flags)
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaSetDevice(int device)
    {
        return wcudaSetDevice(device);
    }

    mcError_t

    cudaGetDevice(int *device)
    {
        return wcudaGetDevice(device);
    }

    mcError_t
    cudaSetValidDevices(int *device_arr,
                        int len)
    {
        return wcudaSetValidDevices(device_arr, len);
    }

    mcError_t
    cudaSetDeviceFlags(unsigned int flags)
    {
        return wcudaSetDeviceFlags(flags);
    }

    mcError_t
    cudaGetDeviceFlags(unsigned int *flags)
    {
        return wcudaGetDeviceFlags(flags);
    }

    mcError_t
    cudaStreamCreate(mcStream_t *pStream)
    {
        return wcudaStreamCreate(pStream);
    }

    mcError_t
    cudaStreamCreateWithFlags(mcStream_t *pStream, unsigned int flags)
    {
        return wcudaStreamCreateWithFlags(pStream, flags);
    }

    mcError_t
    cudaStreamCreateWithPriority(mcStream_t *pStream, unsigned int flags,
                                 int priority)
    {
        return wcudaStreamCreateWithPriority(pStream, flags, priority);
    }

    mcError_t
    cudaStreamGetPriority(mcStream_t hStream, int *priority)
    {
        return wcudaStreamGetPriority(hStream, priority);
    }

    mcError_t
    cudaStreamGetFlags(mcStream_t hStream, unsigned int *flags)
    {
        return wcudaStreamGetFlags(hStream, flags);
    }

    mcError_t
    cudaStreamGetId(mcStream_t hStream, unsigned long long *streamId)
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaCtxResetPersistingL2Cache(void)
    {
        return wcudaCtxResetPersistingL2Cache();
    }

    mcError_t
    cudaStreamCopyAttributes(mcStream_t dst, mcStream_t src)
    {
        return wcudaStreamCopyAttributes(dst, src);
    }

    mcError_t
    cudaStreamGetAttribute(mcStream_t hStream, mcStreamAttrID_enum attr,
                           mcStreamAttrValue *value_out)
    {
        return wcudaStreamGetAttribute(hStream, attr, value_out);
    }

    mcError_t
    cudaStreamSetAttribute(mcStream_t hStream, mcStreamAttrID_enum attr,
                           const mcStreamAttrValue *value)
    {
        return wcudaStreamSetAttribute(hStream, attr, value);
    }

    mcError_t
    cudaStreamDestroy(mcStream_t stream)
    {
        return wcudaStreamDestroy(stream);
    }

    mcError_t
    cudaStreamWaitEvent(
        mcStream_t stream, mcEvent_t event, unsigned int flags /*__dv(0)*/)
    {
        return wcudaStreamWaitEvent(stream, event, flags);
    }

    mcError_t
    cudaStreamAddCallback(mcStream_t stream, mcStreamCallback_t callback,
                          void *userData, unsigned int flags)
    {
        return wcudaStreamAddCallback(stream, callback, userData, flags);
    }

    mcError_t
    cudaStreamSynchronize(mcStream_t stream)
    {
        return wcudaStreamSynchronize(stream);
    }

    mcError_t
    cudaStreamQuery(mcStream_t stream)
    {
        return wcudaStreamQuery(stream);
    }

    mcError_t
    cudaStreamAttachMemAsync(mcStream_t stream, void *devPtr,
                             size_t length /*__dv(0)*/, unsigned int flags)
    {
        return wcudaStreamAttachMemAsync(stream, devPtr, length, flags);
    }

    mcError_t
    cudaStreamBeginCapture(mcStream_t stream, enum mcStreamCaptureMode_enum mode)
    {
        return wcudaStreamBeginCapture(stream, mode);
    }

    mcError_t
    cudaThreadExchangeStreamCaptureMode(enum mcStreamCaptureMode_enum *mode)
    {
        return wcudaThreadExchangeStreamCaptureMode(mode);
    }

    mcError_t
    cudaStreamEndCapture(mcStream_t stream, mcGraph_t *pGraph)
    {
        return wcudaStreamEndCapture(stream, pGraph);
    }

    mcError_t
    cudaStreamIsCapturing(
        mcStream_t stream, enum mcStreamCaptureStatus_enum *pCaptureStatus)
    {
        return wcudaStreamIsCapturing(stream, pCaptureStatus);
    }

    mcError_t
    cudaStreamGetCaptureInfo(
        mcStream_t stream, enum mcStreamCaptureStatus_enum *captureStatus_out,
        unsigned long long *id_out __dv(0), mcGraph_t *graph_out __dv(0),
        const mcGraphNode_t **dependencies_out __dv(0),
        size_t *numDependencies_out __dv(0))
    {
        return wcudaStreamGetCaptureInfo(stream, captureStatus_out, id_out);
    }

    mcError_t
    cudaStreamUpdateCaptureDependencies(
        mcStream_t stream, mcGraphNode_t *dependencies, size_t numDependencies,
        unsigned int flags __dv(0))
    {
        return wcudaStreamUpdateCaptureDependencies(stream, dependencies, numDependencies, flags);
    }

    mcError_t
    cudaEventCreate(mcEvent_t *event)
    {
        return wcudaEventCreate(event);
    }

    mcError_t
    cudaEventCreateWithFlags(mcEvent_t *event, unsigned int flags)
    {
        return wcudaEventCreateWithFlags(event, flags);
    }

    mcError_t
    cudaEventRecord(mcEvent_t event, mcStream_t stream __dv(0))
    {
        return wcudaEventRecord(event, stream);
    }

    mcError_t
    cudaEventRecordWithFlags(mcEvent_t event, mcStream_t stream __dv(0),
                             unsigned int flags __dv(0))
    {
        return wcudaEventRecordWithFlags(event, stream, flags);
    }

    mcError_t
    cudaEventQuery(mcEvent_t event)
    {
        return wcudaEventQuery(event);
    }

    mcError_t
    cudaEventSynchronize(mcEvent_t event)
    {
        return wcudaEventSynchronize(event);
    }

    mcError_t
    cudaEventDestroy(mcEvent_t event)
    {
        return wcudaEventDestroy(event);
    }

    mcError_t
    cudaEventElapsedTime(float *ms,
                         mcEvent_t start,
                         mcEvent_t end)
    {
        return wcudaEventElapsedTime(ms, start, end);
    }

    mcError_t
    cudaImportExternalMemory(
        mcExternalMemory_t *extMem_out,
        const struct mcExternalMemoryHandleDesc_st *memHandleDesc)
    {
        return wcudaImportExternalMemory(extMem_out, memHandleDesc);
    }

    mcError_t
    cudaExternalMemoryGetMappedBuffer(
        void **devPtr, mcExternalMemory_t extMem,
        const struct mcExternalMemoryBufferDesc_st *bufferDesc)
    {
        return wcudaExternalMemoryGetMappedBuffer(devPtr, extMem, bufferDesc);
    }
    mcError_t
    cudaExternalMemoryGetMappedMipmappedArray(
        mcMipmappedArray_t *mipmap, mcExternalMemory_t extMem,
        const struct mcExternalMemoryMipmappedArrayDesc_st *mipmapDesc)
    {
        return wcudaExternalMemoryGetMappedMipmappedArray(mipmap, extMem, mipmapDesc);
    }
    mcError_t
    cudaDestroyExternalMemory(mcExternalMemory_t extMem)
    {
        return wcudaDestroyExternalMemory(extMem);
    }

    mcError_t
    cudaImportExternalSemaphore(
        mcExternalSemaphore_t *extSem_out,
        const struct mcExternalSemaphoreHandleDesc_st *semHandleDesc)
    {
        return wcudaImportExternalSemaphore(extSem_out, semHandleDesc);
    }

    mcError_t
    cudaSignalExternalSemaphoresAsync(
        const mcExternalSemaphore_t *extSemArray,
        const struct mcExternalSemaphoreSignalParams_st *paramsArray,
        unsigned int numExtSems, mcStream_t stream __dv(0))
    {
        return wcudaSignalExternalSemaphoresAsync(extSemArray, paramsArray, numExtSems, stream);
    }

    mcError_t
    cudaWaitExternalSemaphoresAsync(
        const mcExternalSemaphore_t *extSemArray,
        const struct mcExternalSemaphoreWaitParams_st *paramsArray,
        unsigned int numExtSems, mcStream_t stream __dv(0))
    {
        return wcudaWaitExternalSemaphoresAsync(extSemArray, paramsArray, numExtSems, stream);
    }

    mcError_t
    cudaDestroyExternalSemaphore(mcExternalSemaphore_t extSem)
    {
        return wcudaDestroyExternalSemaphore(extSem);
    }

    mcError_t
    cudaLaunchKernel(const void *func, dim3 gridDim, dim3 blockDim, void **args,
                     size_t sharedMem, mcStream_t stream)
    {
        return wcudaLaunchKernel(func, gridDim, blockDim, args, sharedMem, stream);
    }

    mcError_t cudaLaunchKernelExC(const mcLaunchConfig_t *config, const void *func, void **args)
    {
        return wcudaLaunchKernelExC(config, func, args);
    }

    mcError_t
    cudaLaunchCooperativeKernel(
        const void *func, dim3 gridDim, dim3 blockDim, void **args,
        size_t sharedMem, mcStream_t stream)
    {
        return wcudaLaunchCooperativeKernel(func, gridDim, blockDim, args, sharedMem, stream);
    }

    mcError_t
    cudaLaunchCooperativeKernelMultiDevice(
        struct mcLaunchParams_t *launchParamsList, unsigned int numDevices,
        unsigned int flags __dv(0))
    {
        return wcudaLaunchCooperativeKernelMultiDevice(launchParamsList, numDevices, flags);
    }

    mcError_t
    cudaFuncSetCacheConfig(const void *func, enum mcFuncCache_t cacheConfig)
    {
        return wcudaFuncSetCacheConfig(func, cacheConfig);
    }

    mcError_t
    cudaFuncSetSharedMemConfig(const void *func, enum mcSharedMemConfig config)
    {
        return wcudaFuncSetSharedMemConfig(func, config);
    }

    mcError_t
    cudaFuncGetAttributes(struct mcFuncAttributes *attr, const void *func)
    {
        return wcudaFuncGetAttributes(attr, func);
    }

    mcError_t
    cudaFuncSetAttribute(const void *func, enum mcFuncAttribute_enum attr, int value)
    {
        return wcudaFuncSetAttribute(func, attr, value);
    }

    mcError_t
    cudaSetDoubleForDevice(double *d)
    {
        return wcudaSetDoubleForDevice(d);
    }

    mcError_t
    cudaSetDoubleForHost(double *d)
    {
        return wcudaSetDoubleForHost(d);
    }

    mcError_t
    cudaLaunchHostFunc(mcStream_t stream,
                       mcHostFn_t fn,
                       void *userData)
    {
        return wcudaLaunchHostFunc(stream, fn, userData);
    }

    mcError_t
    cudaOccupancyMaxActiveBlocksPerMultiprocessor(int *numBlocks, const void *func,
                                                  int blockSize,
                                                  size_t dynamicSMemSize)
    {
        return wcudaOccupancyMaxActiveBlocksPerMultiprocessor(numBlocks, func, blockSize, dynamicSMemSize);
    }

    mcError_t
    cudaOccupancyAvailableDynamicSMemPerBlock(size_t *dynamicSmemSize,
                                              const void *func, int numBlocks,
                                              int blockSize)
    {
        return wcudaOccupancyAvailableDynamicSMemPerBlock(dynamicSmemSize, func, numBlocks, blockSize);
    }

    mcError_t
    cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(int *numBlocks,
                                                           const void *func,
                                                           int blockSize,
                                                           size_t dynamicSMemSize,
                                                           unsigned int flags)
    {
        return wcudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(numBlocks, func, blockSize, dynamicSMemSize, flags);
    }

    mcError_t
    cudaOccupancyMaxPotentialClusterSize(int *clusterSize, const void *func,
                                         const void *launchConfig)
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaOccupancyMaxActiveClusters(int *numClusters, const void *func,
                                   const void *launchConfig)
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaMallocManaged(void **devPtr, size_t size, unsigned int flags)
    {
        return wcudaMallocManaged(devPtr, size, flags);
    }

    mcError_t
    cudaMalloc(void **devPtr, size_t size)
    {
        return wcudaMalloc(devPtr, size);
    }

    mcError_t
    cudaMallocHost(void **ptr, size_t size)
    {
        return wcudaMallocHost(ptr, size);
    }

    mcError_t
    cudaMallocPitch(void **devPtr,
                    size_t *pitch,
                    size_t width,
                    size_t height)
    {
        return wcudaMallocPitch(devPtr, pitch, width, height);
    }

    mcError_t
    cudaMallocArray(
        mcArray_t *array, const struct mcChannelFormatDesc_enum *desc, size_t width,
        size_t height __dv(0), unsigned int flags __dv(0))
    {
        return wcudaMallocArray(array, desc, width, height, flags);
    }

    mcError_t
    cudaFree(void *devPtr)
    {
        return wcudaFree(devPtr);
    }

    mcError_t
    cudaFreeHost(void *ptr)
    {
        return wcudaFreeHost(ptr);
    }

    mcError_t
    cudaFreeArray(mcArray_t array)
    {
        return wcudaFreeArray(array);
    }

    mcError_t
    cudaFreeMipmappedArray(mcMipmappedArray_t mipmappedArray)
    {
        return wcudaFreeMipmappedArray(mipmappedArray);
    }

    mcError_t
    cudaHostAlloc(void **pHost, size_t size,
                  unsigned int flags)
    {
        return wcudaHostAlloc(pHost, size, flags);
    }

    mcError_t
    cudaHostRegister(void *ptr, size_t size,
                     unsigned int flags)
    {
        return wcudaHostRegister(ptr, size, flags);
    }

    mcError_t
    cudaHostUnregister(void *ptr)
    {
        return wcudaHostUnregister(ptr);
    }

    mcError_t
    cudaHostGetDevicePointer(void **pDevice, void *pHost, unsigned int flags)
    {
        return wcudaHostGetDevicePointer(pDevice, pHost, flags);
    }

    mcError_t
    cudaHostGetFlags(unsigned int *pFlags,
                     void *pHost)
    {
        return wcudaHostGetFlags(pFlags, pHost);
    }

    mcError_t
    cudaMalloc3D(struct mcPitchedPtr *pitchedDevPtr, struct mcExtent extent)
    {
        return wcudaMalloc3D(pitchedDevPtr, extent);
    }

    mcError_t
    cudaMalloc3DArray(mcArray_t *array, const struct mcChannelFormatDesc_enum *desc,
                      struct mcExtent extent, unsigned int flags __dv(0))
    {
        return wcudaMalloc3DArray(array, desc, extent, flags);
    }

    mcError_t
    cudaMallocMipmappedArray(
        mcMipmappedArray_t *mipmappedArray,
        const struct mcChannelFormatDesc_enum *desc, struct mcExtent extent,
        unsigned int numLevels, unsigned int flags __dv(0))
    {
        return wcudaMallocMipmappedArray(mipmappedArray, desc, extent, numLevels, flags);
    }

    mcError_t
    cudaGetMipmappedArrayLevel(
        mcArray_t *levelArray, mcMipmappedArray_const_t mipmappedArray,
        unsigned int level)
    {
        return wcudaGetMipmappedArrayLevel(levelArray, mipmappedArray, level);
    }

    mcError_t
    cudaMemcpy3D(const struct mcMemcpy3DParms *p)
    {
        return wcudaMemcpy3D(p);
    }

    mcError_t
    cudaMemcpy3DPeer(const struct mcMemcpy3DPeerParms_enum *p)
    {
        return wcudaMemcpy3DPeer(p);
    }

    mcError_t
    cudaMemcpy3DAsync(
        const struct mcMemcpy3DParms *p, mcStream_t stream __dv(0))
    {
        return wcudaMemcpy3DAsync(p, stream);
    }

    mcError_t
    cudaMemcpy3DPeerAsync(
        const struct mcMemcpy3DPeerParms_enum *p, mcStream_t stream __dv(0))
    {
        return wcudaMemcpy3DPeerAsync(p, stream);
    }

    mcError_t
    cudaMemGetInfo(size_t *free,
                   size_t *total)
    {
        return wcudaMemGetInfo(free, total);
    }

    mcError_t
    cudaArrayGetInfo(struct mcChannelFormatDesc_enum *desc, struct mcExtent *extent,
                     unsigned int *flags, mcArray_t array)
    {
        return wcudaArrayGetInfo(desc, extent, flags, array);
    }

    mcError_t
    cudaArrayGetPlane(
        mcArray_t *pPlaneArray, mcArray_t hArray, unsigned int planeIdx)
    {
        return wcudaArrayGetPlane(pPlaneArray, hArray, planeIdx);
    }

    mcError_t
    cudaArrayGetMemoryRequirements(
        struct mcArrayMemoryRequirements_st *memoryRequirements, mcArray_t array,
        int device)
    {
        return wcudaArrayGetMemoryRequirements(memoryRequirements, array, device);
    }

    mcError_t
    cudaMipmappedArrayGetMemoryRequirements(
        struct mcArrayMemoryRequirements_st *memoryRequirements,
        mcMipmappedArray_t mipmap, int device)
    {
        return wcudaMipmappedArrayGetMemoryRequirements(memoryRequirements, mipmap, device);
    }

    mcError_t
    cudaArrayGetSparseProperties(
        struct mcArraySparseProperties_st *sparseProperties, mcArray_t array)
    {
        return wcudaArrayGetSparseProperties(sparseProperties, array);
    }

    mcError_t
    cudaMipmappedArrayGetSparseProperties(
        struct mcArraySparseProperties_st *sparseProperties,
        mcMipmappedArray_t mipmap)
    {
        return wcudaMipmappedArrayGetSparseProperties(sparseProperties, mipmap);
    }

    mcError_t
    cudaMemcpy(void *dst, const void *src,
               size_t count,
               enum _mcMemcpyKind kind)
    {
        return wcudaMemcpy(dst, src, count, kind);
    }

    mcError_t
    cudaMemcpyPeer(void *dst, int dstDevice,
                   const void *src,
                   int srcDevice,
                   size_t count)
    {
        return wcudaMemcpyPeer(dst, dstDevice, src, srcDevice, count);
    }

    mcError_t
    cudaMemcpy2D(void *dst, size_t dpitch,
                 const void *src,
                 size_t spitch, size_t width,
                 size_t height,
                 enum _mcMemcpyKind kind)
    {
        return wcudaMemcpy2D(dst, dpitch, src, spitch, width, height, kind);
    }

    mcError_t
    cudaMemcpy2DToArray(
        mcArray_t dst, size_t wOffset, size_t hOffset, const void *src,
        size_t spitch, size_t width, size_t height, enum _mcMemcpyKind kind)
    {
        return wcudaMemcpy2DToArray(dst, wOffset, hOffset, src, spitch, width, height, kind);
    }

    mcError_t
    cudaMemcpy2DFromArray(
        void *dst, size_t dpitch, mcArray_const_t src, size_t wOffset,
        size_t hOffset, size_t width, size_t height, enum _mcMemcpyKind kind)
    {
        return wcudaMemcpy2DFromArray(dst, dpitch, src, wOffset, hOffset, width, height, kind);
    }

    mcError_t
    cudaMemcpy2DArrayToArray(
        mcArray_t dst, size_t wOffsetDst, size_t hOffsetDst,
        mcArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t width,
        size_t height, enum _mcMemcpyKind kind __dv(mcMemcpyDeviceToDevice))
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaMemcpyToSymbol(
        const void *symbol, const void *src, size_t count, size_t offset __dv(0),
        enum _mcMemcpyKind kind __dv(mcMemcpyHostToDevice))
    {
        return wcudaMemcpyToSymbol(symbol, src, count, offset, kind);
    }

    mcError_t
    cudaMemcpyFromSymbol(
        void *dst, const void *symbol, size_t count, size_t offset __dv(0),
        enum _mcMemcpyKind kind __dv(mcMemcpyDeviceToHost))
    {
        return wcudaMemcpyFromSymbol(dst, symbol, count, offset);
    }

    mcError_t
    cudaMemcpyAsync(void *dst, const void *src, size_t count,
                    enum _mcMemcpyKind kind, mcStream_t stream __dv(0))
    {
        return wcudaMemcpyAsync(dst, src, count, kind, stream);
    }

    mcError_t
    cudaMemcpyPeerAsync(void *dst, int dstDevice, const void *src, int srcDevice,
                        size_t count, mcStream_t stream __dv(0))
    {
        return wcudaMemcpyPeerAsync(dst, dstDevice, src, srcDevice, count, stream);
    }

    mcError_t
    cudaMemcpy2DAsync(
        void *dst, size_t dpitch, const void *src, size_t spitch, size_t width,
        size_t height, enum _mcMemcpyKind kind, mcStream_t stream __dv(0))
    {
        return wcudaMemcpy2DAsync(dst, dpitch, src, spitch, width, height, kind);
    }

    mcError_t
    cudaMemcpy2DToArrayAsync(
        mcArray_t dst, size_t wOffset, size_t hOffset, const void *src,
        size_t spitch, size_t width, size_t height, enum _mcMemcpyKind kind,
        mcStream_t stream __dv(0))
    {
        return wcudaMemcpy2DToArrayAsync(dst, wOffset, hOffset, src, spitch, width, height, kind, stream);
    }

    mcError_t
    cudaMemcpy2DFromArrayAsync(
        void *dst, size_t dpitch, mcArray_const_t src, size_t wOffset,
        size_t hOffset, size_t width, size_t height, enum _mcMemcpyKind kind,
        mcStream_t stream __dv(0))
    {
        return wcudaMemcpy2DFromArrayAsync(dst, dpitch, src, wOffset, hOffset, width, height, kind, stream);
    }

    mcError_t
    cudaMemcpyToSymbolAsync(
        const void *symbol, const void *src, size_t count, size_t offset,
        enum _mcMemcpyKind kind, mcStream_t stream __dv(0))
    {
        return wcudaMemcpyToSymbolAsync(symbol, src, count, offset, kind, stream);
    }

    mcError_t
    cudaMemcpyFromSymbolAsync(
        void *dst, const void *symbol, size_t count, size_t offset,
        enum _mcMemcpyKind kind, mcStream_t stream __dv(0))
    {
        return wcudaMemcpyFromSymbolAsync(dst, symbol, count, offset, kind, stream);
    }

    mcError_t
    cudaMemset(void *devPtr, int value,
               size_t count)
    {
        return wcudaMemset(devPtr, value, count);
    }

    mcError_t
    cudaMemset2D(void *devPtr, size_t pitch,
                 int value, size_t width,
                 size_t height)
    {
        return wcudaMemset2D(devPtr, pitch, value, width, height);
    }

    mcError_t
    cudaMemset3D(
        struct mcPitchedPtr pitchedDevPtr, int value, struct mcExtent extent)
    {
        return wcudaMemset3D(pitchedDevPtr, value, extent);
    }

    mcError_t
    cudaMemsetAsync(
        void *devPtr, int value, size_t count, mcStream_t stream __dv(0))
    {
        return wcudaMemsetAsync(devPtr, value, count, stream);
    }

    mcError_t
    cudaMemset2DAsync(void *devPtr, size_t pitch, int value, size_t width,
                      size_t height, mcStream_t stream __dv(0))
    {
        return wcudaMemset2DAsync(devPtr, pitch, value, width, height, stream);
    }

    mcError_t
    cudaMemset3DAsync(struct mcPitchedPtr pitchedDevPtr, int value,
                      struct mcExtent extent, mcStream_t stream __dv(0))
    {
        return wcudaMemset3DAsync(pitchedDevPtr, value, extent, stream);
    }

    mcError_t
    cudaGetSymbolAddress(void **devPtr,
                         const void *symbol)
    {
        return wcudaGetSymbolAddress(devPtr, symbol);
    }

    mcError_t
    cudaGetSymbolSize(size_t *size,
                      const void *symbol)
    {
        return wcudaGetSymbolSize(size, symbol);
    }

    mcError_t
    cudaMemPrefetchAsync(const void *devPtr, size_t count, int dstDevice,
                         mcStream_t stream __dv(0))
    {
        return wcudaMemPrefetchAsync(devPtr, count, dstDevice, stream);
    }

    mcError_t
    cudaMemAdvise(const void *devPtr, size_t count, enum mcMemoryAdvise_enum advice,
                  int device)
    {
        return wcudaMemAdvise(devPtr, count, advice, device);
    }

    mcError_t
    cudaMemRangeGetAttribute(
        void *data, size_t dataSize, enum mcMemRangeAttribute_enum attribute,
        const void *devPtr, size_t count)
    {
        return wcudaMemRangeGetAttribute(data, dataSize, attribute, devPtr, count);
    }

    mcError_t
    cudaMemRangeGetAttributes(
        void **data, size_t *dataSizes, enum mcMemRangeAttribute_enum *attributes,
        size_t numAttributes, const void *devPtr, size_t count)
    {
        return wcudaMemRangeGetAttributes(data, dataSizes, attributes, numAttributes, devPtr, count);
    }

    mcError_t
    cudaMemcpyToArray(mcArray_t dst, size_t wOffset, size_t hOffset,
                      const void *src, size_t count, enum _mcMemcpyKind kind)
    {
        return wcudaMemcpyToArray(dst, wOffset, hOffset, src, count, kind);
    }

    mcError_t
    cudaMemcpyFromArray(void *dst, mcArray_const_t src, size_t wOffset,
                        size_t hOffset, size_t count, enum _mcMemcpyKind kind)
    {
        return wcudaMemcpyFromArray(dst, src, wOffset, hOffset, count, kind);
    }

    mcError_t
    cudaMemcpyArrayToArray(
        mcArray_t dst, size_t wOffsetDst, size_t hOffsetDst,
        mcArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t count,
        enum _mcMemcpyKind kind __dv(mcMemcpyDeviceToDevice))
    {
        return wcudaMemcpyArrayToArray(dst, wOffsetDst, hOffsetDst, src, wOffsetSrc, hOffsetSrc, count, kind);
    }

    mcError_t
    cudaMemcpyToArrayAsync(
        mcArray_t dst, size_t wOffset, size_t hOffset, const void *src,
        size_t count, enum _mcMemcpyKind kind, mcStream_t stream __dv(0))
    {
        return wcudaMemcpyToArrayAsync(dst, wOffset, hOffset, src, count, kind, stream);
    }

    mcError_t
    cudaMemcpyFromArrayAsync(void *dst, mcArray_const_t src, size_t wOffset,
                             size_t hOffset, size_t count, enum _mcMemcpyKind kind,
                             mcStream_t stream __dv(0))
    {
        return wcudaMemcpyFromArrayAsync(dst, src, wOffset, hOffset, count, kind, stream);
    }

    mcError_t
    cudaMallocAsync(void **devPtr,
                    size_t size,
                    mcStream_t hStream)
    {
        return wcudaMallocAsync(devPtr, size, hStream);
    }

    mcError_t
    cudaFreeAsync(void *devPtr,
                  mcStream_t hStream)
    {
        return wcudaFreeAsync(devPtr, hStream);
    }

    mcError_t
    cudaMemPoolTrimTo(mcMemPool_t memPool,
                      size_t minBytesToKeep)
    {
        return wcudaMemPoolTrimTo(memPool, minBytesToKeep);
    }

    mcError_t
    cudaMemPoolSetAttribute(
        mcMemPool_t memPool, enum mcMemPoolAttr attr, void *value)
    {
        return wcudaMemPoolSetAttribute(memPool, attr, value);
    }

    mcError_t
    cudaMemPoolGetAttribute(
        mcMemPool_t memPool, enum mcMemPoolAttr attr, void *value)
    {
        return wcudaMemPoolGetAttribute(memPool, attr, value);
    }
    mcError_t
    cudaMemPoolSetAccess(mcMemPool_t memPool,
                         const struct mcMemAccessDesc *descList, size_t count)
    {
        return wcudaMemPoolSetAccess(memPool, descList, count);
    }

    mcError_t
    cudaMemPoolGetAccess(enum mcMemAccessFlags *flags, mcMemPool_t memPool,
                         struct mcMemLocation *location)
    {
        return wcudaMemPoolGetAccess(flags, memPool, location);
    }
    mcError_t
    cudaMemPoolCreate(
        mcMemPool_t *memPool, const struct mcMemPoolProps *poolProps)
    {
        return wcudaMemPoolCreate(memPool, poolProps);
    }
    mcError_t
    cudaMemPoolDestroy(mcMemPool_t memPool)
    {
        return wcudaMemPoolDestroy(memPool);
    }

    mcError_t
    cudaMallocFromPoolAsync(
        void **ptr, size_t size, mcMemPool_t memPool, mcStream_t stream)
    {
        return wcudaMallocFromPoolAsync(ptr, size, memPool, stream);
    }

    mcError_t
    cudaMemPoolExportToShareableHandle(
        void *shareableHandle, mcMemPool_t memPool,
        enum mcMemAllocationHandleType handleType, unsigned int flags)
    {
        return wcudaMemPoolExportToShareableHandle(shareableHandle, memPool, handleType, flags);
    }

    mcError_t
    cudaMemPoolImportFromShareableHandle(
        mcMemPool_t *memPool, void *shareableHandle,
        enum mcMemAllocationHandleType handleType, unsigned int flags)
    {
        return wcudaMemPoolImportFromShareableHandle(memPool, shareableHandle, handleType, flags);
    }
    mcError_t
    cudaMemPoolExportPointer(
        struct mcMemPoolPtrExportData *exportData, void *ptr)
    {
        return wcudaMemPoolExportPointer(exportData, ptr);
    }

    mcError_t
    cudaMemPoolImportPointer(void **ptr, mcMemPool_t memPool,
                             struct mcMemPoolPtrExportData *exportData)
    {
        return wcudaMemPoolImportPointer(ptr, memPool, exportData);
    }

    mcError_t
    cudaPointerGetAttributes(
        struct _mcPointerAttribute_t *attributes, const void *ptr)
    {
        return wcudaPointerGetAttributes(attributes, ptr);
    }

    mcError_t
    cudaDeviceCanAccessPeer(int *canAccessPeer, int device, int peerDevice)
    {
        return wcudaDeviceCanAccessPeer(canAccessPeer, device, peerDevice);
    }
    mcError_t
    cudaDeviceEnablePeerAccess(int peerDevice, unsigned int flags)
    {
        return wcudaDeviceEnablePeerAccess(peerDevice, flags);
    }

    mcError_t
    cudaDeviceDisablePeerAccess(int peerDevice)
    {
        return wcudaDeviceDisablePeerAccess(peerDevice);
    }

    mcError_t
    cudaGraphicsUnregisterResource(mcGraphicsResource_t resource)
    {
        return wcudaGraphicsUnregisterResource(resource);
    }

    mcError_t
    cudaGraphicsResourceSetMapFlags(
        mcGraphicsResource_t resource, unsigned int flags)
    {
        return wcudaGraphicsResourceSetMapFlags(resource, flags);
    }

    mcError_t
    cudaGraphicsMapResources(
        int count, mcGraphicsResource_t *resources, mcStream_t stream __dv(0))
    {
        return wcudaGraphicsMapResources(count, resources, stream);
    }

    mcError_t
    cudaGraphicsUnmapResources(
        int count, mcGraphicsResource_t *resources, mcStream_t stream __dv(0))
    {
        return wcudaGraphicsUnmapResources(count, resources, stream);
    }

    mcError_t
    cudaGraphicsResourceGetMappedPointer(
        void **devPtr, size_t *size, mcGraphicsResource_t resource)
    {
        return wcudaGraphicsResourceGetMappedPointer(devPtr, size, resource);
    }

    mcError_t
    cudaGraphicsSubResourceGetMappedArray(
        mcArray_t *array, mcGraphicsResource_t resource,
        unsigned int arrayIndex, unsigned int mipLevel)
    {
        return wcudaGraphicsSubResourceGetMappedArray(array, resource, arrayIndex, mipLevel);
    }

    mcError_t
    cudaGraphicsResourceGetMappedMipmappedArray(
        mcMipmappedArray_t *mipmappedArray, mcGraphicsResource_t resource)
    {
        return wcudaGraphicsResourceGetMappedMipmappedArray(mipmappedArray, resource);
    }

    mcError_t
    cudaGetChannelDesc(
        struct mcChannelFormatDesc_enum *desc, mcArray_const_t array)
    {
        return wcudaGetChannelDesc(desc, array);
    }

    struct mcChannelFormatDesc_enum
    cudaCreateChannelDesc(
        int x, int y, int z, int w, enum mcChannelFormatKind_enum f)
    {
        return wcudaCreateChannelDesc(x, y, z, w, f);
    }

    mcError_t
    cudaCreateTextureObject(
        mcTextureObject_t *pTexObject, const struct mcResourceDesc *pResDesc,
        const struct mcTextureDesc *pTexDesc,
        const struct mcResourceViewDesc *pResViewDesc)
    {
        return wcudaCreateTextureObject(pTexObject, pResDesc, pTexDesc, pResViewDesc);
    }

    mcError_t
    cudaDestroyTextureObject(mcTextureObject_t texObject)
    {
        return wcudaDestroyTextureObject(texObject);
    }

    mcError_t
    cudaGetTextureObjectResourceDesc(
        struct mcResourceDesc *pResDesc, mcTextureObject_t texObject)
    {
        return wcudaGetTextureObjectResourceDesc(pResDesc, texObject);
    }

    mcError_t
    cudaGetTextureObjectTextureDesc(
        struct mcTextureDesc *pTexDesc, mcTextureObject_t texObject)
    {
        return wcudaGetTextureObjectTextureDesc(pTexDesc, texObject);
    }

    mcError_t
    cudaGetTextureObjectResourceViewDesc(
        struct mcResourceViewDesc *pResViewDesc, mcTextureObject_t texObject)
    {
        return wcudaGetTextureObjectResourceViewDesc(pResViewDesc, texObject);
    }

    mcError_t
    cudaCreateSurfaceObject(
        mcSurfaceObject_t *pSurfObject, const struct mcResourceDesc *pResDesc)
    {
        return wcudaCreateSurfaceObject(pSurfObject, pResDesc);
    }

    mcError_t
    cudaDestroySurfaceObject(mcSurfaceObject_t surfObject)
    {
        return wcudaDestroySurfaceObject(surfObject);
    }

    mcError_t
    cudaGetSurfaceObjectResourceDesc(
        struct mcResourceDesc *pResDesc, mcSurfaceObject_t surfObject)
    {
        return wcudaGetSurfaceObjectResourceDesc(pResDesc, surfObject);
    }

    mcError_t
    cudaDriverGetVersion(int *driverVersion)
    {
        return wcudaDriverGetVersion(driverVersion);
    }

    mcError_t
    cudaRuntimeGetVersion(int *runtimeVersion)
    {
        return wcudaRuntimeGetVersion(runtimeVersion);
    }

    mcError_t
    cudaGraphCreate(mcGraph_t *pGraph,
                    unsigned int flags)
    {
        return wcudaGraphCreate(pGraph, flags);
    }

    mcError_t
    cudaGraphAddKernelNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcKernelNodeParams_st *pNodeParams)
    {
        return wcudaGraphAddKernelNode(pGraphNode, graph, pDependencies, numDependencies, pNodeParams);
    }

    mcError_t
    cudaGraphKernelNodeGetParams(
        mcGraphNode_t node, struct mcKernelNodeParams_st *pNodeParams)
    {
        return wcudaGraphKernelNodeGetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphKernelNodeSetParams(
        mcGraphNode_t node, const struct mcKernelNodeParams_st *pNodeParams)
    {
        return wcudaGraphKernelNodeSetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphKernelNodeCopyAttributes(mcGraphNode_t hSrc, mcGraphNode_t hDst)
    {
        return wcudaGraphKernelNodeCopyAttributes(hSrc, hDst);
    }

    mcError_t
    cudaGraphKernelNodeGetAttribute(
        mcGraphNode_t hNode, mcKernelNodeAttrID attr,
        mcKernelNodeAttrValue *value_out)
    {
        return wcudaGraphKernelNodeGetAttribute(hNode, attr, value_out);
    }

    mcError_t
    cudaGraphKernelNodeSetAttribute(
        mcGraphNode_t hNode, mcKernelNodeAttrID attr,
        const mcKernelNodeAttrValue *value)
    {
        return wcudaGraphKernelNodeSetAttribute(hNode, attr, value);
    }

    mcError_t
    cudaGraphAddMemcpyNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcMemcpy3DParms *pCopyParams)
    {
        return wcudaGraphAddMemcpyNode(pGraphNode, graph, pDependencies, numDependencies, pCopyParams);
    }

    mcError_t
    cudaGraphAddMemcpyNodeToSymbol(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const void *symbol, const void *src, size_t count, size_t offset,
        enum _mcMemcpyKind kind)
    {
        return wcudaGraphAddMemcpyNodeToSymbol(pGraphNode, graph, pDependencies, numDependencies, symbol, src, count, offset, kind);
    }

    mcError_t
    cudaGraphAddMemcpyNodeFromSymbol(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies, void *dst,
        const void *symbol, size_t count, size_t offset, enum _mcMemcpyKind kind)
    {
        return wcudaGraphAddMemcpyNodeFromSymbol(pGraphNode, graph, pDependencies, numDependencies, dst, symbol, count, offset, kind);
    }

    mcError_t
    cudaGraphAddMemcpyNode1D(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies, void *dst,
        const void *src, size_t count, enum _mcMemcpyKind kind)
    {
        return wcudaGraphAddMemcpyNode1D(pGraphNode, graph, pDependencies, numDependencies, dst, src, count, kind);
    }

    mcError_t
    cudaGraphMemcpyNodeGetParams(
        mcGraphNode_t node, struct mcMemcpy3DParms *pNodeParams)
    {
        return wcudaGraphMemcpyNodeGetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphMemcpyNodeSetParams(
        mcGraphNode_t node, const struct mcMemcpy3DParms *pNodeParams)
    {
        return wcudaGraphMemcpyNodeSetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphMemcpyNodeSetParamsToSymbol(
        mcGraphNode_t node, const void *symbol, const void *src, size_t count,
        size_t offset, enum _mcMemcpyKind kind)
    {
        return wcudaGraphMemcpyNodeSetParamsToSymbol(node, symbol, src, count, offset, kind);
    }

    mcError_t
    cudaGraphMemcpyNodeSetParamsFromSymbol(
        mcGraphNode_t node, void *dst, const void *symbol, size_t count,
        size_t offset, enum _mcMemcpyKind kind)
    {
        return wcudaGraphMemcpyNodeSetParamsFromSymbol(node, dst, symbol, count, offset, kind);
    }

    mcError_t
    cudaGraphMemcpyNodeSetParams1D(mcGraphNode_t node, void *dst, const void *src,
                                   size_t count, enum _mcMemcpyKind kind)
    {
        return wcudaGraphMemcpyNodeSetParams1D(node, dst, src, count, kind);
    }

    mcError_t
    cudaGraphAddMemsetNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcMemsetParams_st *pMemsetParams)
    {
        return wcudaGraphAddMemsetNode(pGraphNode, graph, pDependencies, numDependencies, pMemsetParams);
    }

    mcError_t
    cudaGraphMemsetNodeGetParams(
        mcGraphNode_t node, struct mcMemsetParams_st *pNodeParams)
    {
        return wcudaGraphMemsetNodeGetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphMemsetNodeSetParams(
        mcGraphNode_t node, const struct mcMemsetParams_st *pNodeParams)
    {
        return wcudaGraphMemsetNodeSetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphAddHostNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcHostNodeParams_st *pNodeParams)
    {
        return wcudaGraphAddHostNode(pGraphNode, graph, pDependencies, numDependencies, pNodeParams);
    }

    mcError_t
    cudaGraphHostNodeGetParams(
        mcGraphNode_t node, struct mcHostNodeParams_st *pNodeParams)
    {
        return wcudaGraphHostNodeGetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphHostNodeSetParams(
        mcGraphNode_t node, const struct mcHostNodeParams_st *pNodeParams)
    {
        return wcudaGraphHostNodeSetParams(node, pNodeParams);
    }

    mcError_t
    cudaGraphAddChildGraphNode(mcGraphNode_t *pGraphNode, mcGraph_t graph,
                               const mcGraphNode_t *pDependencies,
                               size_t numDependencies, mcGraph_t childGraph)
    {
        return wcudaGraphAddChildGraphNode(pGraphNode, graph, pDependencies, numDependencies, childGraph);
    }

    mcError_t
    cudaGraphChildGraphNodeGetGraph(mcGraphNode_t node, mcGraph_t *pGraph)
    {
        return wcudaGraphChildGraphNodeGetGraph(node, pGraph);
    }

    mcError_t
    cudaGraphAddEmptyNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies)
    {
        return wcudaGraphAddEmptyNode(pGraphNode, graph, pDependencies, numDependencies);
    }

    mcError_t
    cudaGraphAddEventRecordNode(mcGraphNode_t *pGraphNode, mcGraph_t graph,
                                const mcGraphNode_t *pDependencies,
                                size_t numDependencies, mcEvent_t event)
    {
        return wcudaGraphAddEventRecordNode(pGraphNode, graph, pDependencies, numDependencies, event);
    }

    mcError_t
    cudaGraphEventRecordNodeGetEvent(mcGraphNode_t node, mcEvent_t *event_out)
    {
        return wcudaGraphEventRecordNodeGetEvent(node, event_out);
    }

    mcError_t
    cudaGraphEventRecordNodeSetEvent(mcGraphNode_t node, mcEvent_t event)
    {
        return wcudaGraphEventRecordNodeSetEvent(node, event);
    }

    mcError_t
    cudaGraphAddEventWaitNode(mcGraphNode_t *pGraphNode, mcGraph_t graph,
                              const mcGraphNode_t *pDependencies,
                              size_t numDependencies, mcEvent_t event)
    {
        return wcudaGraphAddEventWaitNode(pGraphNode, graph, pDependencies, numDependencies, event);
    }

    mcError_t
    cudaGraphEventWaitNodeGetEvent(mcGraphNode_t node, mcEvent_t *event_out)
    {
        return wcudaGraphEventWaitNodeGetEvent(node, event_out);
    }

    mcError_t
    cudaGraphEventWaitNodeSetEvent(mcGraphNode_t node, mcEvent_t event)
    {
        return wcudaGraphEventWaitNodeSetEvent(node, event);
    }

    mcError_t
    cudaGraphAddExternalSemaphoresSignalNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcExternalSemaphoreSignalNodeParams_st *nodeParams)
    {
        return wcudaGraphAddExternalSemaphoresSignalNode(pGraphNode, graph, pDependencies, numDependencies, nodeParams);
    }

    mcError_t
    cudaGraphExternalSemaphoresSignalNodeGetParams(
        mcGraphNode_t hNode,
        struct mcExternalSemaphoreSignalNodeParams_st *params_out)
    {
        return wcudaGraphExternalSemaphoresSignalNodeGetParams(hNode, params_out);
    }

    mcError_t
    cudaGraphExternalSemaphoresSignalNodeSetParams(
        mcGraphNode_t hNode,
        const struct mcExternalSemaphoreSignalNodeParams_st *nodeParams)
    {
        return wcudaGraphExternalSemaphoresSignalNodeSetParams(hNode, nodeParams);
    }

    mcError_t
    cudaGraphAddExternalSemaphoresWaitNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        const struct mcExternalSemaphoreWaitNodeParams_st *nodeParams)
    {
        return wcudaGraphAddExternalSemaphoresWaitNode(pGraphNode, graph, pDependencies, numDependencies, nodeParams);
    }

    mcError_t
    cudaGraphExternalSemaphoresWaitNodeGetParams(
        mcGraphNode_t hNode,
        struct mcExternalSemaphoreWaitNodeParams_st *params_out)
    {
        return wcudaGraphExternalSemaphoresWaitNodeGetParams(hNode, params_out);
    }

    mcError_t
    cudaGraphAddMemAllocNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies,
        struct mcMemAllocNodeParams *nodeParams)
    {
        return wcudaGraphAddMemAllocNode(pGraphNode, graph, pDependencies, numDependencies, nodeParams);
    }

    mcError_t
    cudaGraphMemAllocNodeGetParams(
        mcGraphNode_t node, struct mcMemAllocNodeParams *params_out)
    {
        return wcudaGraphMemAllocNodeGetParams(node, params_out);
    }

    mcError_t
    cudaGraphAddMemFreeNode(
        mcGraphNode_t *pGraphNode, mcGraph_t graph,
        const mcGraphNode_t *pDependencies, size_t numDependencies, void *dptr)
    {
        return wcudaGraphAddMemFreeNode(pGraphNode, graph, pDependencies, numDependencies, dptr);
    }

    mcError_t
    cudaGraphMemFreeNodeGetParams(mcGraphNode_t node, void *dptr_out)
    {
        return wcudaGraphMemFreeNodeGetParams(node, dptr_out);
    }

    mcError_t
    cudaDeviceGraphMemTrim(int device)
    {
        return wcudaDeviceGraphMemTrim(device);
    }

    mcError_t
    cudaDeviceGetGraphMemAttribute(
        int device, enum mcGraphMemAttributeType_enum attr, void *value)
    {
        return wcudaDeviceGetGraphMemAttribute(device, attr, value);
    }

    mcError_t
    cudaDeviceSetGraphMemAttribute(
        int device, enum mcGraphMemAttributeType_enum attr, void *value)
    {
        return wcudaDeviceSetGraphMemAttribute(device, attr, value);
    }

    mcError_t
    cudaGraphClone(mcGraph_t *pGraphClone, mcGraph_t originalGraph)
    {
        return wcudaGraphClone(pGraphClone, originalGraph);
    }

    mcError_t
    cudaGraphNodeFindInClone(mcGraphNode_t *pNode, mcGraphNode_t originalNode,
                             mcGraph_t clonedGraph)
    {
        return wcudaGraphNodeFindInClone(pNode, originalNode, clonedGraph);
    }

    mcError_t
    cudaGraphNodeGetType(mcGraphNode_t node, enum mcGraphNodeType_t *pType)
    {
        return wcudaGraphNodeGetType(node, pType);
    }

    mcError_t
    cudaGraphGetNodes(mcGraph_t graph,
                      mcGraphNode_t *nodes,
                      size_t *numNodes)
    {
        return wcudaGraphGetNodes(graph, nodes, numNodes);
    }

    mcError_t
    cudaGraphGetRootNodes(
        mcGraph_t graph, mcGraphNode_t *pRootNodes, size_t *pNumRootNodes)
    {
        return wcudaGraphGetRootNodes(graph, pRootNodes, pNumRootNodes);
    }

    mcError_t
    cudaGraphGetEdges(mcGraph_t graph,
                      mcGraphNode_t *from,
                      mcGraphNode_t *to,
                      size_t *numEdges)
    {
        return wcudaGraphGetEdges(graph, from, to, numEdges);
    }

    mcError_t
    cudaGraphNodeGetDependencies(
        mcGraphNode_t node, mcGraphNode_t *pDependencies,
        size_t *pNumDependencies)
    {
        return wcudaGraphNodeGetDependencies(node, pDependencies, pNumDependencies);
    }

    mcError_t
    cudaGraphNodeGetDependentNodes(
        mcGraphNode_t node, mcGraphNode_t *pDependentNodes,
        size_t *pNumDependentNodes)
    {

        return wcudaGraphNodeGetDependentNodes(node, pDependentNodes, pNumDependentNodes);
    }

    mcError_t
    cudaGraphAddDependencies(mcGraph_t graph, const mcGraphNode_t *from,
                             const mcGraphNode_t *to, size_t numDependencies)
    {
        return wcudaGraphAddDependencies(graph, from, to, numDependencies);
    }

    mcError_t
    cudaGraphRemoveDependencies(mcGraph_t graph, const mcGraphNode_t *from,
                                const mcGraphNode_t *to, size_t numDependencies)
    {
        return wcudaGraphRemoveDependencies(graph, from, to, numDependencies);
    }

    mcError_t
    cudaGraphDestroyNode(mcGraphNode_t node)
    {
        return wcudaGraphDestroyNode(node);
    }

    mcError_t
    cudaGraphInstantiate(mcGraphExec_t *pGraphExec, mcGraph_t graph,
                         unsigned long long flags __dv(0))
    {
        return wcudaGraphInstantiateWithFlags(pGraphExec, graph, flags);
    }

    mcError_t
    cudaGraphInstantiateWithFlags(mcGraphExec_t *pGraphExec, mcGraph_t graph,
                                  unsigned long long flags __dv(0))
    {
        return wcudaGraphInstantiateWithFlags(pGraphExec, graph, flags);
    }

    mcError_t
    cudaGraphInstantiateWithParams(mcGraphExec_t *pGraphExec, mcGraph_t graph,
                                   mcGraphInstantiateParams_st *instantiateParams)
    {
        return wcudaGraphInstantiateWithParams(pGraphExec, graph, instantiateParams);
    }

    mcError_t
    cudaGraphExecGetFlags(mcGraphExec_t graphExec, unsigned long long *flags)
    {
        return wcudaGraphExecGetFlags(graphExec, flags);
    }

    mcError_t
    cudaGraphExecKernelNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t node,
        const struct mcKernelNodeParams_st *pNodeParams)
    {
        return wcudaGraphExecKernelNodeSetParams(hGraphExec, node, pNodeParams);
    }

    mcError_t
    cudaGraphExecMemcpyNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t node,
        const struct mcMemcpy3DParms *pNodeParams)
    {
        return wcudaGraphExecMemcpyNodeSetParams(hGraphExec, node, pNodeParams);
    }

    mcError_t
    cudaGraphExecMemcpyNodeSetParamsToSymbol(
        mcGraphExec_t hGraphExec, mcGraphNode_t node, const void *symbol,
        const void *src, size_t count, size_t offset, enum _mcMemcpyKind kind)
    {
        return wcudaGraphExecMemcpyNodeSetParamsToSymbol(hGraphExec, node, symbol, src, count, offset, kind);
    }

    mcError_t
    cudaGraphExecMemcpyNodeSetParamsFromSymbol(mcGraphExec_t hGraphExec,
                                               mcGraphNode_t node, void *dst,
                                               const void *symbol, size_t count,
                                               size_t offset,
                                               enum _mcMemcpyKind kind)
    {
        return wcudaGraphExecMemcpyNodeSetParamsFromSymbol(hGraphExec, node, dst, symbol, count, offset, kind);
    }

    mcError_t
    cudaGraphExecMemcpyNodeSetParams1D(
        mcGraphExec_t hGraphExec, mcGraphNode_t node, void *dst,
        const void *src, size_t count, enum _mcMemcpyKind kind)
    {
        return wcudaGraphExecMemcpyNodeSetParams1D(hGraphExec, node, dst, src, count, kind);
    }

    mcError_t
    cudaGraphExecMemsetNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t node,
        const struct mcMemsetParams_st *pNodeParams)
    {
        return wcudaGraphExecMemsetNodeSetParams(hGraphExec, node, pNodeParams);
    }

    mcError_t
    cudaGraphExecHostNodeSetParams(mcGraphExec_t hGraphExec, mcGraphNode_t node,
                                   const struct mcHostNodeParams_st *pNodeParams)
    {
        return wcudaGraphExecHostNodeSetParams(hGraphExec, node, pNodeParams);
    }

    mcError_t
    cudaGraphExecChildGraphNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t node, mcGraph_t childGraph)
    {
        return wcudaGraphExecChildGraphNodeSetParams(hGraphExec, node, childGraph);
    }

    mcError_t
    cudaGraphExecEventRecordNodeSetEvent(
        mcGraphExec_t hGraphExec, mcGraphNode_t hNode, mcEvent_t event)
    {
        return wcudaGraphExecEventRecordNodeSetEvent(hGraphExec, hNode, event);
    }

    mcError_t
    cudaGraphExecEventWaitNodeSetEvent(
        mcGraphExec_t hGraphExec, mcGraphNode_t hNode, mcEvent_t event)
    {
        return wcudaGraphExecEventWaitNodeSetEvent(hGraphExec, hNode, event);
    }

    mcError_t
    cudaGraphExecExternalSemaphoresSignalNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t hNode,
        const struct mcExternalSemaphoreSignalNodeParams_st *nodeParams)
    {
        return wcudaGraphExecExternalSemaphoresSignalNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcError_t
    cudaGraphExecExternalSemaphoresWaitNodeSetParams(
        mcGraphExec_t hGraphExec, mcGraphNode_t hNode,
        const struct mcExternalSemaphoreWaitNodeParams_st *nodeParams)
    {
        return wcudaGraphExecExternalSemaphoresWaitNodeSetParams(hGraphExec, hNode, nodeParams);
    }

    mcError_t
    cudaGraphNodeSetEnabled(
        mcGraphExec_t hGraphExec, mcGraphNode_t hNode, unsigned int isEnabled)
    {
        return wcudaGraphNodeSetEnabled(hGraphExec, hNode, isEnabled);
    }

    mcError_t
    cudaGraphNodeGetEnabled(mcGraphExec_t hGraphExec, mcGraphNode_t hNode,
                            unsigned int *isEnabled)
    {
        return wcudaGraphNodeGetEnabled(hGraphExec, hNode, isEnabled);
    }

    mcError_t
    cudaGraphExecUpdate(mcGraphExec_t hGraphExec, mcGraph_t hGraph)
    {
        return mcErrorNotSupported;
    }

    mcError_t
    cudaGraphUpload(mcGraphExec_t graphExec,
                    mcStream_t stream)
    {
        return wcudaGraphUpload(graphExec, stream);
    }
    mcError_t
    cudaGraphLaunch(mcGraphExec_t graphExec,
                    mcStream_t stream)
    {
        return wcudaGraphLaunch(graphExec, stream);
    }

    mcError_t
    cudaGraphExecDestroy(mcGraphExec_t graphExec)
    {
        return wcudaGraphExecDestroy(graphExec);
    }

    mcError_t
    cudaGraphDestroy(mcGraph_t graph)
    {
        return wcudaGraphDestroy(graph);
    }

    mcError_t
    cudaGraphDebugDotPrint(
        mcGraph_t graph, const char *path, unsigned int flags)
    {
        return wcudaGraphDebugDotPrint(graph, path, flags);
    }

    mcError_t
    cudaUserObjectCreate(
        mcUserObject_t *object_out, void *ptr, mcHostFn_t destroy,
        unsigned int initialRefcount, unsigned int flags)
    {
        return wcudaUserObjectCreate(object_out, ptr, destroy, initialRefcount, flags);
    }

    mcError_t
    cudaUserObjectRetain(mcUserObject_t object, unsigned int count __dv(1))
    {
        return wcudaUserObjectRetain(object, count);
    }

    mcError_t
    cudaUserObjectRelease(mcUserObject_t object, unsigned int count __dv(1))
    {
        return wcudaUserObjectRelease(object, count);
    }

    mcError_t
    cudaGraphRetainUserObject(
        mcGraph_t graph, mcUserObject_t object, unsigned int count __dv(1),
        unsigned int flags __dv(0))
    {
        return wcudaGraphRetainUserObject(graph, object, count, flags);
    }

    mcError_t
    cudaGraphReleaseUserObject(
        mcGraph_t graph, mcUserObject_t object, unsigned int count __dv(1))
    {

        return wcudaGraphReleaseUserObject(graph, object, count);
    }

    mcError_t
    cudaGetDriverEntryPoint(
        const char *symbol, void **funcPtr, unsigned long long flagss)
    {
        return wcudaGetDriverEntryPoint(symbol, funcPtr, flagss);
    }

    mcError_t
    cudaGetExportTable(
        const void **ppExportTable, const mcUuid_t *pExportTableId)
    {
        return wcudaGetExportTable(ppExportTable, pExportTableId);
    }

#ifdef __cplusplus
}
#endif
