#ifndef __LIBSYMBOL_CU_H__
#define __LIBSYMBOL_CU_H__

#include <mc_runtime.h>
#include "../../../include/bridge/runtime/cuda_driver_wrapper.h"
#include "../../../include/bridge/runtime/cuda_runtime_wrapper.h"
//#include "../../../include/cuda_runtime.h"


#undef __dv
#define __dv(v)

typedef class MCkern_st * MCkernel;
typedef class MClib_st * MClibrary;

#endif //__LIBSYMBOL_CU_H__