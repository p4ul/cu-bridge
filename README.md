### 前言
**cu-bridge** 尝试成为GPU程序员使用一种编程语言进行开发、并且可运行在多种GPU硬件平台的解决方案。

**使GPU程序员沉浸在编程体验和算法创造的大千世界里**

*本系列项目将持续扩展和更新*

*欢迎大家Star关注，一起尝试更多的可能！*



### 背景

#### 行业现状

- 数字经济时代，算力成为新的生产力，CPU加GPU的异构计算系统是算力中心的主流配置，其中大部分算力由GPU提供。
- GPU作为算力的基石，CPU加GPU的异构计算也就成为了众多GPU厂商的角斗场，各个厂商陆续推出了自己的异构计算软件生态，从最初NVIDIA公司的一家独大，进入到目前的百花齐放。
- GPU主流编程语言和技术包括[CUDA](https://developer.nvidia.com/cuda-toolkit)、[OpenCL](https://www.khronos.org/opencl/)、[ROCm](https://www.amd.com/zh-hans/graphics/servers-solutions-rocm-ml)+[HIP](https://rocm.docs.amd.com/projects/HIP/en/latest/index.html)、[SYCL](https://www.khronos.org/sycl/)、[SPIR-V](https://www.khronos.org/spir/)、[Vulkan](https://www.vulkan.org/)和[MXMACA](https://www.metax-tech.com/platform.html?cid=4)等，分别应用于不同厂商和不同架构的GPU异构硬件平台。

#### 开发困境

- GPU程序员开发一个GPU应用程序，如果需要运行到不同厂商的GPU异构硬件平台，就需要重新编写GPU程序代码、并进行项目工程重新构建以及各种调试验证。
- 异构计算平台上的应用程序，其源代码项目即包含借助GPU加速异构计算代码，也包含CPU控制和计算部分代码；有些应用的项目工程构建由非常多的互相调用的子工程共同组成，一些用于生成库文件，一些用于实现逻辑功能，相互之间的调用关系复杂而严格。
- 不同异构计算平台的硬件有很多不同特性，软件接口也有很大的差别。GPU程序员需要把同一个应用程序的开发任务，分别适配到不同厂商的GPU异构平台，对大多数程序员来说都是一个非常痛苦和抓狂的过程。

### 目标
- **cu-bridge** 让GPU程序员只需要用CUDA编程语言开发一个应用程序，然后轻松地生成可以在不同厂商的GPU异构硬件平台上可以运行的目标文件。
- 首先使用CUDA编程语言开发应用程序，通过**cu-bridge**生成在MXMACA异构硬件平台上可以运行的目标文件。MXMACA是一个进入异构计算软件生态圈的一门新兴GPU编程语言，如果**cu-bridge**能够在MXMACA上实验成功，**cu-bridge**通往其它更加成熟GPU编程语言的桥梁也必然可以打造成功。



### 实现简介/原理
详见[实现原理](./docs/01_Design)



### 项目环境

cu-bridge项目代码开发环境是Ubuntu 18.04/20.04，cu-bridge项目部署在服务器相应的OS环境上可下载源码安装。

### 使用指南

详见：
- [使用指南](./docs/02_User_Manual)
- [CUDA应用迁移指南](./docs/02_User_Manual/CUDA_APP_PORTING_GUIDE.pdf)


### 参考示例

详见[参考示例](./docs/03_Examples)



### 版本说明

| TAG   | 说明                         |
| ----- | ---------------------------- |
| 0.0.1 | 想法和方案设计。             |
| 1.0.0 | 支持CUDA Runtime API         |
| 1.0.1 | 支持CUDA Driver API          |
| 1.0.2 | 支持CUDA 部分头文件（Part1） |
| 1.0.3 | 支持CUDA 部分头文件（Part2） |
| 1.0.4 | 支持CUDA 部分头文件（Part3） |
| 2.0.0 | 支持CUDA 部分加速库（Part1） |
| 2.0.1 | 支持CUDA 部分加速库（Part2） |
| 2.0.2 | 支持CUDA 部分加速库（Part3） |
| 2.0.3 | 一些功能加强和质量改进（bug-fix per feedbacks） |
| 2.0.4 | 一些功能加强和质量改进（bug-fix per feedbacks） |

### 开源协议

基于[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)开源协议

