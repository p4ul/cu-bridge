###### Environmental Dependence ######
1. wcuda tool has been installed, i.e. /path/to/where/installed/wcuda

###### Deployment and Usage ######
1. Install non-commercial enhance of this wcuda tool, i.e. wcuda-enhance.deb
	sudo dpkg -i wcuda-enhance.deb

2. Build your CMake project with wcuda enhance
    export CUDA_PATH = /path/to/where/installed/wcuda
	mkdir build
	cd build
	cmake_maca
	make_maca VERBOSE=1


###### Directory structure ######
|-- Readme.md                   // help
|-- cmake_maca                  // wcuda enhance file


###### Legal Notices ######
This enhance is only used for non-commercial purpose.

The software installed by the enhance may use the related third-party software or services,
which have been installed in the same server system already.

Please pay special attention that the intellectual property related to third-party software
belongs to the company that owns the copyright of the third-party software installed in the same server.

When installing and using third-party software, you should follow the relevant regulations of the company that owns the copyright.
This software is only permitted to be used in accordance with the relevant regulations of third-party software copyright companies.