set(MACA_FOUND   true)
set(MACA_INCLUDE_DIR   ./ ) 
set(MACA_LIBRARY ./ )


macro(MACA_ADD_EXECUTABLE maca_target  )
  message(STATUS "MACA_ADD_EXECUTABLE ${maca_target}  ARGN: ${ARGN} " )
  add_executable(${maca_target} ${ARGN})

  set_target_properties(${maca_target}
    PROPERTIES
    LINKER_LANGUAGE MACA
    )
endmacro()




macro(MACA_ADD_CUBLAS_TO_TARGET target  )
  target_link_libraries(${maca_target}  ${ARGN})
endmacro()


macro(MACA_ADD_CUBLAS_TO_TARGET target  )
  target_link_libraries(${maca_target}  ${ARGN})
endmacro()



macro(MACA_ADD_LIBRARY maca_target)
  # Add the library.
  add_library(${maca_target} ${ARGN})

  # Set the linker language.
  set_target_properties(${maca_target}
    PROPERTIES
    LINKER_LANGUAGE MACA
    )
endmacro()


macro(MACA_BUILD_CLEAN_TARGET)
endmacro()


macro(maca_compile_base maca_target format generated_files)
  # Create custom commands and targets for each file.
  MACA_WRAP_SRCS( ${_maca_target} ${format} _generated_files ${_sources}
                  ${_cmake_options} OPTIONS ${_options} PHONY)

  set( ${generated_files} ${_generated_files})

endmacro()


macro(MACA_COMPILE generated_files)
  maca_compile_base(maca_compile OBJ ${generated_files})
endmacro()

macro(MACA_COMPILE_PTX generated_files)
  maca_compile_base(maca_compile_ptx PTX ${generated_files})
endmacro()

macro(MACA_COMPILE_FATBIN generated_files)
  maca_compile_base(maca_compile_fatbin FATBIN ${generated_files})
endmacro()

macro(MACA_COMPILE_CUBIN generated_files)
  maca_compile_base(maca_compile_cubin CUBIN ${generated_files} )
endmacro()


function(MACA_COMPUTE_SEPARABLE_COMPILATION_OBJECT_FILE_NAME output_file_var maca_target object_files)
  if (object_files)
    set(generated_extension ${CMAKE_${MACA_C_OR_CXX}_OUTPUT_EXTENSION})
    set(output_file "${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/${maca_target}.dir/${CMAKE_CFG_INTDIR}/${maca_target}_intermediate_link${generated_extension}")
  else()
    set(output_file)
  endif()

  set(${output_file_var} "${output_file}" PARENT_SCOPE)
endfunction()


macro(MACA_INCLUDE_DIRECTORIES)
  foreach(dir ${ARGN})
    list(APPEND MACA_NVCC_INCLUDE_DIRS_USER ${dir})
  endforeach()
endmacro()


function(MACA_SELECT_NVCC_ARCH_FLAGS out_variable)
endfunction()