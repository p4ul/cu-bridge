# Find the compiler
set(CUCC_COMPILER nvcc)
if(DEFINED ENV{CUCC_PATH})
  set(CUCC_COMPILER cucc)
endif()
find_program(
    CMAKE_MACA_COMPILER 
        NAMES "$ENV{CUDA_PATH}/bin/${CUCC_COMPILER}"
        HINTS "${CMAKE_SOURCE_DIR}"
        DOC "maca compiler" 
)

mark_as_advanced( CMAKE_MACA_COMPILER )

set( CMAKE_MACA_SOURCE_FILE_EXTENSIONS cc;cu;cpp;c )
# Remember this as a potential error
set( CMAKE_MACA_OUTPUT_EXTENSION .o )
set( CMAKE_MACA_COMPILER_ENV_VAR "" )

# Configure variables set in this file for fast reload later on
configure_file( ${CMAKE_CURRENT_LIST_DIR}/CMakeMACACompiler.cmake.in
                ${CMAKE_PLATFORM_INFO_DIR}/CMakeMACACompiler.cmake )