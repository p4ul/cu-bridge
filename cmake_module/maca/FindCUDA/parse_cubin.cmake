
#######################################################################
# Parses a .cubin file produced by nvcc and reports statistics about the file.


file(READ ${input_file} file_text)

if (NOT "${file_text}" STREQUAL "")

  string(REPLACE ";" "\\;" file_text ${file_text})
  string(REPLACE "\ncode" ";code" file_text ${file_text})

  list(LENGTH file_text len)

  foreach(line ${file_text})

    # Only look at "code { }" blocks.
    if(line MATCHES "^code")

      # Break into individual lines.
      string(REGEX REPLACE "\n" ";" line ${line})

      foreach(entry ${line})

        # Extract kernel names.
        if (${entry} MATCHES "[^g]name = ([^ ]+)")
          set(entry "${CMAKE_MATCH_1}")

          # Check to see if the kernel name starts with "_"
          set(skip FALSE)
          # if (${entry} MATCHES "^_")
            # Skip the rest of this block.
            # message("Skipping ${entry}")
            # set(skip TRUE)
          # else ()
            message("Kernel:    ${entry}")
          # endif ()

        endif()

        # Skip the rest of the block if necessary
        if(NOT skip)

          # Registers
          if (${entry} MATCHES "reg([ ]+)=([ ]+)([^ ]+)")
            set(entry "${CMAKE_MATCH_3}")
            message("Registers: ${entry}")
          endif()

          # Local memory
          if (${entry} MATCHES "lmem([ ]+)=([ ]+)([^ ]+)")
            set(entry "${CMAKE_MATCH_3}")
            message("Local:     ${entry}")
          endif()

          # Shared memory
          if (${entry} MATCHES "smem([ ]+)=([ ]+)([^ ]+)")
            set(entry "${CMAKE_MATCH_3}")
            message("Shared:    ${entry}")
          endif()

          if (${entry} MATCHES "^}")
            message("")
          endif()

        endif()


      endforeach()

    endif()

  endforeach()

else()
  # message("FOUND NO DEPENDS")
endif()


