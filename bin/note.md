# notes when you deal conf*.json
conf*_bazel.json is used for compile tensorflow which use bazel.
if you need change conf*.json, modify the corresponding file which has the name conf*.json and conf*_bazel.json.
Please do not add processing options using absolute path for head file deal in conf*_bazel.json
For example: must not add "-I ${MACA_PATH}/include/mcpti" in section `[all][adder]`,
but you can add "-imacros __macro_mxcc.h" and "-forward-unknown-to-compiler" in section `[all][adder]`,
also you can add `"/usr/local/cuda/targets/x86_64-linux/lib/stubs/libcusolver.so","-lmcsolver"` which use absolute path
in section `[link][replacer]`.
