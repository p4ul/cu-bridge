
### 使用指南

#### 系统需求
建议在安装了以下软件的系统环境中安装**cu-bridge**，然后再开始实验和使用 ：
| 软件安装   | 安装指南                                                         |
| ----- | ------------------------------------------------------------ |
| CUDA工具包| 详见[NVIDIA CUDA Installation Guide for Linux](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html) |
| MXMACA工具包| 详见[MXMACA快速上手指南](https://www.metax-tech.com/platform.html?cid=4) |

#### 安装**cu-bridge**
以下是**cu-bridge**安装指南，有问题或建议请私信[p4ul](https://gitee.com/p4ul)。
##### Step 1：确定**cu-bridge**安装位置
**cu-bridge**对安装位置没有限制，可以是编译机器的任意目录。

在step2中，将`CMAKE_INSTALL_PREFIX`的路径设置为**cu-bridge**的安装位置。

推荐安装到MACA_PATH环境变量所在位置的tools子目录，例如cu-bridge解压到/opt/maca/tools/目录下。

##### Step 2：安装**cu-bridge**
安装前，根据[MXMACA快速上手指南](https://www.metax-tech.com/platform.html?cid=4) 先设置MACA_PATH环境变量

###### 方法1：直接使用git clone安装
```shell
git clone https://gitee.com/p4ul/cu-bridge.git
sudo chmod 755 cu-bridge -Rf
#set env MACA_PATH
cd cu-bridge
mkdir build && cd ./build
cmake -DCMAKE_INSTALL_PREFIX=/opt/maca/tools/cu-bridge ../
make && make install
```
###### 方法2：下载后解压安装
```shell
wget https://gitee.com/p4ul/cu-bridge/repository/archive/2.0.4.zip
unzip cu-bridge-2.0.4.zip
mv cu-bridge-2.0.4 cu-bridge
sudo chmod 755 cu-bridge -Rf
cd cu-bridge
mkdir build && cd ./build
cmake -DCMAKE_INSTALL_PREFIX=/opt/maca/tools/cu-bridge ../
make && make install
```
##### Step 3：配置**cu-bridge**编译环境
```shell
export CUCC_PATH=/path/to/where/installed/cu-bridge
export PATH=$PATH:${CUCC_PATH}/tools:${CUCC_PATH}/bin
```
#### 使用****cu-bridge****
以下是**cu-bridge**使用指南，有问题或建议请私信[p4ul](https://gitee.com/p4ul)。

##### Step 1：用CUDA语言编译一个GPU程序
用户可以自己用CUDA语言编写一个GPU程序。
为方便起见，**cu-bridge**使用指南以[cuda-samples](https://github.com/NVIDIA/cuda-samples)里的一个[vectorAdd](https://github.com/NVIDIA/cuda-samples/tree/master/Samples/0_Introduction/vectorAdd)程序为例。

1. 将CUDA_PATH环境变量设置为CUDA工具包安装位置
```shell
export CUDA_PATH=/path/to/where/installed/cuda
```
2. 将当前目录更改为vectorAdd源代码项目目录，ls查看该目录下文件，然后运行make：
```shell
cd vectorAdd
ls
make
```
3. 再次ls查看该目录下是否新增了vectorAdd可执行程序文件，如果make成功，可以在安装了英伟达GPU的机器上启动该vectorAdd程序：
```shell
ls
./vectorAdd
```
##### Step 2: 编译和生成MXMACA程序
###### Step 2.1: 确认MXMACA编程环境工作正常
参考[MXMACA快速上手指南](https://www.metax-tech.com/platform.html?cid=4)安装MXMACA编程环境后，执行以下步骤检查结果。
1. 检查MXMACA编译器是否正确安装：
```shell
which mxcc
```
正确安装的结果可能是：
```shell
/opt/maca/bin/mxcc
```
2. 检查MXMACA运行环境是否正常：
```shell
macainfo
```
如果macainfo不能正常执行，可能是MXMACA编程环境尚未正确配置，需要正确配置相关的环境变量，例如
```shell
export MACA_PATH=/opt/maca 
export PATH=$PATH:${MACA_PATH}/mxgpu_llvm/bin: ${MACA_PATH}/bin
export LD_LIBRARY_PATH=${MACA_PATH}/lib:${LD_LIBRARY_PATH}
```
然后再重新启动macainfo，看看是否能正常执行。
###### Step 2.2: 将CUDA_PATH环境变量设置为**CUDA Toolkit**安装位置
```shell
export CUDA_PATH=/path/to/where/installed/cuda
export PATH=$PATH:${CUDA_PATH}/bin
```
###### Step 2.3: 将CUCC_PATH环境变量设置为**cu-bridge**安装位置
```shell
export CUCC_PATH=/path/to/where/installed/cu-brdige
export PATH=$PATH:${CUCC_PATH}/tools:${CUCC_PATH}/bin
```
###### Step 2.4: 使用**cu-bridge**编译CUDA源代码项目
**方法1：CUDA源代码项目使用Make构建**
[cuda-samples](https://github.com/NVIDIA/cuda-samples)里的[vectorAdd](https://github.com/NVIDIA/cuda-samples/tree/master/Samples/0_Introduction/vectorAdd)源代码项目是使用Makefile构建的，下载后可以直接使用以下方式生成MXMACA程序。

1. 将当前目录更改为vectorAdd源代码项目目录，ls查看该目录下文件，然后运行 make_maca：
```shell
cd vectorAdd
ls
make_maca
```

2. 再次ls查看该目录下是否新增了vectorAdd可执行程序文件，如果make成功，可以在安装了沐曦GPU的机器上启动该vectorAdd程序：
```shell
ls
./vectorAdd
```
**方法2：CUDA源代码项目使用CMake构建**
[cuda-samples](https://github.com/NVIDIA/cuda-samples)里的[vectorAdd](https://github.com/NVIDIA/cuda-samples/tree/master/Samples/0_Introduction/vectorAdd)源代码项目是使用Makefile构建的。
下载后我们可以先改造成CMake构建，再进行实验和测试。

1. 创建CMakeLists.txt，用于构建和生成Makefile
```shell
cmake_minimum_required(VERSION 3.17)
set(CMAKE_VERBOSE_MAKEFILE on)
#set(CMAKE_CXX_SYSROOT_FLAG_CODE "list(APPEND CMAKE_CXX_SOURCE_FILE_EXTENSIONS cu)")
set_source_files_properties(vectorAdd.cu PROPERTIES LANGUAGE CXX) 
project(vectorAdd CXX)
set(CMAKE_CXX_COMPILER "$ENV{CUDA_PATH}/bin/nvcc")
set(CMAKE_C_COMPILER "$ENV{CUDA_PATH}/bin/nvcc")
#include_directories(../../common/inc)
add_executable(vectorAdd vectorAdd.cu)
```
2. 前面Step2.2已经将CUDA_PATH环境变量设置为**CUDA Toolkit**安装位置，将当前目录更改为vectorAdd源代码项目目录，通过cmake_maca生成Makefile
```shell
cd vectorAdd
mkdir build
cd build
cmake_maca ..
```
使用cmake_maca命令需要编译机器上正确**安装CUDA工具包**( 安装方法详见[NVIDIA CUDA Installation Guide for Linux](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html))
如果编译机器上没有安装CUDA工具包，也可以尝试用cmake命令替代上面的cmake_maca命令

3. ls查看vectorAdd目录下文件，然后运行make_maca：
```shell
make_maca
```
4. 再次ls查看该目录下是否新增了vectorAdd可执行程序文件，如果make成功，可以在安装了沐曦GPU的机器上启动该vectorAdd程序：
```shell
ls
./vectorAdd
```
**方法3：使用cucc直接编译CUDA源代码**
1. 将CUDA_PATH修改为**cu-bridge**的安装路径，
```shell
CUDA_PATH=/path/to/where/installed/cu-brdige
```
2. **cu-bridge**提供的cucc命令也可以用于直接编译CUDA源代码，例如：
```shell
cd vectorAdd
cucc vectorAdd.cu -o vectorAdd
```
3. ls查看该目录下是否新增了vectorAdd可执行程序文件，如果make成功，可以在安装了沐曦GPU的机器上启动该vectorAdd程序：
```shell
ls
./vectorAdd
```
#### FAQ
**Q1. 为什么使用cu-bridge时报错"FileNotFoundError:[Errno2] No such file or directory:/opt/maca/tools/cu-bridge/bin/nvcc"?**

**cu-bridge**提供的编译命令是cucc，不是nvcc。通常情况这是因为项目工程构建直接调用了nvcc进行编译，有两种变通方法可以尝试：
变通方法1: 修改项目构建把nvcc替换成cucc;
变通方法2: 或者在${CUCC_PATH}/bin目录位置创建一个nvcc的软连接，指向${CUCC_PATH}/bin/cucc。例如：

```shell
sudo ln -s ${CUCC_PATH}/bin/cucc ${CUCC_PATH}/bin/nvcc
```

**Q2.  kernel 语法<<<>>> 连续三个尖括号之间有空格**

nvcc支持此类写法，cu-bridge不一定支持，取决于cucc所依赖的编译器，比如mxcc目前不支持此类写法的。

**Q3.  如何选择mpi编译器**

目前cu-bridge默认配置是使用目标生态软件栈中的mpicc。如果需要选择使用用户自己安装的mpicc，只需要将cu-bridge目录中link.json中mpicc相关配置删除，或者将link.json中的mpicc编译器指向用户安装的mpicc编译器。

**Q4.  configure命令如何处理**

对于configure的命令，可以使用pre_makefile进行包裹。比如原始命令为`configure xxx`，可以转化为 `pre_makefile confiugre xxx`。

**Q5.  使用cu-bridge是否需要安装CUDA Toolkit**

为了验证原始工程在CUDA环境下是否有问题，都建议安装CUDA Toolkit。对于cmake工程，一般是需要安装CUDA Toolkit的。

**Q6.  python setup.py编译报错**

cu-bridge只是对cmake/make工程进行了一些处理，对于python库的安装，需要用户自行处理。

**Q7.  ${CUBRIDGE_HOME}或者~/cu-bridge目录下的内容是什么?** 

CUBRIDGE_HOME或者~/cu-bridge 目录为临时目录，用完后可以删除。如果编译出错，建议删除这些临时目录后，重新进行尝试。





