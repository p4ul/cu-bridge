#export PATH=/home/ubuntu/data/package/go/bin:$PATH
which go
go version
pwd
#go mod init .
#build aarch64
if [ -f "/.dockerenv" ] ; then
    mkdir /tmp/go-build
    export GOCACHE=/tmp/go-build
fi
GOOS=linux GOARCH=arm64 go build -o ./bin/aarch64/gomxccbin ./app/gomxccbin.go
if [ $? -ne 0 ]; then
    echo "update aarch64/gomxccbin fail"
else
    echo "update aarch64/gomxccbin ok"
fi
#build amd64
GOOS=linux GOARCH=amd64 go build -o ./bin/x86_64/gomxccbin ./app/gomxccbin.go
if [ $? -ne 0 ]; then
    echo "update x86_64/gomxccbin fail"
else
    echo "update x86_64/gomxccbin ok"
fi
