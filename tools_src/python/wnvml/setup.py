from setuptools import setup, find_packages


setup(
    name='wnvml',
    version="0.0.1",
    python_requires=">=3.0",
    packages=find_packages(),
    install_requires=["pymxsml"],
)
