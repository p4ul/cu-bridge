### Dependence
Make sure pymxsml has been installed

### Installation
```python setup.py install```

### Usage
```python
>>> from wnvml import *

>>> nvmlInit()
>>> device_count = nvmlDeviceGetCount()
>>> for i in range(device_count):
...     handle = nvmlDeviceGetHandleByIndex(i)
...     name = nvmlDeviceGetName(handle)
...     print("Device", i, ":", name)
...
Device 0 : MXC500
```
