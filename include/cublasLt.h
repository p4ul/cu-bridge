// NOTE(xbei): CUDA's cublasLt.h use #pragma once, may cause conflict
#if !defined(CUBLAS_LT_H_)
#define CUBLAS_LT_H_

#include <bridge/blas/cublasLt_wrapper.h>
#include "cublas_v2.h"

#endif /* !defined(CUBLAS_LT_H_) */
