#ifndef NVTOOLSEXT_CUDART_H_
#define NVTOOLSEXT_CUDART_H_

#include "mcToolsExtMaca.h"
#include "bridge/tools_ext/nvtx_wrapper.h"

#ifdef UNICODE
#define nvtxNameCudaDevice wnvtxNameCudaDeviceW
#define nvtxNameCudaStream wnvtxNameCudaStreamW
#define nvtxNameCudaEvent  wnvtxNameCudaEventW
#else
#define nvtxNameCudaDevice wnvtxNameCudaDeviceA
#define nvtxNameCudaStream wnvtxNameCudaStreamA
#define nvtxNameCudaEvent  wnvtxNameCudaEventA
#endif

#endif /* NVTOOLSEXT_CUDART_H_ */