#if !defined(__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__)
#define __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__

#include<__cuda_runtime.h>

#if !defined(__CUDACC_RTC__)
#define EXCLUDE_FROM_RTC
#include "cuda_runtime_api.h"
#undef EXCLUDE_FROM_RTC
#endif /* !__CUDACC_RTC__ */

#undef __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
#endif