#pragma once
#if !defined(__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__)
#define __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
#endif

#if defined CUDART_VERSION
#define WCUDART_VERSION CUDART_VERSION
#else
#define CUDART_VERSION WCUDART_VERSION
#endif

#include<__cuda_runtime.h>
#ifndef __DOXYGEN_ONLY__
#include "crt/host_defines.h"
#endif
#include "builtin_types.h"

#include "cuda_device_runtime_api.h"
#undef __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
