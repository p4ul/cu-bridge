#pragma once
#include <soft-link/thrust/system/mc/config.h>

// to gen a cuda namespace wrapper
THRUST_NAMESPACE_BEGIN
namespace mc {};
namespace cuda = THRUST_NS_QUALIFIER::mc;
namespace mc_cub {};
namespace cuda_cub = THRUST_NS_QUALIFIER::mc_cub;
namespace system
{
    namespace mc {};
    namespace cuda = THRUST_NS_QUALIFIER::system::mc;
}
THRUST_NAMESPACE_END