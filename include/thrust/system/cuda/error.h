#pragma once
#include <soft-link/thrust/system/mc/error.h>

THRUST_NAMESPACE_BEGIN

namespace system
{
    inline const error_category &cuda_category(void){
        return mc_category();
    }
}

using system::cuda_category;

THRUST_NAMESPACE_END