//===----------------------------------------------------------------------===//
//
// Part of libcu++, the C++ Standard Library for your entire system,
// under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#define TO_V_U32_PTR(x) reinterpret_cast<volatile uint32_t*>(x)
#define TO_C_V_U32_PTR(x) reinterpret_cast<const volatile uint32_t*>(x)
#define TO_V_U64_PTR(x) reinterpret_cast<volatile uint64_t*>(x)
#define TO_C_V_U64_PTR(x) reinterpret_cast<const volatile uint64_t*>(x)

static inline __device__ void __cuda_membar_block() { __syncthreads();}
static inline __device__ void __cuda_fence_acq_rel_block() { __syncthreads();}
static inline __device__ void __cuda_fence_sc_block() { __syncthreads(); }
static inline __device__ void __atomic_thread_fence_cuda(int __memorder, __thread_scope_block_tag) {
  NV_DISPATCH_TARGET(
    NV_PROVIDES_SM_70, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST: __cuda_fence_sc_block(); break;
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_fence_acq_rel_block(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    ),
    NV_IS_DEVICE, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST:
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_membar_block(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    )
  )
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_32_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_ACQUIRE, memory_scope_block)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_32_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_block)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_32_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_block)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_block_tag) {
    uint32_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_32_block(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_32_block(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_32_block(__ptr, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_32_block(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_64_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_ACQUIRE, memory_scope_block)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_64_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_block)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_64_block(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_block)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_block_tag) {
    uint64_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_64_block(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_64_block(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_64_block(__ptr, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_64_block(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_32_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_32_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_32_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_block_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_32_block(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_32_block(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_block();
          case __ATOMIC_RELAXED: __cuda_store_volatile_32_block(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_64_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_64_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_64_block(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_block_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_64_block(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_64_block(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_block();
          case __ATOMIC_RELAXED: __cuda_store_volatile_64_block(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.cta.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.cta.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.cta.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.cta.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.cta.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_block_tag) {
#if 0
    uint32_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 4);
    memcpy(&__old, __expected, 4);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_32_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_32_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_32_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_32_block(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_32_block(__ptr, __old, __old_tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_compare_exchange_volatile_32_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_32_block(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 4);
    return __ret;
#endif
  return __maca_atomic_compare_exchange_strong(__ptr, __expected, *__desired, __success_memorder, __failure_memorder, memory_scope_block);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_block_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_exchange_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_add_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_and_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_max_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_min_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_or_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { 
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { 
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_sub_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_32_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block);}
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_32_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_xor_volatile_32_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_32_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.cta.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.cta.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.cta.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.cta.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.cta.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_block_tag) {
#if 0
    uint64_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 8);
    memcpy(&__old, __expected, 8);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_64_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_64_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_64_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_64_block(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_64_block(__ptr, __old, __old_tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_compare_exchange_volatile_64_block(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_64_block(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 8);
    return __ret;
#endif
    return __maca_atomic_compare_exchange_strong(__ptr, __expected, *__desired, __success_memorder, __failure_memorder, memory_scope_block);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); } 
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_block_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_exchange_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_and_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_max_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_min_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_or_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { 
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_sub_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_block); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_64_block(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_block); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_block_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_xor_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_add_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_block_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp *= sizeof(_Type);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_block(__ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_sub_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_block_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp = -__tmp;
    __tmp *= sizeof(_Type);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_block(__ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_block();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); __cuda_membar_block(); break;
          case __ATOMIC_RELEASE: __cuda_membar_block(); __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_block(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
static inline __device__ void __cuda_membar_device() { __syncthreads(); }
static inline __device__ void __cuda_fence_acq_rel_device() { __syncthreads(); }
static inline __device__ void __cuda_fence_sc_device() { __syncthreads(); }
static inline __device__ void __atomic_thread_fence_cuda(int __memorder, __thread_scope_device_tag) {
  NV_DISPATCH_TARGET(
    NV_PROVIDES_SM_70, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST: __cuda_fence_sc_device(); break;
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_fence_acq_rel_device(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    ),
    NV_IS_DEVICE, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST:
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_membar_device(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    )
  )
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_32_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_ACQUIRE, memory_scope_device)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_32_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_device)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_32_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_device)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_device_tag) {
    uint32_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_32_device(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_32_device(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_32_device(__ptr, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_32_device(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_64_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(reinterpret_cast<const volatile unsigned long *>(__ptr), __ATOMIC_ACQUIRE, memory_scope_device)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_64_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(reinterpret_cast<const volatile unsigned long *>(__ptr), __ATOMIC_RELAXED, memory_scope_device)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_64_device(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(reinterpret_cast<const volatile unsigned long *>(__ptr), __ATOMIC_RELAXED, memory_scope_device)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_device_tag) {
    uint64_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_64_device(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_64_device(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_64_device(__ptr, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_64_device(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_32_device(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_32_device(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_32_device(_CUDA_A __ptr, _CUDA_B __src) {__maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_device_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_32_device(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_32_device(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_device();
          case __ATOMIC_RELAXED: __cuda_store_volatile_32_device(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_64_device(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(reinterpret_cast<volatile unsigned long*>(__ptr), __src, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_64_device(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(reinterpret_cast<volatile unsigned long*>(__ptr), __src, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_64_device(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(reinterpret_cast<volatile unsigned long*>(__ptr), __src, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_device_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_64_device(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_64_device(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_device();
          case __ATOMIC_RELAXED: __cuda_store_volatile_64_device(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.gpu.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.gpu.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.gpu.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.gpu.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.gpu.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_device_tag) {
#if 0
    uint32_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 4);
    memcpy(&__old, __expected, 4);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_32_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_32_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_32_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_32_device(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_32_device(__ptr, __old, __old_tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_compare_exchange_volatile_32_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_32_device(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 4);
    return __ret;
#endif
    return __maca_atomic_compare_exchange_strong(__ptr, __expected, *__desired, __success_memorder, __failure_memorder, memory_scope_device);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_device_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_exchange_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_add_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_and_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_max_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_min_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_or_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { 
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_sub_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_32_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_32_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_xor_volatile_32_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_32_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.gpu.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.gpu.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.gpu.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.gpu.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.gpu.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_device_tag) {
#if 0
    uint64_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 8);
    memcpy(&__old, __expected, 8);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_64_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_64_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_64_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_64_device(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_64_device(__ptr, __old, __old_tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_compare_exchange_volatile_64_device(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_64_device(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 8);
    return __ret;
#endif
    return __maca_atomic_compare_exchange_strong(reinterpret_cast<volatile unsigned long *>(__ptr), reinterpret_cast<unsigned long *>(__expected), *reinterpret_cast<const unsigned long *>(__desired), __success_memorder, __failure_memorder, memory_scope_device);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_device_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_exchange_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_and_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_max_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_min_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_or_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_sub_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_device); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_64_device(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_device); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_device_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_xor_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_add_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_device_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp *= sizeof(_Type);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_device(__ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_sub_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_device_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp = -__tmp;
    __tmp *= sizeof(_Type);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_device(__ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_device();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); __cuda_membar_device(); break;
          case __ATOMIC_RELEASE: __cuda_membar_device(); __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_device(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
static inline __device__ void __cuda_membar_system() { __syncthreads(); }
static inline __device__ void __cuda_fence_acq_rel_system() { __syncthreads();}
static inline __device__ void __cuda_fence_sc_system() { __syncthreads(); }
static inline __device__ void __atomic_thread_fence_cuda(int __memorder, __thread_scope_system_tag) {
  NV_DISPATCH_TARGET(
    NV_PROVIDES_SM_70, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST: __cuda_fence_sc_system(); break;
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_fence_acq_rel_system(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    ),
    NV_IS_DEVICE, (
      switch (__memorder) {
        case __ATOMIC_SEQ_CST:
        case __ATOMIC_CONSUME:
        case __ATOMIC_ACQUIRE:
        case __ATOMIC_ACQ_REL:
        case __ATOMIC_RELEASE: __cuda_membar_system(); break;
        case __ATOMIC_RELAXED: break;
        default: assert(0);
      }
    )
  )
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_32_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_ACQUIRE, memory_scope_system)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_32_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_system)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_32_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U32_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_system)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_system_tag) {
    uint32_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_32_system(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_32_system(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_32_system(__ptr, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_32_system(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template<class _Type, typename _CUDA_VSTD::enable_if<is_pointer<_Type>::value, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_system_tag) {
    uint64_t __tmp = 0;
    const volatile unsigned long * ptr = reinterpret_cast<const volatile unsigned long*>(__ptr);
    __tmp = __maca_atomic_load(ptr, __memorder, memory_scope_system);
    memcpy(__ret, &__tmp, 8);
}
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_acquire_64_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_ACQUIRE, memory_scope_system)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_relaxed_64_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_system)); }
template <class _CUDA_A, class _CUDA_B>
static inline __device__ void __cuda_load_volatile_64_system(_CUDA_A __ptr, _CUDA_B &__dst) { __dst = static_cast<_CUDA_B>(__maca_atomic_load(TO_C_V_U64_PTR(__ptr), __ATOMIC_RELAXED, memory_scope_system)); }
template<class _Type, typename _CUDA_VSTD::enable_if<sizeof(_Type)==8 && !is_pointer<_Type>::value, int>::type = 0>
__device__ void __atomic_load_cuda(const volatile _Type *__ptr, _Type *__ret, int __memorder, __thread_scope_system_tag) {
    uint64_t __tmp = 0;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_acquire_64_system(__ptr, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_load_relaxed_64_system(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_load_volatile_64_system(__ptr, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELAXED: __cuda_load_volatile_64_system(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_32_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_32_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_32_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U32_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_system_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_32_system(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_32_system(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_system();
          case __ATOMIC_RELAXED: __cuda_store_volatile_32_system(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
template<class _Type, typename cuda::std::enable_if<is_pointer<_Type>::value, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_system_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    volatile unsigned long * ptr = reinterpret_cast<volatile unsigned long*>(__ptr);
    __maca_atomic_store(ptr, __tmp, __memorder, memory_scope_system);
}
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_relaxed_64_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_release_64_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B> static inline __device__ void __cuda_store_volatile_64_system(_CUDA_A __ptr, _CUDA_B __src) { __maca_atomic_store(TO_V_U64_PTR(__ptr), __src, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8 && !is_pointer<_Type>::value, int>::type = 0>
__device__ void __atomic_store_cuda(volatile _Type *__ptr, _Type *__val, int __memorder, __thread_scope_system_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_RELEASE: __cuda_store_release_64_system(__ptr, __tmp); break;
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_RELAXED: __cuda_store_relaxed_64_system(__ptr, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_RELEASE:
          case __ATOMIC_SEQ_CST: __cuda_membar_system();
          case __ATOMIC_RELAXED: __cuda_store_volatile_64_system(__ptr, __tmp); break;
          default: assert(0);
        }
      )
    )
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.sys.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.sys.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.sys.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.sys.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.sys.b32 %0,[%1],%2,%3;" : "=r"(__dst) : "l"(__ptr),"r"(__cmp),"r"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_system_tag) {
#if 0
    uint32_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 4);
    memcpy(&__old, __expected, 4);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_32_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_32_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_32_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_32_system(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_32_system(__ptr, __old, __old_tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_compare_exchange_volatile_32_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_32_system(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 4);
    return __ret;
#endif
    return __maca_atomic_compare_exchange_strong(__ptr, __expected, *__desired, __success_memorder, __failure_memorder, memory_scope_system);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_system_tag) {
    uint32_t __tmp = 0;
    memcpy(&__tmp, __val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_exchange_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 4);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_add_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_and_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_max_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_min_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_or_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_sub_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_32_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==4, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint32_t __tmp = 0;
    memcpy(&__tmp, &__val, 4);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_32_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_xor_volatile_32_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_32_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 4);
    return __ret;
}
#if 0
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acq_rel.sys.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.acquire.sys.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.relaxed.sys.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.release.sys.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C, class _CUDA_D> static inline __device__ void __cuda_compare_exchange_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __cmp, _CUDA_D __op) { asm volatile("atom.cas.sys.b64 %0,[%1],%2,%3;" : "=l"(__dst) : "l"(__ptr),"l"(__cmp),"l"(__op) : "memory"); }
#endif
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ bool __atomic_compare_exchange_cuda(volatile _Type *__ptr, _Type *__expected, const _Type *__desired, bool, int __success_memorder, int __failure_memorder, __thread_scope_system_tag) {
#if 0
    uint64_t __tmp = 0, __old = 0, __old_tmp;
    memcpy(&__tmp, __desired, 8);
    memcpy(&__old, __expected, 8);
    __old_tmp = __old;
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_acquire_64_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_compare_exchange_acq_rel_64_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_compare_exchange_release_64_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_relaxed_64_system(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__stronger_order_cuda(__success_memorder, __failure_memorder)) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_compare_exchange_volatile_64_system(__ptr, __old, __old_tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_compare_exchange_volatile_64_system(__ptr, __old, __old_tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_compare_exchange_volatile_64_system(__ptr, __old, __old_tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    bool const __ret = __old == __old_tmp;
    memcpy(__expected, &__old, 8);
    return __ret;
#endif
    return __maca_atomic_compare_exchange_strong(__ptr, __expected, *__desired, __success_memorder, __failure_memorder, memory_scope_system);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_exchange_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {  __dst = __maca_atomic_exchange(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ void __atomic_exchange_cuda(volatile _Type *__ptr, _Type *__val, _Type *__ret, int __memorder, __thread_scope_system_tag) {
    uint64_t __tmp = 0;
    memcpy(&__tmp, __val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_exchange_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_exchange_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_exchange_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_exchange_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_exchange_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(__ret, &__tmp, 8);
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_add_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_add(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_add_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_and_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_and(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_and_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_and_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_and_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_and_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_and_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_and_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_max_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_max(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_max_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_max_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_max_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_max_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_max_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_max_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_min_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_min(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_min_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_min_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_min_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_min_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_min_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_min_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_or_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_or(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_or_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_or_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_or_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_or_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_or_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_or_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system);}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_sub_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) {
__dst = __maca_atomic_fetch_sub(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_sub_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_sub_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_sub_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_sub_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_sub_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_sub_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acq_rel_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_ACQ_REL, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_acquire_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_ACQUIRE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_relaxed_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_release_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELEASE, memory_scope_system); }
template<class _CUDA_A, class _CUDA_B, class _CUDA_C> static inline __device__ void __cuda_fetch_xor_volatile_64_system(_CUDA_A __ptr, _CUDA_B& __dst, _CUDA_C __op) { __dst = __maca_atomic_fetch_xor(__ptr, __op, __ATOMIC_RELAXED, memory_scope_system); }
template<class _Type, typename cuda::std::enable_if<sizeof(_Type)==8, int>::type = 0>
__device__ _Type __atomic_fetch_xor_cuda(volatile _Type *__ptr, _Type __val, int __memorder, __thread_scope_system_tag) {
    _Type __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_xor_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_xor_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_relaxed_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_xor_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_xor_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_xor_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_add_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_system_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp *= sizeof(_Type);
    volatile unsigned long *ptr = reinterpret_cast<volatile unsigned long *>(__ptr);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_system(ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_system(ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_system(ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_system(ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
template<class _Type>
__device__ _Type* __atomic_fetch_sub_cuda(_Type *volatile *__ptr, ptrdiff_t __val, int __memorder, __thread_scope_system_tag) {
    _Type* __ret;
    uint64_t __tmp = 0;
    memcpy(&__tmp, &__val, 8);
    __tmp = -__tmp;
    __tmp *= sizeof(_Type);
    NV_DISPATCH_TARGET(
      NV_PROVIDES_SM_70, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST: __cuda_fence_sc_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_acquire_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_ACQ_REL: __cuda_fetch_add_acq_rel_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELEASE: __cuda_fetch_add_release_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_relaxed_64_system(__ptr, __tmp, __tmp); break;
        }
      ),
      NV_IS_DEVICE, (
        switch (__memorder) {
          case __ATOMIC_SEQ_CST:
          case __ATOMIC_ACQ_REL: __cuda_membar_system();
          case __ATOMIC_CONSUME:
          case __ATOMIC_ACQUIRE: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); __cuda_membar_system(); break;
          case __ATOMIC_RELEASE: __cuda_membar_system(); __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          case __ATOMIC_RELAXED: __cuda_fetch_add_volatile_64_system(__ptr, __tmp, __tmp); break;
          default: assert(0);
        }
      )
    )
    memcpy(&__ret, &__tmp, 8);
    return __ret;
}
