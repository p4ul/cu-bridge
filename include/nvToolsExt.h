#ifndef NVTOOLSEXT_H_
#define NVTOOLSEXT_H_

#include "mcToolsExt.h"
#include "bridge/tools_ext/nvtx_wrapper.h"

#if defined(_MSC_VER)
#ifdef NVTX_EXPORTS
#define NVTX_DECLSPEC
#else
#define NVTX_DECLSPEC __declspec(dllimport)
#endif /* NVTX_EXPORTS */
#define NVTX_API           __stdcall
#define NVTX_INLINE_STATIC __inline static
#else /*defined(__GNUC__)*/
#define NVTX_DECLSPEC
#define NVTX_API
#define NVTX_INLINE_STATIC inline static
#endif /* Platform */

#define NVTX_VERSION 2

#define nvtxInitialize wnvtxInitialize_v2

#ifdef UNICODE
#define nvtxMark         wnvtxMarkW
#define nvtxRangeStart   wnvtxRangeStartW
#define nvtxRangePush    wnvtxRangePushW
#define nvtxNameCategory wnvtxNameCategoryW
#define nvtxNameOsThread wnvtxNameOsThreadW
/* NVTX_VERSION_2 */
#define nvtxDomainCreate         wnvtxDomainCreateW
#define nvtxDomainRegisterString wnvtxDomainRegisterStringW
#define nvtxDomainNameCategory   wnvtxDomainNameCategoryW
#else
#define nvtxMark                 wnvtxMarkA
#define nvtxRangeStart           wnvtxRangeStartA
#define nvtxRangePush            wnvtxRangePushA
#define nvtxNameCategory         wnvtxNameCategoryA
#define nvtxNameOsThread         wnvtxNameOsThreadA
/* NVTX_VERSION_2 */
#define nvtxDomainCreate         wnvtxDomainCreateA
#define nvtxDomainRegisterString wnvtxDomainRegisterStringA
#define nvtxDomainNameCategory   wnvtxDomainNameCategoryA
#endif

#endif /* NVTOOLSEXT_H_ */