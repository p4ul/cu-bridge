/**
 * @brief  Dense Linear Algebra functions wrapper of cuSolver APIs
 */

#ifndef CUSOLVER_DN_WRAPPER_H_
#define CUSOLVER_DN_WRAPPER_H_

#include <bridge/blas/cublas_v2_wrapper.h>
#include "cusolver_common_wrapper.h"
#include "mcsolverDn.h"

#define CUSOLVERDN_H_ MCSOLVER_CUSOLVER_DN_H_

#define cusolverDnContext mcsolverDnContext
#define cusolverDnHandle_t mcsolverDnHandle_t

#define syevjInfo mcsolverSyevjInfo
#define syevjInfo_t mcsolverSyevjInfo_t

#define gesvdjInfo mcsolverGesvdjInfo
#define gesvdjInfo_t mcsolverGesvdjInfo_t

//------------------------------------------------------
// opaque cusolverDnIRS structure for IRS solver
#define cusolverDnIRSParams mcsolverDnIRSParams
#define cusolverDnIRSParams_t mcsolverDnIRSParams_t

#define cusolverDnIRSInfos mcsolverDnIRSInfos
#define cusolverDnIRSInfos_t mcsolverDnIRSInfos_t
//------------------------------------------------------

#define cusolverDnParams mcsolverDnParams
#define cusolverDnParams_t mcsolverDnParams_t

#define cusolverDnFunction_t mcsolverDnFunction_t
#define CUSOLVERDN_GETRF MCSOLVERDN_GETRF
#define CUSOLVERDN_POTRF MCSOLVERDN_POTRF

/*******************************************************************************/
#define cusolverDnCreate mcsolverDnCreate
#define cusolverDnDestroy mcsolverDnDestroy
#define cusolverDnSetStream mcsolverDnSetStream
#define cusolverDnGetStream mcsolverDnGetStream

//============================================================
// IRS headers
//============================================================

// =============================================================================
// IRS helper function API
// =============================================================================
#define cusolverDnIRSParamsCreate mcsolverDnIRSParamsCreate
#define cusolverDnIRSParamsDestroy mcsolverDnIRSParamsDestroy
#define cusolverDnIRSParamsSetRefinementSolver mcsolverDnIRSParamsSetRefinementSolver
#define cusolverDnIRSParamsSetSolverMainPrecision mcsolverDnIRSParamsSetSolverMainPrecision
#define cusolverDnIRSParamsSetSolverLowestPrecision mcsolverDnIRSParamsSetSolverLowestPrecision
#define cusolverDnIRSParamsSetSolverPrecisions mcsolverDnIRSParamsSetSolverPrecisions
#define cusolverDnIRSParamsSetTol mcsolverDnIRSParamsSetTol
#define cusolverDnIRSParamsSetTolInner mcsolverDnIRSParamsSetTolInner
#define cusolverDnIRSParamsSetMaxIters mcsolverDnIRSParamsSetMaxIters
#define cusolverDnIRSParamsSetMaxItersInner mcsolverDnIRSParamsSetMaxItersInner
#define cusolverDnIRSParamsGetMaxIters mcsolverDnIRSParamsGetMaxIters
#define cusolverDnIRSParamsEnableFallback mcsolverDnIRSParamsEnableFallback
#define cusolverDnIRSParamsDisableFallback mcsolverDnIRSParamsDisableFallback

// =============================================================================
// cusolverDnIRSInfos prototypes
// =============================================================================
#define cusolverDnIRSInfosDestroy mcsolverDnIRSInfosDestroy
#define cusolverDnIRSInfosCreate mcsolverDnIRSInfosCreate
#define cusolverDnIRSInfosGetNiters mcsolverDnIRSInfosGetNiters
#define cusolverDnIRSInfosGetOuterNiters mcsolverDnIRSInfosGetOuterNiters
#define cusolverDnIRSInfosRequestResidual mcsolverDnIRSInfosRequestResidual
#define cusolverDnIRSInfosGetResidualHistory mcsolverDnIRSInfosGetResidualHistory
#define cusolverDnIRSInfosGetMaxIters mcsolverDnIRSInfosGetMaxIters

//============================================================
//  IRS functions API
//============================================================
/*******************************************************************************/
/*
 * [ZZ, ZC, ZK, ZE, ZY, CC, CK, CE, CY, DD, DS, DH, DB, DX, SS, SH, SB, SX]
 * gesv users API Prototypes
 */
/*******************************************************************************/
#define cusolverDnZZgesv mcsolverDnZZgesv
#define cusolverDnZCgesv mcsolverDnZCgesv
#define cusolverDnZKgesv mcsolverDnZKgesv
#define cusolverDnZEgesv mcsolverDnZEgesv
#define cusolverDnZYgesv mcsolverDnZYgesv
#define cusolverDnCCgesv mcsolverDnCCgesv
#define cusolverDnCEgesv mcsolverDnCEgesv
#define cusolverDnCKgesv mcsolverDnCKgesv
#define cusolverDnCYgesv mcsolverDnCYgesv
#define cusolverDnDDgesv mcsolverDnDDgesv
#define cusolverDnDSgesv mcsolverDnDSgesv
#define cusolverDnDHgesv mcsolverDnDHgesv
#define cusolverDnDBgesv mcsolverDnDBgesv
#define cusolverDnDXgesv mcsolverDnDXgesv
#define cusolverDnSSgesv mcsolverDnSSgesv
#define cusolverDnSHgesv mcsolverDnSHgesv
#define cusolverDnSBgesv mcsolverDnSBgesv
#define cusolverDnSXgesv mcsolverDnSXgesv

/*******************************************************************************/

/*******************************************************************************/
/*
 * [ZZ, ZC, ZK, ZE, ZY, CC, CK, CE, CY, DD, DS, DH, DB, DX, SS, SH, SB, SX]
 * gesv_bufferSize users API Prototypes
 */
/*******************************************************************************/
#define cusolverDnZZgesv_bufferSize mcsolverDnZZgesv_bufferSize
#define cusolverDnZCgesv_bufferSize mcsolverDnZCgesv_bufferSize
#define cusolverDnZKgesv_bufferSize mcsolverDnZKgesv_bufferSize
#define cusolverDnZEgesv_bufferSize mcsolverDnZEgesv_bufferSize
#define cusolverDnZYgesv_bufferSize mcsolverDnZYgesv_bufferSize
#define cusolverDnCCgesv_bufferSize mcsolverDnCCgesv_bufferSize
#define cusolverDnCKgesv_bufferSize mcsolverDnCKgesv_bufferSize
#define cusolverDnCEgesv_bufferSize mcsolverDnCEgesv_bufferSize
#define cusolverDnCYgesv_bufferSize mcsolverDnCYgesv_bufferSize
#define cusolverDnDDgesv_bufferSize mcsolverDnDDgesv_bufferSize
#define cusolverDnDSgesv_bufferSize mcsolverDnDSgesv_bufferSize
#define cusolverDnDHgesv_bufferSize mcsolverDnDHgesv_bufferSize
#define cusolverDnDBgesv_bufferSize mcsolverDnDBgesv_bufferSize
#define cusolverDnDXgesv_bufferSize mcsolverDnDXgesv_bufferSize
#define cusolverDnSSgesv_bufferSize mcsolverDnSSgesv_bufferSize
#define cusolverDnSHgesv_bufferSize mcsolverDnSHgesv_bufferSize
#define cusolverDnSBgesv_bufferSize mcsolverDnSBgesv_bufferSize
#define cusolverDnSXgesv_bufferSize mcsolverDnSXgesv_bufferSize
/*******************************************************************************/

/*******************************************************************************/
/*
 * [ZZ, ZC, ZK, ZE, ZY, CC, CK, CE, CY, DD, DS, DH, DB, DX, SS, SH, SB, SX]
 * gels users API Prototypes
 */
/*******************************************************************************/
#define cusolverDnZZgels mcsolverDnZZgels
#define cusolverDnZCgels mcsolverDnZCgels
#define cusolverDnZKgels mcsolverDnZKgels
#define cusolverDnZEgels mcsolverDnZEgels
#define cusolverDnZYgels mcsolverDnZYgels
#define cusolverDnCCgels mcsolverDnCCgels
#define cusolverDnCKgels mcsolverDnCKgels
#define cusolverDnCEgels mcsolverDnCEgels
#define cusolverDnCYgels mcsolverDnCYgels
#define cusolverDnDDgels mcsolverDnDDgels
#define cusolverDnDSgels mcsolverDnDSgels
#define cusolverDnDHgels mcsolverDnDHgels
#define cusolverDnDBgels mcsolverDnDBgels
#define cusolverDnDXgels mcsolverDnDXgels
#define cusolverDnSSgels mcsolverDnSSgels
#define cusolverDnSHgels mcsolverDnSHgels
#define cusolverDnSBgels mcsolverDnSBgels
#define cusolverDnSXgels mcsolverDnSXgels
/*******************************************************************************/

/*******************************************************************************/
/*
 * [ZZ, ZC, ZK, ZE, ZY, CC, CK, CE, CY, DD, DS, DH, DB, DX, SS, SH, SB, SX]
 * gels_bufferSize API prototypes
 */
/*******************************************************************************/
#define cusolverDnZZgels_bufferSize mcsolverDnZZgels_bufferSize
#define cusolverDnZCgels_bufferSize mcsolverDnZCgels_bufferSize
#define cusolverDnZKgels_bufferSize mcsolverDnZKgels_bufferSize
#define cusolverDnZEgels_bufferSize mcsolverDnZEgels_bufferSize
#define cusolverDnZYgels_bufferSize mcsolverDnZYgels_bufferSize
#define cusolverDnCCgels_bufferSize mcsolverDnCCgels_bufferSize
#define cusolverDnCKgels_bufferSize mcsolverDnCKgels_bufferSize
#define cusolverDnCEgels_bufferSize mcsolverDnCEgels_bufferSize
#define cusolverDnCYgels_bufferSize mcsolverDnCYgels_bufferSize
#define cusolverDnDDgels_bufferSize mcsolverDnDDgels_bufferSize
#define cusolverDnDSgels_bufferSize mcsolverDnDSgels_bufferSize
#define cusolverDnDHgels_bufferSize mcsolverDnDHgels_bufferSize
#define cusolverDnDBgels_bufferSize mcsolverDnDBgels_bufferSize
#define cusolverDnDXgels_bufferSize mcsolverDnDXgels_bufferSize
#define cusolverDnSSgels_bufferSize mcsolverDnSSgels_bufferSize
#define cusolverDnSHgels_bufferSize mcsolverDnSHgels_bufferSize
#define cusolverDnSBgels_bufferSize mcsolverDnSBgels_bufferSize
#define cusolverDnSXgels_bufferSize mcsolverDnSXgels_bufferSize
/*******************************************************************************/

/*******************************************************************************/
/*
 * expert users API for IRS Prototypes
 */
/*******************************************************************************/
#define cusolverDnIRSXgesv mcsolverDnIRSXgesv
#define cusolverDnIRSXgesv_bufferSize mcsolverDnIRSXgesv_bufferSize
#define cusolverDnIRSXgels mcsolverDnIRSXgels
#define cusolverDnIRSXgels_bufferSize mcsolverDnIRSXgels_bufferSize
/*******************************************************************************/

/* Cholesky factorization and its solver */
#define cusolverDnSpotrf_bufferSize mcsolverDnSpotrf_bufferSize
#define cusolverDnDpotrf_bufferSize mcsolverDnDpotrf_bufferSize
#define cusolverDnCpotrf_bufferSize mcsolverDnCpotrf_bufferSize
#define cusolverDnZpotrf_bufferSize mcsolverDnZpotrf_bufferSize
#define cusolverDnSpotrf mcsolverDnSpotrf
#define cusolverDnDpotrf mcsolverDnDpotrf
#define cusolverDnCpotrf mcsolverDnCpotrf
#define cusolverDnZpotrf mcsolverDnZpotrf
#define cusolverDnSpotrs mcsolverDnSpotrs
#define cusolverDnDpotrs mcsolverDnDpotrs
#define cusolverDnCpotrs mcsolverDnCpotrs
#define cusolverDnZpotrs mcsolverDnZpotrs

/* batched Cholesky factorization and its solver */
#define cusolverDnSpotrfBatched mcsolverDnSpotrfBatched
#define cusolverDnDpotrfBatched mcsolverDnDpotrfBatched
#define cusolverDnCpotrfBatched mcsolverDnCpotrfBatched
#define cusolverDnZpotrfBatched mcsolverDnZpotrfBatched
#define cusolverDnSpotrsBatched mcsolverDnSpotrsBatched
#define cusolverDnDpotrsBatched mcsolverDnDpotrsBatched
#define cusolverDnCpotrsBatched mcsolverDnCpotrsBatched
#define cusolverDnZpotrsBatched mcsolverDnZpotrsBatched

/* s.p.d. matrix inversion (POTRI) and auxiliary routines (TRTRI and LAUUM)
 */
#define cusolverDnSpotri_bufferSize mcsolverDnSpotri_bufferSize
#define cusolverDnDpotri_bufferSize mcsolverDnDpotri_bufferSize
#define cusolverDnCpotri_bufferSize mcsolverDnCpotri_bufferSize
#define cusolverDnZpotri_bufferSize mcsolverDnZpotri_bufferSize
#define cusolverDnSpotri mcsolverDnSpotri
#define cusolverDnDpotri mcsolverDnDpotri
#define cusolverDnCpotri mcsolverDnCpotri
#define cusolverDnZpotri mcsolverDnZpotri
#define cusolverDnXtrtri_bufferSize mcsolverDnXtrtri_bufferSize
#define cusolverDnXtrtri mcsolverDnXtrtri

/* lauum, auxiliar routine for s.p.d matrix inversion */
#define cusolverDnSlauum_bufferSize mcsolverDnSlauum_bufferSize
#define cusolverDnDlauum_bufferSize mcsolverDnDlauum_bufferSize
#define cusolverDnClauum_bufferSize mcsolverDnClauum_bufferSize
#define cusolverDnZlauum_bufferSize mcsolverDnZlauum_bufferSize
#define cusolverDnSlauum mcsolverDnSlauum
#define cusolverDnDlauum mcsolverDnDlauum
#define cusolverDnClauum mcsolverDnClauum
#define cusolverDnZlauum mcsolverDnZlauum

/* LU Factorization */
#define cusolverDnSgetrf_bufferSize mcsolverDnSgetrf_bufferSize
#define cusolverDnDgetrf_bufferSize mcsolverDnDgetrf_bufferSize
#define cusolverDnCgetrf_bufferSize mcsolverDnCgetrf_bufferSize
#define cusolverDnZgetrf_bufferSize mcsolverDnZgetrf_bufferSize
#define cusolverDnSgetrf mcsolverDnSgetrf
#define cusolverDnDgetrf mcsolverDnDgetrf
#define cusolverDnCgetrf mcsolverDnCgetrf
#define cusolverDnZgetrf mcsolverDnZgetrf

/* Row pivoting */
#define cusolverDnSlaswp mcsolverDnSlaswp
#define cusolverDnDlaswp mcsolverDnDlaswp
#define cusolverDnClaswp mcsolverDnClaswp
#define cusolverDnZlaswp mcsolverDnZlaswp

/* LU solve */
#define cusolverDnSgetrs mcsolverDnSgetrs
#define cusolverDnDgetrs mcsolverDnDgetrs
#define cusolverDnCgetrs mcsolverDnCgetrs
#define cusolverDnZgetrs mcsolverDnZgetrs

/* QR factorization */
#define cusolverDnSgeqrf_bufferSize mcsolverDnSgeqrf_bufferSize
#define cusolverDnDgeqrf_bufferSize mcsolverDnDgeqrf_bufferSize
#define cusolverDnCgeqrf_bufferSize mcsolverDnCgeqrf_bufferSize
#define cusolverDnZgeqrf_bufferSize mcsolverDnZgeqrf_bufferSize
#define cusolverDnSgeqrf mcsolverDnSgeqrf
#define cusolverDnDgeqrf mcsolverDnDgeqrf
#define cusolverDnCgeqrf mcsolverDnCgeqrf
#define cusolverDnZgeqrf mcsolverDnZgeqrf

/* generate unitary matrix Q from QR factorization */
#define cusolverDnSorgqr_bufferSize mcsolverDnSorgqr_bufferSize
#define cusolverDnDorgqr_bufferSize mcsolverDnDorgqr_bufferSize
#define cusolverDnCungqr_bufferSize mcsolverDnCungqr_bufferSize
#define cusolverDnZungqr_bufferSize mcsolverDnZungqr_bufferSize
#define cusolverDnSorgqr mcsolverDnSorgqr
#define cusolverDnDorgqr mcsolverDnDorgqr
#define cusolverDnCungqr mcsolverDnCungqr
#define cusolverDnZungqr mcsolverDnZungqr

/* compute Q**T*b in solve min||A*x = b|| */
#define cusolverDnSormqr_bufferSize mcsolverDnSormqr_bufferSize
#define cusolverDnDormqr_bufferSize mcsolverDnDormqr_bufferSize
#define cusolverDnCunmqr_bufferSize mcsolverDnCunmqr_bufferSize
#define cusolverDnZunmqr_bufferSize mcsolverDnZunmqr_bufferSize
#define cusolverDnSormqr mcsolverDnSormqr
#define cusolverDnDormqr mcsolverDnDormqr
#define cusolverDnCunmqr mcsolverDnCunmqr
#define cusolverDnZunmqr mcsolverDnZunmqr

/* L*D*L**T,U*D*U**T factorization */
#define cusolverDnSsytrf_bufferSize mcsolverDnSsytrf_bufferSize
#define cusolverDnDsytrf_bufferSize mcsolverDnDsytrf_bufferSize
#define cusolverDnCsytrf_bufferSize mcsolverDnCsytrf_bufferSize
#define cusolverDnZsytrf_bufferSize mcsolverDnZsytrf_bufferSize
#define cusolverDnSsytrf mcsolverDnSsytrf
#define cusolverDnDsytrf mcsolverDnDsytrf
#define cusolverDnCsytrf mcsolverDnCsytrf
#define cusolverDnZsytrf mcsolverDnZsytrf

/* Symmetric indefinite solve (SYTRS) */
#define cusolverDnXsytrs_bufferSize mcsolverDnXsytrs_bufferSize
#define cusolverDnXsytrs mcsolverDnXsytrs

/* Symmetric indefinite inversion (sytri) */
#define cusolverDnSsytri_bufferSize mcsolverDnSsytri_bufferSize
#define cusolverDnDsytri_bufferSize mcsolverDnDsytri_bufferSize
#define cusolverDnCsytri_bufferSize mcsolverDnCsytri_bufferSize
#define cusolverDnZsytri_bufferSize mcsolverDnZsytri_bufferSize
#define cusolverDnSsytri mcsolverDnSsytri
#define cusolverDnDsytri mcsolverDnDsytri
#define cusolverDnCsytri mcsolverDnCsytri
#define cusolverDnZsytri mcsolverDnZsytri

/* bidiagonal factorization */
#define cusolverDnSgebrd_bufferSize mcsolverDnSgebrd_bufferSize
#define cusolverDnDgebrd_bufferSize mcsolverDnDgebrd_bufferSize
#define cusolverDnCgebrd_bufferSize mcsolverDnCgebrd_bufferSize
#define cusolverDnZgebrd_bufferSize mcsolverDnZgebrd_bufferSize
#define cusolverDnSgebrd mcsolverDnSgebrd
#define cusolverDnDgebrd mcsolverDnDgebrd
#define cusolverDnCgebrd mcsolverDnCgebrd
#define cusolverDnZgebrd mcsolverDnZgebrd

/* generates one of the unitary matrices Q or P**T determined by GEBRD*/
#define cusolverDnSorgbr_bufferSize mcsolverDnSorgbr_bufferSize
#define cusolverDnDorgbr_bufferSize mcsolverDnDorgbr_bufferSize
#define cusolverDnCungbr_bufferSize mcsolverDnCungbr_bufferSize
#define cusolverDnZungbr_bufferSize mcsolverDnZungbr_bufferSize
#define cusolverDnSorgbr mcsolverDnSorgbr
#define cusolverDnDorgbr mcsolverDnDorgbr
#define cusolverDnCungbr mcsolverDnCungbr
#define cusolverDnZungbr mcsolverDnZungbr

/* tridiagonal factorization */
#define cusolverDnSsytrd_bufferSize mcsolverDnSsytrd_bufferSize
#define cusolverDnDsytrd_bufferSize mcsolverDnDsytrd_bufferSize
#define cusolverDnChetrd_bufferSize mcsolverDnChetrd_bufferSize
#define cusolverDnZhetrd_bufferSize mcsolverDnZhetrd_bufferSize
#define cusolverDnSsytrd mcsolverDnSsytrd
#define cusolverDnDsytrd mcsolverDnDsytrd
#define cusolverDnChetrd mcsolverDnChetrd
#define cusolverDnZhetrd mcsolverDnZhetrd

/* generate unitary Q comes from sytrd */
#define cusolverDnSorgtr_bufferSize mcsolverDnSorgtr_bufferSize
#define cusolverDnDorgtr_bufferSize mcsolverDnDorgtr_bufferSize
#define cusolverDnCungtr_bufferSize mcsolverDnCungtr_bufferSize
#define cusolverDnZungtr_bufferSize mcsolverDnZungtr_bufferSize
#define cusolverDnSorgtr mcsolverDnSorgtr
#define cusolverDnDorgtr mcsolverDnDorgtr
#define cusolverDnCungtr mcsolverDnCungtr
#define cusolverDnZungtr mcsolverDnZungtr

/* compute op(Q)*C or C*op(Q) where Q comes from sytrd */
#define cusolverDnSormtr_bufferSize mcsolverDnSormtr_bufferSize
#define cusolverDnDormtr_bufferSize mcsolverDnDormtr_bufferSize
#define cusolverDnCunmtr_bufferSize mcsolverDnCunmtr_bufferSize
#define cusolverDnZunmtr_bufferSize mcsolverDnZunmtr_bufferSize
#define cusolverDnSormtr mcsolverDnSormtr
#define cusolverDnDormtr mcsolverDnDormtr
#define cusolverDnCunmtr mcsolverDnCunmtr
#define cusolverDnZunmtr mcsolverDnZunmtr

/* singular value decomposition, A = U * Sigma * V^H */
#define cusolverDnSgesvd_bufferSize mcsolverDnSgesvd_bufferSize
#define cusolverDnDgesvd_bufferSize mcsolverDnDgesvd_bufferSize
#define cusolverDnCgesvd_bufferSize mcsolverDnCgesvd_bufferSize
#define cusolverDnZgesvd_bufferSize mcsolverDnZgesvd_bufferSize
#define cusolverDnSgesvd mcsolverDnSgesvd
#define cusolverDnDgesvd mcsolverDnDgesvd
#define cusolverDnCgesvd mcsolverDnCgesvd
#define cusolverDnZgesvd mcsolverDnZgesvd

/* standard symmetric eigenvalue solver, A*x = lambda*x, by
 * divide-and-conquer  */
#define cusolverDnSsyevd_bufferSize mcsolverDnSsyevd_bufferSize
#define cusolverDnDsyevd_bufferSize mcsolverDnDsyevd_bufferSize
#define cusolverDnCheevd_bufferSize mcsolverDnCheevd_bufferSize
#define cusolverDnZheevd_bufferSize mcsolverDnZheevd_bufferSize
#define cusolverDnSsyevd mcsolverDnSsyevd
#define cusolverDnDsyevd mcsolverDnDsyevd
#define cusolverDnCheevd mcsolverDnCheevd
#define cusolverDnZheevd mcsolverDnZheevd

/* standard selective symmetric eigenvalue solver, A*x = lambda*x, by
 * divide-and-conquer  */
#define cusolverDnSsyevdx_bufferSize mcsolverDnSsyevdx_bufferSize
#define cusolverDnDsyevdx_bufferSize mcsolverDnDsyevdx_bufferSize
#define cusolverDnCheevdx_bufferSize mcsolverDnCheevdx_bufferSize
#define cusolverDnZheevdx_bufferSize mcsolverDnZheevdx_bufferSize
#define cusolverDnSsyevdx mcsolverDnSsyevdx
#define cusolverDnDsyevdx mcsolverDnDsyevdx
#define cusolverDnCheevdx mcsolverDnCheevdx
#define cusolverDnZheevdx mcsolverDnZheevdx

/* selective generalized symmetric eigenvalue solver, A*x = lambda*B*x, by
 * divide-and-conquer  */
#define cusolverDnSsygvdx_bufferSize mcsolverDnSsygvdx_bufferSize
#define cusolverDnDsygvdx_bufferSize mcsolverDnDsygvdx_bufferSize
#define cusolverDnChegvdx_bufferSize mcsolverDnChegvdx_bufferSize
#define cusolverDnZhegvdx_bufferSize mcsolverDnZhegvdx_bufferSize
#define cusolverDnSsygvdx mcsolverDnSsygvdx
#define cusolverDnDsygvdx mcsolverDnDsygvdx
#define cusolverDnChegvdx mcsolverDnChegvdx
#define cusolverDnZhegvdx mcsolverDnZhegvdx

/* generalized symmetric eigenvalue solver, A*x = lambda*B*x, by
 * divide-and-conquer  */
#define cusolverDnSsygvd_bufferSize mcsolverDnSsygvd_bufferSize
#define cusolverDnDsygvd_bufferSize mcsolverDnDsygvd_bufferSize
#define cusolverDnChegvd_bufferSize mcsolverDnChegvd_bufferSize
#define cusolverDnZhegvd_bufferSize mcsolverDnZhegvd_bufferSize
#define cusolverDnSsygvd mcsolverDnSsygvd
#define cusolverDnDsygvd mcsolverDnDsygvd
#define cusolverDnChegvd mcsolverDnChegvd
#define cusolverDnZhegvd mcsolverDnZhegvd
#define cusolverDnCreateSyevjInfo mcsolverDnCreateSyevjInfo
#define cusolverDnDestroySyevjInfo mcsolverDnDestroySyevjInfo
#define cusolverDnXsyevjSetTolerance mcsolverDnXsyevjSetTolerance
#define cusolverDnXsyevjSetMaxSweeps mcsolverDnXsyevjSetMaxSweeps
#define cusolverDnXsyevjSetSortEig mcsolverDnXsyevjSetSortEig
#define cusolverDnXsyevjGetResidual mcsolverDnXsyevjGetResidual
#define cusolverDnXsyevjGetSweeps mcsolverDnXsyevjGetSweeps
#define cusolverDnSsyevjBatched_bufferSize mcsolverDnSsyevjBatched_bufferSize
#define cusolverDnDsyevjBatched_bufferSize mcsolverDnDsyevjBatched_bufferSize
#define cusolverDnCheevjBatched_bufferSize mcsolverDnCheevjBatched_bufferSize
#define cusolverDnZheevjBatched_bufferSize mcsolverDnZheevjBatched_bufferSize
#define cusolverDnSsyevjBatched mcsolverDnSsyevjBatched
#define cusolverDnDsyevjBatched mcsolverDnDsyevjBatched
#define cusolverDnCheevjBatched mcsolverDnCheevjBatched
#define cusolverDnZheevjBatched mcsolverDnZheevjBatched
#define cusolverDnSsyevj_bufferSize mcsolverDnSsyevj_bufferSize
#define cusolverDnDsyevj_bufferSize mcsolverDnDsyevj_bufferSize
#define cusolverDnCheevj_bufferSize mcsolverDnCheevj_bufferSize
#define cusolverDnZheevj_bufferSize mcsolverDnZheevj_bufferSize
#define cusolverDnSsyevj mcsolverDnSsyevj
#define cusolverDnDsyevj mcsolverDnDsyevj
#define cusolverDnCheevj mcsolverDnCheevj
#define cusolverDnZheevj mcsolverDnZheevj
#define cusolverDnSsygvj_bufferSize mcsolverDnSsygvj_bufferSize
#define cusolverDnDsygvj_bufferSize mcsolverDnDsygvj_bufferSize
#define cusolverDnChegvj_bufferSize mcsolverDnChegvj_bufferSize
#define cusolverDnZhegvj_bufferSize mcsolverDnZhegvj_bufferSize
#define cusolverDnSsygvj mcsolverDnSsygvj
#define cusolverDnDsygvj mcsolverDnDsygvj
#define cusolverDnChegvj mcsolverDnChegvj
#define cusolverDnZhegvj mcsolverDnZhegvj
#define cusolverDnCreateGesvdjInfo mcsolverDnCreateGesvdjInfo
#define cusolverDnDestroyGesvdjInfo mcsolverDnDestroyGesvdjInfo
#define cusolverDnXgesvdjSetTolerance mcsolverDnXgesvdjSetTolerance
#define cusolverDnXgesvdjSetMaxSweeps mcsolverDnXgesvdjSetMaxSweeps
#define cusolverDnXgesvdjSetSortEig mcsolverDnXgesvdjSetSortEig
#define cusolverDnXgesvdjGetResidual mcsolverDnXgesvdjGetResidual
#define cusolverDnXgesvdjGetSweeps mcsolverDnXgesvdjGetSweeps
#define cusolverDnSgesvdjBatched_bufferSize mcsolverDnSgesvdjBatched_bufferSize
#define cusolverDnDgesvdjBatched_bufferSize mcsolverDnDgesvdjBatched_bufferSize
#define cusolverDnCgesvdjBatched_bufferSize mcsolverDnCgesvdjBatched_bufferSize
#define cusolverDnZgesvdjBatched_bufferSize mcsolverDnZgesvdjBatched_bufferSize
#define cusolverDnSgesvdjBatched mcsolverDnSgesvdjBatched
#define cusolverDnDgesvdjBatched mcsolverDnDgesvdjBatched
#define cusolverDnCgesvdjBatched mcsolverDnCgesvdjBatched
#define cusolverDnZgesvdjBatched mcsolverDnZgesvdjBatched
#define cusolverDnSgesvdj_bufferSize mcsolverDnSgesvdj_bufferSize
#define cusolverDnDgesvdj_bufferSize mcsolverDnDgesvdj_bufferSize
#define cusolverDnCgesvdj_bufferSize mcsolverDnCgesvdj_bufferSize
#define cusolverDnZgesvdj_bufferSize mcsolverDnZgesvdj_bufferSize
#define cusolverDnSgesvdj mcsolverDnSgesvdj
#define cusolverDnDgesvdj mcsolverDnDgesvdj
#define cusolverDnCgesvdj mcsolverDnCgesvdj
#define cusolverDnZgesvdj mcsolverDnZgesvdj

/* batched approximate SVD */
#define cusolverDnSgesvdaStridedBatched_bufferSize mcsolverDnSgesvdaStridedBatched_bufferSize
#define cusolverDnDgesvdaStridedBatched_bufferSize mcsolverDnDgesvdaStridedBatched_bufferSize
#define cusolverDnCgesvdaStridedBatched_bufferSize mcsolverDnCgesvdaStridedBatched_bufferSize
#define cusolverDnZgesvdaStridedBatched_bufferSize mcsolverDnZgesvdaStridedBatched_bufferSize
#define cusolverDnSgesvdaStridedBatched mcsolverDnSgesvdaStridedBatched
#define cusolverDnDgesvdaStridedBatched mcsolverDnDgesvdaStridedBatched
#define cusolverDnCgesvdaStridedBatched mcsolverDnCgesvdaStridedBatched
#define cusolverDnZgesvdaStridedBatched mcsolverDnZgesvdaStridedBatched
#define cusolverDnCreateParams mcsolverDnCreateParams
#define cusolverDnDestroyParams mcsolverDnDestroyParams
#define cusolverDnSetAdvOptions mcsolverDnSetAdvOptions

/* CUSOLVER_DEPRECATED */
#define cusolverDnPotrf_bufferSize mcsolverDnPotrf_bufferSize
#define cusolverDnPotrf mcsolverDnPotrf
#define cusolverDnPotrs mcsolverDnPotrs
#define cusolverDnGeqrf_bufferSize mcsolverDnGeqrf_bufferSize
#define cusolverDnGeqrf mcsolverDnGeqrf
#define cusolverDnGetrf_bufferSize mcsolverDnGetrf_bufferSize
#define cusolverDnGetrf mcsolverDnGetrf
#define cusolverDnGetrs mcsolverDnGetrs
#define cusolverDnSyevd_bufferSize mcsolverDnSyevd_bufferSize
#define cusolverDnSyevd mcsolverDnSyevd
#define cusolverDnSyevdx_bufferSize mcsolverDnSyevdx_bufferSize
#define cusolverDnSyevdx mcsolverDnSyevdx
#define cusolverDnGesvd_bufferSize mcsolverDnGesvd_bufferSize
#define cusolverDnGesvd mcsolverDnGesvd

/*
 * new 64-bit API
 */
#define cusolverDnXpotrf_bufferSize mcsolverDnXpotrf_bufferSize
#define cusolverDnXpotrf mcsolverDnXpotrf
#define cusolverDnXpotrs mcsolverDnXpotrs
#define cusolverDnXgeqrf_bufferSize mcsolverDnXgeqrf_bufferSize
#define cusolverDnXgeqrf mcsolverDnXgeqrf
#define cusolverDnXgetrf_bufferSize mcsolverDnXgetrf_bufferSize
#define cusolverDnXgetrf mcsolverDnXgetrf
#define cusolverDnXgetrs mcsolverDnXgetrs
#define cusolverDnXsyevd_bufferSize mcsolverDnXsyevd_bufferSize
#define cusolverDnXsyevd mcsolverDnXsyevd
#define cusolverDnXsyevdx_bufferSize mcsolverDnXsyevdx_bufferSize
#define cusolverDnXsyevdx mcsolverDnXsyevdx
#define cusolverDnXgesvd_bufferSize mcsolverDnXgesvd_bufferSize
#define cusolverDnXgesvd mcsolverDnXgesvd
#define cusolverDnXgesvdp_bufferSize mcsolverDnXgesvdp_bufferSize
#define cusolverDnXgesvdp mcsolverDnXgesvdp
#define cusolverDnXgesvdr_bufferSize mcsolverDnXgesvdr_bufferSize
#define cusolverDnXgesvdr mcsolverDnXgesvdr

#endif
