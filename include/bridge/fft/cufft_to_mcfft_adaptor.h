#ifndef CUDA_ADAPTOR_INCLUDE_CUFFT_TO_MCFFT_ADAPTOR_H_
#define CUDA_ADAPTOR_INCLUDE_CUFFT_TO_MCFFT_ADAPTOR_H_

// header define
#ifndef _CUFFT_H_
#define _CUFFT_H_
#endif /* _CUFFT_H_ */

// enum redefine
#define CUFFTAPI MCFFTAPI
#define CUFFT_VER_MAJOR MCFFT_VER_MAJOR
#define CUFFT_VER_MINOR MCFFT_VER_MINOR
#define CUFFT_VER_PATCH MCFFT_VER_PATCH
#define CUFFT_VER_BUILD MCFFT_VER_BUILD
#define CUFFT_VERSION MCFFT_VERSION
#define CUFFT_SUCCESS MCFFT_SUCCESS
#define CUFFT_INVALID_PLAN MCFFT_INVALID_PLAN
#define CUFFT_ALLOC_FAILED MCFFT_ALLOC_FAILED
#define CUFFT_INVALID_TYPE MCFFT_INVALID_TYPE
#define CUFFT_INVALID_VALUE MCFFT_INVALID_VALUE
#define CUFFT_INTERNAL_ERROR MCFFT_INTERNAL_ERROR
#define CUFFT_EXEC_FAILED MCFFT_EXEC_FAILED
#define CUFFT_SETUP_FAILED MCFFT_SETUP_FAILED
#define CUFFT_INVALID_SIZE MCFFT_INVALID_SIZE
#define CUFFT_UNALIGNED_DATA MCFFT_UNALIGNED_DATA
#define CUFFT_INCOMPLETE_PARAMETER_LIST MCFFT_INCOMPLETE_PARAMETER_LIST
#define CUFFT_INVALID_DEVICE MCFFT_INVALID_DEVICE
#define CUFFT_PARSE_ERROR MCFFT_PARSE_ERROR
#define CUFFT_NO_WORKSPACE MCFFT_NO_WORKSPACE
#define CUFFT_NOT_IMPLEMENTED MCFFT_NOT_IMPLEMENTED
#define CUFFT_LICENSE_ERROR MCFFT_LICENSE_ERROR
#define CUFFT_NOT_SUPPORTED MCFFT_NOT_SUPPORTED
#define MAX_CUFFT_ERROR MAX_MCFFT_ERROR
#define CUFFT_FORWARD MCFFT_FORWARD
#define CUFFT_INVERSE MCFFT_INVERSE
#define CUFFT_R2C MCFFT_R2C
#define CUFFT_C2R MCFFT_C2R
#define CUFFT_C2C MCFFT_C2C
#define CUFFT_D2Z MCFFT_D2Z
#define CUFFT_Z2D MCFFT_Z2D
#define CUFFT_Z2Z MCFFT_Z2Z
#define CUFFT_COMPATIBILITY_FFTW_PADDING MCFFT_COMPATIBILITY_FFTW_PADDING
#define CUFFT_COMPATIBILITY_DEFAULT MCFFT_COMPATIBILITY_DEFAULT

// class redefine
#define cufftResult_t mcfftResult_t
#define cufftResult mcfftResult
#define cufftReal mcfftReal
#define cufftDoubleReal mcfftDoubleReal
#define cufftComplex mcfftComplex
#define cufftDoubleComplex mcfftDoubleComplex
#define cufftType_t mcfftType_t
#define cufftType mcfftType
#define cufftCompatibility_t mcfftCompatibility_t
#define cufftCompatibility mcfftCompatibility
#define cufftHandle mcfftHandle

// func redefine
#define cufftPlan1d mcfftPlan1d
#define cufftPlan2d mcfftPlan2d
#define cufftPlan3d mcfftPlan3d
#define cufftPlanMany mcfftPlanMany
#define cufftMakePlan1d mcfftMakePlan1d
#define cufftMakePlan2d mcfftMakePlan2d
#define cufftMakePlan3d mcfftMakePlan3d
#define cufftMakePlanMany mcfftMakePlanMany
#define cufftMakePlanMany64 mcfftMakePlanMany64
#define cufftGetSizeMany64 mcfftGetSizeMany64
#define cufftEstimate1d mcfftEstimate1d
#define cufftEstimate2d mcfftEstimate2d
#define cufftEstimate3d mcfftEstimate3d
#define cufftEstimateMany mcfftEstimateMany
#define cufftCreate mcfftCreate
#define cufftGetSize1d mcfftGetSize1d
#define cufftGetSize2d mcfftGetSize2d
#define cufftGetSize3d mcfftGetSize3d
#define cufftGetSizeMany mcfftGetSizeMany
#define cufftGetSize mcfftGetSize
#define cufftSetWorkArea mcfftSetWorkArea
#define cufftSetAutoAllocation mcfftSetAutoAllocation
#define cufftExecC2C mcfftExecC2C
#define cufftExecR2C mcfftExecR2C
#define cufftExecC2R mcfftExecC2R
#define cufftExecZ2Z mcfftExecZ2Z
#define cufftExecD2Z mcfftExecD2Z
#define cufftExecZ2D mcfftExecZ2D
#define cufftSetStream mcfftSetStream
#define cufftDestroy mcfftDestroy
#define cufftGetVersion mcfftGetVersion
#define cufftGetProperty mcfftGetProperty

#endif // CUDA_ADAPTOR_INCLUDE_CUFFT_TO_MCFFT_ADAPTOR_H_
