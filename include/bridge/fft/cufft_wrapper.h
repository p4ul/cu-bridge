#ifndef CUFFT_WRAPPER_H_
#define CUFFT_WRAPPER_H_

#define ENABLE_CUDA_TO_MACA_ADAPTOR

#include "bridge/runtime/cuda_to_maca_mcr_adaptor.h"
#include "bridge/runtime/cuda_runtime_wrapper.h"
#include "cufft_to_mcfft_adaptor.h"
#include "mcfft.h"


#endif //CUFFT_WRAPPER_H_

