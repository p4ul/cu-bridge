#ifndef CUFFTXT_WRAPPER_H_
#define CUFFTXT_WRAPPER_H_

#define ENABLE_CUDA_TO_MACA_ADAPTOR

#include "bridge/runtime/cuda_to_maca_mcr_adaptor.h"
#include "bridge/runtime/cuda_runtime_wrapper.h"
#include "cufftXt_to_mcfftXt_adaptor.h"
#include "mcfftXt.h"


#endif //CUFFTXT_WRAPPER_H_

