#ifndef CUSPARSE_WRAPPER_H
#define CUSPARSE_WRAPPER_H

#include "mcsparse.h"

#define ENABLE_CUDA_TO_MACA_ADAPTOR
//##############################################################################
//# CUSPARSE VERSION INFORMATION
//##############################################################################

// Current cusparse wrapper follows cuda 11.5
#define CUSPARSE_VER_MAJOR 11
#define CUSPARSE_VER_MINOR 5
#define CUSPARSE_VER_PATCH 0
#define CUSPARSE_VER_BUILD 0
#define CUSPARSE_VERSION (CUSPARSE_VER_MAJOR * 1000 + CUSPARSE_VER_MINOR * 100 + CUSPARSE_VER_PATCH)

// #############################################################################
// # CUSPARSE BASIC MACROS
// #############################################################################

#define CUSPARSE_H_ MCSPARSE_CUSPARSE_H_
#define CUSPARSE_V2_H_ MCSPARSE_CUSPARSE_V2_H_

#ifndef CUSPARSEAPI
#ifdef _WIN32
#define CUSPARSEAPI __stdcall
#else
#define CUSPARSEAPI
#endif
#endif

#ifndef _MSC_VER
#define CUSPARSE_CPP_VERSION __cplusplus
#elif _MSC_FULL_VER >= 190024210  // Visual Studio 2015 Update 3
#define CUSPARSE_CPP_VERSION _MSVC_LANG
#else
#define CUSPARSE_CPP_VERSION 0
#endif

// #############################################################################
// # CUSPARSE_DEPRECATED MACRO
// #############################################################################

#ifndef DISABLE_CUSPARSE_DEPRECATED

#if CUSPARSE_CPP_VERSION >= 201402L

#define CUSPARSE_DEPRECATED(new_func) [[deprecated("please use " #new_func " instead")]]

#elif defined(_MSC_VER)

#define CUSPARSE_DEPRECATED(new_func) __declspec(deprecated("please use " #new_func " instead"))

#elif defined(__INTEL_COMPILER) || defined(__clang__) || \
    (defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 5)))

#define CUSPARSE_DEPRECATED(new_func) __attribute__((deprecated("please use " #new_func " instead")))

#elif defined(__GNUC__) || defined(__xlc__)

#define CUSPARSE_DEPRECATED(new_func) __attribute__((deprecated))

#else

#define CUSPARSE_DEPRECATED(new_func)

#endif  // defined(__cplusplus) && __cplusplus >= 201402L
//------------------------------------------------------------------------------

#if CUSPARSE_CPP_VERSION >= 201703L

#define CUSPARSE_DEPRECATED_ENUM(new_enum) [[deprecated("please use " #new_enum " instead")]]

#elif defined(__clang__) || (defined(__GNUC__) && __GNUC__ >= 6 && !defined(__PGI))

#define CUSPARSE_DEPRECATED_ENUM(new_enum) __attribute__((deprecated("please use " #new_enum " instead")))

#else

#define CUSPARSE_DEPRECATED_ENUM(new_enum)

#endif  // defined(__cplusplus) && __cplusplus >= 201402L

#else  // defined(DISABLE_CUSPARSE_DEPRECATED)

#define CUSPARSE_DEPRECATED(new_func)
#define CUSPARSE_DEPRECATED_ENUM(new_enum)

#endif  // !defined(DISABLE_CUSPARSE_DEPRECATED)

#undef CUSPARSE_CPP_VERSION

//##############################################################################
//# OPAQUE DATA STRUCTURES
//##############################################################################
#define cusparseHandle_t mcsparseHandle_t
#define cusparseContext mcsparseHandle
#define cusparseMatDescr_t mcsparseMatDescr_t
#define cusparseMatDescr mcsparseMatDescr

#define csrsv2Info_t mcsparseCsrsv2Info_t
#define csrsv2Info mcsparseCsrsv2Info
#define csrsm2Info_t mcsparseCsrsm2Info_t
#define csrsm2Info mcsparseCsrsm2Info
#define bsrsv2Info_t mcsparseBsrsv2Info_t
#define bsrsv2Info mcsparseBsrsv2Info
#define bsrsm2Info_t mcsparseBsrsm2Info_t
#define bsrsm2Info mcsparseBsrsm2Info
#define csric02Info_t mcsparseCsric02Info_t
#define csric02Info mcsparseCsric02Info
#define bsric02Info_t mcsparseBsric02Info_t
#define bsric02Info mcsparseBsric02Info
#define csrilu02Info_t mcsparseCsrilu02Info_t
#define csrilu02Info mcsparseCsrilu02Info
#define bsrilu02Info_t mcsparseBsrilu02Info_t
#define bsrilu02Info mcsparseBsrilu02Info
#define csrgemm2Info_t mcsparseCsrgemm2Info_t
#define csrgemm2Info mcsparseCsrgemm2Info
#define csru2csrInfo_t mcsparseCsru2csrInfo_t
#define csru2csrInfo mcsparseCsru2csrInfo
#define cusparseColorInfo_t mcsparseColorInfo_t
#define cusparseColorInfo mcsparseColorInfo
#define pruneInfo_t mcsparsePruneInfo_t
#define pruneInfo mcsparsePruneInfo

//##############################################################################
//# ENUMERATORS
//##############################################################################

// cusparseStatus_t -> mcsparseStatus_t
#define cusparseStatus_t mcsparseStatus_t
#define CUSPARSE_STATUS_SUCCESS MCSPARSE_STATUS_SUCCESS
#define CUSPARSE_STATUS_NOT_INITIALIZED MCSPARSE_STATUS_NOT_INITIALIZED
#define CUSPARSE_STATUS_ALLOC_FAILED MCSPARSE_STATUS_ALLOC_FAILED
#define CUSPARSE_STATUS_INVALID_VALUE MCSPARSE_STATUS_INVALID_VALUE
#define CUSPARSE_STATUS_ARCH_MISMATCH MCSPARSE_STATUS_ARCH_MISMATCH
#define CUSPARSE_STATUS_MAPPING_ERROR MCSPARSE_STATUS_MAPPING_ERROR
#define CUSPARSE_STATUS_EXECUTION_FAILED MCSPARSE_STATUS_EXECUTION_FAILED
#define CUSPARSE_STATUS_INTERNAL_ERROR MCSPARSE_STATUS_INTERNAL_ERROR
#define CUSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED MCSPARSE_STATUS_MATRIX_TYPE_NOT_SUPPORTED
#define CUSPARSE_STATUS_ZERO_PIVOT MCSPARSE_STATUS_ZERO_PIVOT
#define CUSPARSE_STATUS_NOT_SUPPORTED MCSPARSE_STATUS_NOT_SUPPORTED
#define CUSPARSE_STATUS_INSUFFICIENT_RESOURCES MCSPARSE_STATUS_INSUFFICIENT_RESOURCES

// cusparsePointerMode_t -> mcsparsePointerMode_t
#define cusparsePointerMode_t mcsparsePointerMode_t
#define CUSPARSE_POINTER_MODE_HOST MCSPARSE_POINTER_MODE_HOST
#define CUSPARSE_POINTER_MODE_DEVICE MCSPARSE_POINTER_MODE_DEVICE

// cusparseAction_t -> mcsparseAction_t
#define cusparseAction_t mcsparseAction_t
#define CUSPARSE_ACTION_SYMBOLIC MCSPARSE_ACTION_SYMBOLIC
#define CUSPARSE_ACTION_NUMERIC MCSPARSE_ACTION_NUMERIC

// cusparseMatrixType_t -> mcsparseMatrixType_t
#define cusparseMatrixType_t mcsparseMatrixType_t
#define CUSPARSE_MATRIX_TYPE_GENERAL MCSPARSE_MATRIX_TYPE_GENERAL
#define CUSPARSE_MATRIX_TYPE_SYMMETRIC MCSPARSE_MATRIX_TYPE_SYMMETRIC
#define CUSPARSE_MATRIX_TYPE_HERMITIAN MCSPARSE_MATRIX_TYPE_HERMITIAN
#define CUSPARSE_MATRIX_TYPE_TRIANGULAR MCSPARSE_MATRIX_TYPE_TRIANGULAR

// cusparseFillMode_t -> mcsparseFillMode_t
#define cusparseFillMode_t mcsparseFillMode_t
#define CUSPARSE_FILL_MODE_LOWER MCSPARSE_FILL_MODE_LOWER
#define CUSPARSE_FILL_MODE_UPPER MCSPARSE_FILL_MODE_UPPER

// cusparseDiagType_t -> mcsparseDiagType_t
#define cusparseDiagType_t mcsparseDiagType_t
#define CUSPARSE_DIAG_TYPE_NON_UNIT MCSPARSE_DIAG_TYPE_NON_UNIT
#define CUSPARSE_DIAG_TYPE_UNIT MCSPARSE_DIAG_TYPE_UNIT

// cusparseIndexBase_t -> mcsparseIndexBase_t
#define cusparseIndexBase_t mcsparseIndexBase_t
#define CUSPARSE_INDEX_BASE_ZERO MCSPARSE_INDEX_BASE_ZERO
#define CUSPARSE_INDEX_BASE_ONE MCSPARSE_INDEX_BASE_ONE

// cusparseOperation_t -> mcsparseOperation_t
#define cusparseOperation_t mcsparseOperation_t
#define CUSPARSE_OPERATION_NON_TRANSPOSE MCSPARSE_OPERATION_NON_TRANSPOSE
#define CUSPARSE_OPERATION_TRANSPOSE MCSPARSE_OPERATION_TRANSPOSE
#define CUSPARSE_OPERATION_CONJUGATE_TRANSPOSE MCSPARSE_OPERATION_CONJUGATE_TRANSPOSE

// cusparseDirection_t -> mcsparseDirection_t
#define cusparseDirection_t mcsparseDirection_t
#define CUSPARSE_DIRECTION_ROW MCSPARSE_DIRECTION_ROW
#define CUSPARSE_DIRECTION_COLUMN MCSPARSE_DIRECTION_COLUMN

// cusparseSolvePolicy_t -> mcsparseSolvePolicy_t
#define cusparseSolvePolicy_t mcsparseSolvePolicy_t
#define CUSPARSE_SOLVE_POLICY_NO_LEVEL MCSPARSE_SOLVE_POLICY_NO_LEVEL
#define CUSPARSE_SOLVE_POLICY_USE_LEVEL MCSPARSE_SOLVE_POLICY_USE_LEVEL

// cusparseColorAlg_t -> mcsparseColorAlg_t
#define cusparseColorAlg_t mcsparseColorAlg_t
#define CUSPARSE_COLOR_ALG0 MCSPARSE_COLOR_ALG0
#define CUSPARSE_COLOR_ALG1 MCSPARSE_COLOR_ALG1

// cusparseAlgMode_t -> mcsparseAlgMode_t
#define cusparseAlgMode_t mcsparseAlgMode_t
#define CUSPARSE_ALG_MERGE_PATH MCSPARSE_ALG_MERGE_PATH

#define cusparseCsr2CscAlg_t mcsparseCsr2CscAlg_t
#define CUSPARSE_CSR2CSC_ALG1 MCSPARSE_CSR2CSC_ALG1
#define CUSPARSE_CSR2CSC_ALG2 MCSPARSE_CSR2CSC_ALG2

//##############################################################################
//# INITIALIZATION AND MANAGEMENT ROUTINES
//##############################################################################
#define cusparseCreate mcsparseCreate
#define cusparseDestroy mcsparseDestroy
#define cusparseGetVersion mcsparseGetVersion
#define cusparseGetProperty mcsparseGetProperty
#define cusparseGetErrorName mcsparseGetErrorName
#define cusparseGetErrorString mcsparseGetErrorString
#define cusparseSetStream mcsparseSetStream
#define cusparseGetStream mcsparseGetStream
#define cusparseGetPointerMode mcsparseGetPointerMode
#define cusparseSetPointerMode mcsparseSetPointerMode

//##############################################################################
//# LOGGING APIs
//##############################################################################
// TODO

//##############################################################################
//# HELPER ROUTINES
//##############################################################################
#define cusparseCreateMatDescr mcsparseCreateMatDescr
#define cusparseDestroyMatDescr mcsparseDestroyMatDescr
#define cusparseCopyMatDescr mcsparseCopyMatDescr

#define cusparseSetMatType mcsparseSetMatType
#define cusparseGetMatType mcsparseGetMatType

#define cusparseSetMatFillMode mcsparseSetMatFillMode
#define cusparseGetMatFillMode mcsparseGetMatFillMode

#define cusparseSetMatDiagType mcsparseSetMatDiagType
#define cusparseGetMatDiagType mcsparseGetMatDiagType

#define cusparseSetMatIndexBase mcsparseSetMatIndexBase
#define cusparseGetMatIndexBase mcsparseGetMatIndexBase

#define cusparseCreateCsrsv2Info mcsparseCreateCsrsv2Info
#define cusparseDestroyCsrsv2Info mcsparseDestroyCsrsv2Info
#define cusparseCreateCsric02Info mcsparseCreateCsric02Info
#define cusparseDestroyCsric02Info mcsparseDestroyCsric02Info
#define cusparseCreateBsric02Info mcsparseCreateBsric02Info
#define cusparseDestroyBsric02Info mcsparseDestroyBsric02Info
#define cusparseCreateCsrilu02Info mcsparseCreateCsrilu02Info
#define cusparseDestroyCsrilu02Info mcsparseDestroyCsrilu02Info
#define cusparseCreateBsrilu02Info mcsparseCreateBsrilu02Info
#define cusparseDestroyBsrilu02Info mcsparseDestroyBsrilu02Info
#define cusparseCreateBsrsv2Info mcsparseCreateBsrsv2Info
#define cusparseDestroyBsrsv2Info mcsparseDestroyBsrsv2Info
#define cusparseCreateBsrsm2Info mcsparseCreateBsrsm2Info
#define cusparseDestroyBsrsm2Info mcsparseDestroyBsrsm2Info
#define cusparseCreateCsru2csrInfo mcsparseCreateCsru2csrInfo
#define cusparseDestroyCsru2csrInfo mcsparseDestroyCsru2csrInfo
#define cusparseCreateColorInfo mcsparseCreateColorInfo
#define cusparseDestroyColorInfo mcsparseDestroyColorInfo
#define cusparseSetColorAlgs mcsparseSetColorAlgs
#define cusparseGetColorAlgs mcsparseGetColorAlgs
#define cusparseCreatePruneInfo mcsparseCreatePruneInfo
#define cusparseDestroyPruneInfo mcsparseDestroyPruneInfo
#define cusparseCreateCsrsm2Info mcsparseCreateCsrsm2Info
#define cusparseDestroyCsrsm2Info mcsparseDestroyCsrsm2Info
#define cusparseCreateCsrgemm2Info mcsparseCreateCsrgemm2Info
#define cusparseDestroyCsrgemm2Info mcsparseDestroyCsrgemm2Info

//##############################################################################
//# SPARSE LEVEL 1 ROUTINES
//##############################################################################

#define cusparseSaxpyi mcsparseSaxpyi
#define cusparseDaxpyi mcsparseDaxpyi
#define cusparseCaxpyi mcsparseCaxpyi
#define cusparseZaxpyi mcsparseZaxpyi

#define cusparseSgthr mcsparseSgthr
#define cusparseDgthr mcsparseDgthr
#define cusparseCgthr mcsparseCgthr
#define cusparseZgthr mcsparseZgthr

#define cusparseSgthrz mcsparseSgthrz
#define cusparseDgthrz mcsparseDgthrz
#define cusparseCgthrz mcsparseCgthrz
#define cusparseZgthrz mcsparseZgthrz

#define cusparseSsctr mcsparseSsctr
#define cusparseDsctr mcsparseDsctr
#define cusparseCsctr mcsparseCsctr
#define cusparseZsctr mcsparseZsctr

#define cusparseSroti mcsparseSroti
#define cusparseDroti mcsparseDroti

//##############################################################################
//# SPARSE LEVEL 2 ROUTINES
//##############################################################################

#define cusparseDgemvi_bufferSize mcsparseDgemvi_bufferSize
#define cusparseSgemvi_bufferSize mcsparseSgemvi_bufferSize
#define cusparseCgemvi_bufferSize mcsparseCgemvi_bufferSize
#define cusparseZgemvi_bufferSize mcsparseZgemvi_bufferSize
#define cusparseSgemvi mcsparseSgemvi
#define cusparseDgemvi mcsparseDgemvi
#define cusparseCgemvi mcsparseCgemvi
#define cusparseZgemvi mcsparseZgemvi

#define cusparseCsrmvEx mcsparseCsrmvEx
#define cusparseCsrmvEx_bufferSize mcsparseCsrmvEx_bufferSize

#define cusparseScsrmv mcsparseScsrmv
#define cusparseDcsrmv mcsparseDcsrmv
#define cusparseCcsrmv mcsparseCcsrmv
#define cusparseZcsrmv mcsparseZcsrmv

#define cusparseSbsrmv mcsparseSbsrmv
#define cusparseDbsrmv mcsparseDbsrmv
#define cusparseCbsrmv mcsparseCbsrmv
#define cusparseZbsrmv mcsparseZbsrmv

#define cusparseSbsrxmv mcsparseSbsrxmv
#define cusparseDbsrxmv mcsparseDbsrxmv
#define cusparseCbsrxmv mcsparseCbsrxmv
#define cusparseZbsrxmv mcsparseZbsrxmv

// Xcsrsv2
#define cusparseXcsrsv2_zeroPivot mcsparseXcsrsv2_zeroPivot

#define cusparseScsrsv2_bufferSize mcsparseScsrsv2_bufferSize
#define cusparseDcsrsv2_bufferSize mcsparseDcsrsv2_bufferSize
#define cusparseCcsrsv2_bufferSize mcsparseCcsrsv2_bufferSize
#define cusparseZcsrsv2_bufferSize mcsparseZcsrsv2_bufferSize

#define cusparseScsrsv2_bufferSizeExt mcsparseScsrsv2_bufferSizeExt
#define cusparseDcsrsv2_bufferSizeExt mcsparseDcsrsv2_bufferSizeExt
#define cusparseCcsrsv2_bufferSizeExt mcsparseCcsrsv2_bufferSizeExt
#define cusparseZcsrsv2_bufferSizeExt mcsparseZcsrsv2_bufferSizeExt

#define cusparseScsrsv2_analysis mcsparseScsrsv2_analysis
#define cusparseDcsrsv2_analysis mcsparseDcsrsv2_analysis
#define cusparseCcsrsv2_analysis mcsparseCcsrsv2_analysis
#define cusparseZcsrsv2_analysis mcsparseZcsrsv2_analysis

#define cusparseScsrsv2_solve mcsparseScsrsv2_solve
#define cusparseDcsrsv2_solve mcsparseDcsrsv2_solve
#define cusparseCcsrsv2_solve mcsparseCcsrsv2_solve
#define cusparseZcsrsv2_solve mcsparseZcsrsv2_solve

// Xbsrsv2
#define cusparseXbsrsv2_zeroPivot mcsparseXbsrsv2_zeroPivot
#define cusparseSbsrsv2_bufferSize mcsparseSbsrsv2_bufferSize
#define cusparseDbsrsv2_bufferSize mcsparseDbsrsv2_bufferSize
#define cusparseCbsrsv2_bufferSize mcsparseCbsrsv2_bufferSize
#define cusparseZbsrsv2_bufferSize mcsparseZbsrsv2_bufferSize

#define cusparseSbsrsv2_bufferSizeExt mcsparseSbsrsv2_bufferSizeExt
#define cusparseDbsrsv2_bufferSizeExt mcsparseDbsrsv2_bufferSizeExt
#define cusparseCbsrsv2_bufferSizeExt mcsparseCbsrsv2_bufferSizeExt
#define cusparseZbsrsv2_bufferSizeExt mcsparseZbsrsv2_bufferSizeExt

#define cusparseSbsrsv2_analysis mcsparseSbsrsv2_analysis
#define cusparseDbsrsv2_analysis mcsparseDbsrsv2_analysis
#define cusparseCbsrsv2_analysis mcsparseCbsrsv2_analysis
#define cusparseZbsrsv2_analysis mcsparseZbsrsv2_analysis

#define cusparseSbsrsv2_solve mcsparseSbsrsv2_solve
#define cusparseDbsrsv2_solve mcsparseDbsrsv2_solve
#define cusparseCbsrsv2_solve mcsparseCbsrsv2_solve
#define cusparseZbsrsv2_solve mcsparseZbsrsv2_solve

//##############################################################################
//# SPARSE LEVEL 3 ROUTINES
//##############################################################################

#define cusparseSbsrmm mcsparseSbsrmm
#define cusparseDbsrmm mcsparseDbsrmm
#define cusparseCbsrmm mcsparseCbsrmm
#define cusparseZbsrmm mcsparseZbsrmm

#define cusparseSgemmi mcsparseSgemmi
#define cusparseDgemmi mcsparseDgemmi
#define cusparseCgemmi mcsparseCgemmi
#define cusparseZgemmi mcsparseZgemmi

#define cusparseXcsrsm2_zeroPivot mcsparseXcsrsm2_zeroPivot
#define cusparseScsrsm2_bufferSizeExt mcsparseScsrsm2_bufferSizeExt
#define cusparseDcsrsm2_bufferSizeExt mcsparseDcsrsm2_bufferSizeExt
#define cusparseCcsrsm2_bufferSizeExt mcsparseCcsrsm2_bufferSizeExt
#define cusparseZcsrsm2_bufferSizeExt mcsparseZcsrsm2_bufferSizeExt
#define cusparseScsrsm2_analysis mcsparseScsrsm2_analysis
#define cusparseDcsrsm2_analysis mcsparseDcsrsm2_analysis
#define cusparseCcsrsm2_analysis mcsparseCcsrsm2_analysis
#define cusparseZcsrsm2_analysis mcsparseZcsrsm2_analysis
#define cusparseScsrsm2_solve mcsparseScsrsm2_solve
#define cusparseDcsrsm2_solve mcsparseDcsrsm2_solve
#define cusparseCcsrsm2_solve mcsparseCcsrsm2_solve
#define cusparseZcsrsm2_solve mcsparseZcsrsm2_solve

#define cusparseXbsrsm2_zeroPivot mcsparseXbsrsm2_zeroPivot
#define cusparseSbsrsm2_bufferSize mcsparseSbsrsm2_bufferSize
#define cusparseDbsrsm2_bufferSize mcsparseDbsrsm2_bufferSize
#define cusparseCbsrsm2_bufferSize mcsparseCbsrsm2_bufferSize
#define cusparseZbsrsm2_bufferSize mcsparseZbsrsm2_bufferSize
#define cusparseSbsrsm2_bufferSizeExt mcsparseSbsrsm2_bufferSizeExt
#define cusparseDbsrsm2_bufferSizeExt mcsparseDbsrsm2_bufferSizeExt
#define cusparseCbsrsm2_bufferSizeExt mcsparseCbsrsm2_bufferSizeExt
#define cusparseZbsrsm2_bufferSizeExt mcsparseZbsrsm2_bufferSizeExt
#define cusparseSbsrsm2_analysis mcsparseSbsrsm2_analysis
#define cusparseDbsrsm2_analysis mcsparseDbsrsm2_analysis
#define cusparseCbsrsm2_analysis mcsparseCbsrsm2_analysis
#define cusparseZbsrsm2_analysis mcsparseZbsrsm2_analysis
#define cusparseSbsrsm2_solve mcsparseSbsrsm2_solve
#define cusparseDbsrsm2_solve mcsparseDbsrsm2_solve
#define cusparseCbsrsm2_solve mcsparseCbsrsm2_solve
#define cusparseZbsrsm2_solve mcsparseZbsrsm2_solve

//##############################################################################
//# PRECONDITIONERS
//##############################################################################

#define cusparseScsrilu02_numericBoost mcsparseScsrilu02_numericBoost
#define cusparseDcsrilu02_numericBoost mcsparseDcsrilu02_numericBoost
#define cusparseCcsrilu02_numericBoost mcsparseCcsrilu02_numericBoost
#define cusparseZcsrilu02_numericBoost mcsparseZcsrilu02_numericBoost
#define cusparseXcsrilu02_zeroPivot mcsparseXcsrilu02_zeroPivot

#define cusparseScsrilu02_bufferSize mcsparseScsrilu02_bufferSize
#define cusparseDcsrilu02_bufferSize mcsparseDcsrilu02_bufferSize
#define cusparseCcsrilu02_bufferSize mcsparseCcsrilu02_bufferSize
#define cusparseZcsrilu02_bufferSize mcsparseZcsrilu02_bufferSize

#define cusparseScsrilu02_bufferSizeExt mcsparseScsrilu02_bufferSizeExt
#define cusparseDcsrilu02_bufferSizeExt mcsparseDcsrilu02_bufferSizeExt
#define cusparseCcsrilu02_bufferSizeExt mcsparseCcsrilu02_bufferSizeExt
#define cusparseZcsrilu02_bufferSizeExt mcsparseZcsrilu02_bufferSizeExt

#define cusparseScsrilu02_analysis mcsparseScsrilu02_analysis
#define cusparseDcsrilu02_analysis mcsparseDcsrilu02_analysis
#define cusparseCcsrilu02_analysis mcsparseCcsrilu02_analysis
#define cusparseZcsrilu02_analysis mcsparseZcsrilu02_analysis

#define cusparseScsrilu02 mcsparseScsrilu02
#define cusparseDcsrilu02 mcsparseDcsrilu02
#define cusparseCcsrilu02 mcsparseCcsrilu02
#define cusparseZcsrilu02 mcsparseZcsrilu02

#define cusparseSbsrilu02_numericBoost mcsparseSbsrilu02_numericBoost
#define cusparseDbsrilu02_numericBoost mcsparseDbsrilu02_numericBoost
#define cusparseCbsrilu02_numericBoost mcsparseCbsrilu02_numericBoost
#define cusparseZbsrilu02_numericBoost mcsparseZbsrilu02_numericBoost
#define cusparseXbsrilu02_zeroPivot mcsparseXbsrilu02_zeroPivot
#define cusparseSbsrilu02_bufferSize mcsparseSbsrilu02_bufferSize
#define cusparseDbsrilu02_bufferSize mcsparseDbsrilu02_bufferSize
#define cusparseCbsrilu02_bufferSize mcsparseCbsrilu02_bufferSize
#define cusparseZbsrilu02_bufferSize mcsparseZbsrilu02_bufferSize
#define cusparseSbsrilu02_bufferSizeExt mcsparseSbsrilu02_bufferSizeExt
#define cusparseDbsrilu02_bufferSizeExt mcsparseDbsrilu02_bufferSizeExt
#define cusparseCbsrilu02_bufferSizeExt mcsparseCbsrilu02_bufferSizeExt
#define cusparseZbsrilu02_bufferSizeExt mcsparseZbsrilu02_bufferSizeExt
#define cusparseSbsrilu02_analysis mcsparseSbsrilu02_analysis
#define cusparseDbsrilu02_analysis mcsparseDbsrilu02_analysis
#define cusparseCbsrilu02_analysis mcsparseCbsrilu02_analysis
#define cusparseZbsrilu02_analysis mcsparseZbsrilu02_analysis
#define cusparseSbsrilu02 mcsparseSbsrilu02
#define cusparseDbsrilu02 mcsparseDbsrilu02
#define cusparseCbsrilu02 mcsparseCbsrilu02
#define cusparseZbsrilu02 mcsparseZbsrilu02

#define cusparseXcsric02_zeroPivot mcsparseXcsric02_zeroPivot
#define cusparseScsric02_bufferSize mcsparseScsric02_bufferSize
#define cusparseDcsric02_bufferSize mcsparseDcsric02_bufferSize
#define cusparseCcsric02_bufferSize mcsparseCcsric02_bufferSize
#define cusparseZcsric02_bufferSize mcsparseZcsric02_bufferSize

#define cusparseScsric02_bufferSizeExt mcsparseScsric02_bufferSizeExt
#define cusparseDcsric02_bufferSizeExt mcsparseDcsric02_bufferSizeExt
#define cusparseCcsric02_bufferSizeExt mcsparseCcsric02_bufferSizeExt
#define cusparseZcsric02_bufferSizeExt mcsparseZcsric02_bufferSizeExt

#define cusparseScsric02_analysis mcsparseScsric02_analysis
#define cusparseDcsric02_analysis mcsparseDcsric02_analysis
#define cusparseCcsric02_analysis mcsparseCcsric02_analysis
#define cusparseZcsric02_analysis mcsparseZcsric02_analysis

#define cusparseScsric02 mcsparseScsric02
#define cusparseDcsric02 mcsparseDcsric02
#define cusparseCcsric02 mcsparseCcsric02
#define cusparseZcsric02 mcsparseZcsric02

#define cusparseXbsric02_zeroPivot mcsparseXbsric02_zeroPivot
#define cusparseSbsric02_bufferSize mcsparseSbsric02_bufferSize
#define cusparseDbsric02_bufferSize mcsparseDbsric02_bufferSize
#define cusparseCbsric02_bufferSize mcsparseCbsric02_bufferSize
#define cusparseZbsric02_bufferSize mcsparseZbsric02_bufferSize
#define cusparseSbsric02_bufferSizeExt mcsparseSbsric02_bufferSizeExt
#define cusparseDbsric02_bufferSizeExt mcsparseDbsric02_bufferSizeExt
#define cusparseCbsric02_bufferSizeExt mcsparseCbsric02_bufferSizeExt
#define cusparseZbsric02_bufferSizeExt mcsparseZbsric02_bufferSizeExt
#define cusparseSbsric02_analysis mcsparseSbsric02_analysis
#define cusparseDbsric02_analysis mcsparseDbsric02_analysis
#define cusparseCbsric02_analysis mcsparseCbsric02_analysis
#define cusparseZbsric02_analysis mcsparseZbsric02_analysis
#define cusparseSbsric02 mcsparseSbsric02
#define cusparseDbsric02 mcsparseDbsric02
#define cusparseCbsric02 mcsparseCbsric02
#define cusparseZbsric02 mcsparseZbsric02

#define cusparseSgtsv2_bufferSizeExt mcsparseSgtsv2_bufferSizeExt
#define cusparseDgtsv2_bufferSizeExt mcsparseDgtsv2_bufferSizeExt
#define cusparseCgtsv2_bufferSizeExt mcsparseCgtsv2_bufferSizeExt
#define cusparseZgtsv2_bufferSizeExt mcsparseZgtsv2_bufferSizeExt
#define cusparseSgtsv2 mcsparseSgtsv2
#define cusparseDgtsv2 mcsparseDgtsv2
#define cusparseCgtsv2 mcsparseCgtsv2
#define cusparseZgtsv2 mcsparseZgtsv2
#define cusparseSgtsv mcsparseSgtsv
#define cusparseDgtsv mcsparseDgtsv
#define cusparseCgtsv mcsparseCgtsv
#define cusparseZgtsv mcsparseZgtsv
#define cusparseSgtsv2_nopivot_bufferSizeExt mcsparseSgtsv2_nopivot_bufferSizeExt
#define cusparseDgtsv2_nopivot_bufferSizeExt mcsparseDgtsv2_nopivot_bufferSizeExt
#define cusparseCgtsv2_nopivot_bufferSizeExt mcsparseCgtsv2_nopivot_bufferSizeExt
#define cusparseZgtsv2_nopivot_bufferSizeExt mcsparseZgtsv2_nopivot_bufferSizeExt
#define cusparseSgtsv2_nopivot mcsparseSgtsv2_nopivot
#define cusparseDgtsv2_nopivot mcsparseDgtsv2_nopivot
#define cusparseCgtsv2_nopivot mcsparseCgtsv2_nopivot
#define cusparseZgtsv2_nopivot mcsparseZgtsv2_nopivot
#define cusparseSgtsv_nopivot mcsparseSgtsv_nopivot
#define cusparseDgtsv_nopivot mcsparseDgtsv_nopivot
#define cusparseCgtsv_nopivot mcsparseCgtsv_nopivot
#define cusparseZgtsv_nopivot mcsparseZgtsv_nopivot

#define cusparseSgtsv2StridedBatch_bufferSizeExt mcsparseSgtsv2StridedBatch_bufferSizeExt
#define cusparseDgtsv2StridedBatch_bufferSizeExt mcsparseDgtsv2StridedBatch_bufferSizeExt
#define cusparseCgtsv2StridedBatch_bufferSizeExt mcsparseCgtsv2StridedBatch_bufferSizeExt
#define cusparseZgtsv2StridedBatch_bufferSizeExt mcsparseZgtsv2StridedBatch_bufferSizeExt
#define cusparseSgtsv2StridedBatch mcsparseSgtsv2StridedBatch
#define cusparseDgtsv2StridedBatch mcsparseDgtsv2StridedBatch
#define cusparseCgtsv2StridedBatch mcsparseCgtsv2StridedBatch
#define cusparseZgtsv2StridedBatch mcsparseZgtsv2StridedBatch
#define cusparseSgtsvStridedBatch mcsparseSgtsvStridedBatch
#define cusparseDgtsvStridedBatch mcsparseDgtsvStridedBatch
#define cusparseCgtsvStridedBatch mcsparseCgtsvStridedBatch
#define cusparseZgtsvStridedBatch mcsparseZgtsvStridedBatch

#define cusparseSgtsvInterleavedBatch_bufferSizeExt mcsparseSgtsvInterleavedBatch_bufferSizeExt
#define cusparseDgtsvInterleavedBatch_bufferSizeExt mcsparseDgtsvInterleavedBatch_bufferSizeExt
#define cusparseCgtsvInterleavedBatch_bufferSizeExt mcsparseCgtsvInterleavedBatch_bufferSizeExt
#define cusparseZgtsvInterleavedBatch_bufferSizeExt mcsparseZgtsvInterleavedBatch_bufferSizeExt
#define cusparseSgtsvInterleavedBatch mcsparseSgtsvInterleavedBatch
#define cusparseDgtsvInterleavedBatch mcsparseDgtsvInterleavedBatch
#define cusparseCgtsvInterleavedBatch mcsparseCgtsvInterleavedBatch
#define cusparseZgtsvInterleavedBatch mcsparseZgtsvInterleavedBatch

#define cusparseSgpsvInterleavedBatch_bufferSizeExt mcsparseSgpsvInterleavedBatch_bufferSizeExt
#define cusparseDgpsvInterleavedBatch_bufferSizeExt mcsparseDgpsvInterleavedBatch_bufferSizeExt
#define cusparseCgpsvInterleavedBatch_bufferSizeExt mcsparseCgpsvInterleavedBatch_bufferSizeExt
#define cusparseZgpsvInterleavedBatch_bufferSizeExt mcsparseZgpsvInterleavedBatch_bufferSizeExt
#define cusparseSgpsvInterleavedBatch mcsparseSgpsvInterleavedBatch
#define cusparseDgpsvInterleavedBatch mcsparseDgpsvInterleavedBatch
#define cusparseCgpsvInterleavedBatch mcsparseCgpsvInterleavedBatch
#define cusparseZgpsvInterleavedBatch mcsparseZgpsvInterleavedBatch

//##############################################################################
//# EXTRA ROUTINES
//##############################################################################

#define cusparseScsrgemm2_bufferSizeExt mcsparseScsrgemm2_bufferSizeExt
#define cusparseDcsrgemm2_bufferSizeExt mcsparseDcsrgemm2_bufferSizeExt
#define cusparseCcsrgemm2_bufferSizeExt mcsparseCcsrgemm2_bufferSizeExt
#define cusparseZcsrgemm2_bufferSizeExt mcsparseZcsrgemm2_bufferSizeExt

#define cusparseXcsrgemm2Nnz mcsparseXcsrgemm2Nnz

#define cusparseScsrgemm2 mcsparseScsrgemm2
#define cusparseDcsrgemm2 mcsparseDcsrgemm2
#define cusparseCcsrgemm2 mcsparseCcsrgemm2
#define cusparseZcsrgemm2 mcsparseZcsrgemm2

#define cusparseScsrgeam2_bufferSizeExt mcsparseScsrgeam2_bufferSizeExt
#define cusparseDcsrgeam2_bufferSizeExt mcsparseDcsrgeam2_bufferSizeExt
#define cusparseCcsrgeam2_bufferSizeExt mcsparseCcsrgeam2_bufferSizeExt
#define cusparseZcsrgeam2_bufferSizeExt mcsparseZcsrgeam2_bufferSizeExt

#define cusparseXcsrgeam2Nnz mcsparseXcsrgeam2Nnz

#define cusparseScsrgeam2 mcsparseScsrgeam2
#define cusparseDcsrgeam2 mcsparseDcsrgeam2
#define cusparseCcsrgeam2 mcsparseCcsrgeam2
#define cusparseZcsrgeam2 mcsparseZcsrgeam2

#define cusparseScsrmm2 mcsparseScsrmm2
#define cusparseDcsrmm2 mcsparseDcsrmm2
#define cusparseCcsrmm2 mcsparseCcsrmm2
#define cusparseZcsrmm2 mcsparseZcsrmm2

#define cusparseScsrmm mcsparseScsrmm
#define cusparseDcsrmm mcsparseDcsrmm
#define cusparseCcsrmm mcsparseCcsrmm
#define cusparseZcsrmm mcsparseZcsrmm

//##############################################################################
//# SPARSE MATRIX REORDERING
//##############################################################################

#define cusparseScsrcolor mcsparseScsrcolor
#define cusparseDcsrcolor mcsparseDcsrcolor
#define cusparseCcsrcolor mcsparseCcsrcolor
#define cusparseZcsrcolor mcsparseZcsrcolor

//##############################################################################
//# SPARSE FORMAT CONVERSION
//##############################################################################

#define cusparseSnnz mcsparseSnnz
#define cusparseDnnz mcsparseDnnz
#define cusparseCnnz mcsparseCnnz
#define cusparseZnnz mcsparseZnnz

#define cusparseSnnz_compress mcsparseSnnz_compress
#define cusparseDnnz_compress mcsparseDnnz_compress
#define cusparseCnnz_compress mcsparseCnnz_compress
#define cusparseZnnz_compress mcsparseZnnz_compress

#define cusparseScsr2csr_compress mcsparseScsr2csr_compress
#define cusparseDcsr2csr_compress mcsparseDcsr2csr_compress
#define cusparseCcsr2csr_compress mcsparseCcsr2csr_compress
#define cusparseZcsr2csr_compress mcsparseZcsr2csr_compress

#define cusparseSdense2csr mcsparseSdense2csr
#define cusparseDdense2csr mcsparseDdense2csr
#define cusparseCdense2csr mcsparseCdense2csr
#define cusparseZdense2csr mcsparseZdense2csr

#define cusparseScsr2dense mcsparseScsr2dense
#define cusparseDcsr2dense mcsparseDcsr2dense
#define cusparseCcsr2dense mcsparseCcsr2dense
#define cusparseZcsr2dense mcsparseZcsr2dense

#define cusparseSdense2csc mcsparseSdense2csc
#define cusparseDdense2csc mcsparseDdense2csc
#define cusparseCdense2csc mcsparseCdense2csc
#define cusparseZdense2csc mcsparseZdense2csc

#define cusparseScsc2dense mcsparseScsc2dense
#define cusparseDcsc2dense mcsparseDcsc2dense
#define cusparseCcsc2dense mcsparseCcsc2dense
#define cusparseZcsc2dense mcsparseZcsc2dense

#define cusparseXcoo2csr mcsparseXcoo2csr
#define cusparseXcsr2coo mcsparseXcsr2coo

#define cusparseCsr2cscEx2_bufferSize mcsparseCsr2cscEx2_bufferSize
#define cusparseCsr2cscEx2 mcsparseCsr2cscEx2

#define cusparseScsr2csru mcsparseScsr2csru
#define cusparseDcsr2csru mcsparseDcsr2csru
#define cusparseCcsr2csru mcsparseCcsr2csru
#define cusparseZcsr2csru mcsparseZcsr2csru

#define cusparseXcsr2bsrNnz mcsparseXcsr2bsrNnz
#define cusparseScsr2bsr mcsparseScsr2bsr
#define cusparseDcsr2bsr mcsparseDcsr2bsr
#define cusparseCcsr2bsr mcsparseCcsr2bsr
#define cusparseZcsr2bsr mcsparseZcsr2bsr
#define cusparseSbsr2csr mcsparseSbsr2csr
#define cusparseDbsr2csr mcsparseDbsr2csr
#define cusparseCbsr2csr mcsparseCbsr2csr
#define cusparseZbsr2csr mcsparseZbsr2csr

#define cusparseSgebsr2gebsc_bufferSize mcsparseSgebsr2gebsc_bufferSize
#define cusparseDgebsr2gebsc_bufferSize mcsparseDgebsr2gebsc_bufferSize
#define cusparseCgebsr2gebsc_bufferSize mcsparseCgebsr2gebsc_bufferSize
#define cusparseZgebsr2gebsc_bufferSize mcsparseZgebsr2gebsc_bufferSize
#define cusparseSgebsr2gebsc_bufferSizeExt mcsparseSgebsr2gebsc_bufferSizeExt
#define cusparseDgebsr2gebsc_bufferSizeExt mcsparseDgebsr2gebsc_bufferSizeExt
#define cusparseCgebsr2gebsc_bufferSizeExt mcsparseCgebsr2gebsc_bufferSizeExt
#define cusparseZgebsr2gebsc_bufferSizeExt mcsparseZgebsr2gebsc_bufferSizeExt
#define cusparseSgebsr2gebsc mcsparseSgebsr2gebsc
#define cusparseDgebsr2gebsc mcsparseDgebsr2gebsc
#define cusparseCgebsr2gebsc mcsparseCgebsr2gebsc
#define cusparseZgebsr2gebsc mcsparseZgebsr2gebsc

#define cusparseXgebsr2csr mcsparseXgebsr2csr
#define cusparseSgebsr2csr mcsparseSgebsr2csr
#define cusparseDgebsr2csr mcsparseDgebsr2csr
#define cusparseCgebsr2csr mcsparseCgebsr2csr
#define cusparseZgebsr2csr mcsparseZgebsr2csr

#define cusparseScsr2gebsr_bufferSize mcsparseScsr2gebsr_bufferSize
#define cusparseDcsr2gebsr_bufferSize mcsparseDcsr2gebsr_bufferSize
#define cusparseCcsr2gebsr_bufferSize mcsparseCcsr2gebsr_bufferSize
#define cusparseZcsr2gebsr_bufferSize mcsparseZcsr2gebsr_bufferSize
#define cusparseScsr2gebsr_bufferSizeExt mcsparseScsr2gebsr_bufferSizeExt
#define cusparseDcsr2gebsr_bufferSizeExt mcsparseDcsr2gebsr_bufferSizeExt
#define cusparseCcsr2gebsr_bufferSizeExt mcsparseCcsr2gebsr_bufferSizeExt
#define cusparseZcsr2gebsr_bufferSizeExt mcsparseZcsr2gebsr_bufferSizeExt
#define cusparseXcsr2gebsrNnz mcsparseXcsr2gebsrNnz
#define cusparseScsr2gebsr mcsparseScsr2gebsr
#define cusparseDcsr2gebsr mcsparseDcsr2gebsr
#define cusparseCcsr2gebsr mcsparseCcsr2gebsr
#define cusparseZcsr2gebsr mcsparseZcsr2gebsr

#define cusparseSgebsr2gebsr_bufferSize mcsparseSgebsr2gebsr_bufferSize
#define cusparseDgebsr2gebsr_bufferSize mcsparseDgebsr2gebsr_bufferSize
#define cusparseCgebsr2gebsr_bufferSize mcsparseCgebsr2gebsr_bufferSize
#define cusparseZgebsr2gebsr_bufferSize mcsparseZgebsr2gebsr_bufferSize
#define cusparseSgebsr2gebsr_bufferSizeExt mcsparseSgebsr2gebsr_bufferSizeExt
#define cusparseDgebsr2gebsr_bufferSizeExt mcsparseDgebsr2gebsr_bufferSizeExt
#define cusparseCgebsr2gebsr_bufferSizeExt mcsparseCgebsr2gebsr_bufferSizeExt
#define cusparseZgebsr2gebsr_bufferSizeExt mcsparseZgebsr2gebsr_bufferSizeExt
#define cusparseXgebsr2gebsrNnz mcsparseXgebsr2gebsrNnz
#define cusparseSgebsr2gebsr mcsparseSgebsr2gebsr
#define cusparseDgebsr2gebsr mcsparseDgebsr2gebsr
#define cusparseCgebsr2gebsr mcsparseCgebsr2gebsr
#define cusparseZgebsr2gebsr mcsparseZgebsr2gebsr

//##############################################################################
//# SPARSE MATRIX SORTING
//##############################################################################

#define cusparseCreateIdentityPermutation mcsparseCreateIdentityPermutation

#define cusparseXcoosort_bufferSizeExt mcsparseXcoosort_bufferSizeExt
#define cusparseXcoosortByRow mcsparseXcoosortByRow
#define cusparseXcoosortByColumn mcsparseXcoosortByColumn

#define cusparseXcsrsort_bufferSizeExt mcsparseXcsrsort_bufferSizeExt
#define cusparseXcsrsort mcsparseXcsrsort

#define cusparseXcscsort_bufferSizeExt mcsparseXcscsort_bufferSizeExt
#define cusparseXcscsort mcsparseXcscsort

#define cusparseScsru2csr_bufferSizeExt mcsparseScsru2csr_bufferSizeExt
#define cusparseDcsru2csr_bufferSizeExt mcsparseDcsru2csr_bufferSizeExt
#define cusparseCcsru2csr_bufferSizeExt mcsparseCcsru2csr_bufferSizeExt
#define cusparseZcsru2csr_bufferSizeExt mcsparseZcsru2csr_bufferSizeExt
#define cusparseScsru2csr mcsparseScsru2csr
#define cusparseDcsru2csr mcsparseDcsru2csr
#define cusparseCcsru2csr mcsparseCcsru2csr
#define cusparseZcsru2csr mcsparseZcsru2csr

//##############################################################################
//# PRUNE MATRIX
//##############################################################################

#define cusparseSpruneDense2csr_bufferSizeExt mcsparseSpruneDense2csr_bufferSizeExt
#define cusparseDpruneDense2csr_bufferSizeExt mcsparseDpruneDense2csr_bufferSizeExt
#define cusparseSpruneDense2csrNnz mcsparseSpruneDense2csrNnz
#define cusparseDpruneDense2csrNnz mcsparseDpruneDense2csrNnz
#define cusparseSpruneDense2csr mcsparseSpruneDense2csr
#define cusparseDpruneDense2csr mcsparseDpruneDense2csr

#define cusparseSpruneCsr2csr_bufferSizeExt mcsparseSpruneCsr2csr_bufferSizeExt
#define cusparseDpruneCsr2csr_bufferSizeExt mcsparseDpruneCsr2csr_bufferSizeExt
#define cusparseSpruneCsr2csrNnz mcsparseSpruneCsr2csrNnz
#define cusparseDpruneCsr2csrNnz mcsparseDpruneCsr2csrNnz
#define cusparseSpruneCsr2csr mcsparseSpruneCsr2csr
#define cusparseDpruneCsr2csr mcsparseDpruneCsr2csr

#define cusparseSpruneDense2csrByPercentage_bufferSizeExt mcsparseSpruneDense2csrByPercentage_bufferSizeExt
#define cusparseDpruneDense2csrByPercentage_bufferSizeExt mcsparseDpruneDense2csrByPercentage_bufferSizeExt
#define cusparseSpruneDense2csrNnzByPercentage mcsparseSpruneDense2csrNnzByPercentage
#define cusparseDpruneDense2csrNnzByPercentage mcsparseDpruneDense2csrNnzByPercentage
#define cusparseSpruneDense2csrByPercentage mcsparseSpruneDense2csrByPercentage
#define cusparseDpruneDense2csrByPercentage mcsparseDpruneDense2csrByPercentage

#define cusparseSpruneCsr2csrByPercentage_bufferSizeExt mcsparseSpruneCsr2csrByPercentage_bufferSizeExt
#define cusparseDpruneCsr2csrByPercentage_bufferSizeExt mcsparseDpruneCsr2csrByPercentage_bufferSizeExt
#define cusparseSpruneCsr2csrNnzByPercentage mcsparseSpruneCsr2csrNnzByPercentage
#define cusparseDpruneCsr2csrNnzByPercentage mcsparseDpruneCsr2csrNnzByPercentage
#define cusparseSpruneCsr2csrByPercentage mcsparseSpruneCsr2csrByPercentage
#define cusparseDpruneCsr2csrByPercentage mcsparseDpruneCsr2csrByPercentage

// #############################################################################
// # GENERIC APIs - Enumerators and Opaque Data Structures
// #############################################################################

#define cusparseFormat_t mcsparseFormat_t
#define CUSPARSE_FORMAT_CSR MCSPARSE_FORMAT_CSR
#define CUSPARSE_FORMAT_CSC MCSPARSE_FORMAT_CSC
#define CUSPARSE_FORMAT_COO MCSPARSE_FORMAT_COO
#define CUSPARSE_FORMAT_COO_AOS MCSPARSE_FORMAT_COO_AOS
#define CUSPARSE_FORMAT_BLOCKED_ELL MCSPARSE_FORMAT_BLOCKED_ELL

#define cusparseOrder_t mcsparseOrder_t
#define CUSPARSE_ORDER_COL MCSPARSE_ORDER_COL
#define CUSPARSE_ORDER_ROW MCSPARSE_ORDER_ROW

#define cusparseIndexType_t mcsparseIndexType_t
#define CUSPARSE_INDEX_16U MCSPARSE_INDEX_16U
#define CUSPARSE_INDEX_32I MCSPARSE_INDEX_32I
#define CUSPARSE_INDEX_64I MCSPARSE_INDEX_64I

#define cusparseSpVecDescr mcsparseSpVecDescr
#define cusparseDnVecDescr mcsparseDnVecDescr
#define cusparseSpMatDescr mcsparseSpMatDescr
#define cusparseDnMatDescr mcsparseDnMatDescr
#define cusparseSpVecDescr_t mcsparseSpVecDescr_t
#define cusparseDnVecDescr_t mcsparseDnVecDescr_t
#define cusparseSpMatDescr_t mcsparseSpMatDescr_t
#define cusparseDnMatDescr_t mcsparseDnMatDescr_t

#define cusparseSpMatAttribute_t mcsparseSpMatAttribute_t
#define CUSPARSE_SPMAT_FILL_MODE MCSPARSE_SPMAT_FILL_MODE
#define CUSPARSE_SPMAT_DIAG_TYPE MCSPARSE_SPMAT_DIAG_TYPE

// #############################################################################
// # SPARSE VECTOR DESCRIPTOR
// #############################################################################

#define cusparseCreateSpVec mcsparseCreateSpVec
#define cusparseDestroySpVec mcsparseDestroySpVec
#define cusparseSpVecGet mcsparseSpVecGet
#define cusparseSpVecGetIndexBase mcsparseSpVecGetIndexBase
#define cusparseSpVecGetValues mcsparseSpVecGetValues
#define cusparseSpVecSetValues mcsparseSpVecSetValues

// #############################################################################
// # DENSE VECTOR DESCRIPTOR
// #############################################################################

#define cusparseCreateDnVec mcsparseCreateDnVec
#define cusparseDestroyDnVec mcsparseDestroyDnVec
#define cusparseDnVecGet mcsparseDnVecGet
#define cusparseDnVecGetValues mcsparseDnVecGetValues
#define cusparseDnVecSetValues mcsparseDnVecSetValues

// #############################################################################
// # SPARSE MATRIX DESCRIPTOR
// #############################################################################

#define cusparseDestroySpMat mcsparseDestroySpMat
#define cusparseSpMatGetFormat mcsparseSpMatGetFormat
#define cusparseSpMatGetIndexBase mcsparseSpMatGetIndexBase
#define cusparseSpMatGetValues mcsparseSpMatGetValues
#define cusparseSpMatSetValues mcsparseSpMatSetValues
#define cusparseSpMatGetSize mcsparseSpMatGetSize
#define cusparseSpMatSetStridedBatch mcsparseSpMatSetStridedBatch
#define cusparseSpMatGetStridedBatch mcsparseSpMatGetStridedBatch
#define cusparseCooSetStridedBatch mcsparseCooSetStridedBatch
#define cusparseCsrSetStridedBatch mcsparseCsrSetStridedBatch
#define cusparseSpMatGetAttribute mcsparseSpMatGetAttribute
#define cusparseSpMatSetAttribute mcsparseSpMatSetAttribute

//------------------------------------------------------------------------------
// ### CSR ###

#define cusparseCreateCsr mcsparseCreateCsr
#define cusparseCreateCsc mcsparseCreateCsc
#define cusparseCsrGet mcsparseCsrGet
#define cusparseCsrSetPointers mcsparseCsrSetPointers
#define cusparseCscSetPointers mcsparseCscSetPointers

//------------------------------------------------------------------------------
// ### COO ###

#define cusparseCreateCoo mcsparseCreateCoo
#define cusparseCreateCooAoS mcsparseCreateCooAoS
#define cusparseCooGet mcsparseCooGet
#define cusparseCooAoSGet mcsparseCooAoSGet
#define cusparseCooSetPointers mcsparseCooSetPointers

//------------------------------------------------------------------------------
// ### BLOCKED ELL ###

#define cusparseCreateBlockedEll mcsparseCreateBlockedEll
#define cusparseBlockedEllGet mcsparseBlockedEllGet

// #############################################################################
// # DENSE MATRIX DESCRIPTOR
// #############################################################################

#define cusparseCreateDnMat mcsparseCreateDnMat
#define cusparseDestroyDnMat mcsparseDestroyDnMat
#define cusparseDnMatGet mcsparseDnMatGet
#define cusparseDnMatGetValues mcsparseDnMatGetValues
#define cusparseDnMatSetValues mcsparseDnMatSetValues
#define cusparseDnMatSetStridedBatch mcsparseDnMatSetStridedBatch
#define cusparseDnMatGetStridedBatch mcsparseDnMatGetStridedBatch

// #############################################################################
// # VECTOR-VECTOR OPERATIONS
// #############################################################################

#define cusparseAxpby mcsparseAxpby
#define cusparseGather mcsparseGather
#define cusparseScatter mcsparseScatter
#define cusparseRot mcsparseRot
#define cusparseSpVV_bufferSize mcsparseSpVV_bufferSize
#define cusparseSpVV mcsparseSpVV

// #############################################################################
// # SPARSE TO DENSE
// #############################################################################

#define cusparseSparseToDenseAlg_t mcsparseSparseToDenseAlg_t
#define CUSPARSE_SPARSETODENSE_ALG_DEFAULT MCSPARSE_SPARSETODENSE_ALG_DEFAULT

#define cusparseSparseToDense_bufferSize mcsparseSparseToDense_bufferSize
#define cusparseSparseToDense mcsparseSparseToDense

// #############################################################################
// # DENSE TO SPARSE
// #############################################################################

#define cusparseDenseToSparseAlg_t mcsparseDenseToSparseAlg_t
#define CUSPARSE_DENSETOSPARSE_ALG_DEFAULT MCSPARSE_DENSETOSPARSE_ALG_DEFAULT

#define cusparseDenseToSparse_bufferSize mcsparseDenseToSparse_bufferSize
#define cusparseDenseToSparse_analysis mcsparseDenseToSparse_analysis
#define cusparseDenseToSparse_convert mcsparseDenseToSparse_convert

// half
#define cusparseHpruneDense2csr_bufferSizeExt mcsparseHpruneDense2csr_bufferSizeExt
#define cusparseHpruneDense2csrNnz mcsparseHpruneDense2csrNnz
#define cusparseHpruneDense2csr mcsparseHpruneDense2csr
#define cusparseHpruneCsr2csr_bufferSizeExt mcsparseHpruneCsr2csr_bufferSizeExt
#define cusparseHpruneCsr2csrNnz mcsparseHpruneCsr2csrNnz
#define cusparseHpruneCsr2csr mcsparseHpruneCsr2csr
#define cusparseHpruneDense2csrByPercentage_bufferSizeExt mcsparseHpruneDense2csrByPercentage_bufferSizeExt
#define cusparseHpruneDense2csrNnzByPercentage mcsparseHpruneDense2csrNnzByPercentage
#define cusparseHpruneDense2csrByPercentage mcsparseHpruneDense2csrByPercentage
#define cusparseHpruneCsr2csrByPercentage_bufferSizeExt mcsparseHpruneCsr2csrByPercentage_bufferSizeExt
#define cusparseHpruneCsr2csrNnzByPercentage mcsparseHpruneCsr2csrNnzByPercentage
#define cusparseHpruneCsr2csrByPercentage mcsparseHpruneCsr2csrByPercentage

// #############################################################################
// # SPARSE MATRIX-VECTOR MULTIPLICATION
// #############################################################################

#define cusparseSpMVAlg_t mcsparseSpMVAlg_t
#define CUSPARSE_MV_ALG_DEFAULT MCSPARSE_MV_ALG_DEFAULT
#define CUSPARSE_COOMV_ALG MCSPARSE_COOMV_ALG
#define CUSPARSE_CSRMV_ALG1 MCSPARSE_CSRMV_ALG1
#define CUSPARSE_CSRMV_ALG2 MCSPARSE_CSRMV_ALG2
#define CUSPARSE_SPMV_ALG_DEFAULT MCSPARSE_SPMV_ALG_DEFAULT
#define CUSPARSE_SPMV_CSR_ALG1 MCSPARSE_SPMV_CSR_ALG1
#define CUSPARSE_SPMV_CSR_ALG2 MCSPARSE_SPMV_CSR_ALG2
#define CUSPARSE_SPMV_COO_ALG1 MCSPARSE_SPMV_COO_ALG1
#define CUSPARSE_SPMV_COO_ALG2 MCSPARSE_SPMV_COO_ALG2

#define cusparseSpMV mcsparseSpMV
#define cusparseSpMV_bufferSize mcsparseSpMV_bufferSize

// #############################################################################
// # SPARSE TRIANGULAR VECTOR SOLVE
// #############################################################################

#define cusparseSpSVDescr_t mcsparseSpSVDescr_t
#define cusparseSpSVDescr mcsparseSpSVDescr
#define cusparseSpSVAlg_t mcsparseSpSVAlg_t
#define CUSPARSE_SPSV_ALG_DEFAULT MCSPARSE_SPSV_ALG_DEFAULT

#define cusparseSpSV_createDescr mcsparseSpSV_createDescr
#define cusparseSpSV_destroyDescr mcsparseSpSV_destroyDescr
#define cusparseSpSV_bufferSize mcsparseSpSV_bufferSize
#define cusparseSpSV_analysis mcsparseSpSV_analysis
#define cusparseSpSV_solve mcsparseSpSV_solve

// #############################################################################
// # SPARSE MATRIX-MATRIX MULTIPLICATION
// #############################################################################

#define cusparseSpMMAlg_t mcsparseSpMMAlg_t
#define CUSPARSE_MM_ALG_DEFAULT MCSPARSE_MM_ALG_DEFAULT
#define CUSPARSE_COOMM_ALG1 MCSPARSE_COOMM_ALG1
#define CUSPARSE_COOMM_ALG2 MCSPARSE_COOMM_ALG2
#define CUSPARSE_COOMM_ALG3 MCSPARSE_COOMM_ALG3
#define CUSPARSE_CSRMM_ALG1 MCSPARSE_CSRMM_ALG1
#define CUSPARSE_SPMM_ALG_DEFAULT MCSPARSE_SPMM_ALG_DEFAULT
#define CUSPARSE_SPMM_COO_ALG1 MCSPARSE_SPMM_COO_ALG1
#define CUSPARSE_SPMM_COO_ALG2 MCSPARSE_SPMM_COO_ALG2
#define CUSPARSE_SPMM_COO_ALG3 MCSPARSE_SPMM_COO_ALG3
#define CUSPARSE_SPMM_COO_ALG4 MCSPARSE_SPMM_COO_ALG4
#define CUSPARSE_SPMM_CSR_ALG1 MCSPARSE_SPMM_CSR_ALG1
#define CUSPARSE_SPMM_CSR_ALG2 MCSPARSE_SPMM_CSR_ALG2
#define CUSPARSE_SPMM_CSR_ALG3 MCSPARSE_SPMM_CSR_ALG3
#define CUSPARSE_SPMM_BLOCKED_ELL_ALG1 MCSPARSE_SPMM_BLOCKED_ELL_ALG1

#define cusparseSpMM_bufferSize mcsparseSpMM_bufferSize
#define cusparseSpMM_preprocess mcsparseSpMM_preprocess
#define cusparseSpMM mcsparseSpMM

// #############################################################################
// # SPARSE MATRIX - SPARSE MATRIX MULTIPLICATION (SpGEMM)
// #############################################################################

#define cusparseSpGEMMDescr_t mcsparseSpGEMMDescr_t
#define cusparseSpGEMMDescr mcsparseSpGEMMDescr
#define cusparseSpGEMMAlg_t mcsparseSpGEMMAlg_t
#define CUSPARSE_SPGEMM_DEFAULT MCSPARSE_SPGEMM_DEFAULT
#define CUSPARSE_SPGEMM_CSR_ALG_DETERMINITIC MCSPARSE_SPGEMM_CSR_ALG_DETERMINITIC
#define CUSPARSE_SPGEMM_CSR_ALG_NONDETERMINITIC MCSPARSE_SPGEMM_CSR_ALG_NONDETERMINITIC

#define cusparseSpGEMM_createDescr mcsparseSpGEMM_createDescr
#define cusparseSpGEMM_destroyDescr mcsparseSpGEMM_destroyDescr
#define cusparseSpGEMM_workEstimation mcsparseSpGEMM_workEstimation
#define cusparseSpGEMM_compute mcsparseSpGEMM_compute
#define cusparseSpGEMM_copy mcsparseSpGEMM_copy

// #############################################################################
// # SPARSE MATRIX - SPARSE MATRIX MULTIPLICATION (SpGEMM) STRUCTURE REUSE
// #############################################################################

#define cusparseSpGEMMreuse_workEstimation mcsparseSpGEMMreuse_workEstimation
#define cusparseSpGEMMreuse_nnz mcsparseSpGEMMreuse_nnz
#define cusparseSpGEMMreuse_copy mcsparseSpGEMMreuse_copy
#define cusparseSpGEMMreuse_compute mcsparseSpGEMMreuse_compute

// #############################################################################
// # SAMPLED DENSE-DENSE MATRIX MULTIPLICATION
// #############################################################################

#define cusparseConstrainedGeMM mcsparseConstrainedGeMM
#define cusparseConstrainedGeMM_bufferSize mcsparseConstrainedGeMM_bufferSize

#define cusparseSDDMMAlg_t mcsparseSDDMMAlg_t
#define CUSPARSE_SDDMM_ALG_DEFAULT MCSPARSE_SDDMM_ALG_DEFAULT

#define cusparseSDDMM_bufferSize mcsparseSDDMM_bufferSize
#define cusparseSDDMM_preprocess mcsparseSDDMM_preprocess
#define cusparseSDDMM mcsparseSDDMM

// #############################################################################
// # GENERIC APIs WITH CUSTOM OPERATORS (PREVIEW)
// #############################################################################

#define cusparseSpMMOpAlg_t mcsparseSpMMOpAlg_t
#define CUSPARSE_SPMM_OP_ALG_DEFAULT MCSPARSE_SPMM_OP_ALG_DEFAULT

#define cusparseSpMMOpPlan_t mcsparseSpMMOpPlan_t
#define cusparseSpMMOpPlan mcsparseSpMMOpPlan
#define cusparseSpMMOp_createPlan mcsparseSpMMOp_createPlan
#define cusparseSpMMOp mcsparseSpMMOp
#define cusparseSpMMOp_destroyPlan mcsparseSpMMOp_destroyPlan

// #############################################################################
// # SPARSE TRIANGULAR MATRIX SOLVE
// #############################################################################

#define cusparseSpSMAlg_t mcsparseSpSMAlg_t
#define cusparseSpSMDescr_t mcsparseSpSMDescr_t
#define cusparseSpSMDescr mcsparseSpSMDescr
#define CUSPARSE_SPSM_ALG_DEFAULT MCSPARSE_SPSM_ALG_DEFAULT
#define cusparseSpSM_createDescr mcsparseSpSM_createDescr
#define cusparseSpSM_destroyDescr mcsparseSpSM_destroyDescr
#define cusparseSpSM_bufferSize mcsparseSpSM_bufferSize
#define cusparseSpSM_analysis mcsparseSpSM_analysis
#define cusparseSpSM_solve mcsparseSpSM_solve

// #############################################################################
// # SPARSE LOGGER
// #############################################################################

#define cusparseLoggerCallback_t mcsparseLoggerCallback_t
#define cusparseLoggerSetCallback mcsparseLoggerSetCallback
#define cusparseLoggerSetFile mcsparseLoggerSetFile
#define cusparseLoggerOpenFile mcsparseLoggerOpenFile
#define cusparseLoggerSetLevel mcsparseLoggerSetLevel
#define cusparseLoggerSetMask mcsparseLoggerSetMask
#define cusparseLoggerForceDisable mcsparseLoggerForceDisable

#endif
