#pragma once

#define CUDNN_ADV_TRAIN_H_ MCDNN_MCDNN_ADV_TRAIN_H_

#define cudnnCreateCTCLossDescriptor                mcdnnCreateCTCLossDescriptor
#define cudnnSetCTCLossDescriptor                   mcdnnSetCTCLossDescriptor
#define cudnnSetCTCLossDescriptorEx                 mcdnnSetCTCLossDescriptorEx
#define cudnnSetCTCLossDescriptor_v8                mcdnnSetCTCLossDescriptor_v8
#define cudnnGetCTCLossDescriptor                   mcdnnGetCTCLossDescriptor
#define cudnnGetCTCLossDescriptorEx                 mcdnnGetCTCLossDescriptorEx
#define cudnnGetCTCLossDescriptor_v8                mcdnnGetCTCLossDescriptor_v8
#define cudnnDestroyCTCLossDescriptor               mcdnnDestroyCTCLossDescriptor
#define cudnnCTCLoss                                mcdnnCTCLoss
#define cudnnCTCLoss_v8                             mcdnnCTCLoss_v8
#define cudnnGetCTCLossWorkspaceSize                mcdnnGetCTCLossWorkspaceSize
#define cudnnGetCTCLossWorkspaceSize_v8             mcdnnGetCTCLossWorkspaceSize_v8
#define cudnnAdvTrainVersionCheck                   mcdnnAdvTrainVersionCheck
#define cudnnGetRNNBackwardDataAlgorithmMaxCount    mcdnnGetRNNBackwardDataAlgorithmMaxCount
#define cudnnGetRNNBackwardWeightsAlgorithmMaxCount mcdnnGetRNNBackwardWeightsAlgorithmMaxCount
#define cudnnFindRNNBackwardWeightsAlgorithmEx      mcdnnFindRNNBackwardWeightsAlgorithmEx
#define cudnnFindRNNBackwardDataAlgorithmEx         mcdnnFindRNNBackwardDataAlgorithmEx
#define cudnnRNNForwardTraining                     mcdnnRNNForwardTraining
#define cudnnMultiHeadAttnBackwardData              mcdnnMultiHeadAttnBackwardData
#define cudnnMultiHeadAttnBackwardWeights           mcdnnMultiHeadAttnBackwardWeights
#define cudnnRNNBackwardData_v8                     mcdnnRNNBackwardData_v8
#define cudnnRNNBackwardWeights_v8                  mcdnnRNNBackwardWeights_v8
#define cudnnRNNForwardTrainingEx                   mcdnnRNNForwardTrainingEx
#define cudnnRNNBackwardDataEx                      mcdnnRNNBackwardDataEx
#define cudnnRNNBackwardWeightsEx                   mcdnnRNNBackwardWeightsEx
#define cudnnGetRNNForwardTrainingAlgorithmMaxCount mcdnnGetRNNForwardTrainingAlgorithmMaxCount
#define cudnnFindRNNForwardTrainingAlgorithmEx      mcdnnFindRNNForwardTrainingAlgorithmEx
#define cudnnRNNBackwardData                        mcdnnRNNBackwardData
#define cudnnRNNBackwardWeights                     mcdnnRNNBackwardWeights
