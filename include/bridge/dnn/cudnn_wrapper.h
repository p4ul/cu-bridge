#pragma once

#include "cudnn_adv_infer_wrapper.h"
#include "cudnn_adv_train_wrapper.h"
#include "cudnn_backend_wrapper.h"
#include "cudnn_cnn_infer_wrapper.h"
#include "cudnn_cnn_train_wrapper.h"
#include "cudnn_ops_infer_wrapper.h"
#include "cudnn_ops_train_wrapper.h"
#include "mcdnn/mcdnn.h"

#define CUDNN_H_ MCDNN_MCDNN_H_

#define CUDNN_MAJOR      8
#define CUDNN_MINOR      4
#define CUDNN_PATCHLEVEL 1

#define CUDNN_VERSION (CUDNN_MAJOR * 1000 + CUDNN_MINOR * 100 + CUDNN_PATCHLEVEL)

#define cudaStream_t mcStream_t

#define cudnnGetConvolutionForwardAlgorithm        mcdnnGetConvolutionForwardAlgorithm
#define cudnnGetConvolutionBackwardDataAlgorithm   mcdnnGetConvolutionBackwardDataAlgorithm
#define cudnnGetConvolutionBackwardFilterAlgorithm mcdnnGetConvolutionBackwardFilterAlgorithm

#define cudnnConvolutionFwdPreference_t               mcdnnConvolutionFwdPreference_t
#define CUDNN_CONVOLUTION_FWD_NO_WORKSPACE            MCDNN_CONVOLUTION_FWD_NO_WORKSPACE
#define CUDNN_CONVOLUTION_FWD_PREFER_FASTEST          MCDNN_CONVOLUTION_FWD_PREFER_FASTEST
#define CUDNN_CONVOLUTION_FWD_SPECIFY_WORKSPACE_LIMIT MCDNN_CONVOLUTION_FWD_SPECIFY_WORKSPACE_LIMIT

#define cudnnConvolutionBwdDataPreference_t       mcdnnConvolutionBwdDataPreference_t
#define CUDNN_CONVOLUTION_BWD_DATA_NO_WORKSPACE   MCDNN_CONVOLUTION_BWD_DATA_NO_WORKSPACE
#define CUDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST MCDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST
#define CUDNN_CONVOLUTION_BWD_DATA_SPECIFY_WORKSPACE_LIMIT                                         \
    MCDNN_CONVOLUTION_BWD_DATA_SPECIFY_WORKSPACE_LIMIT

#define cudnnConvolutionBwdFilterPreference_t       mcdnnConvolutionBwdFilterPreference_t
#define CUDNN_CONVOLUTION_BWD_FILTER_NO_WORKSPACE   MCDNN_CONVOLUTION_BWD_FILTER_NO_WORKSPACE
#define CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST MCDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST
#define CUDNN_CONVOLUTION_BWD_FILTER_SPECIFY_WORKSPACE_LIMIT                                       \
    MCDNN_CONVOLUTION_BWD_FILTER_SPECIFY_WORKSPACE_LIMIT

