#pragma once

#define CUDNN_CNN_INFER_H_ MCDNN_MCDNN_CNN_INFER_H_

#define cudnnCnnInferVersionCheck                   mcdnnCnnInferVersionCheck
#define cudnnConvolutionBackwardData                mcdnnConvolutionBackwardData
#define cudnnConvolutionBiasActivationForward       mcdnnConvolutionBiasActivationForward
#define cudnnConvolutionForward                     mcdnnConvolutionForward
#define cudnnCreateConvolutionDescriptor            mcdnnCreateConvolutionDescriptor
#define cudnnDestroyConvolutionDescriptor           mcdnnDestroyConvolutionDescriptor
#define cudnnFindConvolutionBackwardDataAlgorithm   mcdnnFindConvolutionBackwardDataAlgorithm
#define cudnnFindConvolutionBackwardDataAlgorithmEx mcdnnFindConvolutionBackwardDataAlgorithmEx
#define cudnnFindConvolutionForwardAlgorithm        mcdnnFindConvolutionForwardAlgorithm
#define cudnnFindConvolutionForwardAlgorithmEx      mcdnnFindConvolutionForwardAlgorithmEx
#define cudnnGetConvolution2dDescriptor             mcdnnGetConvolution2dDescriptor
#define cudnnGetConvolution2dForwardOutputDim       mcdnnGetConvolution2dForwardOutputDim
#define cudnnGetConvolutionBackwardDataAlgorithmMaxCount                                           \
    mcdnnGetConvolutionBackwardDataAlgorithmMaxCount
#define cudnnGetConvolutionBackwardDataAlgorithm_v7  mcdnnGetConvolutionBackwardDataAlgorithm_v7
#define cudnnGetConvolutionBackwardDataWorkspaceSize mcdnnGetConvolutionBackwardDataWorkspaceSize
#define cudnnGetConvolutionForwardAlgorithmMaxCount  mcdnnGetConvolutionForwardAlgorithmMaxCount
#define cudnnGetConvolutionForwardAlgorithm_v7       mcdnnGetConvolutionForwardAlgorithm_v7
#define cudnnGetConvolutionForwardWorkspaceSize      mcdnnGetConvolutionForwardWorkspaceSize
#define cudnnGetConvolutionGroupCount                mcdnnGetConvolutionGroupCount
#define cudnnGetConvolutionMathType                  mcdnnGetConvolutionMathType
#define cudnnGetConvolutionNdDescriptor              mcdnnGetConvolutionNdDescriptor
#define cudnnGetConvolutionNdForwardOutputDim        mcdnnGetConvolutionNdForwardOutputDim
#define cudnnGetConvolutionReorderType               mcdnnGetConvolutionReorderType
#define cudnnGetFoldedConvBackwardDataDescriptors    mcdnnGetFoldedConvBackwardDataDescriptors
#define cudnnIm2Col                                  mcdnnIm2Col
#define cudnnReorderFilterAndBias                    mcdnnReorderFilterAndBias
#define cudnnSetConvolution2dDescriptor              mcdnnSetConvolution2dDescriptor
#define cudnnSetConvolutionGroupCount                mcdnnSetConvolutionGroupCount
#define cudnnSetConvolutionMathType                  mcdnnSetConvolutionMathType
#define cudnnSetConvolutionNdDescriptor              mcdnnSetConvolutionNdDescriptor
#define cudnnSetConvolutionReorderType               mcdnnSetConvolutionReorderType

#define cudnnConvolutionFwdAlgo_t                mcdnnConvolutionFwdAlgo_t
#define CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM MCDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM
#define CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM                                           \
    MCDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM
#define CUDNN_CONVOLUTION_FWD_ALGO_GEMM              MCDNN_CONVOLUTION_FWD_ALGO_GEMM
#define CUDNN_CONVOLUTION_FWD_ALGO_DIRECT            MCDNN_CONVOLUTION_FWD_ALGO_DIRECT
#define CUDNN_CONVOLUTION_FWD_ALGO_FFT               MCDNN_CONVOLUTION_FWD_ALGO_FFT
#define CUDNN_CONVOLUTION_FWD_ALGO_FFT_TILING        MCDNN_CONVOLUTION_FWD_ALGO_FFT_TILING
#define CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD          MCDNN_CONVOLUTION_FWD_ALGO_WINOGRAD
#define CUDNN_CONVOLUTION_FWD_ALGO_WINOGRAD_NONFUSED MCDNN_CONVOLUTION_FWD_ALGO_WINOGRAD_NONFUSED
#define CUDNN_CONVOLUTION_FWD_ALGO_COUNT             MCDNN_CONVOLUTION_FWD_ALGO_COUNT

#define cudnnConvolutionBwdFilterAlgo_t            mcdnnConvolutionBwdFilterAlgo_t
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_0        MCDNN_CONVOLUTION_BWD_FILTER_ALGO_0
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_1        MCDNN_CONVOLUTION_BWD_FILTER_ALGO_1
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_FFT      MCDNN_CONVOLUTION_BWD_FILTER_ALGO_FFT
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_3        MCDNN_CONVOLUTION_BWD_FILTER_ALGO_3
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_WINOGRAD MCDNN_CONVOLUTION_BWD_FILTER_ALGO_WINOGRAD
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_WINOGRAD_NONFUSED                                        \
    MCDNN_CONVOLUTION_BWD_FILTER_ALGO_WINOGRAD_NONFUSED
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_FFT_TILING MCDNN_CONVOLUTION_BWD_FILTER_ALGO_FFT_TILING
#define CUDNN_CONVOLUTION_BWD_FILTER_ALGO_COUNT      MCDNN_CONVOLUTION_BWD_FILTER_ALGO_COUNT

#define cudnnConvolutionBwdDataAlgo_t              mcdnnConvolutionBwdDataAlgo_t
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_0          MCDNN_CONVOLUTION_BWD_DATA_ALGO_0
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_1          MCDNN_CONVOLUTION_BWD_DATA_ALGO_1
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_FFT        MCDNN_CONVOLUTION_BWD_DATA_ALGO_FFT
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_FFT_TILING MCDNN_CONVOLUTION_BWD_DATA_ALGO_FFT_TILING
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_WINOGRAD   MCDNN_CONVOLUTION_BWD_DATA_ALGO_WINOGRAD
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_WINOGRAD_NONFUSED                                          \
    MCDNN_CONVOLUTION_BWD_DATA_ALGO_WINOGRAD_NONFUSED
#define CUDNN_CONVOLUTION_BWD_DATA_ALGO_COUNT MCDNN_CONVOLUTION_BWD_DATA_ALGO_COUNT

#define cudnnConvolutionMode_t  mcdnnConvolutionMode_t
#define CUDNN_CONVOLUTION       MCDNN_CONVOLUTION
#define CUDNN_CROSS_CORRELATION MCDNN_CROSS_CORRELATION

#define cudnnReorderType_t    mcdnnReorderType_t
#define CUDNN_DEFAULT_REORDER MCDNN_DEFAULT_REORDER
#define CUDNN_NO_REORDER      MCDNN_NO_REORDER

#define cudnnConvolutionFwdAlgoPerf_t         mcdnnConvolutionFwdAlgoPerf_t
#define cudnnConvolutionFwdAlgoPerfStruct     mcdnnConvolutionFwdAlgoPerf
#define cudnnConvolutionBwdDataAlgoPerf_t     mcdnnConvolutionBwdDataAlgoPerf_t
#define cudnnConvolutionBwdDataAlgoPerfStruct mcdnnConvolutionBwdDataAlgoPerf
#define cudnnConvolutionDescriptor_t          mcdnnConvolutionDescriptor_t
#define cudnnConvolutionStruct                mcdnnConvolutionDescriptor

// mcdnn quant
#define CUDNN_CONVOLUTION_QUANT_PER_TENSOR         MCDNN_CONVOLUTION_QUANT_PER_TENSOR
#define CUDNN_CONVOLUTION_QUANT_PER_CHANNEL        MCDNN_CONVOLUTION_QUANT_PER_CHANNEL
#define cudnnConvolutionQuantMode_t                mcdnnConvolutionQuantMode_t
#define cudnnConvolutionBiasActivationQuantForward mcdnnConvolutionBiasActivationQuantForward
