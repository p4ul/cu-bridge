#pragma once

#define CUDNN_OPS_TRAIN_H_ MCDNN_MCDNN_OPS_TRAIN_H_

#define cudnnActivationBackward                  mcdnnActivationBackward
#define cudnnBatchNormalizationBackward          mcdnnBatchNormalizationBackward
#define cudnnBatchNormalizationBackwardEx        mcdnnBatchNormalizationBackwardEx
#define cudnnBatchNormalizationForwardTraining   mcdnnBatchNormalizationForwardTraining
#define cudnnBatchNormalizationForwardTrainingEx mcdnnBatchNormalizationForwardTrainingEx
#define cudnnDivisiveNormalizationBackward       mcdnnDivisiveNormalizationBackward
#define cudnnDropoutBackward                     mcdnnDropoutBackward
#define cudnnGetBatchNormalizationBackwardExWorkspaceSize                                          \
    mcdnnGetBatchNormalizationBackwardExWorkspaceSize
#define cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize                                   \
    mcdnnGetBatchNormalizationForwardTrainingExWorkspaceSize
#define cudnnGetBatchNormalizationTrainingExReserveSpaceSize                                       \
    mcdnnGetBatchNormalizationTrainingExReserveSpaceSize
#define cudnnGetNormalizationBackwardWorkspaceSize mcdnnGetNormalizationBackwardWorkspaceSize
#define cudnnGetNormalizationForwardTrainingWorkspaceSize                                          \
    mcdnnGetNormalizationForwardTrainingWorkspaceSize
#define cudnnGetNormalizationTrainingReserveSpaceSize mcdnnGetNormalizationTrainingReserveSpaceSize
#define cudnnLRNCrossChannelBackward                  mcdnnLRNCrossChannelBackward
#define cudnnNormalizationBackward                    mcdnnNormalizationBackward
#define cudnnNormalizationForwardTraining             mcdnnNormalizationForwardTraining
#define cudnnOpsTrainVersionCheck                     mcdnnOpsTrainVersionCheck
#define cudnnPoolingBackward                          mcdnnPoolingBackward
#define cudnnSoftmaxBackward                          mcdnnSoftmaxBackward
#define cudnnSpatialTfGridGeneratorBackward           mcdnnSpatialTfGridGeneratorBackward
#define cudnnSpatialTfSamplerBackward                 mcdnnSpatialTfSamplerBackward
