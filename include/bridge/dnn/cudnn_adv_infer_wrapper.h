#pragma once

#define CUDNN_ADV_INFER_H_ MCDNN_MCDNN_ADV_INFER_H_

#define CUDNN_ATTN_QUERYMAP_ALL_TO_ONE MCDNN_ATTN_QUERYMAP_ALL_TO_ONE
#define CUDNN_ATTN_QUERYMAP_ONE_TO_ONE MCDNN_ATTN_QUERYMAP_ONE_TO_ONE
#define CUDNN_ATTN_DISABLE_PROJ_BIASES MCDNN_ATTN_DISABLE_PROJ_BIASES
#define CUDNN_ATTN_ENABLE_PROJ_BIASES  MCDNN_ATTN_ENABLE_PROJ_BIASES
#define CUDNN_SEQDATA_DIM_COUNT        MCDNN_SEQDATA_DIM_COUNT
#define CUDNN_ATTN_WKIND_COUNT         MCDNN_ATTN_WKIND_COUNT
#define CUDNN_RNN_PADDED_IO_DISABLED   MCDNN_RNN_PADDED_IO_DISABLED
#define CUDNN_RNN_PADDED_IO_ENABLED    MCDNN_RNN_PADDED_IO_ENABLED

#define cudnnGetRNNDataDescriptor                    mcdnnGetRNNDataDescriptor
#define cudnnAdvInferVersionCheck                    mcdnnAdvInferVersionCheck
#define cudnnGetMultiHeadAttnWeights                 mcdnnGetMultiHeadAttnWeights
#define cudnnMultiHeadAttnForward                    mcdnnMultiHeadAttnForward
#define cudnnCreateAttnDescriptor                    mcdnnCreateAttnDescriptor
#define cudnnDestroyAttnDescriptor                   mcdnnDestroyAttnDescriptor
#define cudnnSetAttnDescriptor                       mcdnnSetAttnDescriptor
#define cudnnGetAttnDescriptor                       mcdnnGetAttnDescriptor
#define cudnnGetMultiHeadAttnBuffers                 mcdnnGetMultiHeadAttnBuffers
#define cudnnCreateSeqDataDescriptor                 mcdnnCreateSeqDataDescriptor
#define cudnnDestroySeqDataDescriptor                mcdnnDestroySeqDataDescriptor
#define cudnnSetSeqDataDescriptor                    mcdnnSetSeqDataDescriptor
#define cudnnGetSeqDataDescriptor                    mcdnnGetSeqDataDescriptor
#define cudnnBuildRNNDynamic                         mcdnnBuildRNNDynamic
#define cudnnCreatePersistentRNNPlan                 mcdnnCreatePersistentRNNPlan
#define cudnnDestroyPersistentRNNPlan                mcdnnDestroyPersistentRNNPlan
#define cudnnFindRNNForwardInferenceAlgorithmEx      mcdnnFindRNNForwardInferenceAlgorithmEx
#define cudnnSetRNNDescriptor_v8                     mcdnnSetRNNDescriptor_v8
#define cudnnGetRNNDescriptor_v8                     mcdnnGetRNNDescriptor_v8
#define cudnnSetRNNDescriptor_v6                     mcdnnSetRNNDescriptor_v6
#define cudnnGetRNNDescriptor_v6                     mcdnnGetRNNDescriptor_v6
#define cudnnGetRNNDescriptor                        mcdnnGetRNNDescriptor
#define cudnnGetRNNBiasMode                          mcdnnGetRNNBiasMode
#define cudnnCreateRNNDataDescriptor                 mcdnnCreateRNNDataDescriptor
#define cudnnDestroyRNNDataDescriptor                mcdnnDestroyRNNDataDescriptor
#define cudnnCreateRNNDescriptor                     mcdnnCreateRNNDescriptor
#define cudnnSetRNNDescriptor                        mcdnnSetRNNDescriptor
#define cudnnSetRNNDataDescriptor                    mcdnnSetRNNDataDescriptor
#define cudnnSetRNNAlgorithmDescriptor               mcdnnSetRNNAlgorithmDescriptor
#define cudnnGetRNNForwardInferenceAlgorithmMaxCount mcdnnGetRNNForwardInferenceAlgorithmMaxCount
#define cudnnRNNForwardInferenceEx                   mcdnnRNNForwardInferenceEx
#define cudnnRNNForwardInference                     mcdnnRNNForwardInference
#define cudnnGetRNNLinLayerMatrixParams              mcdnnGetRNNLinLayerMatrixParams
#define cudnnGetRNNLinLayerBiasParams                mcdnnGetRNNLinLayerBiasParams
#define cudnnGetRNNParamsSize                        mcdnnGetRNNParamsSize
#define cudnnGetRNNWorkspaceSize                     mcdnnGetRNNWorkspaceSize
#define cudnnGetRNNTrainingReserveSize               mcdnnGetRNNTrainingReserveSize
#define cudnnSetRNNMatrixMathType                    mcdnnSetRNNMatrixMathType
#define cudnnGetRNNMatrixMathType                    mcdnnGetRNNMatrixMathType
#define cudnnSetRNNBiasMode                          mcdnnSetRNNBiasMode
#define cudnnRNNSetClip_v8                           mcdnnRNNSetClip_v8
#define cudnnRNNGetClip_v8                           mcdnnRNNGetClip_v8
#define cudnnRNNSetClip                              mcdnnRNNSetClip
#define cudnnRNNGetClip                              mcdnnRNNGetClip
#define cudnnSetPersistentRNNPlan                    mcdnnSetPersistentRNNPlan
#define cudnnSetRNNPaddingMode                       mcdnnSetRNNPaddingMode
#define cudnnGetRNNPaddingMode                       mcdnnGetRNNPaddingMode
#define cudnnSetRNNProjectionLayers                  mcdnnSetRNNProjectionLayers
#define cudnnGetRNNProjectionLayers                  mcdnnGetRNNProjectionLayers
#define cudnnDestroyRNNDescriptor                    mcdnnDestroyRNNDescriptor
#define cudnnGetRNNWeightSpaceSize                   mcdnnGetRNNWeightSpaceSize
#define cudnnGetRNNTempSpaceSizes                    mcdnnGetRNNTempSpaceSizes
#define cudnnGetRNNWeightParams                      mcdnnGetRNNWeightParams
#define cudnnRNNForward                              mcdnnRNNForward

#define cudnnLossNormalizationMode_t     mcdnnLossNormalizationMode_t
#define CUDNN_LOSS_NORMALIZATION_NONE    MCDNN_LOSS_NORMALIZATION_NONE
#define CUDNN_LOSS_NORMALIZATION_SOFTMAX MCDNN_LOSS_NORMALIZATION_SOFTMAX

#define cudnnWgradMode_t     mcdnnWgradMode_t
#define CUDNN_WGRAD_MODE_ADD MCDNN_WGRAD_MODE_ADD
#define CUDNN_WGRAD_MODE_SET MCDNN_WGRAD_MODE_SET

#define cudnnForwardMode_t       mcdnnForwardMode_t
#define CUDNN_FWD_MODE_INFERENCE MCDNN_FWD_MODE_INFERENCE
#define CUDNN_FWD_MODE_TRAINING  MCDNN_FWD_MODE_TRAINING

#define cudnnRNNMode_t mcdnnRNNMode_t
#define CUDNN_RNN_RELU MCDNN_RNN_RELU
#define CUDNN_RNN_TANH MCDNN_RNN_TANH
#define CUDNN_LSTM     MCDNN_LSTM
#define CUDNN_GRU      MCDNN_GRU

#define cudnnRNNBiasMode_t        mcdnnRNNBiasMode_t
#define CUDNN_RNN_NO_BIAS         MCDNN_RNN_NO_BIAS
#define CUDNN_RNN_SINGLE_INP_BIAS MCDNN_RNN_SINGLE_INP_BIAS
#define CUDNN_RNN_DOUBLE_BIAS     MCDNN_RNN_DOUBLE_BIAS
#define CUDNN_RNN_SINGLE_REC_BIAS MCDNN_RNN_SINGLE_REC_BIAS

#define cudnnDirectionMode_t mcdnnDirectionMode_t
#define CUDNN_UNIDIRECTIONAL MCDNN_UNIDIRECTIONAL
#define CUDNN_BIDIRECTIONAL  MCDNN_BIDIRECTIONAL

#define cudnnRNNInputMode_t mcdnnRNNInputMode_t
#define CUDNN_LINEAR_INPUT  MCDNN_LINEAR_INPUT
#define CUDNN_SKIP_INPUT    MCDNN_SKIP_INPUT

#define cudnnRNNClipMode_t    mcdnnRNNClipMode_t
#define CUDNN_RNN_CLIP_NONE   MCDNN_RNN_CLIP_NONE
#define CUDNN_RNN_CLIP_MINMAX MCDNN_RNN_CLIP_MINMAX

#define cudnnRNNDataLayout_t                       mcdnnRNNDataLayout_t
#define CUDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_UNPACKED   MCDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_UNPACKED
#define CUDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_PACKED     MCDNN_RNN_DATA_LAYOUT_SEQ_MAJOR_PACKED
#define CUDNN_RNN_DATA_LAYOUT_BATCH_MAJOR_UNPACKED MCDNN_RNN_DATA_LAYOUT_BATCH_MAJOR_UNPACKED

#define cudnnMultiHeadAttnWeightKind_t mcdnnMultiHeadAttnWeightKind_t
#define CUDNN_MH_ATTN_Q_WEIGHTS        MCDNN_MH_ATTN_Q_WEIGHTS
#define CUDNN_MH_ATTN_K_WEIGHTS        MCDNN_MH_ATTN_K_WEIGHTS
#define CUDNN_MH_ATTN_V_WEIGHTS        MCDNN_MH_ATTN_V_WEIGHTS
#define CUDNN_MH_ATTN_O_WEIGHTS        MCDNN_MH_ATTN_O_WEIGHTS
#define CUDNN_MH_ATTN_Q_BIASES         MCDNN_MH_ATTN_Q_BIASES
#define CUDNN_MH_ATTN_K_BIASES         MCDNN_MH_ATTN_K_BIASES
#define CUDNN_MH_ATTN_V_BIASES         MCDNN_MH_ATTN_V_BIASES
#define CUDNN_MH_ATTN_O_BIASES         MCDNN_MH_ATTN_O_BIASES

#define cudnnSeqDataAxis_t      mcdnnSeqDataAxis_t
#define CUDNN_SEQDATA_TIME_DIM  MCDNN_SEQDATA_TIME_DIM
#define CUDNN_SEQDATA_BATCH_DIM MCDNN_SEQDATA_BATCH_DIM
#define CUDNN_SEQDATA_BEAM_DIM  MCDNN_SEQDATA_BEAM_DIM
#define CUDNN_SEQDATA_VECT_DIM  MCDNN_SEQDATA_VECT_DIM

#define cudnnRNNPaddingMode_t        mcdnnRNNPaddingMode_t
#define CUDNN_RNN_PADDED_IO_DISABLED MCDNN_RNN_PADDED_IO_DISABLED
#define CUDNN_PADDED_IO_ENABLED      MCDNN_PADDED_IO_ENABLED

#define cudnnAttnQueryMap_t mcdnnAttnQueryMap_t

#define cudnnAttnDescriptor_t    mcdnnAttnDescriptor_t
#define cudnnPersistentRNNPlan_t mcdnnPersistentRNNPlan_t
#define cudnnRNNDataDescriptor_t mcdnnRNNDataDescriptor_t
#define cudnnSeqDataDescriptor_t mcdnnSeqDataDescriptor_t
#define cudnnRNNDescriptor_t     mcdnnRNNDescriptor_t

#define cudnnAttnStruct        mcdnnAttnDescriptor
#define cudnnRNNDataStruct     mcdnnRNNDataDescriptor
#define cudnnPersistentRNNPlan mcdnnPersistentRNNPlan
#define cudnnSeqDataStruct     mcdnnSeqDataDescriptor
#define cudnnRNNStruct         mcdnnRNNDescriptor
