#pragma once

#define CUDNNWINAPI          MCDNNWINAPI
#define CUDNN_BN_MIN_EPSILON MCDNN_BN_MIN_EPSILON
#define CUDNN_DEPRECATED     MCDNN_DEPRECATED
#define CUDNN_DIM_MAX        MCDNN_DIM_MAX
#define CUDNN_LRN_MIN_N      MCDNN_LRN_MIN_N
#define CUDNN_LRN_MAX_N      MCDNN_LRN_MAX_N
#define CUDNN_LRN_MIN_K      MCDNN_LRN_MIN_K
#define CUDNN_LRN_MIN_BETA   MCDNN_LRN_MIN_BETA
#define CUDNN_SEV_ERROR_EN   MCDNN_SEV_ERROR_EN
#define CUDNN_SEV_WARNING_EN MCDNN_SEV_WARNING_EN
#define CUDNN_SEV_INFO_EN    MCDNN_SEV_INFO_EN
#define CUDNN_OPS_INFER_H_   MCDNN_MCDNN_OPS_INFER_H_

#define cudnnGetCudartVersion                    mcdnnGetMacartVersionWrapper
#define cudnnGetVersion                          mcdnnGetVersionWrapper
#define cudnnGetProperty                         mcdnnGetPropertyWrapper
#define cudnnActivationForward                   mcdnnActivationForward
#define cudnnAddTensor                           mcdnnAddTensor
#define cudnnBatchNormalizationForwardInference  mcdnnBatchNormalizationForwardInference
#define cudnnCopyAlgorithmDescriptor             mcdnnCopyAlgorithmDescriptor
#define cudnnCreate                              mcdnnCreate
#define cudnnCreateActivationDescriptor          mcdnnCreateActivationDescriptor
#define cudnnCreateAlgorithmDescriptor           mcdnnCreateAlgorithmDescriptor
#define cudnnCreateAlgorithmPerformance          mcdnnCreateAlgorithmPerformance
#define cudnnCreateDropoutDescriptor             mcdnnCreateDropoutDescriptor
#define cudnnCreateFilterDescriptor              mcdnnCreateFilterDescriptor
#define cudnnCreateLRNDescriptor                 mcdnnCreateLRNDescriptor
#define cudnnCreateOpTensorDescriptor            mcdnnCreateOpTensorDescriptor
#define cudnnCreatePoolingDescriptor             mcdnnCreatePoolingDescriptor
#define cudnnCreateReduceTensorDescriptor        mcdnnCreateReduceTensorDescriptor
#define cudnnCreateSpatialTransformerDescriptor  mcdnnCreateSpatialTransformerDescriptor
#define cudnnCreateTensorDescriptor              mcdnnCreateTensorDescriptor
#define cudnnCreateTensorTransformDescriptor     mcdnnCreateTensorTransformDescriptor
#define cudnnDeriveBNTensorDescriptor            mcdnnDeriveBNTensorDescriptor
#define cudnnDeriveNormTensorDescriptor          mcdnnDeriveNormTensorDescriptor
#define cudnnDestroy                             mcdnnDestroy
#define cudnnDestroyActivationDescriptor         mcdnnDestroyActivationDescriptor
#define cudnnDestroyAlgorithmDescriptor          mcdnnDestroyAlgorithmDescriptor
#define cudnnDestroyAlgorithmPerformance         mcdnnDestroyAlgorithmPerformance
#define cudnnDestroyDropoutDescriptor            mcdnnDestroyDropoutDescriptor
#define cudnnDestroyFilterDescriptor             mcdnnDestroyFilterDescriptor
#define cudnnDestroyLRNDescriptor                mcdnnDestroyLRNDescriptor
#define cudnnDestroyOpTensorDescriptor           mcdnnDestroyOpTensorDescriptor
#define cudnnDestroyPoolingDescriptor            mcdnnDestroyPoolingDescriptor
#define cudnnDestroyReduceTensorDescriptor       mcdnnDestroyReduceTensorDescriptor
#define cudnnDestroySpatialTransformerDescriptor mcdnnDestroySpatialTransformerDescriptor
#define cudnnDestroyTensorDescriptor             mcdnnDestroyTensorDescriptor
#define cudnnDestroyTensorTransformDescriptor    mcdnnDestroyTensorTransformDescriptor
#define cudnnDivisiveNormalizationForward        mcdnnDivisiveNormalizationForward
#define cudnnDropoutForward                      mcdnnDropoutForward
#define cudnnDropoutGetReserveSpaceSize          mcdnnDropoutGetReserveSpaceSize
#define cudnnDropoutGetStatesSize                mcdnnDropoutGetStatesSize
#define cudnnGetActivationDescriptor             mcdnnGetActivationDescriptor
#define cudnnGetActivationDescriptorSwishBeta    mcdnnGetActivationDescriptorSwishBeta
#define cudnnGetAlgorithmDescriptor              mcdnnGetAlgorithmDescriptor
#define cudnnGetAlgorithmPerformance             mcdnnGetAlgorithmPerformance
#define cudnnGetAlgorithmSpaceSize               mcdnnGetAlgorithmSpaceSize
#define cudnnGetCallback                         mcdnnGetCallback
#define cudnnGetDropoutDescriptor                mcdnnGetDropoutDescriptor
#define cudnnGetErrorString                      mcdnnGetErrorString
#define cudnnGetFilter4dDescriptor               mcdnnGetFilter4dDescriptor
#define cudnnGetFilterNdDescriptor               mcdnnGetFilterNdDescriptor
#define cudnnGetFilterSizeInBytes                mcdnnGetFilterSizeInBytes
#define cudnnGetFilterGetSizeInBytes             mcdnnGetFilterGetSizeInBytes
#define cudnnGetLRNDescriptor                    mcdnnGetLRNDescriptor
#define cudnnGetOpTensorDescriptor               mcdnnGetOpTensorDescriptor
#define cudnnGetPooling2dDescriptor              mcdnnGetPooling2dDescriptor
#define cudnnGetPooling2dForwardOutputDim        mcdnnGetPooling2dForwardOutputDim
#define cudnnGetPoolingNdDescriptor              mcdnnGetPoolingNdDescriptor
#define cudnnGetPoolingNdForwardOutputDim        mcdnnGetPoolingNdForwardOutputDim

#define cudnnGetReduceTensorDescriptor         mcdnnGetReduceTensorDescriptor
#define cudnnGetReductionIndicesSize           mcdnnGetReductionIndicesSize
#define cudnnGetReductionWorkspaceSize         mcdnnGetReductionWorkspaceSize
#define cudnnGetStream                         mcdnnGetStream
#define cudnnGetTensor4dDescriptor             mcdnnGetTensor4dDescriptor
#define cudnnGetTensorNdDescriptor             mcdnnGetTensorNdDescriptor
#define cudnnGetTensorSizeInBytes              mcdnnGetTensorSizeInBytes
#define cudnnGetTensorTransformDescriptor      mcdnnGetTensorTransformDescriptor
#define cudnnInitTransformDest                 mcdnnInitTransformDest
#define cudnnLRNCrossChannelForward            mcdnnLRNCrossChannelForward
#define cudnnNormalizationForwardInference     mcdnnNormalizationForwardInference
#define cudnnOpsInferVersionCheck              mcdnnOpsInferVersionCheck
#define cudnnOpTensor                          mcdnnOpTensor
#define cudnnPoolingForward                    mcdnnPoolingForward
#define cudnnQueryRuntimeError                 mcdnnQueryRuntimeError
#define cudnnReduceTensor                      mcdnnReduceTensor
#define cudnnRestoreAlgorithm                  mcdnnRestoreAlgorithm
#define cudnnRestoreDropoutDescriptor          mcdnnRestoreDropoutDescriptor
#define cudnnSaveAlgorithm                     mcdnnSaveAlgorithm
#define cudnnScaleTensor                       mcdnnScaleTensor
#define cudnnSetActivationDescriptor           mcdnnSetActivationDescriptor
#define cudnnSetActivationDescriptorSwishBeta  mcdnnSetActivationDescriptorSwishBeta
#define cudnnSetAlgorithmDescriptor            mcdnnSetAlgorithmDescriptor
#define cudnnSetAlgorithmPerformance           mcdnnSetAlgorithmPerformance
#define cudnnSetCallback                       mcdnnSetCallback
#define cudnnSetDropoutDescriptor              mcdnnSetDropoutDescriptor
#define cudnnSetFilter4dDescriptor             mcdnnSetFilter4dDescriptor
#define cudnnSetFilterNdDescriptor             mcdnnSetFilterNdDescriptor
#define cudnnSetLRNDescriptor                  mcdnnSetLRNDescriptor
#define cudnnSetOpTensorDescriptor             mcdnnSetOpTensorDescriptor
#define cudnnSetPooling2dDescriptor            mcdnnSetPooling2dDescriptor
#define cudnnSetPoolingNdDescriptor            mcdnnSetPoolingNdDescriptor
#define cudnnSetReduceTensorDescriptor         mcdnnSetReduceTensorDescriptor
#define cudnnSetSpatialTransformerNdDescriptor mcdnnSetSpatialTransformerNdDescriptor
#define cudnnSetStream                         mcdnnSetStream
#define cudnnSetTensor                         mcdnnSetTensor
#define cudnnSetTensor4dDescriptor             mcdnnSetTensor4dDescriptor
#define cudnnSetTensor4dDescriptorEx           mcdnnSetTensor4dDescriptorEx
#define cudnnSetTensorNdDescriptor             mcdnnSetTensorNdDescriptor
#define cudnnSetTensorNdDescriptorEx           mcdnnSetTensorNdDescriptorEx
#define cudnnSetTensorTransformDescriptor      mcdnnSetTensorTransformDescriptor
#define cudnnSoftmaxForward                    mcdnnSoftmaxForward
#define cudnnSpatialTfGridGeneratorForward     mcdnnSpatialTfGridGeneratorForward
#define cudnnSpatialTfSamplerForward           mcdnnSpatialTfSamplerForward
#define cudnnTransformFilter                   mcdnnTransformFilter
#define cudnnTransformTensor                   mcdnnTransformTensor
#define cudnnTransformTensorEx                 mcdnnTransformTensorEx

#define cudnnActivationMode_t         mcdnnActivationMode_t
#define CUDNN_ACTIVATION_SIGMOID      MCDNN_ACTIVATION_SIGMOID
#define CUDNN_ACTIVATION_RELU         MCDNN_ACTIVATION_RELU
#define CUDNN_ACTIVATION_TANH         MCDNN_ACTIVATION_TANH
#define CUDNN_ACTIVATION_CLIPPED_RELU MCDNN_ACTIVATION_CLIPPED_RELU
#define CUDNN_ACTIVATION_ELU          MCDNN_ACTIVATION_ELU
#define CUDNN_ACTIVATION_IDENTITY     MCDNN_ACTIVATION_IDENTITY
#define CUDNN_ACTIVATION_SWISH        MCDNN_ACTIVATION_SWISH

#define cudnnBatchNormMode_t               mcdnnBatchNormMode_t
#define CUDNN_BATCHNORM_SPATIAL            MCDNN_BATCHNORM_SPATIAL
#define CUDNN_BATCHNORM_PER_ACTIVATION     MCDNN_BATCHNORM_PER_ACTIVATION
#define CUDNN_BATCHNORM_SPATIAL_PERSISTENT MCDNN_BATCHNORM_SPATIAL_PERSISTENT

#define cudnnBatchNormOps_t                   mcdnnBatchNormOps_t
#define CUDNN_BATCHNORM_OPS_BN                MCDNN_BATCHNORM_OPS_BN
#define CUDNN_BATCHNORM_OPS_BN_ACTIVATION     MCDNN_BATCHNORM_OPS_BN_ACTIVATION
#define CUDNN_BATCHNORM_OPS_BN_ADD_ACTIVATION MCDNN_BATCHNORM_OPS_BN_ADD_ACTIVATION

#define cudnnCTCLossAlgo_t                    mcdnnCTCLossAlgo_t
#define CUDNN_CTC_LOSS_ALGO_DETERMINISTIC     MCDNN_CTC_LOSS_ALGO_DETERMINISTIC
#define CUDNN_CTC_LOSS_ALGO_NON_DETERMINISTIC MCDNN_CTC_LOSS_ALGO_NON_DETERMINISTIC

#define cudnnDataType_t     mcdnnDataType_t
#define CUDNN_DATA_FLOAT    MCDNN_DATA_FLOAT
#define CUDNN_DATA_DOUBLE   MCDNN_DATA_DOUBLE
#define CUDNN_DATA_HALF     MCDNN_DATA_HALF
#define CUDNN_DATA_INT8     MCDNN_DATA_INT8
#define CUDNN_DATA_UINT8    MCDNN_DATA_UINT8
#define CUDNN_DATA_INT32    MCDNN_DATA_INT32
#define CUDNN_DATA_INT8x4   MCDNN_DATA_INT8x4
#define CUDNN_DATA_INT8x32  MCDNN_DATA_INT8x32
#define CUDNN_DATA_UINT8x4  MCDNN_DATA_UINT8x4
#define CUDNN_DATA_BFLOAT16 MCDNN_DATA_BFLOAT16
#define CUDNN_DATA_INT64    MCDNN_DATA_INT64
#define CUDNN_DATA_BOOLEAN  MCDNN_DATA_BOOLEAN
#define CUDNN_DATA_MAX      MCDNN_DATA_MAX

#define cudnnDeterminism_t      mcdnnDeterminism_t
#define CUDNN_NON_DETERMINISTIC MCDNN_NON_DETERMINISTIC
#define CUDNN_DETERMINISTIC     MCDNN_DETERMINISTIC

#define cudnnDivNormMode_t              mcdnnDivNormMode_t
#define CUDNN_DIVNORM_PRECOMPUTED_MEANS MCDNN_DIVNORM_PRECOMPUTED_MEANS

#define cudnnErrQueryMode_t        mcdnnErrQueryMode_t
#define CUDNN_ERRQUERY_RAWCODE     MCDNN_ERRQUERY_RAWCODE
#define CUDNN_ERRQUERY_NONBLOCKING MCDNN_ERRQUERY_NONBLOCKING
#define CUDNN_ERRQUERY_BLOCKING    MCDNN_ERRQUERY_NONBLOCKING

#define cudnnFoldingDirection_t mcdnnFoldingDirection_t
#define CUDNN_TRANSFORM_FOLD    MCDNN_TRANSFORM_FOLD
#define CUDNN_TRANSFORM_UNFOLD  MCDNN_TRANSFORM_UNFOLD

#define cudnnIndicesType_t  mcdnnIndicesType_t
#define CUDNN_32BIT_INDICES MCDNN_32BIT_INDICES
#define CUDNN_64BIT_INDICES MCDNN_64BIT_INDICES
#define CUDNN_16BIT_INDICES MCDNN_16BIT_INDICES
#define CUDNN_8BIT_INDICES  MCDNN_8BIT_INDICES

#define cudnnLRNMode_t               mcdnnLRNMode_t
#define CUDNN_LRN_CROSS_CHANNEL_DIM1 MCDNN_LRN_CROSS_CHANNEL_DIM1

#define cudnnMathType_t                       mcdnnMathType_t
#define CUDNN_DEFAULT_MATH                    MCDNN_DEFAULT_MATH
#define CUDNN_TENSOR_OP_MATH                  MCDNN_TENSOR_OP_MATH
#define CUDNN_TENSOR_OP_MATH_ALLOW_CONVERSION MCDNN_TENSOR_OP_MATH_ALLOW_CONVERSION
#define CUDNN_FMA_MATH                        MCDNN_FMA_MATH

#define cudnnNanPropagation_t   mcdnnNanPropagation_t
#define CUDNN_NOT_PROPAGATE_NAN MCDNN_NOT_PROPAGATE_NAN
#define CUDNN_PROPAGATE_NAN     MCDNN_PROPAGATE_NAN

#define cudnnNormAlgo_t          mcdnnNormAlgo_t
#define CUDNN_NORM_ALGO_STANDARD MCDNN_NORM_ALGO_STANDARD
#define CUDNN_NORM_ALGO_PERSIST  MCDNN_NORM_ALGO_PERSIST

#define cudnnNormMode_t           mcdnnNormMode_t
#define CUDNN_NORM_PER_ACTIVATION MCDNN_NORM_PER_ACTIVATION
#define CUDNN_NORM_PER_CHANNEL    MCDNN_NORM_PER_CHANNEL

#define cudnnNormOps_t                     mcdnnNormOps_t
#define CUDNN_NORM_OPS_NORM                MCDNN_NORM_OPS_NORM
#define CUDNN_NORM_OPS_NORM_ACTIVATION     MCDNN_NORM_OPS_NORM_ACTIVATION
#define CUDNN_NORM_OPS_NORM_ADD_ACTIVATION MCDNN_NORM_OPS_NORM_ADD_ACTIVATION

#define cudnnOpTensorOp_t    mcdnnOpTensorOp_t
#define CUDNN_OP_TENSOR_ADD  MCDNN_OP_TENSOR_ADD
#define CUDNN_OP_TENSOR_MUL  MCDNN_OP_TENSOR_MUL
#define CUDNN_OP_TENSOR_MIN  MCDNN_OP_TENSOR_MIN
#define CUDNN_OP_TENSOR_MAX  MCDNN_OP_TENSOR_MAX
#define CUDNN_OP_TENSOR_SQRT MCDNN_OP_TENSOR_SQRT
#define CUDNN_OP_TENSOR_NOT  MCDNN_OP_TENSOR_NOT

#define cudnnPoolingMode_t                          mcdnnPoolingMode_t
#define CUDNN_POOLING_MAX                           MCDNN_POOLING_MAX
#define CUDNN_POOLING_AVERAGE_COUNT_INCLUDE_PADDING MCDNN_POOLING_AVERAGE_COUNT_INCLUDE_PADDING
#define CUDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING MCDNN_POOLING_AVERAGE_COUNT_EXCLUDE_PADDING
#define CUDNN_POOLING_MAX_DETERMINISTIC             MCDNN_POOLING_MAX_DETERMINISTIC

#define cudnnReduceTensorIndices_t            mcdnnReduceTensorIndices_t
#define CUDNN_REDUCE_TENSOR_NO_INDICES        MCDNN_REDUCE_TENSOR_NO_INDICES
#define CUDNN_REDUCE_TENSOR_FLATTENED_INDICES MCDNN_REDUCE_TENSOR_FLATTENED_INDICES

#define cudnnReduceTensorOp_t            mcdnnReduceTensorOp_t
#define CUDNN_REDUCE_TENSOR_ADD          MCDNN_REDUCE_TENSOR_ADD
#define CUDNN_REDUCE_TENSOR_MUL          MCDNN_REDUCE_TENSOR_MUL
#define CUDNN_REDUCE_TENSOR_MIN          MCDNN_REDUCE_TENSOR_MIN
#define CUDNN_REDUCE_TENSOR_MAX          MCDNN_REDUCE_TENSOR_MAX
#define CUDNN_REDUCE_TENSOR_AMAX         MCDNN_REDUCE_TENSOR_AMAX
#define CUDNN_REDUCE_TENSOR_AVG          MCDNN_REDUCE_TENSOR_AVG
#define CUDNN_REDUCE_TENSOR_NORM1        MCDNN_REDUCE_TENSOR_NORM1
#define CUDNN_REDUCE_TENSOR_NORM2        MCDNN_REDUCE_TENSOR_NORM2
#define CUDNN_REDUCE_TENSOR_MUL_NO_ZEROS MCDNN_REDUCE_TENSOR_MUL_NO_ZEROS

#define cudnnRNNAlgo_t                        mcdnnRNNAlgo_t
#define CUDNN_RNN_ALGO_STANDARD               MCDNN_RNN_ALGO_STANDARD
#define CUDNN_RNN_ALGO_PERSIST_STATIC         MCDNN_RNN_ALGO_PERSIST_STATIC
#define CUDNN_RNN_ALGO_PERSIST_DYNAMIC        MCDNN_RNN_ALGO_PERSIST_DYNAMIC
#define CUDNN_RNN_ALGO_PERSIST_STATIC_SMALL_H MCDNN_RNN_ALGO_PERSIST_STATIC_SMALL_H
#define CUDNN_RNN_ALGO_COUNT                  MCDNN_RNN_ALGO_COUNT

#define cudnnSamplerType_t     mcdnnSamplerType_t
#define CUDNN_SAMPLER_BILINEAR MCDNN_SAMPLER_BILINEAR

#define cudnnSeverity_t   mcdnnSeverity_t
#define CUDNN_SEV_FATAL   MCDNN_SEV_FATAL
#define CUDNN_SEV_ERROR   MCDNN_SEV_ERROR
#define CUDNN_SEV_WARNING MCDNN_SEV_WARNING
#define CUDNN_SEV_INFO    MCDNN_SEV_INFO

#define cudnnSoftmaxAlgorithm_t mcdnnSoftmaxAlgorithm_t
#define CUDNN_SOFTMAX_FAST      MCDNN_SOFTMAX_FAST
#define CUDNN_SOFTMAX_ACCURATE  MCDNN_SOFTMAX_ACCURATE
#define CUDNN_SOFTMAX_LOG       MCDNN_SOFTMAX_LOG

#define cudnnSoftmaxMode_t          mcdnnSoftmaxMode_t
#define CUDNN_SOFTMAX_MODE_INSTANCE MCDNN_SOFTMAX_MODE_INSTANCE
#define CUDNN_SOFTMAX_MODE_CHANNEL  MCDNN_SOFTMAX_MODE_CHANNEL

#define cudnnStatus_t                             mcdnnStatus_t
#define CUDNN_STATUS_SUCCESS                      MCDNN_STATUS_SUCCESS
#define CUDNN_STATUS_NOT_INITIALIZED              MCDNN_STATUS_NOT_INITIALIZED
#define CUDNN_STATUS_ALLOC_FAILED                 MCDNN_STATUS_ALLOC_FAILED
#define CUDNN_STATUS_BAD_PARAM                    MCDNN_STATUS_BAD_PARAM
#define CUDNN_STATUS_INTERNAL_ERROR               MCDNN_STATUS_INTERNAL_ERROR
#define CUDNN_STATUS_INVALID_VALUE                MCDNN_STATUS_INVALID_VALUE
#define CUDNN_STATUS_ARCH_MISMATCH                MCDNN_STATUS_ARCH_MISMATCH
#define CUDNN_STATUS_MAPPING_ERROR                MCDNN_STATUS_MAPPING_ERROR
#define CUDNN_STATUS_EXECUTION_FAILED             MCDNN_STATUS_EXECUTION_FAILED
#define CUDNN_STATUS_NOT_SUPPORTED                MCDNN_STATUS_NOT_SUPPORTED
#define CUDNN_STATUS_LICENSE_ERROR                MCDNN_STATUS_LICENSE_ERROR
#define CUDNN_STATUS_RUNTIME_PREREQUISITE_MISSING MCDNN_STATUS_RUNTIME_PREREQUISITE_MISSING
#define CUDNN_STATUS_RUNTIME_IN_PROGRESS          MCDNN_STATUS_RUNTIME_IN_PROGRESS
#define CUDNN_STATUS_RUNTIME_FP_OVERFLOW          MCDNN_STATUS_RUNTIME_FP_OVERFLOW
#define CUDNN_STATUS_VERSION_MISMATCH             MCDNN_STATUS_VERSION_MISMATCH

#define cudnnTensorFormat_t      mcdnnTensorFormat_t
#define CUDNN_TENSOR_NCHW        MCDNN_TENSOR_NCHW
#define CUDNN_TENSOR_NHWC        MCDNN_TENSOR_NHWC
#define CUDNN_TENSOR_NCHW_VECT_C MCDNN_TENSOR_NCHW_VECT_C
#define CUDNN_TENSOR_MAX         MCDNN_TENSOR_MAX

#define cudnnRuntimeTag_t        mcdnnRuntimeTag_t
#define CUDNN_NORM_ALGO_STANDARD MCDNN_NORM_ALGO_STANDARD
#define CUDNN_NORM_ALGO_PERSIST  MCDNN_NORM_ALGO_PERSIST

#define cudnnCallback_t mcdnnCallback_t
#define Algorithm       Algorithm

#define cudnnAlgorithm_t                    mcdnnAlgorithm_t
#define cudnnAlgorithmUnionStruct           mcdnnAlgorithm
#define cudnnDebug_t                        mcdnnDebug_t
#define cudnnDebugStruct                    mcdnnDebug
#define cudnnActivationDescriptor_t         mcdnnActivationDescriptor_t
#define cudnnActivationStruct               mcdnnActivationDescriptor
#define cudnnCTCLossDescriptor_t            mcdnnCTCLossDescriptor_t
#define cudnnCTCLossStruct                  mcdnnCTCLossDescriptor
#define cudnnDropoutDescriptor_t            mcdnnDropoutDescriptor_t
#define cudnnDropoutStruct                  mcdnnDropoutDescriptor
#define cudnnFilterDescriptor_t             mcdnnFilterDescriptor_t
#define cudnnFilterStruct                   mcdnnFilterDescriptor
#define cudnnHandle_t                       mcdnnHandle_t
#define cudnnContext                        mcdnnHandle
#define cudnnLRNDescriptor_t                mcdnnLRNDescriptor_t
#define cudnnLRNStruct                      mcdnnLRNDescriptor
#define cudnnOpTensorDescriptor_t           mcdnnOpTensorDescriptor_t
#define cudnnOpTensorStruct                 mcdnnOpTensorDescriptor
#define cudnnPoolingDescriptor_t            mcdnnPoolingDescriptor_t
#define cudnnPoolingStruct                  mcdnnPoolingDescriptor
#define cudnnReduceTensorDescriptor_t       mcdnnReduceTensorDescriptor_t
#define cudnnReduceTensorStruct             mcdnnReduceTensorDescriptor
#define cudnnTensorTransformDescriptor_t    mcdnnTensorTransformDescriptor_t
#define cudnnTensorTransformStruct          mcdnnTensorTransformDescriptor
#define cudnnTensorDescriptor_t             mcdnnTensorDescriptor_t
#define cudnnTensorStruct                   mcdnnTensorDescriptor
#define cudnnSpatialTransformerDescriptor_t mcdnnSpatialTransformerDescriptor_t
#define cudnnSpatialTransformerStruct       mcdnnSpatialTransformerDescriptor
#define cudnnAlgorithmDescriptor_t          mcdnnAlgorithmDescriptor_t
#define cudnnAlgorithmStruct                mcdnnAlgorithmDescriptor
#define cudnnAlgorithmPerformance_t         mcdnnAlgorithmPerformance_t
#define cudnnAlgorithmPerformanceStruct     mcdnnAlgorithmPerformance
