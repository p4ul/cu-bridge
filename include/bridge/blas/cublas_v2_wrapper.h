#pragma once

#include "cublas_api_wrapper.h"

/* Helper Functions */
#define cublasCreate mcblasCreate
#define cublasDestroy mcblasDestroy
#define cublasGetVersion mcblasGetVersion
#define cublasSetWorkspace mcblasSetWorkspace
#define cublasSetStream mcblasSetStream
#define cublasGetStream mcblasGetStream
#define cublasGetPointerMode mcblasGetPointerMode
#define cublasSetPointerMode mcblasSetPointerMode

/* Blas1 Routines   */
#define cublasSnrm2 mcblasSnrm2
#define cublasDnrm2 mcblasDnrm2
#define cublasScnrm2 mcblasScnrm2
#define cublasDznrm2 mcblasDznrm2

#define cublasSdot mcblasSdot
#define cublasDdot mcblasDdot
#define cublasCdotu mcblasCdotu
#define cublasCdotc mcblasCdotc
#define cublasZdotu mcblasZdotu
#define cublasZdotc mcblasZdotc

#define cublasSscal mcblasSscal
#define cublasDscal mcblasDscal
#define cublasCscal mcblasCscal
#define cublasCsscal mcblasCsscal
#define cublasZscal mcblasZscal
#define cublasZdscal mcblasZdscal

#define cublasSaxpy mcblasSaxpy
#define cublasDaxpy mcblasDaxpy
#define cublasCaxpy mcblasCaxpy
#define cublasZaxpy mcblasZaxpy

#define cublasScopy mcblasScopy
#define cublasDcopy mcblasDcopy
#define cublasCcopy mcblasCcopy
#define cublasZcopy mcblasZcopy

#define cublasSswap mcblasSswap
#define cublasDswap mcblasDswap
#define cublasCswap mcblasCswap
#define cublasZswap mcblasZswap

#define cublasIsamax mcblasIsamax
#define cublasIdamax mcblasIdamax
#define cublasIcamax mcblasIcamax
#define cublasIzamax mcblasIzamax

#define cublasIsamin mcblasIsamin
#define cublasIdamin mcblasIdamin
#define cublasIcamin mcblasIcamin
#define cublasIzamin mcblasIzamin

#define cublasSasum mcblasSasum
#define cublasDasum mcblasDasum
#define cublasScasum mcblasScasum
#define cublasDzasum mcblasDzasum

#define cublasSrot mcblasSrot
#define cublasDrot mcblasDrot
#define cublasCrot mcblasCrot
#define cublasCsrot mcblasCsrot
#define cublasZrot mcblasZrot
#define cublasZdrot mcblasZdrot

#define cublasSrotg mcblasSrotg
#define cublasDrotg mcblasDrotg
#define cublasCrotg mcblasCrotg
#define cublasZrotg mcblasZrotg

#define cublasSrotm mcblasSrotm
#define cublasDrotm mcblasDrotm

#define cublasSrotmg mcblasSrotmg
#define cublasDrotmg mcblasDrotmg

/* Blas2 Routines */
#define cublasSgemv mcblasSgemv
#define cublasDgemv mcblasDgemv
#define cublasCgemv mcblasCgemv
#define cublasZgemv mcblasZgemv

#define cublasSgbmv mcblasSgbmv
#define cublasDgbmv mcblasDgbmv
#define cublasCgbmv mcblasCgbmv
#define cublasZgbmv mcblasZgbmv

#define cublasStrmv mcblasStrmv
#define cublasDtrmv mcblasDtrmv
#define cublasCtrmv mcblasCtrmv
#define cublasZtrmv mcblasZtrmv

#define cublasStbmv mcblasStbmv
#define cublasDtbmv mcblasDtbmv
#define cublasCtbmv mcblasCtbmv
#define cublasZtbmv mcblasZtbmv

#define cublasStpmv mcblasStpmv
#define cublasDtpmv mcblasDtpmv
#define cublasCtpmv mcblasCtpmv
#define cublasZtpmv mcblasZtpmv

#define cublasStrsv mcblasStrsv
#define cublasDtrsv mcblasDtrsv
#define cublasCtrsv mcblasCtrsv
#define cublasZtrsv mcblasZtrsv

#define cublasStpsv mcblasStpsv
#define cublasDtpsv mcblasDtpsv
#define cublasCtpsv mcblasCtpsv
#define cublasZtpsv mcblasZtpsv

#define cublasStbsv mcblasStbsv
#define cublasDtbsv mcblasDtbsv
#define cublasCtbsv mcblasCtbsv
#define cublasZtbsv mcblasZtbsv

#define cublasSsymv mcblasSsymv
#define cublasDsymv mcblasDsymv
#define cublasCsymv mcblasCsymv
#define cublasZsymv mcblasZsymv
#define cublasChemv mcblasChemv
#define cublasZhemv mcblasZhemv

#define cublasSsbmv mcblasSsbmv
#define cublasDsbmv mcblasDsbmv
#define cublasChbmv mcblasChbmv
#define cublasZhbmv mcblasZhbmv

#define cublasSspmv mcblasSspmv
#define cublasDspmv mcblasDspmv
#define cublasChpmv mcblasChpmv
#define cublasZhpmv mcblasZhpmv

#define cublasSger mcblasSger
#define cublasDger mcblasDger
#define cublasCgeru mcblasCgeru
#define cublasCgerc mcblasCgerc
#define cublasZgeru mcblasZgeru
#define cublasZgerc mcblasZgerc

#define cublasSsyr mcblasSsyr
#define cublasDsyr mcblasDsyr
#define cublasCsyr mcblasCsyr
#define cublasZsyr mcblasZsyr
#define cublasCher mcblasCher
#define cublasZher mcblasZher

#define cublasSspr mcblasSspr
#define cublasDspr mcblasDspr
#define cublasChpr mcblasChpr
#define cublasZhpr mcblasZhpr

#define cublasSsyr2 mcblasSsyr2
#define cublasDsyr2 mcblasDsyr2
#define cublasCsyr2 mcblasCsyr2
#define cublasZsyr2 mcblasZsyr2
#define cublasCher2 mcblasCher2
#define cublasZher2 mcblasZher2

#define cublasSspr2 mcblasSspr2
#define cublasDspr2 mcblasDspr2
#define cublasChpr2 mcblasChpr2
#define cublasZhpr2 mcblasZhpr2

/* Blas3 Routines   */
#define cublasSgemm mcblasSgemm
#define cublasDgemm mcblasDgemm
#define cublasCgemm mcblasCgemm
#define cublasZgemm mcblasZgemm

#define cublasSsyrk mcblasSsyrk
#define cublasDsyrk mcblasDsyrk
#define cublasCsyrk mcblasCsyrk
#define cublasZsyrk mcblasZsyrk
#define cublasCherk mcblasCherk
#define cublasZherk mcblasZherk

#define cublasSsyr2k mcblasSsyr2k
#define cublasDsyr2k mcblasDsyr2k
#define cublasCsyr2k mcblasCsyr2k
#define cublasZsyr2k mcblasZsyr2k
#define cublasCher2k mcblasCher2k
#define cublasZher2k mcblasZher2k

#define cublasSsymm mcblasSsymm
#define cublasDsymm mcblasDsymm
#define cublasCsymm mcblasCsymm
#define cublasZsymm mcblasZsymm
#define cublasChemm mcblasChemm
#define cublasZhemm mcblasZhemm

#define cublasStrsm mcblasStrsm
#define cublasDtrsm mcblasDtrsm
#define cublasCtrsm mcblasCtrsm
#define cublasZtrsm mcblasZtrsm

#define cublasStrmm mcblasStrmm
#define cublasDtrmm mcblasDtrmm
#define cublasCtrmm mcblasCtrmm
#define cublasZtrmm mcblasZtrmm
