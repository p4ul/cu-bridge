#ifndef MCBLAS_MCBLASXT_H_
#define MCBLAS_MCBLASXT_H_

#include "mcblas/mcblas.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

struct mcblasXtContext;
typedef struct mcblasXtContext* mcblasXtHandle_t;

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCreate(mcblasXtHandle_t* handle) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
static MCBLASAPIENTRY mcblasStatus_t mcblasXtDestroy(mcblasXtHandle_t handle) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
static MCBLASAPIENTRY mcblasStatus_t mcblasXtGetNumBoards(int nbDevices, int deviceId[], int* nbBoards) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
static MCBLASAPIENTRY mcblasStatus_t mcblasXtMaxBoards(int* nbGpuBoards) { return MCBLAS_STATUS_NOT_SUPPORTED; }
/* This routine selects the Gpus that the user want to use for MCBLAS-XT */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtDeviceSelect(mcblasXtHandle_t handle,
                                                   int nbDevices,
                                                   int deviceId[]) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* This routine allows to change the dimension of the tiles ( blockDim x blockDim ) */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSetBlockDim(mcblasXtHandle_t handle, int blockDim) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
static MCBLASAPIENTRY mcblasStatus_t mcblasXtGetBlockDim(mcblasXtHandle_t handle, int* blockDim) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

typedef enum {
    MCBLASXT_PINNING_DISABLED = 0,
    MCBLASXT_PINNING_ENABLED = 1
} mcblasXtPinnedMemMode_t;
/* This routine allows to MCBLAS-XT to pin the Host memory if it find out that some of the matrix
   passed are not pinned : Pinning/Unpinning the Host memory is still a costly operation It is
   better if the user controls the memory on its own (by pinning/unpinning oly when necessary)
*/
static MCBLASAPIENTRY mcblasStatus_t mcblasXtGetPinningMemMode(mcblasXtHandle_t handle,
                                                        mcblasXtPinnedMemMode_t* mode) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSetPinningMemMode(mcblasXtHandle_t handle,
                                                        mcblasXtPinnedMemMode_t mode) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* This routines is to provide a CPU Blas routines, used for too small sizes or hybrid computation
 */
typedef enum {
    MCBLASXT_FLOAT = 0,
    MCBLASXT_DOUBLE = 1,
    MCBLASXT_COMPLEX = 2,
    MCBLASXT_DOUBLECOMPLEX = 3,
} mcblasXtOpType_t;

typedef enum {
    MCBLASXT_GEMM = 0,
    MCBLASXT_SYRK = 1,
    MCBLASXT_HERK = 2,
    MCBLASXT_SYMM = 3,
    MCBLASXT_HEMM = 4,
    MCBLASXT_TRSM = 5,
    MCBLASXT_SYR2K = 6,
    MCBLASXT_HER2K = 7,

    MCBLASXT_SPMM = 8,
    MCBLASXT_SYRKX = 9,
    MCBLASXT_HERKX = 10,
    MCBLASXT_TRMM = 11,
    MCBLASXT_ROUTINE_MAX = 12,
} mcblasXtBlasOp_t;

/* Currently only 32-bit integer BLAS routines are supported */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSetCpuRoutine(mcblasXtHandle_t handle,
                                                    mcblasXtBlasOp_t blasOp,
                                                    mcblasXtOpType_t type,
                                                    void* blasFunctor) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* Specified the percentage of work that should done by the CPU, default is 0 (no work) */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSetCpuRatio(mcblasXtHandle_t handle,
                                                  mcblasXtBlasOp_t blasOp,
                                                  mcblasXtOpType_t type,
                                                  float ratio) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* GEMM */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSgemm(mcblasXtHandle_t handle,
                                            mcblasOperation_t transa,
                                            mcblasOperation_t transb,
                                            size_t m,
                                            size_t n,
                                            size_t k,
                                            const float* alpha,
                                            const float* A,
                                            size_t lda,
                                            const float* B,
                                            size_t ldb,
                                            const float* beta,
                                            float* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDgemm(mcblasXtHandle_t handle,
                                            mcblasOperation_t transa,
                                            mcblasOperation_t transb,
                                            size_t m,
                                            size_t n,
                                            size_t k,
                                            const double* alpha,
                                            const double* A,
                                            size_t lda,
                                            const double* B,
                                            size_t ldb,
                                            const double* beta,
                                            double* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCgemm(mcblasXtHandle_t handle,
                                            mcblasOperation_t transa,
                                            mcblasOperation_t transb,
                                            size_t m,
                                            size_t n,
                                            size_t k,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const mcComplex* B,
                                            size_t ldb,
                                            const mcComplex* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZgemm(mcblasXtHandle_t handle,
                                            mcblasOperation_t transa,
                                            mcblasOperation_t transb,
                                            size_t m,
                                            size_t n,
                                            size_t k,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const mcDoubleComplex* B,
                                            size_t ldb,
                                            const mcDoubleComplex* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* ------------------------------------------------------- */
/* SYRK */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSsyrk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const float* alpha,
                                            const float* A,
                                            size_t lda,
                                            const float* beta,
                                            float* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDsyrk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const double* alpha,
                                            const double* A,
                                            size_t lda,
                                            const double* beta,
                                            double* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCsyrk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const mcComplex* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZsyrk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const mcDoubleComplex* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* HERK */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtCherk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const float* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const float* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZherk(mcblasXtHandle_t handle,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            size_t n,
                                            size_t k,
                                            const double* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const double* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* SYR2K */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSsyr2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const float* alpha,
                                             const float* A,
                                             size_t lda,
                                             const float* B,
                                             size_t ldb,
                                             const float* beta,
                                             float* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDsyr2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const double* alpha,
                                             const double* A,
                                             size_t lda,
                                             const double* B,
                                             size_t ldb,
                                             const double* beta,
                                             double* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCsyr2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcComplex* alpha,
                                             const mcComplex* A,
                                             size_t lda,
                                             const mcComplex* B,
                                             size_t ldb,
                                             const mcComplex* beta,
                                             mcComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZsyr2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcDoubleComplex* alpha,
                                             const mcDoubleComplex* A,
                                             size_t lda,
                                             const mcDoubleComplex* B,
                                             size_t ldb,
                                             const mcDoubleComplex* beta,
                                             mcDoubleComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* HERKX : variant extension of HERK */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtCherkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcComplex* alpha,
                                             const mcComplex* A,
                                             size_t lda,
                                             const mcComplex* B,
                                             size_t ldb,
                                             const float* beta,
                                             mcComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZherkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcDoubleComplex* alpha,
                                             const mcDoubleComplex* A,
                                             size_t lda,
                                             const mcDoubleComplex* B,
                                             size_t ldb,
                                             const double* beta,
                                             mcDoubleComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* -------------------------------------------------------------------- */
/* TRSM */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtStrsm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const float* alpha,
                                            const float* A,
                                            size_t lda,
                                            float* B,
                                            size_t ldb) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDtrsm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const double* alpha,
                                            const double* A,
                                            size_t lda,
                                            double* B,
                                            size_t ldb) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCtrsm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            mcComplex* B,
                                            size_t ldb) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZtrsm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            mcDoubleComplex* B,
                                            size_t ldb) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* SYMM : Symmetric Multiply Matrix*/
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSsymm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const float* alpha,
                                            const float* A,
                                            size_t lda,
                                            const float* B,
                                            size_t ldb,
                                            const float* beta,
                                            float* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDsymm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const double* alpha,
                                            const double* A,
                                            size_t lda,
                                            const double* B,
                                            size_t ldb,
                                            const double* beta,
                                            double* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCsymm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const mcComplex* B,
                                            size_t ldb,
                                            const mcComplex* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZsymm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const mcDoubleComplex* B,
                                            size_t ldb,
                                            const mcDoubleComplex* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* HEMM : Hermitian Matrix Multiply */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtChemm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const mcComplex* B,
                                            size_t ldb,
                                            const mcComplex* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZhemm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const mcDoubleComplex* B,
                                            size_t ldb,
                                            const mcDoubleComplex* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* -------------------------------------------------------------------- */
/* SYRKX : variant extension of SYRK  */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSsyrkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const float* alpha,
                                             const float* A,
                                             size_t lda,
                                             const float* B,
                                             size_t ldb,
                                             const float* beta,
                                             float* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDsyrkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const double* alpha,
                                             const double* A,
                                             size_t lda,
                                             const double* B,
                                             size_t ldb,
                                             const double* beta,
                                             double* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCsyrkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcComplex* alpha,
                                             const mcComplex* A,
                                             size_t lda,
                                             const mcComplex* B,
                                             size_t ldb,
                                             const mcComplex* beta,
                                             mcComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZsyrkx(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcDoubleComplex* alpha,
                                             const mcDoubleComplex* A,
                                             size_t lda,
                                             const mcDoubleComplex* B,
                                             size_t ldb,
                                             const mcDoubleComplex* beta,
                                             mcDoubleComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}
/* -------------------------------------------------------------------- */
/* HER2K : variant extension of HERK  */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtCher2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcComplex* alpha,
                                             const mcComplex* A,
                                             size_t lda,
                                             const mcComplex* B,
                                             size_t ldb,
                                             const float* beta,
                                             mcComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZher2k(mcblasXtHandle_t handle,
                                             mcblasFillMode_t uplo,
                                             mcblasOperation_t trans,
                                             size_t n,
                                             size_t k,
                                             const mcDoubleComplex* alpha,
                                             const mcDoubleComplex* A,
                                             size_t lda,
                                             const mcDoubleComplex* B,
                                             size_t ldb,
                                             const double* beta,
                                             mcDoubleComplex* C,
                                             size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* -------------------------------------------------------------------- */
/* SPMM : Symmetric Packed Multiply Matrix*/
static MCBLASAPIENTRY mcblasStatus_t mcblasXtSspmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const float* alpha,
                                            const float* AP,
                                            const float* B,
                                            size_t ldb,
                                            const float* beta,
                                            float* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDspmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const double* alpha,
                                            const double* AP,
                                            const double* B,
                                            size_t ldb,
                                            const double* beta,
                                            double* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCspmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcComplex* alpha,
                                            const mcComplex* AP,
                                            const mcComplex* B,
                                            size_t ldb,
                                            const mcComplex* beta,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZspmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            size_t m,
                                            size_t n,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* AP,
                                            const mcDoubleComplex* B,
                                            size_t ldb,
                                            const mcDoubleComplex* beta,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

/* -------------------------------------------------------------------- */
/* TRMM */
static MCBLASAPIENTRY mcblasStatus_t mcblasXtStrmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const float* alpha,
                                            const float* A,
                                            size_t lda,
                                            const float* B,
                                            size_t ldb,
                                            float* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtDtrmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const double* alpha,
                                            const double* A,
                                            size_t lda,
                                            const double* B,
                                            size_t ldb,
                                            double* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtCtrmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const mcComplex* alpha,
                                            const mcComplex* A,
                                            size_t lda,
                                            const mcComplex* B,
                                            size_t ldb,
                                            mcComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

static MCBLASAPIENTRY mcblasStatus_t mcblasXtZtrmm(mcblasXtHandle_t handle,
                                            mcblasSideMode_t side,
                                            mcblasFillMode_t uplo,
                                            mcblasOperation_t trans,
                                            mcblasDiagType_t diag,
                                            size_t m,
                                            size_t n,
                                            const mcDoubleComplex* alpha,
                                            const mcDoubleComplex* A,
                                            size_t lda,
                                            const mcDoubleComplex* B,
                                            size_t ldb,
                                            mcDoubleComplex* C,
                                            size_t ldc) {
    return MCBLAS_STATUS_NOT_SUPPORTED;
}

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif  // MCBLAS_MCBLASXT_H_
