#pragma once

// header define
#if !defined(CUBLAS_XT_H_)
#define CUBLAS_XT_H_
#endif /* !defined(CUBLAS_XT_H_) */

// enum redefine
#define CUBLASXT_PINNING_DISABLED MCBLASXT_PINNING_DISABLED
#define CUBLASXT_PINNING_ENABLED MCBLASXT_PINNING_ENABLED
#define CUBLASXT_FLOAT MCBLASXT_FLOAT
#define CUBLASXT_DOUBLE MCBLASXT_DOUBLE
#define CUBLASXT_COMPLEX MCBLASXT_COMPLEX
#define CUBLASXT_DOUBLECOMPLEX MCBLASXT_DOUBLECOMPLEX
#define CUBLASXT_GEMM MCBLASXT_GEMM
#define CUBLASXT_SYRK MCBLASXT_SYRK
#define CUBLASXT_HERK MCBLASXT_HERK
#define CUBLASXT_SYMM MCBLASXT_SYMM
#define CUBLASXT_HEMM MCBLASXT_HEMM
#define CUBLASXT_TRSM MCBLASXT_TRSM
#define CUBLASXT_SYR2K MCBLASXT_SYR2K
#define CUBLASXT_HER2K MCBLASXT_HER2K
#define CUBLASXT_SPMM MCBLASXT_SPMM
#define CUBLASXT_SYRKX MCBLASXT_SYRKX
#define CUBLASXT_HERKX MCBLASXT_HERKX
#define CUBLASXT_TRMM MCBLASXT_TRMM
#define CUBLASXT_ROUTINE_MAX MCBLASXT_ROUTINE_MAX

// class redefine
#define cublasXtContext mcblasXtContext
#define cublasXtHandle_t mcblasXtHandle_t
#define cublasXtPinnedMemMode_t mcblasXtPinnedMemMode_t
#define cublasXtOpType_t mcblasXtOpType_t
#define cublasXtBlasOp_t mcblasXtBlasOp_t

// func redefine
#define cublasXtCreate mcblasXtCreate
#define cublasXtDestroy mcblasXtDestroy
#define cublasXtGetNumBoards mcblasXtGetNumBoards
#define cublasXtMaxBoards mcblasXtMaxBoards
#define cublasXtDeviceSelect mcblasXtDeviceSelect
#define cublasXtSetBlockDim mcblasXtSetBlockDim
#define cublasXtGetBlockDim mcblasXtGetBlockDim
#define cublasXtGetPinningMemMode mcblasXtGetPinningMemMode
#define cublasXtSetPinningMemMode mcblasXtSetPinningMemMode
#define cublasXtSetCpuRoutine mcblasXtSetCpuRoutine
#define cublasXtSetCpuRatio mcblasXtSetCpuRatio
#define cublasXtSgemm mcblasXtSgemm
#define cublasXtDgemm mcblasXtDgemm
#define cublasXtCgemm mcblasXtCgemm
#define cublasXtZgemm mcblasXtZgemm
#define cublasXtSsyrk mcblasXtSsyrk
#define cublasXtDsyrk mcblasXtDsyrk
#define cublasXtCsyrk mcblasXtCsyrk
#define cublasXtZsyrk mcblasXtZsyrk
#define cublasXtCherk mcblasXtCherk
#define cublasXtZherk mcblasXtZherk
#define cublasXtSsyr2k mcblasXtSsyr2k
#define cublasXtDsyr2k mcblasXtDsyr2k
#define cublasXtCsyr2k mcblasXtCsyr2k
#define cublasXtZsyr2k mcblasXtZsyr2k
#define cublasXtCherkx mcblasXtCherkx
#define cublasXtZherkx mcblasXtZherkx
#define cublasXtStrsm mcblasXtStrsm
#define cublasXtDtrsm mcblasXtDtrsm
#define cublasXtCtrsm mcblasXtCtrsm
#define cublasXtZtrsm mcblasXtZtrsm
#define cublasXtSsymm mcblasXtSsymm
#define cublasXtDsymm mcblasXtDsymm
#define cublasXtCsymm mcblasXtCsymm
#define cublasXtZsymm mcblasXtZsymm
#define cublasXtChemm mcblasXtChemm
#define cublasXtZhemm mcblasXtZhemm
#define cublasXtSsyrkx mcblasXtSsyrkx
#define cublasXtDsyrkx mcblasXtDsyrkx
#define cublasXtCsyrkx mcblasXtCsyrkx
#define cublasXtZsyrkx mcblasXtZsyrkx
#define cublasXtCher2k mcblasXtCher2k
#define cublasXtZher2k mcblasXtZher2k
#define cublasXtSspmm mcblasXtSspmm
#define cublasXtDspmm mcblasXtDspmm
#define cublasXtCspmm mcblasXtCspmm
#define cublasXtZspmm mcblasXtZspmm
#define cublasXtStrmm mcblasXtStrmm
#define cublasXtDtrmm mcblasXtDtrmm
#define cublasXtCtrmm mcblasXtCtrmm
#define cublasXtZtrmm mcblasXtZtrmm

// others redefine
