#pragma once

// NOTE: format this file with "ColumnLimit:     200" in .clang-format to make macro defination more
// readable
#include "bridge/runtime/cuda_driver_types.h"
#include "bridge/runtime/cuda_to_maca_mcr_adaptor.h"

////////////////////////////////////////////////////////////////
// COMMON TYPE
////////////////////////////////////////////////////////////////
#define NVJPEGAPI MCJPEGAPI

#define NVJPEG_MAX_COMPONENT MCJPEG_MAX_COMPONENT

#define NVJPEG_VER_MAJOR MCJPEG_VER_MAJOR
#define NVJPEG_VER_MINOR MCJPEG_VER_MINOR
#define NVJPEG_VER_PATCH MCJPEG_VER_PATCH
#define NVJPEG_VER_BUILD MCJPEG_VER_BUILD

/* Enum */
#define nvjpegStatus_t							   mcjpegStatus_t
#define NVJPEG_STATUS_SUCCESS					   MCJPEG_STATUS_SUCCESS
#define NVJPEG_STATUS_NOT_INITIALIZED			   MCJPEG_STATUS_NOT_INITIALIZED
#define NVJPEG_STATUS_INVALID_PARAMETER			   MCJPEG_STATUS_INVALID_PARAMETER
#define NVJPEG_STATUS_BAD_JPEG					   MCJPEG_STATUS_BAD_JPEG
#define NVJPEG_STATUS_JPEG_NOT_SUPPORTED		   MCJPEG_STATUS_JPEG_NOT_SUPPORTED
#define NVJPEG_STATUS_ALLOCATOR_FAILURE			   MCJPEG_STATUS_ALLOCATOR_FAILURE
#define NVJPEG_STATUS_EXECUTION_FAILED			   MCJPEG_STATUS_EXECUTION_FAILED
#define NVJPEG_STATUS_ARCH_MISMATCH				   MCJPEG_STATUS_ARCH_MISMATCH
#define NVJPEG_STATUS_INTERNAL_ERROR			   MCJPEG_STATUS_INTERNAL_ERROR
#define NVJPEG_STATUS_IMPLEMENTATION_NOT_SUPPORTED MCJPEG_STATUS_IMPLEMENTATION_NOT_SUPPORTED
#define NVJPEG_STATUS_INCOMPLETE_BITSTREAM		   MCJPEG_STATUS_INCOMPLETE_BITSTREAM

#define nvjpegExifOrientation_t			   mcjpegExifOrientation_t
#define NVJPEG_ORIENTATION_UNKNOWN		   MCJPEG_ORIENTATION_UNKNOWN
#define NVJPEG_ORIENTATION_NORMAL		   MCJPEG_ORIENTATION_NORMAL
#define NVJPEG_ORIENTATION_FLIP_HORIZONTAL MCJPEG_ORIENTATION_FLIP_HORIZONTAL
#define NVJPEG_ORIENTATION_ROTATE_180	   MCJPEG_ORIENTATION_ROTATE_180
#define NVJPEG_ORIENTATION_FLIP_VERTICAL   MCJPEG_ORIENTATION_FLIP_VERTICAL
#define NVJPEG_ORIENTATION_TRANSPOSE	   MCJPEG_ORIENTATION_TRANSPOSE
#define NVJPEG_ORIENTATION_ROTATE_90	   MCJPEG_ORIENTATION_ROTATE_90
#define NVJPEG_ORIENTATION_TRANSVERSE	   MCJPEG_ORIENTATION_TRANSVERSE
#define NVJPEG_ORIENTATION_ROTATE_270	   MCJPEG_ORIENTATION_ROTATE_270

#define nvjpegChromaSubsampling_t mcjpegChromaSubsampling_t
#define NVJPEG_CSS_444			  MCJPEG_CSS_444
#define NVJPEG_CSS_422			  MCJPEG_CSS_422
#define NVJPEG_CSS_420			  MCJPEG_CSS_420
#define NVJPEG_CSS_440			  MCJPEG_CSS_440
#define NVJPEG_CSS_411			  MCJPEG_CSS_411
#define NVJPEG_CSS_410			  MCJPEG_CSS_410
#define NVJPEG_CSS_GRAY			  MCJPEG_CSS_GRAY
#define NVJPEG_CSS_410V			  MCJPEG_CSS_410V
#define NVJPEG_CSS_UNKNOWN		  MCJPEG_CSS_UNKNOWN

#define nvjpegOutputFormat_t	 mcjpegOutputFormat_t
#define NVJPEG_OUTPUT_UNCHANGED	 MCJPEG_OUTPUT_UNCHANGED
#define NVJPEG_OUTPUT_YUV		 MCJPEG_OUTPUT_YUV
#define NVJPEG_OUTPUT_Y			 MCJPEG_OUTPUT_Y
#define NVJPEG_OUTPUT_RGB		 MCJPEG_OUTPUT_RGB
#define NVJPEG_OUTPUT_BGR		 MCJPEG_OUTPUT_BGR
#define NVJPEG_OUTPUT_RGBI		 MCJPEG_OUTPUT_RGBI
#define NVJPEG_OUTPUT_BGRI		 MCJPEG_OUTPUT_BGRI
#define NVJPEG_OUTPUT_FORMAT_MAX MCJPEG_OUTPUT_FORMAT_MAX

#define nvjpegInputFormat_t mcjpegInputFormat_t
#define NVJPEG_INPUT_RGB	MCJPEG_INPUT_RGB
#define NVJPEG_INPUT_BGR	MCJPEG_INPUT_BGR
#define NVJPEG_INPUT_RGBI	MCJPEG_INPUT_RGBI
#define NVJPEG_INPUT_BGRI	MCJPEG_INPUT_BGRI

#define nvjpegBackend_t					 mcjpegBackend_t
#define NVJPEG_BACKEND_DEFAULT			 MCJPEG_BACKEND_DEFAULT
#define NVJPEG_BACKEND_HYBRID			 MCJPEG_BACKEND_HYBRID
#define NVJPEG_BACKEND_GPU_HYBRID		 MCJPEG_BACKEND_GPU_HYBRID
#define NVJPEG_BACKEND_HARDWARE			 MCJPEG_BACKEND_HARDWARE
#define NVJPEG_BACKEND_GPU_HYBRID_DEVICE MCJPEG_BACKEND_GPU_HYBRID_DEVICE
#define NVJPEG_BACKEND_HARDWARE_DEVICE	 MCJPEG_BACKEND_HARDWARE_DEVICE
#define NVJPEG_BACKEND_LOSSLESS_JPEG	 MCJPEG_BACKEND_LOSSLESS_JPEG

#define NVJPEG_FLAGS_DEFAULT						 MCJPEG_FLAGS_DEFAULT
#define NVJPEG_FLAGS_HW_DECODE_NO_PIPELINE			 MCJPEG_FLAGS_HW_DECODE_NO_PIPELINE
#define NVJPEG_FLAGS_ENABLE_MEMORY_POOLS			 MCJPEG_FLAGS_ENABLE_MEMORY_POOLS
#define NVJPEG_FLAGS_BITSTREAM_STRICT				 MCJPEG_FLAGS_BITSTREAM_STRICT
#define NVJPEG_FLAGS_REDUCED_MEMORY_DECODE			 MCJPEG_FLAGS_REDUCED_MEMORY_DECODE
#define NVJPEG_FLAGS_REDUCED_MEMORY_DECODE_ZERO_COPY MCJPEG_FLAGS_REDUCED_MEMORY_DECODE_ZERO_COPY
#define NVJPEG_FLAGS_UPSAMPLING_WITH_INTERPOLATION	 MCJPEG_FLAGS_UPSAMPLING_WITH_INTERPOLATION

#define nvjpegScaleFactor_t mcjpegScaleFactor_t
#define NVJPEG_SCALE_NONE	MCJPEG_SCALE_NONE
#define NVJPEG_SCALE_1_BY_2 MCJPEG_SCALE_1_BY_2
#define NVJPEG_SCALE_1_BY_4 MCJPEG_SCALE_1_BY_4
#define NVJPEG_SCALE_1_BY_8 MCJPEG_SCALE_1_BY_8

#define nvjpegJpegEncoding_t					mcjpegJpegEncoding_t
#define NVJPEG_ENCODING_UNKNOWN					MCJPEG_ENCODING_UNKNOWN
#define NVJPEG_ENCODING_BASELINE_DCT			MCJPEG_ENCODING_BASELINE_DCT
#define NVJPEG_ENCODING_PROGRESSIVE_DCT_HUFFMAN MCJPEG_ENCODING_PROGRESSIVE_DCT_HUFFMAN
#define NVJPEG_ENCODING_EXTENDED_SEQUENTIAL_DCT_HUFFMAN                                            \
	MCJPEG_ENCODING_EXTENDED_SEQUENTIAL_DCT_HUFFMAN

/**End Enum**/

/** Struct **/
#define nvjpegImage_t			  mcjpegImage_t
#define nvjpegDevAllocator_t	  mcjpegDevAllocator_t
#define nvjpegPinnedAllocator_t	  mcjpegPinnedAllocator_t
#define nvjpegDevAllocatorV2_t	  mcjpegDevAllocatorV2_t
#define nvjpegPinnedAllocatorV2_t mcjpegPinnedAllocatorV2_t
#define nvjpegHandle_t			  mcjpegHandle_t
#define nvjpegJpegState_t		  mcjpegJpegState_t
#define nvjpegEncoderState_t	  mcjpegEncoderState_t
#define nvjpegEncoderParams_t	  mcjpegEncoderParams_t
#define nvjpegDecodeParams_t	  mcjpegDecodeParams_t
#define nvjpegJpegDecoder_t		  mcjpegJpegDecoder_t
#define nvjpegJpegStream_t		  mcjpegJpegStream_t
#define nvjpegBufferPinned_t	  mcjpegBufferPinned_t
#define nvjpegBufferDevice_t	  mcjpegBufferDevice_t
/**End Struct**/

/* API */
#define nvjpegGetProperty		mcjpegGetProperty
#define nvjpegGetCudartProperty mcjpegGetMacartProperty
#define nvjpegCreate			mcjpegCreate
#define nvjpegCreateEx			mcjpegCreateEx
#define nvjpegCreateExV2		mcjpegCreateExV2
#define nvjpegCreateSimple		mcjpegCreateSimple
#define nvjpegDestroy			mcjpegDestroy
/*Buffer*/
#define nvjpegBufferPinnedCreate	  mcjpegBufferPinnedCreate
#define nvjpegBufferPinnedCreateV2	  mcjpegBufferPinnedCreateV2
#define nvjpegBufferPinnedDestroy	  mcjpegBufferPinnedDestroy
#define nvjpegBufferDeviceCreate	  mcjpegBufferDeviceCreate
#define nvjpegBufferDeviceCreateV2	  mcjpegBufferDeviceCreateV2
#define nvjpegBufferDeviceDestroy	  mcjpegBufferDeviceDestroy
#define nvjpegBufferPinnedRetrieve	  mcjpegBufferPinnedRetrieve
#define nvjpegBufferDeviceRetrieve	  mcjpegBufferDeviceRetrieve
#define nvjpegStateAttachPinnedBuffer mcjpegStateAttachPinnedBuffer
#define nvjpegStateAttachDeviceBuffer mcjpegStateAttachDeviceBuffer
/*Decode*/
#define nvjpegJpegStateCreate				   mcjpegJpegStateCreate
#define nvjpegJpegStateDestroy				   mcjpegJpegStateDestroy
#define nvjpegGetImageInfo					   mcjpegGetImageInfo
#define nvjpegGetHardwareDecoderInfo		   mcjpegGetHardwareDecoderInfo
#define nvjpegDecode						   mcjpegDecode
#define nvjpegDecoderCreate					   mcjpegDecoderCreate
#define nvjpegDecoderDestroy				   mcjpegDecoderDestroy
#define nvjpegDecoderJpegSupported			   mcjpegDecoderJpegSupported
#define nvjpegDecoderStateCreate			   mcjpegDecoderStateCreate
#define nvjpegDecodeParamsCreate			   mcjpegDecodeParamsCreate
#define nvjpegDecodeParamsDestroy			   mcjpegDecodeParamsDestroy
#define nvjpegDecodeParamsSetOutputFormat	   mcjpegDecodeParamsSetOutputFormat
#define nvjpegDecodeParamsSetROI			   mcjpegDecodeParamsSetROI
#define nvjpegDecodeParamsSetAllowCMYK		   mcjpegDecodeParamsSetAllowCMYK
#define nvjpegDecodeParamsSetScaleFactor	   mcjpegDecodeParamsSetScaleFactor
#define nvjpegDecodeParamsSetExifOrientation   mcjpegDecodeParamsSetExifOrientation
#define nvjpegJpegStreamCreate				   mcjpegJpegStreamCreate
#define nvjpegJpegStreamDestroy				   mcjpegJpegStreamDestroy
#define nvjpegJpegStreamParse				   mcjpegJpegStreamParse
#define nvjpegJpegStreamParseHeader			   mcjpegJpegStreamParseHeader
#define nvjpegJpegStreamParseTables			   mcjpegJpegStreamParseTables
#define nvjpegJpegStreamGetJpegEncoding		   mcjpegJpegStreamGetJpegEncoding
#define nvjpegJpegStreamGetFrameDimensions	   mcjpegJpegStreamGetFrameDimensions
#define nvjpegJpegStreamGetComponentsNum	   mcjpegJpegStreamGetComponentsNum
#define nvjpegJpegStreamGetComponentDimensions mcjpegJpegStreamGetComponentDimensions
#define nvjpegJpegStreamGetExifOrientation	   mcjpegJpegStreamGetExifOrientation
#define nvjpegJpegStreamGetChromaSubsampling   mcjpegJpegStreamGetChromaSubsampling
#define nvjpegSetDeviceMemoryPadding		   mcjpegSetDeviceMemoryPadding
#define nvjpegGetDeviceMemoryPadding		   mcjpegGetDeviceMemoryPadding
#define nvjpegSetPinnedMemoryPadding		   mcjpegSetPinnedMemoryPadding
#define nvjpegGetPinnedMemoryPadding		   mcjpegGetPinnedMemoryPadding
#define nvjpegDecodeJpeg					   mcjpegDecodeJpeg
#define nvjpegDecodeJpegHost				   mcjpegDecodeJpegHost
#define nvjpegDecodeJpegTransferToDevice	   mcjpegDecodeJpegTransferToDevice
#define nvjpegDecodeJpegDevice				   mcjpegDecodeJpegDevice
#define nvjpegDecodeBatchedEx				   mcjpegDecodeBatchedEx
#define nvjpegDecodeBatchedSupported		   mcjpegDecodeBatchedSupported
#define nvjpegDecodeBatchedSupportedEx		   mcjpegDecodeBatchedSupportedEx
#define nvjpegDecodeBatchedInitialize		   mcjpegDecodeBatchedInitialize
#define nvjpegDecodeBatched					   mcjpegDecodeBatched
#define nvjpegDecodeBatchedPreAllocate		   mcjpegDecodeBatchedPreAllocate
#define nvjpegDecodeBatchedParseJpegTables	   mcjpegDecodeBatchedParseJpegTables
/*Encode*/
#define nvjpegEncoderStateCreate			   mcjpegEncoderStateCreate
#define nvjpegEncoderStateDestroy			   mcjpegEncoderStateDestroy
#define nvjpegEncoderParamsCreate			   mcjpegEncoderParamsCreate
#define nvjpegEncoderParamsDestroy			   mcjpegEncoderParamsDestroy
#define nvjpegEncoderParamsSetQuality		   mcjpegEncoderParamsSetQuality
#define nvjpegEncoderParamsSetEncoding		   mcjpegEncoderParamsSetEncoding
#define nvjpegEncoderParamsSetOptimizedHuffman mcjpegEncoderParamsSetOptimizedHuffman
#define nvjpegEncoderParamsSetSamplingFactors  mcjpegEncoderParamsSetSamplingFactors
#define nvjpegEncodeGetBufferSize			   mcjpegEncodeGetBufferSize
#define nvjpegEncodeYUV						   mcjpegEncodeYUV
#define nvjpegEncodeImage					   mcjpegEncodeImage
#define nvjpegEncodeRetrieveBitstreamDevice	   mcjpegEncodeRetrieveBitstreamDevice
#define nvjpegEncodeRetrieveBitstream		   mcjpegEncodeRetrieveBitstream
/*JPEG Transcoding Functions*/
#define nvjpegEncoderParamsCopyMetadata			  mcjpegEncoderParamsCopyMetadata
#define nvjpegEncoderParamsCopyQuantizationTables mcjpegEncoderParamsCopyQuantizationTables
#define nvjpegEncoderParamsCopyHuffmanTables	  mcjpegEncoderParamsCopyHuffmanTables

/**End API**/