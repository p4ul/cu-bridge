#ifndef __NVML_WRAPPER_H__
#define  __NVML_WRAPPER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "MxSmlExtension.h"

// constant
#define NVML_DEVICE_PCI_BUS_ID_BUFFER_SIZE MXSMLEX_DBDF_ID_BUFFER_SIZE
#define NVML_DEVICE_PCI_BUS_ID_BUFFER_V2_SIZE MXSMLEX_DBDF_ID_BUFFER_V2_SIZE
#define NVML_DEVICE_NAME_V2_BUFFER_SIZE MXSMLEX_NAME_V2_BUFFER_SIZE
#define NVML_DEVICE_UUID_BUFFER_SIZE MXSMLEX_DEVICE_UUID_BUFFER_SIZE
#define NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE MXSMLEX_DRIVER_VERSION_BUFFER_SIZE
#define nvmlClocksThrottleReasonAll mxsmlExClocksThrottleReasonAll
#define nvmlClocksThrottleReasonApplicationsClocksSetting mxsmlExClocksThrottleReasonApplicationsClocksSetting
#define nvmlClocksThrottleReasonDisplayClockSetting mxsmlExClocksThrottleReasonDisplayClockSetting
#define nvmlClocksThrottleReasonGpuIdle mxsmlExClocksThrottleReasonGpuIdle
#define nvmlClocksThrottleReasonHwPowerBrakeSlowdown mxsmlExClocksThrottleReasonHwPowerBrakeSlowdown
#define nvmlClocksThrottleReasonHwSlowdown mxsmlExClocksThrottleReasonHwSlowdown
#define nvmlClocksThrottleReasonHwThermalSlowdown mxsmlExClocksThrottleReasonHwThermalSlowdown
#define nvmlClocksThrottleReasonNone mxsmlExClocksThrottleReasonNone
#define nvmlClocksThrottleReasonSwPowerCap mxsmlExClocksThrottleReasonSwPowerCap
#define nvmlClocksThrottleReasonSwThermalSlowdown mxsmlExClocksThrottleReasonSwThermalSlowdown
#define nvmlClocksThrottleReasonSyncBoost mxsmlExClocksThrottleReasonSyncBoost
#define nvmlClocksThrottleReasonUserDefinedClocks nvmlClocksThrottleReasonApplicationsClocksSetting

// enum
#define nvmlTemperatureThresholds_t mxsmlExTemperatureThresholds_t
#define NVML_TEMPERATURE_THRESHOLD_SHUTDOWN         MXSMLEX_TEMPERATURE_THRESHOLD_SHUTDOWN
#define NVML_TEMPERATURE_THRESHOLD_SLOWDOWN         MXSMLEX_TEMPERATURE_THRESHOLD_SLOWDOWN
#define NVML_TEMPERATURE_THRESHOLD_MEM_MAX          MXSMLEX_TEMPERATURE_THRESHOLD_MEM_MAX
#define NVML_TEMPERATURE_THRESHOLD_GPU_MAX          MXSMLEX_TEMPERATURE_THRESHOLD_GPU_MAX
#define NVML_TEMPERATURE_THRESHOLD_ACOUSTIC_MIN     MXSMLEX_TEMPERATURE_THRESHOLD_ACOUSTIC_MIN
#define NVML_TEMPERATURE_THRESHOLD_ACOUSTIC_CURR    MXSMLEX_TEMPERATURE_THRESHOLD_ACOUSTIC_CURR
#define NVML_TEMPERATURE_THRESHOLD_ACOUSTIC_MAX     MXSMLEX_TEMPERATURE_THRESHOLD_ACOUSTIC_MAX
#define NVML_TEMPERATURE_THRESHOLD_COUNT            MXSMLEX_TEMPERATURE_THRESHOLD_COUNT

#define nvmlReturn_t mxSmlExReturn_t
#define NVML_SUCCESS                            MXSMLEX_SUCCESS
#define NVML_ERROR_UNINITIALIZED                MXSMLEX_ERROR_UNINITIALIZED
#define NVML_ERROR_INVALID_ARGUMENT             MXSMLEX_ERROR_INVALID_ARGUMENT
#define NVML_ERROR_NOT_SUPPORTED                MXSMLEX_ERROR_NOT_SUPPORTED
#define NVML_ERROR_NO_PERMISSION                MXSMLEX_ERROR_NO_PERMISSION
#define NVML_ERROR_ALREADY_INITIALIZED          MXSMLEX_ERROR_ALREADY_INITIALIZED
#define NVML_ERROR_NOT_FOUND                    MXSMLEX_ERROR_NOT_FOUND
#define NVML_ERROR_INSUFFICIENT_SIZE            MXSMLEX_ERROR_INSUFFICIENT_SIZE
#define NVML_ERROR_INSUFFICIENT_POWER           MXSMLEX_ERROR_INSUFFICIENT_POWER
#define NVML_ERROR_DRIVER_NOT_LOADED            MXSMLEX_ERROR_DRIVER_NOT_LOADED
#define NVML_ERROR_TIMEOUT                      MXSMLEX_ERROR_TIMEOUT
#define NVML_ERROR_IRQ_ISSUE                    MXSMLEX_ERROR_IRQ_ISSUE
#define NVML_ERROR_LIBRARY_NOT_FOUND            MXSMLEX_ERROR_LIBRARY_NOT_FOUND
#define NVML_ERROR_FUNCTION_NOT_FOUND           MXSMLEX_ERROR_FUNCTION_NOT_FOUND
#define NVML_ERROR_CORRUPTED_INFOROM            MXSMLEX_ERROR_CORRUPTED_INFOROM
#define NVML_ERROR_GPU_IS_LOST                  MXSMLEX_ERROR_GPU_IS_LOST
#define NVML_ERROR_RESET_REQUIRED               MXSMLEX_ERROR_RESET_REQUIRED
#define NVML_ERROR_OPERATING_SYSTEM             MXSMLEX_ERROR_OPERATING_SYSTEM
#define NVML_ERROR_LIB_RM_VERSION_MISMATCH      MXSMLEX_ERROR_LIB_RM_VERSION_MISMATCH
#define NVML_ERROR_IN_USE                       MXSMLEX_ERROR_IN_USE
#define NVML_ERROR_MEMORY                       MXSMLEX_ERROR_MEMORY
#define NVML_ERROR_NO_DATA                      MXSMLEX_ERROR_NO_DATA
#define NVML_ERROR_VGPU_ECC_NOT_SUPPORTED       MXSMLEX_ERROR_VGPU_ECC_NOT_SUPPORTED
#define NVML_ERROR_INSUFFICIENT_RESOURCES       MXSMLEX_ERROR_INSUFFICIENT_RESOURCES
#define NVML_ERROR_FREQ_NOT_SUPPORTED           MXSMLEX_ERROR_FREQ_NOT_SUPPORTED
#define NVML_ERROR_ARGUMENT_VERSION_MISMATCH    MXSMLEX_ERROR_ARGUMENT_VERSION_MISMATCH
#define NVML_ERROR_DEPRECATED                   MXSMLEX_ERROR_DEPRECATED
#define NVML_ERROR_UNKNOWN                      MXSMLEX_ERROR_UNKNOWN

#define nvmlTemperatureSensors_t mxsmlExTemperatureSensors_t
#define NVML_TEMPERATURE_GPU        MXSMLEX_TEMPERATURE_GPU
#define NVML_TEMPERATURE_COUNT      MXSMLEX_TEMPERATURE_COUNT

#define nvmlPcieUtilCounter_t mxsmlExPcieUtilCounter_t
#define NVML_PCIE_UTIL_TX_BYTES     MXSMLEX_PCIE_UTIL_TX_BYTES
#define NVML_PCIE_UTIL_RX_BYTES     MXSMLEX_PCIE_UTIL_RX_BYTES
#define NVML_PCIE_UTIL_COUNT        MXSMLEX_PCIE_UTIL_COUNT

#define nvmlClockType_t mxsmlExClockType_t
#define NVML_CLOCK_GRAPHICS         MXSMLEX_CLOCK_GRAPHICS
#define NVML_CLOCK_SM               MXSMLEX_CLOCK_SM
#define NVML_CLOCK_MEM              MXSMLEX_CLOCK_MEM
#define NVML_CLOCK_VIDEO            MXSMLEX_CLOCK_VIDEO
#define NVML_CLOCK_COUNT            MXSMLEX_CLOCK_COUNT

#define nvmlEnableState_t mxSmlExEnableState_t
#define NVML_FEATURE_DISABLED       MXSMLEX_FEATURE_DISABLED
#define NVML_FEATURE_ENABLED        MXSMLEX_FEATURE_ENABLED

#define nvmlGpuP2PStatus_t mxSmlExGpuP2PStatus_t
#define NVML_P2P_STATUS_OK                            MXSMLEX_P2P_STATUS_OK
#define NVML_P2P_STATUS_CHIPSET_NOT_SUPPORED          MXSMLEX_P2P_STATUS_CHIPSET_NOT_SUPPORTED
#define NVML_P2P_STATUS_CHIPSET_NOT_SUPPORTED         MXSMLEX_P2P_STATUS_CHIPSET_NOT_SUPPORTED
#define NVML_P2P_STATUS_GPU_NOT_SUPPORTED             MXSMLEX_P2P_STATUS_GPU_NOT_SUPPORTED
#define NVML_P2P_STATUS_IOH_TOPOLOGY_NOT_SUPPORTED    MXSMLEX_P2P_STATUS_IOH_TOPOLOGY_NOT_SUPPORTED
#define NVML_P2P_STATUS_DISABLED_BY_REGKEY            MXSMLEX_P2P_STATUS_DISABLED_BY_REGKEY
#define NVML_P2P_STATUS_NOT_SUPPORTED                 MXSMLEX_P2P_STATUS_NOT_SUPPORTED
#define NVML_P2P_STATUS_UNKNOWN                       MXSMLEX_P2P_STATUS_UNKNOWN

#define nvmlGpuP2PCapsIndex_t mxSmlExGpuP2PCapsIndex_t
#define NVML_P2P_CAPS_INDEX_READ          MXSMLEX_P2P_CAPS_INDEX_READ
#define NVML_P2P_CAPS_INDEX_WRITE         MXSMLEX_P2P_CAPS_INDEX_WRITE
#define NVML_P2P_CAPS_INDEX_NVLINK        MXSMLEX_P2P_CAPS_INDEX_MXLINK
#define NVML_P2P_CAPS_INDEX_ATOMICS       MXSMLEX_P2P_CAPS_INDEX_ATOMICS
#define NVML_P2P_CAPS_INDEX_PROP          MXSMLEX_P2P_CAPS_INDEX_PROP
#define NVML_P2P_CAPS_INDEX_UNKNOWN       MXSMLEX_P2P_CAPS_INDEX_UNKNOWN

#define nvmlPstates_t mxSmlExPstates_t
#define NVML_PSTATE_0 MXSMLEX_PSTATE_0
#define NVML_PSTATE_1 MXSMLEX_PSTATE_1
#define NVML_PSTATE_2 MXSMLEX_PSTATE_2
#define NVML_PSTATE_3 MXSMLEX_PSTATE_3
#define NVML_PSTATE_4 MXSMLEX_PSTATE_4
#define NVML_PSTATE_5 MXSMLEX_PSTATE_5
#define NVML_PSTATE_6 MXSMLEX_PSTATE_6
#define NVML_PSTATE_7 MXSMLEX_PSTATE_7
#define NVML_PSTATE_8 MXSMLEX_PSTATE_8
#define NVML_PSTATE_9 MXSMLEX_PSTATE_9
#define NVML_PSTATE_10 MXSMLEX_PSTATE_10
#define NVML_PSTATE_11 MXSMLEX_PSTATE_11
#define NVML_PSTATE_12 MXSMLEX_PSTATE_12
#define NVML_PSTATE_13 MXSMLEX_PSTATE_13
#define NVML_PSTATE_14 MXSMLEX_PSTATE_14
#define NVML_PSTATE_15 MXSMLEX_PSTATE_15
#define NVML_PSTATE_UNKNOWN MXSMLEX_PSTATE_UNKNOWN

#define nvmlGpuTopologyLevel_t mxSmlExGpuTopologyLevel_t
#define NVML_TOPOLOGY_INTERNAL MXSMLEX_TOPOLOGY_INTERNAL
#define NVML_TOPOLOGY_SINGLE MXSMLEX_TOPOLOGY_SINGLE
#define NVML_TOPOLOGY_MULTIPLE MXSMLEX_TOPOLOGY_MULTIPLE
#define NVML_TOPOLOGY_HOSTBRIDGE MXSMLEX_TOPOLOGY_HOSTBRIDGE
#define NVML_TOPOLOGY_NODE MXSMLEX_TOPOLOGY_NODE
#define NVML_TOPOLOGY_SYSTEM MXSMLEX_TOPOLOGY_SYSTEM

#define nvmlIntNvLinkDeviceType_t mxSmlExMetaXLinkDeviceType_t
#define NVML_NVLINK_DEVICE_TYPE_GPU         MXSMLEX_METAXLINK_DEVICE_TYPE_GPU
#define NVML_NVLINK_DEVICE_TYPE_IBMNPU      MXSMLEX_METAXLINK_DEVICE_TYPE_NPU
#define NVML_NVLINK_DEVICE_TYPE_SWITCH      MXSMLEX_METAXLINK_DEVICE_TYPE_SWITCH
#define NVML_NVLINK_DEVICE_TYPE_UNKNOWN     MXSMLEX_METAXLINK_DEVICE_TYPE_UNKNOWN

#define nvmlFieldValue_t mxSmlExFieldValue_t
#define NVML_FI_DEV_NVLINK_LINK_COUNT  MXSMLEX_FI_DEV_METAXLINK_LINK_COUNT
#define nvmlReturn mxSmlExReturn
#define nvmlValue_t mxSmlExValueType_t
#define nvmlValueType_t mxSmlExValueType_t
#define NVML_VALUE_TYPE_DOUBLE              MXSMLEX_VALUE_TYPE_DOUBLE
#define NVML_VALUE_TYPE_UNSIGNED_INT        MXSMLEX_VALUE_TYPE_UNSIGNED_INT
#define NVML_VALUE_TYPE_UNSIGNED_LONG       MXSMLEX_VALUE_TYPE_UNSIGNED_LONG
#define NVML_VALUE_TYPE_UNSIGNED_LONG_LONG  MXSMLEX_VALUE_TYPE_UNSIGNED_LONG_LONG
#define NVML_VALUE_TYPE_SIGNED_LONG_LONG    MXSMLEX_VALUE_TYPE_SIGNED_LONG_LONG
#define NVML_VALUE_TYPE_SIGNED_INT          MXSMLEX_VALUE_TYPE_SIGNED_INT
#define NVML_VALUE_TYPE_COUNT               MXSMLEX_VALUE_TYPE_COUNT

// structure
#define nvmlDevice_t mxsmlExDevice_t
#define nvmlPciInfo_t mxSmlExPciInfo_t
#define nvmlUtilization_t mxsmlExUtilization_t
#define nvmlMemory_t mxsmlExMemory_t
#define nvmlProcessInfo_t mxsmlExProcessInfo_t
#define nvmlExcludedDeviceInfo_t mxSmlExExcludedDeviceInfo_t

// functions
#define nvmlInit mxSmlExInit
#define nvmlInit_v2 mxSmlExInit
#define nvmlDeviceGetHandleByIndex mxSmlExGetDeviceHandleByIndex
#define nvmlDeviceGetHandleByIndex_v2 mxSmlExGetDeviceHandleByIndex
#define nvmlDeviceGetHandleByPciBusId_v2 mxSmlExGetDeviceHandleByPciBusId
#define nvmlDeviceGetPciInfo mxSmlExDeviceGetPciInfo
#define nvmlDeviceGetFanSpeed mxSmlExDeviceGetFanSpeed
#define nvmlDeviceGetFanSpeed_v2 mxSmlExDeviceGetFanSpeed_v2
#define nvmlDeviceGetUtilizationRates mxSmlExDeviceGetUtilization
#define nvmlSystemGetDriverVersion mxSmlExSystemGetDriverVersion
#define nvmlSystemGetNVMLVersion mxSmlExSystemGetMxsmlVersion
#define nvmlDeviceGetMinorNumber mxSmlExDeviceGetMinorNumber
#define nvmlDeviceGetNvLinkState mxSmlExDeviceGetMetaXLinkState
#define nvmlDeviceGetNvLinkRemotePciInfo_v2 mxSmlExDeviceGetMetaXLinkRemotePciInfo_v2
#define nvmlDeviceGetName mxSmlExDeviceGetName
#define nvmlErrorString mxSmlExErrorString
#define nvmlShutdown mxSmlExShutdown
#define nvmlDeviceGetTemperature mxSmlExDeviceGetTemperature
#define nvmlDeviceGetCount mxSmlExDeviceGetCount
#define nvmlDeviceGetCount_v2 mxSmlExDeviceGetCount
#define nvmlDeviceGetMemoryInfo mxSmlExDeviceGetMemoryInfo
#define nvmlDeviceGetCudaComputeCapability mxSmlExDeviceGetComputeCapability
#define nvmlDeviceGetCurrPcieLinkWidth mxSmlExDeviceGetCurrPcieLinkWidth
#define nvmlDeviceGetComputeRunningProcesses mxSmlExDeviceGetComputeRunningProcesses
#define nvmlDeviceGetPowerUsage mxSmlExGetPowerUsage
#define nvmlDeviceGetPowerManagementLimit mxSmlExGetPowerManagementLimit
#define nvmlDeviceGetMaxPcieLinkWidth mxSmlExGetMaxPcieLinkWidth
#define nvmlDeviceGetPcieThroughput mxSmlExGetPcieThroughput
#define nvmlDeviceGetTemperatureThreshold mxSmlExDeviceGetTemperatureThreshold
#define nvmlDeviceGetClockInfo mxSmlExDeviceGetClockInfo
#define nvmlDeviceGetCurrentClocksThrottleReasons mxSmlExDeviceGetCurrentClocksThrottleReasons
#define nvmlDeviceGetSupportedClocksThrottleReasons mxSmlExDeviceGetSupportedClocksThrottleReasons
#define nvmlDeviceGetCpuAffinity mxSmlExDeviceGetCpuAffinity
#define nvmlDeviceGetSupportedMemoryClocks mxSmlExDeviceGetSupportedMemoryClocks
#define nvmlDeviceGetSupportedGraphicsClocks mxSmlExDeviceGetSupportedGraphicsClocks
#define nvmlDeviceSetApplicationsClocks mxSmlExDeviceSetApplicationsClocks
#define nvmlDeviceResetApplicationsClocks mxSmlExDeviceResetApplicationsClocks
#define nvmlDeviceGetApplicationsClock mxSmlExDeviceGetApplicationsClock
#define nvmlDeviceGetP2PStatus mxSmlExDeviceGetP2PStatus
#define nvmlDeviceGetUUID mxSmlExDeviceGetUUID
#define nvmlDeviceGetHandleByUUID mxSmlExDeviceGetHandleByUUID
#define nvmlDeviceGetPerformanceState mxSmlExDeviceGetPerformanceState
#define nvmlDeviceSetAutoBoostedClocksEnabled mxSmlExDeviceSetAutoBoostedClocksEnabled
#define nvmlDeviceGetAutoBoostedClocksEnabled mxSmlExDeviceGetAutoBoostedClocksEnabled
#define nvmlDeviceGetTopologyCommonAncestor mxSmlExDeviceGetTopologyCommonAncestor
#define nvmlDeviceGetNvLinkRemoteDeviceType mxSmlExDeviceGetMetaXLinkRemoteDeviceType
#define nvmlDeviceGetFieldValues mxSmlExDeviceGetFieldValues

#ifdef __cplusplus
}
#endif

#endif // __NVML_WRAPPER_H__
