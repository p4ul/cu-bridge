#pragma once

#define ENABLE_CUDA_TO_MACA_ADAPTOR

#include <__clang_maca_common_define.h>
#include "curand_to_mcrand_adaptor.h"
#include "../runtime/cuda_to_maca_mcr_adaptor.h"
#include "../runtime/cuda_runtime_wrapper.h"
#include <mcrand_kernel.h>
