#ifndef __CUDA_VDPAU_WRAPPER_H__
#define __CUDA_VDPAU_WRAPPER_H__

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/* TODO: wcuda_skipped - not support now */
mcDrvError_t wcuVDPAUGetDevice(mcDrvDevice_t *pDevice, VdpDevice vdpDevice,
                               VdpGetProcAddress *vdpGetProcAddress)
{
    return MC_ERROR_NOT_SUPPORTED;
}

mcDrvError_t wcuVDPAUCtxCreate(mcDrvcontext_t *pCtx, unsigned int flags, mcDrvDevice_t device,
                               VdpDevice vdpDevice, VdpGetProcAddress *vdpGetProcAddress)
{
    return MC_ERROR_NOT_SUPPORTED;
}

mcDrvError_t wcuGraphicsVDPAURegisterVideoSurface(mcDrvGraphicsResource *pMcResource,
                                                  VdpVideoSurface vdpSurface, unsigned int flags)
{
    return MC_ERROR_NOT_SUPPORTED;
}

mcDrvError_t wcuGraphicsVDPAURegisterOutputSurface(mcDrvGraphicsResource *pMcResource,
                                                   VdpOutputSurface vdpSurface, unsigned int flags)
{
    return MC_ERROR_NOT_SUPPORTED;
}

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* __CUDA_VDPAU_WRAPPER_H__ */
