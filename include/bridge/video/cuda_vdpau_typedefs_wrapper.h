#ifndef __CUDA_VDPAU_TYPEDEFS_WRAPPER_H__
#define __CUDA_VDPAU_TYPEDEFS_WRAPPER_H__

#include <vdpau/vdpau.h>

#include "mcVDPAU.h"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

#define PFN_mcVDPAUGetDevice                     PFN_mcVDPAUGetDevice_v3010
#define PFN_mcVDPAUCtxCreate                     PFN_mcVDPAUCtxCreate_v3020
#define PFN_mcGraphicsVDPAURegisterVideoSurface  PFN_mcGraphicsVDPAURegisterVideoSurface_v3010
#define PFN_mcGraphicsVDPAURegisterOutputSurface PFN_mcGraphicsVDPAURegisterOutputSurface_v3010

typedef mcDrvError_t (*PFN_mcVDPAUGetDevice_v3010)(mcDrvDevice_t *pDevice, VdpDevice vdpDevice,
                                                   VdpGetProcAddress *vdpGetProcAddress);
typedef mcDrvError_t (*PFN_mcVDPAUCtxCreate_v3020)(mcDrvcontext_t *pCtx, unsigned int flags,
                                                   mcDrvDevice_t device, VdpDevice vdpDevice,
                                                   VdpGetProcAddress *vdpGetProcAddress);
typedef mcDrvError_t (*PFN_mcGraphicsVDPAURegisterVideoSurface_v3010)(
    mcDrvGraphicsResource *pMcResource, VdpVideoSurface vdpSurface, unsigned int flags);
typedef mcDrvError_t (*PFN_mcGraphicsVDPAURegisterOutputSurface_v3010)(
    mcDrvGraphicsResource *pMcResource, VdpOutputSurface vdpSurface, unsigned int flags);

typedef CUresult(CUDAAPI *PFN_mcVDPAUCtxCreate_v3010)(mcDrvcontext_t *pCtx, unsigned int flags,
                                              mcDrvDevice_t device, VdpDevice vdpDevice,
                                              VdpGetProcAddress *vdpGetProcAddress);

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* __CUDA_VDPAU_TYPEDEFS_WRAPPER_H__ */
