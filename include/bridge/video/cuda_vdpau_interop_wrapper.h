#ifndef __CUDA_VDPAU_INTEROP_WRAPPER_H__
#define __CUDA_VDPAU_INTEROP_WRAPPER_H__

#include <mc_runtime.h>
#include <vdpau/vdpau.h>

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

/* TODO: wcuda_skipped - not support now */
__host__ mcError_t wcudaVDPAUGetDevice(int *device, VdpDevice vdpDevice,
                                       VdpGetProcAddress *vdpGetProcAddress)
{
    return mcErrorNotSupported;
}

__host__ mcError_t wcudaVDPAUSetVDPAUDevice(int device, VdpDevice vdpDevice,
                                            VdpGetProcAddress *vdpGetProcAddress)
{
    return mcErrorNotSupported;
}

__host__ mcError_t wcudaGraphicsVDPAURegisterVideoSurface(struct cudaGraphicsResource **resource,
                                                          VdpVideoSurface vdpSurface,
                                                          unsigned int flags)
{
    return mcErrorNotSupported;
}

__host__ mcError_t wcudaGraphicsVDPAURegisterOutputSurface(struct cudaGraphicsResource **resource,
                                                           VdpOutputSurface vdpSurface,
                                                           unsigned int flags)
{
    return mcErrorNotSupported;
}

#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* __CUDA_VDPAU_INTEROP_WRAPPER_H__ */