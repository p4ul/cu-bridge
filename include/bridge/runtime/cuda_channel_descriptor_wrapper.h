/**
 * @file cuda_channel_descripotr_wrapper.h
 *
 * @brief file is a wrapper  to cuda channel descriptor template C++.
 *
 */

#ifndef __CUDA_CHANNEL_DESCRIPTOR_WRAPPER_H__
#define __CUDA_CHANNEL_DESCRIPTOR_WRAPPER_H__

#include "cuda_runtime_wrapper.h"
#include "mc_runtime_api.h"
#include "mc_runtime_types.h"

#if defined(__cplusplus)

template <class T>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc(void) {
  return wcudaCreateChannelDesc(0, 0, 0, 0, mcChannelFormatKindNone);
}

static __inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDescHalf(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindFloat);
}

static __inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDescHalf1(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindFloat);
}

static __inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDescHalf2(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindFloat);
}

static __inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDescHalf4(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindFloat);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<char>(void) {
  int e = (int)sizeof(char) * 8;

#if defined(_CHAR_UNSIGNED) || defined(__CHAR_UNSIGNED__)
  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
#else  /* _CHAR_UNSIGNED || __CHAR_UNSIGNED__ */
  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
#endif /* _CHAR_UNSIGNED || __CHAR_UNSIGNED__ */
}

template <>
__inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDesc<signed char>(void) {
  int e = (int)sizeof(signed char) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDesc<unsigned char>(void) {
  int e = (int)sizeof(unsigned char) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<char1>(void) {
  int e = (int)sizeof(signed char) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uchar1>(void) {
  int e = (int)sizeof(unsigned char) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<char2>(void) {
  int e = (int)sizeof(signed char) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uchar2>(void) {
  int e = (int)sizeof(unsigned char) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<char4>(void) {
  int e = (int)sizeof(signed char) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uchar4>(void) {
  int e = (int)sizeof(unsigned char) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<short>(void) {
  int e = (int)sizeof(short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDesc<unsigned short>(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<short1>(void) {
  int e = (int)sizeof(short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ushort1>(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<short2>(void) {
  int e = (int)sizeof(short) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ushort2>(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<short4>(void) {
  int e = (int)sizeof(short) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ushort4>(void) {
  int e = (int)sizeof(unsigned short) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<int>(void) {
  int e = (int)sizeof(int) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDesc<unsigned int>(void) {
  int e = (int)sizeof(unsigned int) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<int1>(void) {
  int e = (int)sizeof(int) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uint1>(void) {
  int e = (int)sizeof(unsigned int) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<int2>(void) {
  int e = (int)sizeof(int) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uint2>(void) {
  int e = (int)sizeof(unsigned int) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<int4>(void) {
  int e = (int)sizeof(int) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<uint4>(void) {
  int e = (int)sizeof(unsigned int) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindUnsigned);
}

#if !defined(__LP64__)

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<long>(void) {
  int e = (int)sizeof(long) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDesc<unsigned long>(void) {
  int e = (int)sizeof(unsigned long) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<long1>(void) {
  int e = (int)sizeof(long) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ulong1>(void) {
  int e = (int)sizeof(unsigned long) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<long2>(void) {
  int e = (int)sizeof(long) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ulong2>(void) {
  int e = (int)sizeof(unsigned long) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindUnsigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<long4>(void) {
  int e = (int)sizeof(long) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindSigned);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<ulong4>(void) {
  int e = (int)sizeof(unsigned long) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindUnsigned);
}

#endif /* !__LP64__ */

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<float>(void) {
  int e = (int)sizeof(float) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindFloat);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<float1>(void) {
  int e = (int)sizeof(float) * 8;

  return wcudaCreateChannelDesc(e, 0, 0, 0, mcChannelFormatKindFloat);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<float2>(void) {
  int e = (int)sizeof(float) * 8;

  return wcudaCreateChannelDesc(e, e, 0, 0, mcChannelFormatKindFloat);
}

template <>
__inline__ __host__ mcChannelFormatDesc wcudaCreateChannelDesc<float4>(void) {
  int e = (int)sizeof(float) * 8;

  return wcudaCreateChannelDesc(e, e, e, e, mcChannelFormatKindFloat);
}

static __inline__ __host__ mcChannelFormatDesc
wcudaCreateChannelDescNV12(void) {
  int e = (int)sizeof(char) * 8;

  return wcudaCreateChannelDesc(e, e, e, 0, mcChannelFormatKindNV12);
}
#endif /* __cplusplus */

/** @} */
/** @} */ /* END MCRUNTIME_TEXTURE_HL */

#endif /* !__CUDA_CHANNEL_DESCRIPTOR_WRAPPER_H__ */
