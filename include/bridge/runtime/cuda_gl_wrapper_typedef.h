#ifndef __CUDA_GL_WRAPPER_TYPEDEF_H__
#define __CUDA_GL_WRAPPER_TYPEDEF_H__

#include <GL/gl.h>

#include "mc_runtime.h"
#include "mc_gl_types.h"

typedef mcDrvError_t (*PFN_wcuGraphicsGLRegisterBuffer_v3000)(mcDrvGraphicsResource *pMacaResource,
                                                              GLuint buffer, unsigned int Flags);
typedef mcDrvError_t (*PFN_wcuGraphicsGLRegisterImage_v3000)(mcDrvGraphicsResource *pMacaResource,
                                                             GLuint image, GLenum target,
                                                             unsigned int Flags);
#if defined(_WIN32)
#if !defined(WGL_NV_gpu_affinity)
typedef void *HGPUNV;
#endif
#endif /* _WIN32 */

#ifdef _WIN32
typedef mcDrvError_t (*PFN_wcuWGLGetDevice_v2020)(mcDrvDevice_t *pDevice, HGPUNV hGpu);
#endif
typedef mcDrvError_t (*PFN_wcuGLGetDevices_v6050)(unsigned int *pMacaDeviceCount,
                                                  mcDrvDevice_t *pMacaDevices,
                                                  unsigned int mcDeviceCount,
                                                  CUGLDeviceList deviceList);
typedef mcDrvError_t (*PFN_wcuGLCtxCreate_v3020)(mcDrvContext_t *pCtx, unsigned int Flags,
                                                 mcDrvDevice_t device);
typedef mcDrvError_t (*PFN_wcuGLInit_v2000)(void);
typedef mcDrvError_t (*PFN_wcuGLRegisterBufferObject_v2000)(GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObject_v7000_ptds)(mcDrvDeviceptr_t *dptr, size_t *size,
                                                            GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLUnmapBufferObject_v2000)(GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLUnregisterBufferObject_v2000)(GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLSetBufferObjectMapFlags_v2030)(GLuint buffer, unsigned int Flags);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObjectAsync_v7000_ptsz)(mcDrvDeviceptr_t *dptr,
                                                                 size_t *size, GLuint buffer,
                                                                 mcDrvStream_t hStream);
typedef mcDrvError_t (*PFN_wcuGLUnmapBufferObjectAsync_v2030)(GLuint buffer, mcDrvStream_t hStream);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObject_v3020)(mcDrvDeviceptr_t *dptr, size_t *size,
                                                       GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObjectAsync_v3020)(mcDrvDeviceptr_t *dptr, size_t *size,
                                                            GLuint buffer, mcDrvStream_t hStream);
typedef mcDrvError_t (*PFN_wcuGLGetDevices_v4010)(unsigned int *pMacaDeviceCount,
                                                  mcDrvDevice_t *pMacaDevices,
                                                  unsigned int cudaDeviceCount,
                                                  CUGLDeviceList deviceList);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObject_v2000)(mcDrvDeviceptrv1_t *dptr, unsigned int *size,
                                                       GLuint buffer);
typedef mcDrvError_t (*PFN_wcuGLMapBufferObjectAsync_v2030)(mcDrvDeviceptrv1_t *dptr,
                                                            unsigned int *size, GLuint buffer,
                                                            mcDrvStream_t hStream);
typedef mcDrvError_t (*PFN_wcuGLCtxCreate_v2000)(mcDrvContext_t *pCtx, unsigned int Flags,
                                                 mcDrvDevice_t device);

#endif