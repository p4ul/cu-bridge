/**
 * @file cuda_driver_template_wrapper.h
 *
 * @brief file is a wrapper  to cuda driver template C++.
 *
 */

#ifndef __INCLUDE_CUDA_DRIVER_TEMPLATE_WRAPPER_H__
#define __INCLUDE_CUDA_DRIVER_TEMPLATE_WRAPPER_H__

#include "mc_runtime_types.h"
#include <stdint.h>

/*
 * Occupancy driver C++ template declaration
 */

/*
 * The new module driver C++ template declaration, Add partion brief introductions please.
 */

#endif /*  __INCLUDE_CUDA_DRIVER_TEMPLATE_WRAPPER_H__  */