#ifndef __CUDA_EGL_WRAPPER_H__
#define __CUDA_EGL_WRAPPER_H__

#include "mcEGL.h"
#include "mc_egl_interop.h"

/* Runtime function wrapper of cuda_egl_interop.h */
mcError_t wcudaGraphicsEGLRegisterImage(struct mcGraphicsResource **pMacaResource,
                                        EGLImageKHR image, unsigned int flags);
mcError_t wcudaEGLStreamConsumerConnect(mcEglStreamConnection *conn, EGLStreamKHR eglStream);
mcError_t wcudaEGLStreamConsumerConnectWithFlags(mcEglStreamConnection *conn,
                                                 EGLStreamKHR eglStream, unsigned int flags);
mcError_t wcudaEGLStreamConsumerDisconnect(mcEglStreamConnection *conn);
mcError_t wcudaEGLStreamConsumerAcquireFrame(mcEglStreamConnection *conn,
                                             mcGraphicsResource_t *pMacaResource,
                                             mcStream_t *pStream, unsigned int timeout);
mcError_t wcudaEGLStreamConsumerReleaseFrame(mcEglStreamConnection *conn,
                                             mcGraphicsResource_t pMacaResource,
                                             mcStream_t *pStream);
mcError_t wcudaEGLStreamProducerConnect(mcEglStreamConnection *conn, EGLStreamKHR eglStream,
                                        EGLint width, EGLint height);
mcError_t wcudaEGLStreamProducerDisconnect(mcEglStreamConnection *conn);

mcError_t wcudaEGLStreamProducerPresentFrame(mcEglStreamConnection *conn, mcEglFrame eglframe,
                                             mcStream_t *pStream);
mcError_t wcudaEGLStreamProducerReturnFrame(mcEglStreamConnection *conn, mcEglFrame *eglframe,
                                            mcStream_t *pStream);
mcError_t wcudaGraphicsResourceGetMappedEglFrame(mcEglFrame *eglFrame,
                                                 mcGraphicsResource_t resource, unsigned int index,
                                                 unsigned int mipLevel);
mcError_t wcudaEventCreateFromEGLSync(mcEvent_t *phEvent, EGLSyncKHR eglSync, unsigned int flags);

/* Driver function wrapper of cudaEGL.h */
mcDrvError_t wcuGraphicsEGLRegisterImage(mcDrvGraphicsResource *pMacaResource, EGLImageKHR image,
                                         unsigned int flags);
mcDrvError_t wcuEGLStreamConsumerConnect(mcDrveglStreamConnection *conn, EGLStreamKHR stream);
mcDrvError_t wcuEGLStreamConsumerConnectWithFlags(mcDrveglStreamConnection *conn,
                                                  EGLStreamKHR stream, unsigned int flags);
mcDrvError_t wcuEGLStreamConsumerDisconnect(mcDrveglStreamConnection *conn);
mcDrvError_t wcuEGLStreamConsumerAcquireFrame(mcDrveglStreamConnection *conn,
                                              mcDrvGraphicsResource *pMacaResource,
                                              mcDrvStream_t *pStream, unsigned int timeout);
mcDrvError_t wcuEGLStreamConsumerReleaseFrame(mcDrveglStreamConnection *conn,
                                              mcDrvGraphicsResource pMacaResource,
                                              mcDrvStream_t *pStream);
mcDrvError_t wcuEGLStreamProducerConnect(mcDrveglStreamConnection *conn, EGLStreamKHR stream,
                                         EGLint width, EGLint height);
mcDrvError_t wcuEGLStreamProducerDisconnect(mcDrveglStreamConnection *conn);
mcDrvError_t wcuEGLStreamProducerPresentFrame(mcDrveglStreamConnection *conn,
                                              mcDrveglFrame eglframe, mcDrvStream_t *pStream);
mcDrvError_t wcuEGLStreamProducerReturnFrame(mcDrveglStreamConnection *conn,
                                             mcDrveglFrame *eglframe, mcDrvStream_t *pStream);
mcDrvError_t wcuGraphicsResourceGetMappedEglFrame(mcDrveglFrame *eglFrame,
                                                  mcDrvGraphicsResource resource,
                                                  unsigned int index, unsigned int mipLevel);
mcDrvError_t wcuEventCreateFromEGLSync(mcDrvEvent_t *phEvent, EGLSyncKHR eglSync,
                                       unsigned int flags);
#endif /* __CUDA_EGL_WRAPPER_H__ */