#ifndef __CUDA_TO_MACA_ADAPTOR_H__
#define __CUDA_TO_MACA_ADAPTOR_H__

#include "cuda_pfn_to_maca_adaptor.h"

//============================CUDA Runtime API====================//

// mem
#define cudaMemcpyKind _mcMemcpyKind
#define cudaMemcpyHostToHost mcMemcpyHostToHost
#define cudaMemcpyHostToDevice mcMemcpyHostToDevice
#define cudaMemcpyDeviceToHost mcMemcpyDeviceToHost
#define cudaMemcpyDeviceToDevice mcMemcpyDeviceToDevice
#define cudaMemcpyDefault mcMemcpyDefault

#define cudaMemoryType mcMemoryType_enum
#define cudaMemoryTypeUnregistered mcMemoryTypeUnregistered
#define cudaMemoryTypeHost mcMemoryTypeHost
#define cudaMemoryTypeDevice mcMemoryTypeDevice
#define cudaMemoryTypeManaged mcMemoryTypeManaged

/* enum, error code */
#define cudaError _mcError_t
#define cudaError_t mcError_t
#define cudaSuccess mcSuccess
#define cudaErrorInvalidValue mcErrorInvalidValue
#define cudaErrorMemoryAllocation mcErrorMemoryAllocation
#define cudaErrorInitializationError mcErrorInitializationError
#define cudaErrorCudartUnloading mcErrorDeinitialized
#define cudaErrorProfilerDisabled mcErrorProfilerDisabled
#define cudaErrorProfilerNotInitialized mcErrorProfilerNotInitialized
#define cudaErrorProfilerAlreadyStarted mcErrorProfilerAlreadyStarted
#define cudaErrorProfilerAlreadyStopped mcErrorProfilerAlreadyStopped
#define cudaErrorInvalidConfiguration mcErrorInvalidConfiguration
#define cudaErrorInvalidPitchValue mcErrorInvalidPitchValue
#define cudaErrorInvalidSymbol mcErrorInvalidSymbol
#define cudaErrorInvalidHostPointer mcErrorInvalidHostPointer
#define cudaErrorInvalidDevicePointer mcErrorInvalidDevicePointer
#define cudaErrorInvalidTexture mcErrorInvalidTexture
#define cudaErrorInvalidTextureBinding mcErrorInvalidTextureBinding
#define cudaErrorInvalidChannelDescriptor mcErrorInvalidChannelDescriptor
#define cudaErrorInvalidMemcpyDirection mcErrorInvalidMemcpyDirection
#define cudaErrorAddressOfConstant mcErrorAddressOfConstant
#define cudaErrorTextureFetchFailed mcErrorTextureFetchFailed
#define cudaErrorTextureNotBound mcErrorTextureNotBound
#define cudaErrorSynchronizationError mcErrorSynchronizationError
#define cudaErrorInvalidFilterSetting mcErrorInvalidFilterSetting
#define cudaErrorInvalidNormSetting mcErrorInvalidNormSetting
#define cudaErrorMixedDeviceExecution mcErrorMixedDeviceExecution
#define cudaErrorNotYetImplemented mcErrorNotYetImplemented
#define cudaErrorMemoryValueTooLarge mcErrorMemoryValueTooLarge
#define cudaErrorStubLibrary mcErrorStubLibrary
#define cudaErrorInsufficientDriver mcErrorInsufficientDriver
#define cudaErrorCallRequiresNewerDriver mcErrorCallRequiresNewerDriver
#define cudaErrorInvalidSurface mcErrorInvalidSurface
#define cudaErrorDuplicateVariableName mcErrorDuplicateVariableName
#define cudaErrorDuplicateTextureName mcErrorDuplicateTextureName
#define cudaErrorDuplicateSurfaceName mcErrorDuplicateSurfaceName
#define cudaErrorDevicesUnavailable mcErrorDevicesUnavailable
#define cudaErrorIncompatibleDriverContext mcErrorIncompatibleDriverContext
#define cudaErrorMissingConfiguration mcErrorMissingConfiguration
#define cudaErrorPriorLaunchFailure mcErrorPriorLaunchFailure
#define cudaErrorLaunchMaxDepthExceeded mcErrorLaunchMaxDepthExceeded
#define cudaErrorLaunchFileScopedTex mcErrorLaunchFileScopedTex
#define cudaErrorLaunchFileScopedSurf mcErrorLaunchFileScopedSurf
#define cudaErrorSyncDepthExceeded mcErrorSyncDepthExceeded
#define cudaErrorLaunchPendingCountExceeded mcErrorLaunchPendingCountExceeded
#define cudaErrorInvalidDeviceFunction mcErrorInvalidDeviceFunction
#define cudaErrorNoDevice mcErrorNoDevice
#define cudaErrorInvalidDevice mcErrorInvalidDevice
#define cudaErrorDeviceNotLicensed mcErrorDeviceNotLicensed
#define cudaErrorSoftwareValidityNotEstablished                                \
  mcErrorSoftwareValidityNotEstablished
#define cudaErrorStartupFailure mcErrorStartupFailure
#define cudaErrorInvalidKernelImage mcErrorInvalidKernelImage
#define cudaErrorDeviceUninitialized mcErrorDeviceUninitialized
#define cudaErrorMapBufferObjectFailed mcErrorMapBufferObjectFailed
#define cudaErrorUnmapBufferObjectFailed mcErrorUnmapBufferObjectFailed
#define cudaErrorArrayIsMapped mcErrorArrayIsMapped
#define cudaErrorAlreadyMapped mcErrorAlreadyMapped
#define cudaErrorNoKernelImageForDevice mcErrorNoKernelImageForDevice
#define cudaErrorAlreadyAcquired mcErrorAlreadyAcquired
#define cudaErrorNotMapped mcErrorNotMapped
#define cudaErrorNotMappedAsArray mcErrorNotMappedAsArray
#define cudaErrorNotMappedAsPointer mcErrorNotMappedAsPointer
#define cudaErrorECCUncorrectable mcErrorECCUncorrectable
#define cudaErrorUnsupportedLimit mcErrorUnsupportedLimit
#define cudaErrorDeviceAlreadyInUse mcErrorDeviceAlreadyInUse
#define cudaErrorPeerAccessUnsupported mcErrorPeerAccessUnsupported
#define cudaErrorInvalidPtx mcErrorInvalidKernelFile
#define cudaErrorInvalidGraphicsContext mcErrorInvalidGraphicsContext
#define cudaErrorNvlinkUncorrectable mcErrorMxlinkUncorrectable
#define cudaErrorJitCompilerNotFound mcErrorJitCompilerNotFound
#define cudaErrorUnsupportedPtxVersion mcErrorUnsupportedKernelVersion
#define cudaErrorJitCompilationDisabled mcErrorJitCompilationDisabled
#define cudaErrorUnsupportedExecAffinity mcErrorUnsupportedExecAffinity
#define cudaErrorInvalidSource mcErrorInvalidSource
#define cudaErrorFileNotFound mcErrorFileNotFound
#define cudaErrorSharedObjectSymbolNotFound mcErrorSharedObjectSymbolNotFound
#define cudaErrorSharedObjectInitFailed mcErrorSharedObjectInitFailed
#define cudaErrorOperatingSystem mcErrorOperatingSystem
#define cudaErrorInvalidResourceHandle mcErrorInvalidResourceHandle
#define cudaErrorIllegalState mcErrorIllegalState
#define cudaErrorSymbolNotFound mcErrorSymbolNotFound
#define cudaErrorNotReady mcErrorNotReady
#define cudaErrorIllegalAddress mcErrorIllegalAddress
#define cudaErrorLaunchOutOfResources mcErrorLaunchOutOfResources
#define cudaErrorLaunchTimeout mcErrorLaunchTimeout
#define cudaErrorLaunchIncompatibleTexturing mcErrorLaunchIncompatibleTexturing
#define cudaErrorPeerAccessAlreadyEnabled mcErrorPeerAccessAlreadyEnabled
#define cudaErrorPeerAccessNotEnabled mcErrorPeerAccessNotEnabled
#define cudaErrorSetOnActiveProcess mcErrorSetOnActiveProcess
#define cudaErrorContextIsDestroyed mcErrorContextIsDestroyed
#define cudaErrorAssert mcErrorAssert
#define cudaErrorTooManyPeers mcErrorTooManyPeers
#define cudaErrorHostMemoryAlreadyRegistered mcErrorHostMemoryAlreadyRegistered
#define cudaErrorHostMemoryNotRegistered mcErrorHostMemoryNotRegistered
#define cudaErrorHardwareStackError mcErrorHardwareStackError
#define cudaErrorIllegalInstruction mcErrorIllegalInstruction
#define cudaErrorMisalignedAddress mcErrorMisalignedAddress
#define cudaErrorInvalidAddressSpace mcErrorInvalidAddressSpace
#define cudaErrorInvalidPc mcErrorInvalidPc
#define cudaErrorLaunchFailure mcErrorLaunchFailure
#define cudaErrorCooperativeLaunchTooLarge mcErrorCooperativeLaunchTooLarge
#define cudaErrorNotPermitted mcErrorNotPermitted
#define cudaErrorNotSupported mcErrorNotSupported
#define cudaErrorSystemNotReady mcErrorSystemNotReady
#define cudaErrorSystemDriverMismatch mcErrorSystemDriverMismatch
#define cudaErrorCompatNotSupportedOnDevice mcErrorCompatNotSupportedOnDevice
#define cudaErrorMpsConnectionFailed mcErrorMpsConnectionFailed
#define cudaErrorMpsRpcFailure mcErrorMpsRpcFailure
#define cudaErrorMpsServerNotReady mcErrorMpsServerNotReady
#define cudaErrorMpsMaxClientsReached mcErrorMpsMaxClientsReached
#define cudaErrorMpsMaxConnectionsReached mcErrorMpsMaxConnectionsReached
#define cudaErrorStreamCaptureUnsupported mcErrorStreamCaptureUnsupported
#define cudaErrorStreamCaptureInvalidated mcErrorStreamCaptureInvalidated
#define cudaErrorStreamCaptureMerge mcErrorStreamCaptureMerge
#define cudaErrorStreamCaptureUnmatched mcErrorStreamCaptureUnmatched
#define cudaErrorStreamCaptureUnjoined mcErrorStreamCaptureUnjoined
#define cudaErrorStreamCaptureIsolation mcErrorStreamCaptureIsolation
#define cudaErrorStreamCaptureImplicit mcErrorStreamCaptureImplicit
#define cudaErrorCapturedEvent mcErrorCapturedEvent
#define cudaErrorStreamCaptureWrongThread mcErrorStreamCaptureWrongThread
#define cudaErrorTimeout mcErrorTimeout
#define cudaErrorGraphExecUpdateFailure mcErrorGraphExecUpdateFailure
#define cudaErrorExternalDevice mcErrorExternalDevice
#define cudaErrorUnknown mcErrorUnknown
#define cudaErrorApiFailureBase mcErrorApiFailureBase

/* enum, device attributes */
#define cudaDeviceAttr mcDeviceAttribute_t
#define cudaDevAttrMaxThreadsPerBlock mcDeviceAttributeMaxThreadsPerBlock
#define cudaDevAttrMaxBlockDimX mcDeviceAttributeMaxBlockDimX
#define cudaDevAttrMaxBlockDimY mcDeviceAttributeMaxBlockDimY
#define cudaDevAttrMaxBlockDimZ mcDeviceAttributeMaxBlockDimZ
#define cudaDevAttrMaxGridDimX mcDeviceAttributeMaxGridDimX
#define cudaDevAttrMaxGridDimY mcDeviceAttributeMaxGridDimY
#define cudaDevAttrMaxGridDimZ mcDeviceAttributeMaxGridDimZ
#define cudaDevAttrMaxSharedMemoryPerBlock                                     \
  mcDeviceAttributeMaxSharedMemoryPerBlock
#define cudaDevAttrTotalConstantMemory mcDeviceAttributeTotalConstantMemory
#define cudaDevAttrWarpSize mcDeviceAttributeWarpSize
#define cudaDevAttrMaxPitch mcDeviceAttributeMaxPitch
#define cudaDevAttrMaxRegistersPerBlock mcDeviceAttributeMaxRegistersPerBlock
#define cudaDevAttrClockRate mcDeviceAttributeClockRate
#define cudaDevAttrTextureAlignment mcDeviceAttributeTextureAlignment
#define cudaDevAttrGpuOverlap mcDeviceAttributeGpuOverlap
#define cudaDevAttrMultiProcessorCount mcDeviceAttributeMultiProcessorCount
#define cudaDevAttrKernelExecTimeout mcDeviceAttributeKernelExecTimeout
#define cudaDevAttrIntegrated mcDeviceAttributeIntegrated
#define cudaDevAttrCanMapHostMemory mcDeviceAttributeCanMapHostMemory
#define cudaDevAttrComputeMode mcDeviceAttributeComputeMode
#define cudaDevAttrMaxTexture1DWidth mcDeviceAttributeMaxTexture1DWidth
#define cudaDevAttrMaxTexture2DWidth mcDeviceAttributeMaxTexture2DWidth
#define cudaDevAttrMaxTexture2DHeight mcDeviceAttributeMaxTexture2DHeight
#define cudaDevAttrMaxTexture3DWidth mcDeviceAttributeMaxTexture3DWidth
#define cudaDevAttrMaxTexture3DHeight mcDeviceAttributeMaxTexture3DHeight
#define cudaDevAttrMaxTexture3DDepth mcDeviceAttributeMaxTexture3DDepth
#define cudaDevAttrMaxTexture2DLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture2DLayeredHeight mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture2DLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrSurfaceAlignment mcDeviceAttributeUnknow
#define cudaDevAttrConcurrentKernels mcDeviceAttributeConcurrentKernels
#define cudaDevAttrEccEnabled mcDeviceAttributeEccEnabled
#define cudaDevAttrPciBusId mcDeviceAttributePciBusId
#define cudaDevAttrPciDeviceId mcDeviceAttributePciDeviceId
#define cudaDevAttrTccDriver                 mcDeviceAttributeTccDriver
#define cudaDevAttrMemoryClockRate mcDeviceAttributeMemoryClockRate
#define cudaDevAttrGlobalMemoryBusWidth mcDeviceAttributeMemoryBusWidth
#define cudaDevAttrL2CacheSize mcDeviceAttributeL2CacheSize
#define cudaDevAttrMaxThreadsPerMultiProcessor                                 \
  mcDeviceAttributeMaxThreadsPerMultiProcessor
#define cudaDevAttrAsyncEngineCount mcDeviceAttributeAsyncEngineCount
#define cudaDevAttrUnifiedAddressing mcDeviceAttributeUnifiedAddressing
#define cudaDevAttrMaxTexture1DLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture1DLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture2DGatherWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture2DGatherHeight mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture3DWidthAlt mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture3DHeightAlt mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture3DDepthAlt mcDeviceAttributeUnknow
#define cudaDevAttrPciDomainId mcDeviceAttributeUnknow
#define cudaDevAttrTexturePitchAlignment mcDeviceAttributeTexturePitchAlignment
#define cudaDevAttrMaxTextureCubemapWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTextureCubemapLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTextureCubemapLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface1DWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface2DWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface2DHeight mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface3DWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface3DHeight mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface3DDepth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface1DLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface1DLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface2DLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface2DLayeredHeight mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurface2DLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurfaceCubemapWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurfaceCubemapLayeredWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxSurfaceCubemapLayeredLayers mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture1DLinearWidth mcDeviceAttributeTexture1DLinearWidth
#define cudaDevAttrMaxTexture2DLinearWidth mcDeviceAttributeTexture2DLinearWidth
#define cudaDevAttrMaxTexture2DLinearHeight                                    \
  mcDeviceAttributeTexture2DLinearHeight
#define cudaDevAttrMaxTexture2DLinearPitch mcDeviceAttributeTexture2DLinearPitch
#define cudaDevAttrMaxTexture2DMipmappedWidth mcDeviceAttributeUnknow
#define cudaDevAttrMaxTexture2DMipmappedHeight mcDeviceAttributeUnknow
#define cudaDevAttrComputeCapabilityMajor                                      \
  mcDeviceAttributeComputeCapabilityMajor
#define cudaDevAttrComputeCapabilityMinor                                      \
  mcDeviceAttributeComputeCapabilityMinor
#define cudaDevAttrMaxTexture1DMipmappedWidth mcDeviceAttributeUnknow
#define cudaDevAttrStreamPrioritiesSupported mcDeviceAttributeUnknow
#define cudaDevAttrGlobalL1CacheSupported mcDeviceAttributeUnknow
#define cudaDevAttrLocalL1CacheSupported mcDeviceAttributeUnknow
#define cudaDevAttrMaxSharedMemoryPerMultiprocessor                            \
  mcDeviceAttributeMaxSharedMemoryPerMultiprocessor
#define cudaDevAttrMaxRegistersPerMultiprocessor                               \
  mcDeviceAttributeMaxRegistersPerMultiprocessor
#define cudaDevAttrManagedMemory mcDeviceAttributeManagedMemory
#define cudaDevAttrIsMultiGpuBoard mcDeviceAttributeIsMultiGpuBoard
#define cudaDevAttrMultiGpuBoardGroupID mcDeviceAttributeUnknow
#define cudaDevAttrHostNativeAtomicSupported mcDeviceAttributeHostNativeAtomicSupported
#define cudaDevAttrSingleToDoublePrecisionPerfRatio mcDeviceAttributeUnknow
#define cudaDevAttrPageableMemoryAccess mcDeviceAttributePageableMemoryAccess
#define cudaDevAttrConcurrentManagedAccess                                     \
  mcDeviceAttributeConcurrentManagedAccess
#define cudaDevAttrComputePreemptionSupported mcDeviceAttributeUnknow
#define cudaDevAttrCanUseHostPointerForRegisteredMem mcDeviceAttributeCanUseHostPointerForRegisteredMem
#define cudaDevAttrReserved92 mcDeviceAttributeUnknow
#define cudaDevAttrReserved93 mcDeviceAttributeUnknow
#define cudaDevAttrReserved94 mcDeviceAttributeUnknow
#define cudaDevAttrCooperativeLaunch mcDeviceAttributeCooperativeLaunch
#define cudaDevAttrCooperativeMultiDeviceLaunch                                \
  mcDeviceAttributeCooperativeMultiDeviceLaunch
#define cudaDevAttrMaxSharedMemoryPerBlockOptin mcDeviceAttributeMaxSharedMemoryPerBlockOptin
#define cudaDevAttrCanFlushRemoteWrites mcDeviceAttributeCanFlushRemoteWrites
#define cudaDevAttrHostRegisterSupported mcDeviceAttributeUnknow
#define cudaDevAttrPageableMemoryAccessUsesHostPageTables                      \
  mcDeviceAttributePageableMemoryAccessUsesHostPageTables
#define cudaDevAttrDirectManagedMemAccessFromHost                              \
  mcDeviceAttributeDirectManagedMemAccessFromHost
#define cudaDevAttrMaxBlocksPerMultiprocessor mcDevAttrMaxBlocksPerMultiprocessor
#define cudaDevAttrMaxPersistingL2CacheSize                                    \
  mcDeviceAttributeMaxPersistingL2CacheSize
#define cudaDevAttrMaxAccessPolicyWindowSize                                   \
  mcDeviceAttributeMaxAccessPolicyWindowSize
#define cudaDevAttrReservedSharedMemoryPerBlock mcDeviceAttributeUnknow
#define cudaDevAttrSparseCudaArraySupported mcDeviceAttributeUnknow
#define cudaDevAttrHostRegisterReadOnlySupported mcDeviceAttributeUnknow
#define cudaDevAttrTimelineSemaphoreInteropSupported mcDeviceAttributeUnknow
#define cudaDevAttrMaxTimelineSemaphoreInteropSupported mcDeviceAttributeUnknow
#define cudaDevAttrMemoryPoolsSupported mcDeviceAttributeMemoryPoolsSupported
#define cudaDevAttrGPUDirectRDMASupported mcDeviceAttributeUnknow
#define cudaDevAttrGPUDirectRDMAFlushWritesOptions mcDeviceAttributeUnknow
#define cudaDevAttrGPUDirectRDMAWritesOrdering mcDeviceAttributeUnknow
#define cudaDevAttrMemoryPoolSupportedHandleTypes                              \
  mcDeviceAttributeMemoryPoolSupportedHandleTypes
#define cudaDevAttrDeferredMappingCudaArraySupported mcDeviceAttributeUnknow
#define cudaDevAttrMax mcDeviceAttributeMax

/* enum */
#define cudaComputeMode mcComputeMode
#define cudaComputeModeDefault mcComputeModeDefault
#define cudaComputeModeExclusive mcComputeModeExclusive
#define cudaComputeModeProhibited mcComputeModeProhibited
#define cudaComputeModeExclusiveProcess mcComputeModeExclusiveProcess

/* enum, func attribute */
#define cudaFuncAttribute mcFuncAttribute_enum
#define cudaFuncAttributeMaxDynamicSharedMemorySize                            \
  mcFuncAttributeMaxDynamicSharedMemorySize
#define cudaFuncAttributePreferredSharedMemoryCarveout                         \
  mcFuncAttributePreferredSharedMemoryCarveout
#define cudaFuncAttributeMax mcFuncAttributeMax

/* macro */
#define cudaHostAllocDefault mcMallocHostDefault
#define cudaHostAllocPortable mcMallocHostPortable
#define cudaHostAllocMapped mcMallocHostMapped
#define cudaHostAllocWriteCombined mcMallocHostWriteCombined

#define cudaHostRegisterDefault mcHostRegisterDefault
#define cudaHostRegisterPortable mcHostRegisterPortable
#define cudaHostRegisterMapped mcHostRegisterMapped
#define cudaHostRegisterIoMemory mcHostRegisterIoMemory
#define cudaHostRegisterReadOnly mcHostRegisterReadOnly

#define cudaPeerAccessDefault mcPeerAccessDefault

#define cudaMemAttachGlobal mcMemAttachGlobal
#define cudaMemAttachHost mcMemAttachHost
#define cudaMemAttachSingle mcMemAttachSingle

#define cudaArrayDefault mcArrayDefault
#define cudaArrayLayered mcArrayLayered
#define cudaArraySurfaceLoadStore mcArraySurfaceLoadStore
#define cudaArrayCubemap mcArrayCubemap
#define cudaArrayTextureGather mcArrayTextureGather
#define cudaArrayColorAttachment mcArrayColorAttachment
#define cudaArraySparse mcArraySparse
#define cudaArrayDeferredMapping mcArrayDeferredMapping

#define cudaEventBlockingSync mcEventBlockingSync
#define cudaEventDefault mcEventDefault
#define cudaEventDisableTiming mcEventDisableTiming
#define cudaEventInterprocess mcEventInterprocess
#define cudaEventRecordDefault mcEventRecordDefault
#define cudaEventRecordExternal mcEventRecordExternal
#define cudaEventWaitDefault mcEventWaitDefault
#define cudaEventWaitExternal mcEventWaitExternal

#define cudaIpcMemLazyEnablePeerAccess mcIpcMemLazyEnablePeerAccess

/* micro */
#define cudaDeviceScheduleAuto mcDeviceScheduleAuto
#define cudaDeviceScheduleSpin mcDeviceScheduleSpin
#define cudaDeviceScheduleYield mcDeviceScheduleYield
#define cudaDeviceScheduleBlockingSync mcDeviceScheduleBlockingSync
#define cudaDeviceBlockingSync mcDeviceScheduleBlockingSync
#define cudaDeviceScheduleMask mcDeviceScheduleMask
#define cudaDeviceMapHost mcDeviceMapHost
#define cudaDeviceLmemResizeToMax mcDeviceLmemResizeToMax
#define cudaDeviceMask mcDeviceFlagsMask

#define cudaStreamDefault mcStreamDefault
#define cudaStreamNonBlocking mcStreamNonBlocking

#define cudaStreamLegacy mcStreamAllThreads
#define cudaStreamPerThread mcStreamPerThread

/* enum */
#define cudaSharedMemConfig mcSharedMemConfig
#define cudaSharedMemBankSizeDefault mcSharedMemBankSizeDefault
#define cudaSharedMemBankSizeFourByte mcSharedMemBankSizeFourByte
#define cudaSharedMemBankSizeEightByte mcSharedMemBankSizeEightByte

/* enum */
#define cudaMemoryAdvise mcMemoryAdvise_enum
#define cudaMemAdviseSetReadMostly mcMemAdviseSetReadMostly
#define cudaMemAdviseUnsetReadMostly mcMemAdviseUnsetReadMostly
#define cudaMemAdviseSetPreferredLocation mcMemAdviseSetPreferredLocation
#define cudaMemAdviseUnsetPreferredLocation mcMemAdviseUnsetPreferredLocation
#define cudaMemAdviseSetAccessedBy mcMemAdviseSetAccessedBy
#define cudaMemAdviseUnsetAccessedBy mcMemAdviseUnsetAccessedBy

/* enum */
#define cudaFuncCache mcFuncCache_t
#define cudaFuncCachePreferNone mcFuncCachePreferNone
#define cudaFuncCachePreferShared mcFuncCachePreferShared
#define cudaFuncCachePreferL1 mcFuncCachePreferL1
#define cudaFuncCachePreferEqual mcFuncCachePreferEqual

/* enum */
#define cudaLimit mcLimit_t
#define cudaLimitStackSize mcLimitStackSize
#define cudaLimitPrintfFifoSize mcLimitPrintfFifoSize
#define cudaLimitMallocHeapSize mcLimitMallocHeapSize
#define cudaLimitDevRuntimeSyncDepth mcLimitDevRuntimeSyncDepth
#define cudaLimitDevRuntimePendingLaunchCount                                  \
  mcLimitDevRuntimePendingLaunchCount
#define cudaLimitMaxL2FetchGranularity mcLimitMaxL2FetchGranularity
#define cudaLimitPersistingL2CacheSize mcLimitPersistingL2CacheSize

#define cudaDeviceP2PAttr mcDeviceP2PAttr
#define cudaDevP2PAttrPerformanceRank mcDevP2PAttrPerformanceRank
#define cudaDevP2PAttrAccessSupported mcDevP2PAttrAccessSupported
#define cudaDevP2PAttrNativeAtomicSupported mcDevP2PAttrNativeAtomicSupported
#define cudaDevP2PAttrCudaArrayAccessSupported                                 \
  mcDevP2PAttrMcArrayAccessSupported

#define cudaFlushGPUDirectRDMAWritesTarget mcFlushGPUDirectRDMAWritesTarget
#define cudaFlushGPUDirectRDMAWritesTargetCurrentDevice                        \
  mcFlushGPUDirectRDMAWritesTargetCurrentDevice

#define cudaFlushGPUDirectRDMAWritesScope mcFlushGPUDirectRDMAWritesScope
#define cudaFlushGPUDirectRDMAWritesToOwner mcFlushGPUDirectRDMAWritesToOwner
#define cudaFlushGPUDirectRDMAWritesToAllDevices                               \
  mcFlushGPUDirectRDMAWritesToAllDevices

#define cudaMemAllocationType mcMemAllocationType
#define cudaMemAllocationTypeInvalid mcMemAllocationTypeInvalid
#define cudaMemAllocationTypePinned mcMemAllocationTypePinned
#define cudaMemAllocationTypeMax mcMemAllocationTypeMax

#define cudaMemAllocationHandleType mcMemAllocationHandleType
#define cudaMemHandleTypeNone mcMemHandleTypeNone
#define cudaMemHandleTypePosixFileDescriptor mcMemHandleTypePosixFileDescriptor
#define cudaMemHandleTypeWin32 mcMemHandleTypeWin32
#define cudaMemHandleTypeWin32Kmt mcMemHandleTypeWin32Kmt

#define cudaMemLocationType mcMemLocationType
#define cudaMemLocationTypeInvalid mcMemLocationTypeInvalid
#define cudaMemLocationTypeDevice mcMemLocationTypeDevice

#define cudaMemAccessFlags mcMemAccessFlags
#define cudaMemAccessFlagsProtNone mcMemAccessFlagsProtNone
#define cudaMemAccessFlagsProtRead mcMemAccessFlagsProtRead
#define cudaMemAccessFlagsProtReadWrite mcMemAccessFlagsProtReadWrite

/* enum */
#define cudaMemPoolAttr mcMemPoolAttr
#define cudaMemPoolReuseFollowEventDependencies                                \
  mcMemPoolReuseFollowEventDependencies
#define cudaMemPoolReuseAllowOpportunistic mcMemPoolReuseAllowOpportunistic
#define cudaMemPoolReuseAllowInternalDependencies                              \
  mcMemPoolReuseAllowInternalDependencies
#define cudaMemPoolAttrReleaseThreshold mcMemPoolAttrReleaseThreshold
#define cudaMemPoolAttrReservedMemCurrent mcMemPoolAttrReservedMemCurrent
#define cudaMemPoolAttrReservedMemHigh mcMemPoolAttrReservedMemHigh
#define cudaMemPoolAttrUsedMemCurrent mcMemPoolAttrUsedMemCurrent
#define cudaMemPoolAttrUsedMemHigh mcMemPoolAttrUsedMemHigh

/* struct */
#define cudaDeviceProp _mcDeviceProp_t
#define cudaDevicePropDontCare mcDevicePropDontCare
#define cudaIpcEventHandle_st mcIpcEventHandle_st
#define cudaIpcEventHandle_t mcIpcEventHandle_t
#define cudaEvent_t mcEvent_t
#define cudaStream_t mcStream_t
#define cudaPointerAttributes _mcPointerAttribute_t
#define cudaExtent mcExtent
#define cudaPitchedPtr mcPitchedPtr
#define cudaPos _mcPos
#define cudaArray_t mcArray_t
#define cudaArray _mcArray
#define cudaArray_const_t mcArray_const_t
#define cudaChannelFormatDesc mcChannelFormatDesc_enum
#define cudaMemcpy3DParms mcMemcpy3DParms
#define cudaMemcpy3DPeerParms mcMemcpy3DPeerParms_enum

#define cudaIpcMemHandle_st mcIpcMemHandle_st
#define cudaIpcMemHandle_t mcIpcMemHandle_t

#define cudaMemPool_t mcMemPool_t
#define cudaMemPoolProps mcMemPoolProps

#define cudaMemPoolPtrExportData mcMemPoolPtrExportData
#define cudaMemLocation mcMemLocation
#define cudaMemAccessDesc mcMemAccessDesc

// function redefinition

/******************Memory runtime API*************************/
#define cudaMalloc wcudaMalloc
#define cudaMallocHost wcudaMallocHost
#define cudaMalloc3D wcudaMalloc3D
#define cudaMalloc3DArray wcudaMalloc3DArray
#define cudaMallocArray wcudaMallocArray
#define cudaMallocMipmappedArray wcudaMallocMipmappedArray
#define cudaFree wcudaFree
#define cudaFreeHost wcudaFreeHost
#define cudaFreeArray wcudaFreeArray
#define cudaFreeMipmappedArray wcudaFreeMipmappedArray
#define cudaHostAlloc wcudaHostAlloc
#define cudaMemcpy wcudaMemcpy
#define cudaMemcpy2DAsync wcudaMemcpy2DAsync
#define cudaMemcpy2DToArrayAsync wcudaMemcpy2DToArrayAsync
#define cudaMemcpy2DFromArrayAsync wcudaMemcpy2DFromArrayAsync
#define cudaMemcpyAsync wcudaMemcpyAsync
#define cudaMemcpyToArray wcudaMemcpyToArray
#define cudaMemcpyFromArray wcudaMemcpyFromArray
#define cudaMemcpyArrayToArray wcudaMemcpyArrayToArray
#define cudaMemcpyToArrayAsync wcudaMemcpyToArrayAsync
#define cudaMemcpyFromArrayAsync wcudaMemcpyFromArrayAsync
#define cudaMemcpy2DToArray wcudaMemcpy2DToArray
#define cudaMemcpy2DFromArray wcudaMemcpy2DFromArray
#define cudaMemcpy2DArrayToArray wcudaMemcpy2DArrayToArray
#define cudaHostGetDevicePointer wcudaHostGetDevicePointer
#define cudaHostRegister wcudaHostRegister
#define cudaHostUnregister wcudaHostUnregister
#define cudaMemGetInfo wcudaMemGetInfo
#define cudaArrayGetInfo wcudaArrayGetInfo
#define cudaArrayGetPlane wcudaArrayGetPlane
#define cudaArrayGetMemoryRequirements wcudaArrayGetMemoryRequirements
#define cudaMipmappedArrayGetMemoryRequirements                                \
  wcudaMipmappedArrayGetMemoryRequirements
#define cudaArrayGetSparseProperties wcudaArrayGetSparseProperties
#define cudaMipmappedArrayGetSparseProperties                                  \
  wcudaMipmappedArrayGetSparseProperties
#define cudaMemset wcudaMemset
#define cudaMemsetAsync wcudaMemsetAsync

#define __CUDART_API_VERSION __WCUDART_API_VERSION
#define __CUDACC_VER_MAJOR__ __WCUDACC_VER_MAJOR__
#define __CUDACC_VER_MINOR__ __WCUDACC_VER_MINOR__
#define __CUDA_API_VER_MAJOR__ __WCUDA_API_VER_MAJOR__
#define __CUDA_API_VER_MINOR__ __WCUDA_API_VER_MINOR__

#define cudaDriverGetVersion wcudaDriverGetVersion
#define cudaRuntimeGetVersion wcudaRuntimeGetVersion

#define cudaGetSymbolAddress wcudaGetSymbolAddress
#define cudaGetSymbolSize wcudaGetSymbolSize
#define cudaHostGetFlags wcudaHostGetFlags
#define cudaMallocPitch wcudaMallocPitch
#define cudaMemcpy2D wcudaMemcpy2D
#define cudaMemcpy3D wcudaMemcpy3D
#define cudaMemcpy3DAsync wcudaMemcpy3DAsync
#define cudaMemcpy3DPeer wcudaMemcpy3DPeer
#define cudaMemcpy3DPeerAsync wcudaMemcpy3DPeerAsync
#define cudaMemcpyFromSymbol wcudaMemcpyFromSymbol
#define cudaMemcpyFromSymbolAsync wcudaMemcpyFromSymbolAsync
#define cudaMemcpyPeer wcudaMemcpyPeer
#define cudaMemcpyPeerAsync wcudaMemcpyPeerAsync
#define cudaMemcpyToSymbol wcudaMemcpyToSymbol
#define cudaMemcpyToSymbolAsync wcudaMemcpyToSymbolAsync
#define cudaMemset2D wcudaMemset2D
#define cudaMemset2DAsync wcudaMemset2DAsync
#define cudaMemset3D wcudaMemset3D
#define cudaMemset3DAsync wcudaMemset3DAsync

#define make_cudaExtent make_wcudaExtent
#define make_cudaPitchedPtr make_wcudaPitchedPtr
#define make_cudaPos make_wcudaPos

/****************Unified Addressing API***********************/
#define cudaMallocManaged wcudaMallocManaged
#define cudaMemAdvise wcudaMemAdvise
#define cudaPointerGetAttributes wcudaPointerGetAttributes
#define cudaMemPrefetchAsync wcudaMemPrefetchAsync
#define cudaMemRangeGetAttribute wcudaMemRangeGetAttribute
#define cudaMemRangeGetAttributes wcudaMemRangeGetAttributes

/*Stream runtime types*/
/*enum*/
#define cudaStreamAttrID mcStreamAttrID_enum
#define cudaStreamAttributeAccessPolicyWindow                                  \
  mcStreamAttributeAccessPolicyWindow
#define cudaStreamAttributeSynchronizationPolicy                               \
  mcStreamAttributeSynchronizationPolicy

#define cudaSynchronizationPolicy mcSynchronizationPolicy
#define cudaSyncPolicyAuto mcSyncPolicyAuto
#define cudaSyncPolicySpin mcSyncPolicySpin
#define cudaSyncPolicyYield mcSyncPolicyYield
#define cudaSyncPolicyBlockingSync mcSyncPolicyBlockingSync

#define cudaAccessProperty mcAccessProperty_enum
#define cudaAccessPropertyNormal mcAccessPropertyNormal
#define cudaAccessPropertyStreaming mcAccessPropertyStreaming
#define cudaAccessPropertyPersisting mcAccessPropertyPersisting

/* union */
#define cudaStreamAttrValue mcStreamAttrValue

/* micro */
#define cudaCpuDeviceId mcCpuDeviceId
#define cudaInvalidDeviceId mcInvalidDeviceId

#define cudaCooperativeLaunchMultiDeviceNoPreSync                              \
  mcCooperativeLaunchMultiDeviceNoPreSync
#define cudaCooperativeLaunchMultiDeviceNoPostSync                             \
  mcCooperativeLaunchMultiDeviceNoPostSync

/* enum */
#define cudaMemRangeAttribute mcMemRangeAttribute_enum
#define cudaMemRangeAttributeReadMostly mcMemRangeAttributeReadMostly
#define cudaMemRangeAttributePreferredLocation                                 \
  mcMemRangeAttributePreferredLocation
#define cudaMemRangeAttributeAccessedBy mcMemRangeAttributeAccessedBy
#define cudaMemRangeAttributeLastPrefetchLocation                              \
  mcMemRangeAttributeLastPrefetchLocation

/* callback_func */
#define cudaStreamCallback_t mcStreamCallback_t

/* enum */
#define cudaStreamCaptureMode mcStreamCaptureMode_enum
#define cudaStreamCaptureModeGlobal mcStreamCaptureModeGlobal
#define cudaStreamCaptureModeThreadLocal mcStreamCaptureModeThreadLocal
#define cudaStreamCaptureModeRelaxed mcStreamCaptureModeRelaxed

#define cudaStreamCaptureStatus mcStreamCaptureStatus_enum
#define cudaStreamCaptureStatusNone mcStreamCaptureStatusNone
#define cudaStreamCaptureStatusActive mcStreamCaptureStatusActive
#define cudaStreamCaptureStatusInvalidated mcStreamCaptureStatusInvalidated

#define cudaStreamUpdateCaptureDependenciesFlags                               \
  mcStreamUpdateCaptureDependenciesFlags_enum
#define cudaStreamAddCaptureDependencies mcStreamAddCaptureDependencies
#define cudaStreamSetCaptureDependencies mcStreamSetCaptureDependencies
/******************Stream runtime API*************************/
#define cudaStreamCreate wcudaStreamCreate
#define cudaStreamCreateWithFlags wcudaStreamCreateWithFlags
#define cudaStreamCreateWithPriority wcudaStreamCreateWithPriority
#define cudaStreamDestroy wcudaStreamDestroy
#define cudaStreamGetFlags wcudaStreamGetFlags
#define cudaStreamGetPriority wcudaStreamGetPriority
#define cudaStreamQuery wcudaStreamQuery
#define cudaStreamSynchronize wcudaStreamSynchronize
#define cudaStreamWaitEvent wcudaStreamWaitEvent
#define cudaStreamAddCallback wcudaStreamAddCallback
#define cudaCtxResetPersistingL2Cache wcudaCtxResetPersistingL2Cache
#define cudaStreamAttachMemAsync wcudaStreamAttachMemAsync
#define cudaStreamCopyAttributes wcudaStreamCopyAttributes
#define cudaStreamGetAttribute wcudaStreamGetAttribute
#define cudaStreamSetAttribute wcudaStreamSetAttribute
#define cudaStreamBeginCapture wcudaStreamBeginCapture
#define cudaStreamEndCapture wcudaStreamEndCapture
#define cudaStreamIsCapturing wcudaStreamIsCapturing
#define cudaStreamGetCaptureInfo wcudaStreamGetCaptureInfo
#define cudaStreamGetCaptureInfo_v2 wcudaStreamGetCaptureInfo_v2
#define cudaStreamUpdateCaptureDependencies wcudaStreamUpdateCaptureDependencies
#define cudaThreadExchangeStreamCaptureMode wcudaThreadExchangeStreamCaptureMode
/**************Execution Control runtime API******************/
#define cudaFuncGetAttributes wcudaFuncGetAttributes
#define cudaLaunchKernel wcudaLaunchKernel
#define cudaFuncSetAttribute wcudaFuncSetAttribute
#define cudaFuncSetCacheConfig wcudaFuncSetCacheConfig
#define cudaFuncSetSharedMemConfig wcudaFuncSetSharedMemConfig
#define cudaLaunchCooperativeKernel wcudaLaunchCooperativeKernel
#define cudaLaunchHostFunc wcudaLaunchHostFunc
#define cudaSetDoubleForDevice wcudaSetDoubleForDevice
#define cudaSetDoubleForHost wcudaSetDoubleForHost
#define cudaLaunchCooperativeKernelMultiDevice                                 \
  wcudaLaunchCooperativeKernelMultiDevice
#define cudaLaunchKernelExC mcLaunchKernelExC

/**************Execution Control runtime type******************/
#define CUDART_CB MC_CB
#define cudaHostFn_t mcHostFn_t
#define cudaFuncAttributes mcFuncAttributes
#define cudaLaunchConfig_t         mcLaunchConfig_t
#define cudaLaunchAttribute        mcLaunchAttribute
#define cudaLaunchMemSyncDomainMap mcLaunchMemSyncDomainMap
#define cudaGraphDeviceNode_t      mcGraphDeviceNode_t
/*enum ClusterSchedulingPolicy*/
#define cudaClusterSchedulingPolicyDefault       mcClusterSchedulingPolicyDefault
#define cudaClusterSchedulingPolicySpread        mcClusterSchedulingPolicySpread
#define cudaClusterSchedulingPolicyLoadBalancing mcClusterSchedulingPolicyLoadBalancing

/*enum LaunchMemSyncDomain*/
#define cudaLaunchMemSyncDomainDefault mcLaunchMemSyncDomainDefault
#define cudaLaunchMemSyncDomainRemote  mcLaunchMemSyncDomainRemote

/*enum LaunchAttributeID*/
#define cudaLaunchAttributeIgnore                mcLaunchAttributeIgnore
#define cudaLaunchAttributeAccessPolicyWindow    mcLaunchAttributeAccessPolicyWindow
#define cudaLaunchAttributeCooperative           mcLaunchAttributeCooperative
#define cudaLaunchAttributeSynchronizationPolicy mcLaunchAttributeSynchronizationPolicy
#define cudaLaunchAttributeClusterDimension      mcLaunchAttributeClusterDimension
#define cudaLaunchAttributeClusterSchedulingPolicyPreference                                       \
    mcLaunchAttributeClusterSchedulingPolicyPreference
#define cudaLaunchAttributeProgrammaticStreamSerialization                                         \
    mcLaunchAttributeProgrammaticStreamSerialization
#define cudaLaunchAttributeProgrammaticEvent         mcLaunchAttributeProgrammaticEvent
#define cudaLaunchAttributePriority                  mcLaunchAttributePriority
#define cudaLaunchAttributeMemSyncDomainMap          mcLaunchAttributeMemSyncDomainMap
#define cudaLaunchAttributeMemSyncDomain             mcLaunchAttributeMemSyncDomain
#define cudaLaunchAttributeLaunchCompletionEvent     mcLaunchAttributeLaunchCompletionEvent
#define cudaLaunchAttributeDeviceUpdatableKernelNode mcLaunchAttributeDeviceUpdatableKernelNode
#define cudaLaunchAttributePreferredSharedMemoryCarveout                                           \
    mcLaunchAttributePreferredSharedMemoryCarveout

/**************Profiler Control runtime API******************/
#define cudaProfilerInitialize wcudaProfilerInitialize
#define cudaProfilerStart wcudaProfilerStart
#define cudaProfilerStop wcudaProfilerStop

/**********Stream Ordered Memory Allocator runtime API************/
#define cudaMallocAsync wcudaMallocAsync
#define cudaMallocFromPoolAsync wcudaMallocFromPoolAsync
#define cudaFreeAsync wcudaFreeAsync
#define cudaMemPoolCreate wcudaMemPoolCreate
#define cudaMemPoolDestroy wcudaMemPoolDestroy
#define cudaMemPoolExportPointer wcudaMemPoolExportPointer
#define cudaMemPoolExportToShareableHandle wcudaMemPoolExportToShareableHandle
#define cudaMemPoolGetAccess wcudaMemPoolGetAccess
#define cudaMemPoolGetAttribute wcudaMemPoolGetAttribute
#define cudaMemPoolImportFromShareableHandle                                   \
  wcudaMemPoolImportFromShareableHandle
#define cudaMemPoolImportPointer wcudaMemPoolImportPointer
#define cudaMemPoolSetAccess wcudaMemPoolSetAccess
#define cudaMemPoolSetAttribute wcudaMemPoolSetAttribute
#define cudaMemPoolTrimTo wcudaMemPoolTrimTo

/******************Device runtime API*************************/
#define cudaGetDevice wcudaGetDevice
#define cudaSetDevice wcudaSetDevice
#define cudaGetDeviceFlags wcudaGetDeviceFlags
#define cudaSetDeviceFlags wcudaSetDeviceFlags
#define cudaGetDeviceCount wcudaGetDeviceCount
#define cudaGetDeviceProperties wcudaGetDeviceProperties
#define cudaDeviceGetAttribute wcudaDeviceGetAttribute
#define cudaDeviceGetByPCIBusId wcudaDeviceGetByPCIBusId
#define cudaDeviceGetPCIBusId wcudaDeviceGetPCIBusId
#define cudaDeviceGetSharedMemConfig wcudaDeviceGetSharedMemConfig
#define cudaDeviceSetSharedMemConfig wcudaDeviceSetSharedMemConfig
#define cudaDeviceGetStreamPriorityRange wcudaDeviceGetStreamPriorityRange
#define cudaDeviceSynchronize wcudaDeviceSynchronize
#define cudaChooseDevice wcudaChooseDevice
#define cudaDeviceGetDefaultMemPool wcudaDeviceGetDefaultMemPool
#define cudaDeviceGetMemPool wcudaDeviceGetMemPool
#define cudaDeviceSetMemPool wcudaDeviceSetMemPool
#define cudaDeviceGetCacheConfig wcudaDeviceGetCacheConfig
#define cudaDeviceSetCacheConfig wcudaDeviceSetCacheConfig
#define cudaDeviceGetLimit wcudaDeviceGetLimit
#define cudaDeviceSetLimit wcudaDeviceSetLimit
#define cudaDeviceReset wcudaDeviceReset
#define cudaSetValidDevices wcudaSetValidDevices
#define cudaDeviceGetP2PAttribute wcudaDeviceGetP2PAttribute
#define cudaDeviceFlushGPUDirectRDMAWrites wcudaDeviceFlushGPUDirectRDMAWrites
#define cudaDeviceGetNvSciSyncAttributes wcudaDeviceGetNvSciSyncAttributes
#define cudaDeviceGetTexture1DLinearMaxWidth                                   \
  wcudaDeviceGetTexture1DLinearMaxWidth
#define cudaIpcGetMemHandle wcudaIpcGetMemHandle
#define cudaIpcOpenMemHandle wcudaIpcOpenMemHandle
#define cudaIpcCloseMemHandle wcudaIpcCloseMemHandle

/******************Thread Management**************************/
/* Thread Management APIs are deprecated and they are replaced by Device
 * Management APIs instead */
#define cudaThreadGetLimit wcudaThreadGetLimit
#define cudaThreadSetLimit wcudaThreadSetLimit
#define cudaThreadGetCacheConfig wcudaThreadGetCacheConfig
#define cudaThreadSetCacheConfig wcudaThreadSetCacheConfig
#define cudaThreadSynchronize wcudaThreadSynchronize
#define cudaThreadExit wcudaThreadExit

/******************Event runtime API**************************/
#define cudaEventCreate wcudaEventCreate
#define cudaEventCreateWithFlags wcudaEventCreateWithFlags
#define cudaEventDestroy wcudaEventDestroy
#define cudaEventElapsedTime wcudaEventElapsedTime
#define cudaEventQuery wcudaEventQuery
#define cudaEventRecord wcudaEventRecord
#define cudaEventRecordWithFlags wcudaEventRecordWithFlags
#define cudaEventSynchronize wcudaEventSynchronize
#define cudaIpcGetEventHandle wcudaIpcGetEventHandle
#define cudaIpcOpenEventHandle wcudaIpcOpenEventHandle

/************Interactions with the MACA Driver API******************/
#define cudaFunction_t mcFunction_t
#define cudaGetFuncBySymbol wcudaGetFuncBySymbol

/************Driver Entry Point Access******************/
/* enum */
#define cudaGetDriverEntryPointFlags mcGetDriverEntryPointFlags
#define cudaEnableDefault mcEnableDefault
#define cudaEnableLegacyStream mcEnableLegacyStream
#define cudaEnablePerThreadDefaultStream mcEnablePerThreadDefaultStream

#define cudaGetDriverEntryPoint wcudaGetDriverEntryPoint

/******************Error Handling runtime API**************************/
#define cudaGetErrorName wcudaGetErrorName
#define cudaGetErrorString wcudaGetErrorString
#define cudaGetLastError wcudaGetLastError
#define cudaPeekAtLastError wcudaPeekAtLastError

/**********Peer Context Memory Access runtime API************/
#define cudaDeviceCanAccessPeer wcudaDeviceCanAccessPeer
#define cudaDeviceEnablePeerAccess wcudaDeviceEnablePeerAccess
#define cudaDeviceDisablePeerAccess wcudaDeviceDisablePeerAccess

/**********External Resource Interoperability API************/
/* enum */
#define cudaExternalSemaphoreHandleType mcExternalSemaphoreHandleType
#define cudaExternalSemaphoreHandleTypeOpaqueFd                                \
  mcExternalSemaphoreHandleTypeOpaqueFd
#define cudaExternalSemaphoreHandleTypeOpaqueWin32                             \
  mcExternalSemaphoreHandleTypeOpaqueWin32
#define cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt                          \
  mcExternalSemaphoreHandleTypeOpaqueWin32Kmt
#define cudaExternalSemaphoreHandleTypeD3D12Fence                              \
  mcExternalSemaphoreHandleTypeD3D12Fence
#define cudaExternalSemaphoreHandleTypeD3D11Fence                              \
  mcExternalSemaphoreHandleTypeD3D11Fence
#define cudaExternalSemaphoreHandleTypeNvSciSync                               \
  mcExternalSemaphoreHandleTypeNvSciSync
#define cudaExternalSemaphoreHandleTypeKeyedMutex                              \
  mcExternalSemaphoreHandleTypeKeyedMutex
#define cudaExternalSemaphoreHandleTypeKeyedMutexKmt                           \
  mcExternalSemaphoreHandleTypeKeyedMutexKmt
#define cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd                     \
  mcExternalSemaphoreHandleTypeTimelineSemaphoreFd
#define cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32                  \
  mcExternalSemaphoreHandleTypeTimelineSemaphoreWin32

/* struct */
#define CUexternalSemaphore_st mcExternalSemaphore_st
#define cudaExternalSemaphore_t mcExternalSemaphore_t
#define cudaExternalSemaphoreHandleDesc mcExternalSemaphoreHandleDesc_st
#define cudaExternalSemaphoreSignalParams mcExternalSemaphoreSignalParams_st
#define cudaExternalSemaphoreWaitParams mcExternalSemaphoreWaitParams_st

#define cudaExternalSemaphoreSignalParams_v1                                   \
  mcExternalSemaphoreSignalParams_deprecated
#define cudaExternalSemaphoreWaitParams_v1                                     \
  mcExternalSemaphoreWaitParams_deprecated

#define cudaImportExternalSemaphore wcudaImportExternalSemaphore
#define cudaDestroyExternalSemaphore wcudaDestroyExternalSemaphore
#define cudaSignalExternalSemaphoresAsync wcudaSignalExternalSemaphoresAsync
#define cudaSignalExternalSemaphoresAsync_v2 wcudaSignalExternalSemaphoresAsync
#define cudaWaitExternalSemaphoresAsync wcudaWaitExternalSemaphoresAsync
#define cudaWaitExternalSemaphoresAsync_v2 wcudaWaitExternalSemaphoresAsync

/* enum */
#define cudaExternalMemoryHandleType mcExternalMemoryHandleType
#define cudaExternalMemoryHandleTypeOpaqueFd mcExternalMemoryHandleTypeOpaqueFd
#define cudaExternalMemoryHandleTypeOpaqueWin32                                \
  mcExternalMemoryHandleTypeOpaqueWin32
#define cudaExternalMemoryHandleTypeOpaqueWin32Kmt                             \
  mcExternalMemoryHandleTypeOpaqueWin32Kmt
#define cudaExternalMemoryHandleTypeD3D12Heap                                  \
  mcExternalMemoryHandleTypeD3D12Heap
#define cudaExternalMemoryHandleTypeD3D12Resource                              \
  mcExternalMemoryHandleTypeD3D12Resource
#define cudaExternalMemoryHandleTypeD3D11Resource                              \
  mcExternalMemoryHandleTypeD3D11Resource
#define cudaExternalMemoryHandleTypeD3D11ResourceKmt                           \
  mcExternalMemoryHandleTypeD3D11ResourceKmt
#define cudaExternalMemoryHandleTypeNvSciBuf mcExternalMemoryHandleTypeNvSciBuf

#define CUexternalMemory_st _mcExternalMemory_t
#define cudaExternalMemory_t mcExternalMemory_t
#define cudaExternalMemoryHandleDesc mcExternalMemoryHandleDesc_st
#define cudaExternalMemoryBufferDesc mcExternalMemoryBufferDesc_st

/* enum */
#define cudaChannelFormatKind mcChannelFormatKind_enum
#define cudaChannelFormatKindSigned mcChannelFormatKindSigned
#define cudaChannelFormatKindUnsigned mcChannelFormatKindUnsigned
#define cudaChannelFormatKindFloat mcChannelFormatKindFloat
#define cudaChannelFormatKindNone mcChannelFormatKindNone
#define cudaChannelFormatKindNV12 mcChannelFormatKindNV12
#define cudaChannelFormatKindUnsignedNormalized8X1                             \
  mcChannelFormatKindUnsignedNormalized8X1
#define cudaChannelFormatKindUnsignedNormalized8X2                             \
  mcChannelFormatKindUnsignedNormalized8X2
#define cudaChannelFormatKindUnsignedNormalized8X4                             \
  mcChannelFormatKindUnsignedNormalized8X4
#define cudaChannelFormatKindUnsignedNormalized16X1                            \
  mcChannelFormatKindUnsignedNormalized16X1
#define cudaChannelFormatKindUnsignedNormalized16X2                            \
  mcChannelFormatKindUnsignedNormalized16X2
#define cudaChannelFormatKindUnsignedNormalized16X4                            \
  mcChannelFormatKindUnsignedNormalized16X4
#define cudaChannelFormatKindSignedNormalized8X1                               \
  mcChannelFormatKindSignedNormalized8X1
#define cudaChannelFormatKindSignedNormalized8X2                               \
  mcChannelFormatKindSignedNormalized8X2
#define cudaChannelFormatKindSignedNormalized8X4                               \
  mcChannelFormatKindSignedNormalized8X4
#define cudaChannelFormatKindSignedNormalized16X1                              \
  mcChannelFormatKindSignedNormalized16X1
#define cudaChannelFormatKindSignedNormalized16X2                              \
  mcChannelFormatKindSignedNormalized16X2
#define cudaChannelFormatKindSignedNormalized16X4                              \
  mcChannelFormatKindSignedNormalized16X4
#define cudaChannelFormatKindUnsignedBlockCompressed1                          \
  mcChannelFormatKindUnsignedBlockCompressed1
#define cudaChannelFormatKindUnsignedBlockCompressed1SRGB                      \
  mcChannelFormatKindUnsignedBlockCompressed1SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed2                          \
  mcChannelFormatKindUnsignedBlockCompressed2
#define cudaChannelFormatKindUnsignedBlockCompressed2SRGB                      \
  mcChannelFormatKindUnsignedBlockCompressed2SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed3                          \
  mcChannelFormatKindUnsignedBlockCompressed3
#define cudaChannelFormatKindUnsignedBlockCompressed3SRGB                      \
  mcChannelFormatKindUnsignedBlockCompressed3SRGB
#define cudaChannelFormatKindUnsignedBlockCompressed4                          \
  mcChannelFormatKindUnsignedBlockCompressed4
#define cudaChannelFormatKindSignedBlockCompressed4                            \
  mcChannelFormatKindSignedBlockCompressed4
#define cudaChannelFormatKindUnsignedBlockCompressed5                          \
  mcChannelFormatKindUnsignedBlockCompressed5
#define cudaChannelFormatKindSignedBlockCompressed5                            \
  mcChannelFormatKindSignedBlockCompressed5
#define cudaChannelFormatKindUnsignedBlockCompressed6H                         \
  mcChannelFormatKindUnsignedBlockCompressed6H
#define cudaChannelFormatKindSignedBlockCompressed6H                           \
  mcChannelFormatKindSignedBlockCompressed6H
#define cudaChannelFormatKindUnsignedBlockCompressed7                          \
  mcChannelFormatKindUnsignedBlockCompressed7
#define cudaChannelFormatKindUnsignedBlockCompressed7SRGB                      \
  mcChannelFormatKindUnsignedBlockCompressed7SRGB

#define cudaMipmappedArray _mcMipmappedArray_t
#define cudaMipmappedArray_t mcMipmappedArray_t
#define cudaMipmappedArray_const_t mcMipmappedArray_const_t
#define cudaExternalMemoryMipmappedArrayDesc                                   \
  mcExternalMemoryMipmappedArrayDesc_st

#define cudaImportExternalMemory wcudaImportExternalMemory
#define cudaDestroyExternalMemory wcudaDestroyExternalMemory
#define cudaExternalMemoryGetMappedBuffer wcudaExternalMemoryGetMappedBuffer
#define cudaExternalMemoryGetMappedMipmappedArray                              \
  wcudaExternalMemoryGetMappedMipmappedArray

/**************Graphics runtime API******************/
#define cudaGraphicsMapResources wcudaGraphicsMapResources
#define cudaGraphicsResourceGetMappedMipmappedArray                            \
  wcudaGraphicsResourceGetMappedMipmappedArray
#define cudaGraphicsResourceGetMappedPointer                                   \
  wcudaGraphicsResourceGetMappedPointer
#define cudaGraphicsResourceSetMapFlags wcudaGraphicsResourceSetMapFlags
#define cudaGraphicsSubResourceGetMappedArray                                  \
  wcudaGraphicsSubResourceGetMappedArray
#define cudaGraphicsUnmapResources wcudaGraphicsUnmapResources
#define cudaGraphicsUnregisterResource wcudaGraphicsUnregisterResource

#define cudaGraphicsResource mcGraphicsResource
/* enum */
#define cudaGraphicsRegisterFlags mcGraphicsRegisterFlags
#define cudaGraphicsRegisterFlagsNone mcGraphicsRegisterFlagsNone
#define cudaGraphicsRegisterFlagsReadOnly mcGraphicsRegisterFlagsReadOnly
#define cudaGraphicsRegisterFlagsWriteDiscard                                  \
  mcGraphicsRegisterFlagsWriteDiscard
#define cudaGraphicsRegisterFlagsSurfaceLoadStore                              \
  mcGraphicsRegisterFlagsSurfaceLoadStore
#define cudaGraphicsRegisterFlagsTextureGather                                 \
  mcGraphicsRegisterFlagsTextureGather

#define cudaGraphicsMapFlags mcGraphicsMapFlags
#define cudaGraphicsMapFlagsNone mcGraphicsMapFlagsNone
#define cudaGraphicsMapFlagsReadOnly mcGraphicsMapFlagsReadOnly
#define cudaGraphicsMapFlagsWriteDiscard mcGraphicsMapFlagsWriteDiscard

#define cudaGraphicsCubeFace mcGraphicsCubeFace
#define cudaGraphicsCubeFacePositiveX mcGraphicsCubeFacePositiveX
#define cudaGraphicsCubeFaceNegativeX mcGraphicsCubeFaceNegativeX
#define cudaGraphicsCubeFacePositiveY mcGraphicsCubeFacePositiveY
#define cudaGraphicsCubeFaceNegativeY mcGraphicsCubeFaceNegativeY
#define cudaGraphicsCubeFacePositiveZ mcGraphicsCubeFacePositiveZ
#define cudaGraphicsCubeFaceNegativeZ mcGraphicsCubeFaceNegativeZ

#define cudaGraphicsResource_t mcGraphicsResource_t

/**************Occupancy runtime API******************/
#define cudaOccupancyAvailableDynamicSMemPerBlock                              \
  wcudaOccupancyAvailableDynamicSMemPerBlock
#define cudaOccupancyMaxPotentialBlockSize wcudaOccupancyMaxPotentialBlockSize
#define cudaOccupancyMaxPotentialBlockSizeWithFlags                            \
  wcudaOccupancyMaxPotentialBlockSizeWithFlags
#define cudaOccupancyMaxActiveBlocksPerMultiprocessor                          \
  wcudaOccupancyMaxActiveBlocksPerMultiprocessor
#define cudaOccupancyMaxPotentialBlockSizeVariableSMem                         \
  wcudaOccupancyMaxPotentialBlockSizeVariableSMem
#define cudaOccupancyMaxPotentialBlockSizeVariableSMemWithFlags                \
  wcudaOccupancyMaxPotentialBlockSizeVariableSMemWithFlags
#define cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags                 \
  wcudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags

#define cudaOccupancyDefault mcOccupancyDefault
#define cudaOccupancyDisableCachingOverride mcOccupancyDisableCachingOverride

#define cudaOccError_enum mcOccError_enum
#define CUDA_OCC_SUCCESS MC_OCC_SUCCESS
#define CUDA_OCC_ERROR_INVALID_INPUT MC_OCC_ERROR_INVALID_INPUT
#define CUDA_OCC_ERROR_UNKNOWN_DEVICE MC_OCC_ERROR_UNKNOWN_DEVICE
#define cudaOccError mcOccError

#define cudaOccResult mcOccResult
#define cudaOccDeviceProp mcOccDeviceProp
#define cudaOccFuncAttributes mcOccFuncAttributes
#define cudaOccFuncShmemConfig_enum mcOccFuncShmemConfig_enum
#define cudaOccFuncShmemConfig mcOccFuncShmemConfig
#define cudaOccCacheConfig_enum mcOccCacheConfig_enum
#define cudaOccCacheConfig mcOccCacheConfig
#define cudaOccCarveoutConfig_enum mcOccCarveoutConfig_enum
#define cudaOccCarveoutConfig mcOccCarveoutConfig
#define cudaOccDeviceState mcOccDeviceState
#define cudaOccPartitionedGCConfig_enum mcOccPartitionedGCConfig_enum
#define cudaOccPartitionedGCConfig mcOccPartitionedGCConfig
#define cudaOccLimitingFactor_enum mcOccLimitingFactor_enum
#define cudaOccLimitingFactor mcOccLimitingFactor
#define cudaOccPartitionedGCSupport_enum mcOccPartitionedGCSupport_enum
#define cudaOccPartitionedGCSupport mcOccPartitionedGCSupport

#define cudaOccMaxActiveBlocksPerMultiprocessor                                \
  mcOccMaxActiveBlocksPerMultiprocessor
#define cudaOccMaxPotentialOccupancyBlockSize                                  \
  mcOccMaxPotentialOccupancyBlockSize
#define cudaOccMaxPotentialOccupancyBlockSizeVariableSMem                      \
  mcOccMaxPotentialOccupancyBlockSizeVariableSMem
#define cudaOccAvailableDynamicSMemPerBlock mcOccAvailableDynamicSMemPerBlock
#define cudaOccSMemAllocationGranularity mcOccSMemAllocationGranularity
#define cudaOccRegAllocationMaxPerThread mcOccRegAllocationMaxPerThread
#define cudaOccRegAllocationGranularity mcOccRegAllocationGranularity
#define cudaOccSubPartitionsPerMultiprocessor                                  \
  mcOccSubPartitionsPerMultiprocessor
#define cudaOccMaxBlocksPerMultiprocessor mcOccMaxBlocksPerMultiprocessor
#define cudaOccAlignUpShmemSizeVoltaPlus mcOccAlignUpShmemSizeVoltaPlus
#define cudaOccSMemPreferenceVoltaPlus mcOccSMemPreferenceVoltaPlus
#define cudaOccSMemPreference mcOccSMemPreference
#define cudaOccSMemPerMultiprocessor mcOccSMemPerMultiprocessor
#define cudaOccSMemPerBlock mcOccSMemPerBlock
#define cudaOccPartitionedGlobalCachingModeSupport                             \
  mcOccPartitionedGlobalCachingModeSupport
#define cudaOccDevicePropCheck mcOccDevicePropCheck
#define cudaOccFuncAttributesCheck mcOccFuncAttributesCheck
#define cudaOccDeviceStateCheck mcOccDeviceStateCheck
#define cudaOccInputCheck mcOccInputCheck
#define cudaOccPartitionedGCExpected mcOccPartitionedGCExpected
#define cudaOccMaxBlocksPerSMWarpsLimit mcOccMaxBlocksPerSMWarpsLimit
#define cudaOccMaxBlocksPerSMSmemLimit mcOccMaxBlocksPerSMSmemLimit
#define cudaOccMaxBlocksPerSMRegsLimit mcOccMaxBlocksPerSMRegsLimit

/* the max supported CUDA compute capability is 8.0 */
#define __CUDA_OCC_MAJOR__ ((int)WNV_TARGET_SM_80 / 10)
#define __CUDA_OCC_MINOR__ ((int)WNV_TARGET_SM_80 % 10)

/**************graph runtime API******************/
/* enum */
#define cudaGraphMemAttributeType mcGraphMemAttributeType_enum
#define cudaGraphMemAttrUsedMemCurrent mcGraphMemAttrUsedMemCurrent
#define cudaGraphMemAttrUsedMemHigh mcGraphMemAttrUsedMemHigh
#define cudaGraphMemAttrReservedMemCurrent mcGraphMemAttrReservedMemCurrent
#define cudaGraphMemAttrReservedMemHigh mcGraphMemAttrReservedMemHigh

#define cudaGraphNodeType mcGraphNodeType_t
#define cudaGraphNodeTypeKernel mcGraphNodeTypeKernel
#define cudaGraphNodeTypeMemcpy mcGraphNodeTypeMemcpy
#define cudaGraphNodeTypeMemset mcGraphNodeTypeMemset
#define cudaGraphNodeTypeHost mcGraphNodeTypeHost
#define cudaGraphNodeTypeGraph mcGraphNodeTypeGraph
#define cudaGraphNodeTypeEmpty mcGraphNodeTypeEmpty
#define cudaGraphNodeTypeWaitEvent mcGraphNodeTypeWaitEvent
#define cudaGraphNodeTypeEventRecord mcGraphNodeTypeEventRecord
#define cudaGraphNodeTypeExtSemaphoreSignal mcGraphNodeTypeExtSemaphoreSignal
#define cudaGraphNodeTypeExtSemaphoreWait mcGraphNodeTypeExtSemaphoreWait
#define cudaGraphNodeTypeMemAlloc mcGraphNodeTypeMemAlloc
#define cudaGraphNodeTypeMemFree mcGraphNodeTypeMemFree
#define cudaGraphNodeTypeCount mcGraphNodeTypeCount

#define cudaGraphDebugDotFlags mcGraphDebugDotFlags_enum
#define cudaGraphDebugDotFlagsVerbose mcGraphDebugDotFlagsVerbose
#define cudaGraphDebugDotFlagsKernelNodeParams                                 \
  mcGraphDebugDotFlagsKernelNodeParams
#define cudaGraphDebugDotFlagsMemcpyNodeParams                                 \
  mcGraphDebugDotFlagsMemcpyNodeParams
#define cudaGraphDebugDotFlagsMemsetNodeParams                                 \
  mcGraphDebugDotFlagsMemsetNodeParams
#define cudaGraphDebugDotFlagsHostNodeParams mcGraphDebugDotFlagsHostNodeParams
#define cudaGraphDebugDotFlagsEventNodeParams                                  \
  mcGraphDebugDotFlagsEventNodeParams
#define cudaGraphDebugDotFlagsExtSemasSignalNodeParams                         \
  mcGraphDebugDotFlagsExtSemasSignalNodeParams
#define cudaGraphDebugDotFlagsExtSemasWaitNodeParams                           \
  mcGraphDebugDotFlagsExtSemasWaitNodeParams
#define cudaGraphDebugDotFlagsKernelNodeAttributes                             \
  mcGraphDebugDotFlagsKernelNodeAttributes
#define cudaGraphDebugDotFlagsHandles mcGraphDebugDotFlagsHandles

#define cudaGraphExecUpdateResult mcGraphExecUpdateResult_enum
#define cudaGraphExecUpdateSuccess mcGraphExecUpdateSuccess
#define cudaGraphExecUpdateError mcGraphExecUpdateError
#define cudaGraphExecUpdateErrorTopologyChanged                                \
  mcGraphExecUpdateErrorTopologyChanged
#define cudaGraphExecUpdateErrorNodeTypeChanged                                \
  mcGraphExecUpdateErrorNodeTypeChanged
#define cudaGraphExecUpdateErrorFunctionChanged                                \
  mcGraphExecUpdateErrorFunctionChanged
#define cudaGraphExecUpdateErrorParametersChanged                              \
  mcGraphExecUpdateErrorParametersChanged
#define cudaGraphExecUpdateErrorNotSupported mcGraphExecUpdateErrorNotSupported
#define cudaGraphExecUpdateErrorUnsupportedFunctionChange                      \
  mcGraphExecUpdateErrorUnsupportedFunctionChange
#define cudaGraphExecUpdateErrorAttributesChanged                              \
  mcGraphExecUpdateErrorAttributesChanged

#define cudaGraphInstantiateFlags mcGraphInstantiateFlags_enum
#define cudaGraphInstantiateFlagAutoFreeOnLaunch                               \
  mcGraphInstantiateFlagAutoFreeOnLaunch
#define cudaGraphInstantiateFlagUpload mcGraphInstantiateFlagUpload
#define cudaGraphInstantiateFlagDeviceLaunch mcGraphInstantiateFlagDeviceLaunch
#define cudaGraphInstantiateFlagUseNodePriority                                \
  mcGraphInstantiateFlagUseNodePriority

#define cudaGraphInstantiateResult mcGraphInstantiateResult_enum
#define cudaGraphInstantiateSuccess mcGraphInstantiateSuccess
#define cudaGraphInstantiateError mcGraphInstantiateError
#define cudaGraphInstantiateInvalidStructure mcGraphInstantiateInvalidStructure
#define cudaGraphInstantiateNodeOperationNotSupported                          \
  mcGraphInstantiateNodeOperationNotSupported
#define cudaGraphInstantiateMultipleDevicesNotSupported                        \
  mcGraphInstantiateMultipleDevicesNotSupported

#define cudaKernelNodeAttrID mcKernelNodeAttrID
#define cudaKernelNodeAttributeAccessPolicyWindow                              \
  mcKernelNodeAttributeAccessPolicyWindow
#define cudaKernelNodeAttributeCooperative mcKernelNodeAttributeCooperative
#define cudaKernelNodeAttributePriority mcKernelNodeAttributePriority

/* struct */
#define cudaAccessPolicyWindow mcAccessPolicywindow
#define cudaArrayMemoryRequirements mcArrayMemoryRequirements_st
#define cudaArraySparseProperties mcArraySparseProperties_st

#define cudaGraphNode_t mcGraphNode_t
#define cudaGraph_t mcGraph_t
#define cudaExternalSemaphoreSignalNodeParams                                  \
  mcExternalSemaphoreSignalNodeParams_st
#define cudaExternalSemaphoreWaitNodeParams mcExternalSemaphoreWaitNodeParams_st
#define cudaHostNodeParams mcHostNodeParams_st
#define cudaLaunchParams mcLaunchParams_t
#define cudaKernelNodeParams mcKernelNodeParams_st
#define cudaMemAllocNodeParams mcMemAllocNodeParams
#define cudaMemcpy3DParms mcMemcpy3DParms
#define cudaMemsetParams mcMemsetParams_st
#define cudaGraphExec_t mcGraphExec_t
#define cudaKernelNodeAttrValue mcKernelNodeAttrValue
#define cudaUserObject_t mcUserObject_t
#define cudaGraphInstantiateParams mcGraphInstantiateParams_st

#define cudaDeviceGetGraphMemAttribute wcudaDeviceGetGraphMemAttribute
#define cudaDeviceGraphMemTrim wcudaDeviceGraphMemTrim
#define cudaDeviceSetGraphMemAttribute wcudaDeviceSetGraphMemAttribute
#define cudaGraphAddChildGraphNode wcudaGraphAddChildGraphNode
#define cudaGraphAddDependencies wcudaGraphAddDependencies
#define cudaGraphAddEmptyNode wcudaGraphAddEmptyNode
#define cudaGraphAddEventRecordNode wcudaGraphAddEventRecordNode
#define cudaGraphAddEventWaitNode wcudaGraphAddEventWaitNode
#define cudaGraphAddExternalSemaphoresSignalNode                               \
  wcudaGraphAddExternalSemaphoresSignalNode
#define cudaGraphAddExternalSemaphoresWaitNode                                 \
  wcudaGraphAddExternalSemaphoresWaitNode
#define cudaGraphAddHostNode wcudaGraphAddHostNode
#define cudaGraphAddKernelNode wcudaGraphAddKernelNode
#define cudaGraphAddMemAllocNode wcudaGraphAddMemAllocNode
#define cudaGraphAddMemFreeNode wcudaGraphAddMemFreeNode
#define cudaGraphAddMemcpyNode wcudaGraphAddMemcpyNode
#define cudaGraphAddMemcpyNode1D wcudaGraphAddMemcpyNode1D
#define cudaGraphAddMemcpyNodeFromSymbol wcudaGraphAddMemcpyNodeFromSymbol
#define cudaGraphAddMemcpyNodeToSymbol wcudaGraphAddMemcpyNodeToSymbol
#define cudaGraphAddMemsetNode wcudaGraphAddMemsetNode
#define cudaGraphChildGraphNodeGetGraph wcudaGraphChildGraphNodeGetGraph
#define cudaGraphClone wcudaGraphClone
#define cudaGraphCreate wcudaGraphCreate
#define cudaGraphDebugDotPrint wcudaGraphDebugDotPrint
#define cudaGraphDestroy wcudaGraphDestroy
#define cudaGraphDestroyNode wcudaGraphDestroyNode
#define cudaGraphEventRecordNodeGetEvent wcudaGraphEventRecordNodeGetEvent
#define cudaGraphEventRecordNodeSetEvent wcudaGraphEventRecordNodeSetEvent
#define cudaGraphEventWaitNodeGetEvent wcudaGraphEventWaitNodeGetEvent
#define cudaGraphEventWaitNodeSetEvent wcudaGraphEventWaitNodeSetEvent
#define cudaGraphExecChildGraphNodeSetParams                                   \
  wcudaGraphExecChildGraphNodeSetParams
#define cudaGraphExecDestroy wcudaGraphExecDestroy
#define cudaGraphExecEventRecordNodeSetEvent                                   \
  wcudaGraphExecEventRecordNodeSetEvent
#define cudaGraphExecEventWaitNodeSetEvent wcudaGraphExecEventWaitNodeSetEvent
#define cudaGraphExecExternalSemaphoresSignalNodeSetParams                     \
  wcudaGraphExecExternalSemaphoresSignalNodeSetParams
#define cudaGraphExecExternalSemaphoresWaitNodeSetParams                       \
  wcudaGraphExecExternalSemaphoresWaitNodeSetParams
#define cudaGraphExecHostNodeSetParams wcudaGraphExecHostNodeSetParams
#define cudaGraphExecKernelNodeSetParams wcudaGraphExecKernelNodeSetParams
#define cudaGraphExecMemcpyNodeSetParams wcudaGraphExecMemcpyNodeSetParams
#define cudaGraphExecMemcpyNodeSetParams1D wcudaGraphExecMemcpyNodeSetParams1D
#define cudaGraphExecMemcpyNodeSetParamsFromSymbol                             \
  wcudaGraphExecMemcpyNodeSetParamsFromSymbol
#define cudaGraphExecMemcpyNodeSetParamsToSymbol                               \
  wcudaGraphExecMemcpyNodeSetParamsToSymbol
#define cudaGraphExecMemsetNodeSetParams wcudaGraphExecMemsetNodeSetParams
#define cudaGraphExecUpdate wcudaGraphExecUpdate
#define cudaGraphExternalSemaphoresSignalNodeGetParams                         \
  wcudaGraphExternalSemaphoresSignalNodeGetParams
#define cudaGraphExternalSemaphoresSignalNodeSetParams                         \
  wcudaGraphExternalSemaphoresSignalNodeSetParams
#define cudaGraphExternalSemaphoresWaitNodeGetParams                           \
  wcudaGraphExternalSemaphoresWaitNodeGetParams
#define cudaGraphExternalSemaphoresWaitNodeSetParams                           \
  wcudaGraphExternalSemaphoresWaitNodeSetParams
#define cudaGraphGetEdges wcudaGraphGetEdges
#define cudaGraphGetNodes wcudaGraphGetNodes
#define cudaGraphGetRootNodes wcudaGraphGetRootNodes
#define cudaGraphHostNodeGetParams wcudaGraphHostNodeGetParams
#define cudaGraphHostNodeSetParams wcudaGraphHostNodeSetParams
#define cudaGraphInstantiate wcudaGraphInstantiate
#define cudaGraphInstantiateWithFlags wcudaGraphInstantiateWithFlags
#define cudaGraphInstantiateWithParams wcudaGraphInstantiateWithParams
#define cudaGraphExecGetFlags wcudaGraphExecGetFlags
#define cudaGraphKernelNodeCopyAttributes wcudaGraphKernelNodeCopyAttributes
#define cudaGraphKernelNodeGetAttribute wcudaGraphKernelNodeGetAttribute
#define cudaGraphKernelNodeGetParams wcudaGraphKernelNodeGetParams
#define cudaGraphKernelNodeSetAttribute wcudaGraphKernelNodeSetAttribute
#define cudaGraphKernelNodeSetParams wcudaGraphKernelNodeSetParams
#define cudaGraphLaunch wcudaGraphLaunch
#define cudaGraphMemAllocNodeGetParams wcudaGraphMemAllocNodeGetParams
#define cudaGraphMemFreeNodeGetParams wcudaGraphMemFreeNodeGetParams
#define cudaGraphMemcpyNodeGetParams wcudaGraphMemcpyNodeGetParams
#define cudaGraphMemcpyNodeSetParams wcudaGraphMemcpyNodeSetParams
#define cudaGraphMemcpyNodeSetParams1D wcudaGraphMemcpyNodeSetParams1D
#define cudaGraphMemcpyNodeSetParamsFromSymbol                                 \
  wcudaGraphMemcpyNodeSetParamsFromSymbol
#define cudaGraphMemcpyNodeSetParamsToSymbol                                   \
  wcudaGraphMemcpyNodeSetParamsToSymbol
#define cudaGraphMemsetNodeGetParams wcudaGraphMemsetNodeGetParams
#define cudaGraphMemsetNodeSetParams wcudaGraphMemsetNodeSetParams
#define cudaGraphNodeFindInClone wcudaGraphNodeFindInClone
#define cudaGraphNodeGetDependencies wcudaGraphNodeGetDependencies
#define cudaGraphNodeGetDependentNodes wcudaGraphNodeGetDependentNodes
#define cudaGraphNodeGetEnabled wcudaGraphNodeGetEnabled
#define cudaGraphNodeGetType wcudaGraphNodeGetType
#define cudaGraphNodeSetEnabled wcudaGraphNodeSetEnabled
#define cudaGraphReleaseUserObject wcudaGraphReleaseUserObject
#define cudaGraphRemoveDependencies wcudaGraphRemoveDependencies
#define cudaGraphRetainUserObject wcudaGraphRetainUserObject
#define cudaGraphUpload wcudaGraphUpload
#define cudaUserObjectCreate wcudaUserObjectCreate
#define cudaUserObjectRelease wcudaUserObjectRelease
#define cudaUserObjectRetain wcudaUserObjectRetain

/**************Texture runtime API******************/
#define cudaGetChannelDesc wcudaGetChannelDesc
#define cudaCreateChannelDesc wcudaCreateChannelDesc
#define cudaCreateTextureObject wcudaCreateTextureObject
#define cudaDestroyTextureObject wcudaDestroyTextureObject
#define cudaGetTextureObjectResourceDesc wcudaGetTextureObjectResourceDesc
#define cudaGetTextureObjectTextureDesc wcudaGetTextureObjectTextureDesc
#define cudaGetTextureObjectResourceViewDesc                                   \
  wcudaGetTextureObjectResourceViewDesc
#define cudaBindTexture wcudaBindTexture
#define cudaBindTexture2D wcudaBindTexture2D
#define cudaBindTextureToArray wcudaBindTextureToArray
#define cudaBindTextureToMipmappedArray wcudaBindTextureToMipmappedArray
#define cudaUnbindTexture wcudaUnbindTexture
#define cudaGetTextureAlignmentOffset wcudaGetTextureAlignmentOffset
#define cudaGetTextureReference wcudaGetTextureReference
#define cudaGetMipmappedArrayLevel wcudaGetMipmappedArrayLevel

/* enum */
#define cudaResourceType mcResourceType
#define cudaResourceTypeArray mcResourceTypeArray
#define cudaResourceTypeMipmappedArray mcResourceTypeMipmappedArray
#define cudaResourceTypeLinear mcResourceTypeLinear
#define cudaResourceTypePitch2D mcResourceTypePitch2D

#define cudaResourceViewFormat mcResourceViewFormat
#define cudaResViewFormatNone mcResViewFormatNone
#define cudaResViewFormatUnsignedChar1 mcResViewFormatUnsignedChar1
#define cudaResViewFormatUnsignedChar2 mcResViewFormatUnsignedChar2
#define cudaResViewFormatUnsignedChar4 mcResViewFormatUnsignedChar4
#define cudaResViewFormatSignedChar1 mcResViewFormatSignedChar1
#define cudaResViewFormatSignedChar2 mcResViewFormatSignedChar2
#define cudaResViewFormatSignedChar4 mcResViewFormatSignedChar4
#define cudaResViewFormatUnsignedShort1 mcResViewFormatUnsignedShort1
#define cudaResViewFormatUnsignedShort2 mcResViewFormatUnsignedShort2
#define cudaResViewFormatUnsignedShort4 mcResViewFormatUnsignedShort4
#define cudaResViewFormatSignedShort1 mcResViewFormatSignedShort1
#define cudaResViewFormatSignedShort2 mcResViewFormatSignedShort2
#define cudaResViewFormatSignedShort4 mcResViewFormatSignedShort4
#define cudaResViewFormatUnsignedInt1 mcResViewFormatUnsignedInt1
#define cudaResViewFormatUnsignedInt2 mcResViewFormatUnsignedInt2
#define cudaResViewFormatUnsignedInt4 mcResViewFormatUnsignedInt4
#define cudaResViewFormatSignedInt1 mcResViewFormatSignedInt1
#define cudaResViewFormatSignedInt2 mcResViewFormatSignedInt2
#define cudaResViewFormatSignedInt4 mcResViewFormatSignedInt4
#define cudaResViewFormatHalf1 mcResViewFormatHalf1
#define cudaResViewFormatHalf2 mcResViewFormatHalf2
#define cudaResViewFormatHalf4 mcResViewFormatHalf4
#define cudaResViewFormatFloat1 mcResViewFormatFloat1
#define cudaResViewFormatFloat2 mcResViewFormatFloat2
#define cudaResViewFormatFloat4 mcResViewFormatFloat4
#define cudaResViewFormatUnsignedBlockCompressed1                              \
  mcResViewFormatUnsignedBlockCompressed1
#define cudaResViewFormatUnsignedBlockCompressed2                              \
  mcResViewFormatUnsignedBlockCompressed2
#define cudaResViewFormatUnsignedBlockCompressed3                              \
  mcResViewFormatUnsignedBlockCompressed3
#define cudaResViewFormatUnsignedBlockCompressed4                              \
  mcResViewFormatUnsignedBlockCompressed4
#define cudaResViewFormatSignedBlockCompressed4                                \
  mcResViewFormatSignedBlockCompressed4
#define cudaResViewFormatUnsignedBlockCompressed5                              \
  mcResViewFormatUnsignedBlockCompressed5
#define cudaResViewFormatSignedBlockCompressed5                                \
  mcResViewFormatSignedBlockCompressed5
#define cudaResViewFormatUnsignedBlockCompressed6H                             \
  mcResViewFormatUnsignedBlockCompressed6H
#define cudaResViewFormatSignedBlockCompressed6H                               \
  mcResViewFormatSignedBlockCompressed6H
#define cudaResViewFormatUnsignedBlockCompressed7                              \
  mcResViewFormatUnsignedBlockCompressed7

#define cudaTextureObject_t mcTextureObject_t
#define cudaTextureDesc mcTextureDesc
#define cudaResourceDesc mcResourceDesc
#define cudaResourceViewDesc mcResourceViewDesc

#define cudaTextureFilterMode mcTextureFilterMode
#define cudaFilterModePoint mcFilterModePoint
#define cudaFilterModeLinear mcFilterModeLinear

#define cudaTextureReadMode mcTextureReadMode
#define cudaReadModeElementType mcReadModeElementType
#define cudaReadModeNormalizedFloat mcReadModeNormalizedFloat

#define cudaTextureType1D mcTextureType1D
#define cudaTextureType2D mcTextureType2D
#define cudaTextureType3D mcTextureType3D
#define cudaTextureTypeCubemap mcTextureTypeCubemap
#define cudaTextureType1DLayered mcTextureType1DLayered
#define cudaTextureType2DLayered mcTextureType2DLayered
#define cudaTextureTypeCubemapLayered mcTextureTypeCubemapLayered

#define cudaTextureAddressMode mcTextureAddressMode
#define cudaAddressModeWrap mcAddressModeWrap
#define cudaAddressModeClamp mcAddressModeClamp
#define cudaAddressModeMirror mcAddressModeMirror
#define cudaAddressModeBorder mcAddressModeBorder

#define __nv_itex_trait __mc_itex_trait
#define __nv_tex_rmet_ret __mc_tex_rmet_ret
#define __nv_tex_rmet_cast __mc_tex_rmet_cast
#define __nv_tex_rmnf_ret __mc_tex_rmnf_ret
#define __nv_tex2dgather_ret __mc_tex2dgather_ret
#define __nv_tex2dgather_rmnf_ret __mc_tex2dgather_rmnf_ret
/**************Surface runtime API******************/
#define cudaBindSurfaceToArray wcudaBindSurfaceToArray
#define cudaGetSurfaceReference wcudaGetSurfaceReference
#define cudaCreateSurfaceObject wcudaCreateSurfaceObject
#define cudaDestroySurfaceObject wcudaDestroySurfaceObject
#define cudaGetSurfaceObjectResourceDesc wcudaGetSurfaceObjectResourceDesc

#define cudaSurfaceType1D mcSurfaceType1D
#define cudaSurfaceType2D mcSurfaceType2D
#define cudaSurfaceType3D mcSurfaceType3D
#define cudaSurfaceTypeCubemap mcSurfaceTypeCubemap
#define cudaSurfaceType1DLayered mcSurfaceType1DLayered
#define cudaSurfaceType2DLayered mcSurfaceType2DLayered
#define cudaSurfaceTypeCubemapLayered mcSurfaceTypeCubemapLayered

#define cudaSurfaceBoundaryMode _mcSurfaceBoundaryMode
#define cudaSurfaceFormatMode _mcSurfaceFormatMode
#define cudaSurfaceObject_t mcSurfaceObject_t

#define cudaBoundaryModeZero mcBoundaryModeZero
#define cudaBoundaryModeClamp mcBoundaryModeClamp
#define cudaBoundaryModeTrap mcBoundaryModeTrap
#define cudaFormatModeForced mcFormatModeForced
#define cudaFormatModeAuto mcFormatModeAuto

#define __nv_surf_trait __mc_surf_trait
#define __nv_isurf_trait __mc_isurf_trait
/**************Data types used by CUDA runtime******************/
/* enum */
#define cudaCGScope mcCGScope
#define cudaCGScopeInvalid mcCGScopeInvalid
#define cudaCGScopeGrid mcCGScopeGrid
#define cudaCGScopeMultiGrid mcCGScopeMultiGrid

#define cudaSharedCarveout mcSharedCarveout
#define cudaSharedmemCarveoutDefault mcSharedmemCarveoutDefault
#define cudaSharedmemCarveoutMaxShared mcSharedmemCarveoutMaxShared
#define cudaSharedmemCarveoutMaxL1 mcSharedmemCarveoutMaxL1

#define cudaOutputMode mcOutputMode
#define cudaOutputMode_t mcOutputMode_t
#define cudaKeyValuePair mcKeyValuePair
#define cudaCSV mcCSV

#define cudaFlushGPUDirectRDMAWritesOptions mcFlushGPUDirectRDMAWritesOptions
#define cudaFlushGPUDirectRDMAWritesOptionHost                                 \
  mcFlushGPUDirectRDMAWritesOptionHost
#define cudaFlushGPUDirectRDMAWritesOptionMemOps                               \
  mcFlushGPUDirectRDMAWritesOptionMemOps

#define cudaGPUDirectRDMAWritesOrdering mcGPUDirectRDMAWritesOrdering
#define cudaGPUDirectRDMAWritesOrderingNone mcGPUDirectRDMAWritesOrderingNone
#define cudaGPUDirectRDMAWritesOrderingOwner mcGPUDirectRDMAWritesOrderingOwner
#define cudaGPUDirectRDMAWritesOrderingAllDevices                              \
  mcGPUDirectRDMAWritesOrderingAllDevices

#define cudaUserObjectFlags mcUserObjectFlags
#define cudaUserObjectNoDestructorSync mcUserObjectNoDestructorSync

#define cudaUserObjectRetainFlags mcUserObjectRetainFlags
#define cudaGraphUserObjectMove mcGraphUserObjectMove

#define cudaGetExportTable wcudaGetExportTable

#define CUDA_IPC_HANDLE_SIZE MC_IPC_HANDLE_SIZE

/* micro */
#define cudaArraySparsePropertiesSingleMipTail                                 \
  mcArraySparsePropertiesSingleMipTail
#define cudaExternalMemoryDedicated mcExternalMemoryDedicated
#define cudaExternalSemaphoreSignalSkipNvSciBufMemSync                         \
  mcExternalSemaphoreSignalSkipNvSciBufMemSync
#define cudaExternalSemaphoreWaitSkipNvSciBufMemSync                           \
  mcExternalSemaphoreWaitSkipNvSciBufMemSync
#define cudaNvSciSyncAttrSignal mcNvSciSyncAttrSignal
#define cudaNvSciSyncAttrWait mcNvSciSyncAttrWait

//=============================CUDA Driver API======================//

#define cuuint32_t mcuint32_t
#define cuuint64_t mcuint64_t

#define CUdeviceptr mcDrvDeviceptr_t
#define CUdeviceptr_v2 mcDrvDeviceptr_t
#define CUdeviceptr_v1 mcDrvDeviceptrv1_t

// error code
#define cudaError_enum mcDrvError_enum
#define CUresult mcDrvError_t
#define CUDA_SUCCESS MC_SUCCESS
#define CUDA_ERROR_INVALID_VALUE MC_ERROR_INVALID_VALUE
#define CUDA_ERROR_OUT_OF_MEMORY MC_ERROR_OUT_OF_MEMORY
#define CUDA_ERROR_NOT_INITIALIZED MC_ERROR_NOT_INITIALIZED
#define CUDA_ERROR_DEINITIALIZED MC_ERROR_DEINITIALIZED
#define CUDA_ERROR_PROFILER_DISABLED MC_ERROR_PROFILER_DISABLED
#define CUDA_ERROR_PROFILER_NOT_INITIALIZED MC_ERROR_PROFILER_NOT_INITIALIZED
#define CUDA_ERROR_PROFILER_ALREADY_STARTED MC_ERROR_PROFILER_ALREADY_STARTED
#define CUDA_ERROR_PROFILER_ALREADY_STOPPED MC_ERROR_PROFILER_ALREADY_STOPPED
#define CUDA_ERROR_STUB_LIBRARY MC_ERROR_STUB_LIBRARY
#define CUDA_ERROR_NO_DEVICE MC_ERROR_NO_DEVICE
#define CUDA_ERROR_INVALID_DEVICE MC_ERROR_INVALID_DEVICE
#define CUDA_ERROR_DEVICE_NOT_LICENSED MC_ERROR_DEVICE_NOT_LICENSED
#define CUDA_ERROR_INVALID_IMAGE MC_ERROR_INVALID_IMAGE
#define CUDA_ERROR_INVALID_CONTEXT MC_ERROR_INVALID_CONTEXT
#define CUDA_ERROR_CONTEXT_ALREADY_CURRENT MC_ERROR_CONTEXT_ALREADY_CURRENT
#define CUDA_ERROR_MAP_FAILED MC_ERROR_MAP_FAILED
#define CUDA_ERROR_UNMAP_FAILED MC_ERROR_UNMAP_FAILED
#define CUDA_ERROR_ARRAY_IS_MAPPED MC_ERROR_ARRAY_IS_MAPPED
#define CUDA_ERROR_ALREADY_MAPPED MC_ERROR_ALREADY_MAPPED
#define CUDA_ERROR_NO_BINARY_FOR_GPU MC_ERROR_NO_BINARY_FOR_GPU
#define CUDA_ERROR_ALREADY_ACQUIRED MC_ERROR_ALREADY_ACQUIRED
#define CUDA_ERROR_NOT_MAPPED MC_ERROR_NOT_MAPPED
#define CUDA_ERROR_NOT_MAPPED_AS_ARRAY MC_ERROR_NOT_MAPPED_AS_ARRAY
#define CUDA_ERROR_NOT_MAPPED_AS_POINTER MC_ERROR_NOT_MAPPED_AS_POINTER
#define CUDA_ERROR_ECC_UNCORRECTABLE MC_ERROR_ECC_UNCORRECTABLE
#define CUDA_ERROR_UNSUPPORTED_LIMIT MC_ERROR_UNSUPPORTED_LIMIT
#define CUDA_ERROR_CONTEXT_ALREADY_IN_USE MC_ERROR_CONTEXT_ALREADY_IN_USE
#define CUDA_ERROR_PEER_ACCESS_UNSUPPORTED MC_ERROR_PEER_ACCESS_UNSUPPORTED
#define CUDA_ERROR_INVALID_PTX MC_ERROR_INVALID_KERNEL_FILE
#define CUDA_ERROR_INVALID_GRAPHICS_CONTEXT MC_ERROR_INVALID_GRAPHICS_CONTEXT
#define CUDA_ERROR_NVLINK_UNCORRECTABLE MC_ERROR_MXLINK_UNCORRECTABLE
#define CUDA_ERROR_JIT_COMPILER_NOT_FOUND MC_ERROR_JIT_COMPILER_NOT_FOUND
#define CUDA_ERROR_UNSUPPORTED_PTX_VERSION MC_ERROR_UNSUPPORTED_KERNEL_VERSION
#define CUDA_ERROR_JIT_COMPILATION_DISABLED MC_ERROR_JIT_COMPILATION_DISABLED
#define CUDA_ERROR_UNSUPPORTED_EXEC_AFFINITY MC_ERROR_UNSUPPORTED_EXEC_AFFINITY
#define CUDA_ERROR_INVALID_SOURCE MC_ERROR_INVALID_SOURCE
#define CUDA_ERROR_FILE_NOT_FOUND MC_ERROR_FILE_NOT_FOUND
#define CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND                              \
  MC_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND
#define CUDA_ERROR_SHARED_OBJECT_INIT_FAILED MC_ERROR_SHARED_OBJECT_INIT_FAILED
#define CUDA_ERROR_OPERATING_SYSTEM MC_ERROR_OPERATING_SYSTEM
#define CUDA_ERROR_INVALID_HANDLE MC_ERROR_INVALID_HANDLE
#define CUDA_ERROR_ILLEGAL_STATE MC_ERROR_ILLEGAL_STATE
#define CUDA_ERROR_NOT_FOUND MC_ERROR_NOT_FOUND
#define CUDA_ERROR_NOT_READY MC_ERROR_NOT_READY
#define CUDA_ERROR_ILLEGAL_ADDRESS MC_ERROR_ILLEGAL_ADDRESS
#define CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES MC_ERROR_LAUNCH_OUT_OF_RESOURCES
#define CUDA_ERROR_LAUNCH_TIMEOUT MC_ERROR_LAUNCH_TIMEOUT
#define CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING                               \
  MC_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING
#define CUDA_ERROR_PEER_ACCESS_ALREADY_ENABLED                                 \
  MC_ERROR_PEER_ACCESS_ALREADY_ENABLED
#define CUDA_ERROR_PEER_ACCESS_NOT_ENABLED MC_ERROR_PEER_ACCESS_NOT_ENABLED
#define CUDA_ERROR_PRIMARY_CONTEXT_ACTIVE MC_ERROR_PRIMARY_CONTEXT_ACTIVE
#define CUDA_ERROR_CONTEXT_IS_DESTROYED MC_ERROR_CONTEXT_IS_DESTROYED
#define CUDA_ERROR_ASSERT MC_ERROR_ASSERT
#define CUDA_ERROR_TOO_MANY_PEERS MC_ERROR_TOO_MANY_PEERS
#define CUDA_ERROR_HOST_MEMORY_ALREADY_REGISTERED                              \
  MC_ERROR_HOST_MEMORY_ALREADY_REGISTERED
#define CUDA_ERROR_HOST_MEMORY_NOT_REGISTERED                                  \
  MC_ERROR_HOST_MEMORY_NOT_REGISTERED
#define CUDA_ERROR_HARDWARE_STACK_ERROR MC_ERROR_HARDWARE_STACK_ERROR
#define CUDA_ERROR_ILLEGAL_INSTRUCTION MC_ERROR_ILLEGAL_INSTRUCTION
#define CUDA_ERROR_MISALIGNED_ADDRESS MC_ERROR_MISALIGNED_ADDRESS
#define CUDA_ERROR_INVALID_ADDRESS_SPACE MC_ERROR_INVALID_ADDRESS_SPACE
#define CUDA_ERROR_INVALID_PC MC_ERROR_INVALID_PC
#define CUDA_ERROR_LAUNCH_FAILED MC_ERROR_LAUNCH_FAILED
#define CUDA_ERROR_COOPERATIVE_LAUNCH_TOO_LARGE                                \
  MC_ERROR_COOPERATIVE_LAUNCH_TOO_LARGE
#define CUDA_ERROR_NOT_PERMITTED MC_ERROR_NOT_PERMITTED
#define CUDA_ERROR_NOT_SUPPORTED MC_ERROR_NOT_SUPPORTED
#define CUDA_ERROR_SYSTEM_NOT_READY MC_ERROR_SYSTEM_NOT_READY
#define CUDA_ERROR_SYSTEM_DRIVER_MISMATCH MC_ERROR_SYSTEM_DRIVER_MISMATCH
#define CUDA_ERROR_COMPAT_NOT_SUPPORTED_ON_DEVICE                              \
  MC_ERROR_COMPAT_NOT_SUPPORTED_ON_DEVICE
#define CUDA_ERROR_MPS_CONNECTION_FAILED MC_ERROR_MPS_CONNECTION_FAILED
#define CUDA_ERROR_MPS_RPC_FAILURE MC_ERROR_MPS_RPC_FAILURE
#define CUDA_ERROR_MPS_SERVER_NOT_READY MC_ERROR_MPS_SERVER_NOT_READY
#define CUDA_ERROR_MPS_MAX_CLIENTS_REACHED MC_ERROR_MPS_MAX_CLIENTS_REACHED
#define CUDA_ERROR_MPS_MAX_CONNECTIONS_REACHED                                 \
  MC_ERROR_MPS_MAX_CONNECTIONS_REACHED
#define CUDA_ERROR_STREAM_CAPTURE_UNSUPPORTED                                  \
  MC_ERROR_STREAM_CAPTURE_UNSUPPORTED
#define CUDA_ERROR_STREAM_CAPTURE_INVALIDATED                                  \
  MC_ERROR_STREAM_CAPTURE_INVALIDATED
#define CUDA_ERROR_STREAM_CAPTURE_MERGE MC_ERROR_STREAM_CAPTURE_MERGE
#define CUDA_ERROR_STREAM_CAPTURE_UNMATCHED MC_ERROR_STREAM_CAPTURE_UNMATCHED
#define CUDA_ERROR_STREAM_CAPTURE_UNJOINED MC_ERROR_STREAM_CAPTURE_UNJOINED
#define CUDA_ERROR_STREAM_CAPTURE_ISOLATION MC_ERROR_STREAM_CAPTURE_ISOLATION
#define CUDA_ERROR_STREAM_CAPTURE_IMPLICIT MC_ERROR_STREAM_CAPTURE_IMPLICIT
#define CUDA_ERROR_CAPTURED_EVENT MC_ERROR_CAPTURED_EVENT
#define CUDA_ERROR_STREAM_CAPTURE_WRONG_THREAD                                 \
  MC_ERROR_STREAM_CAPTURE_WRONG_THREAD
#define CUDA_ERROR_TIMEOUT MC_ERROR_TIMEOUT
#define CUDA_ERROR_GRAPH_EXEC_UPDATE_FAILURE MC_ERROR_GRAPH_EXEC_UPDATE_FAILURE
#define CUDA_ERROR_EXTERNAL_DEVICE MC_ERROR_EXTERNAL_DEVICE
#define CUDA_ERROR_UNKNOWN MC_ERROR_UNKNOWN

#define CUuuid_st _mcUuid
#define cudaUUID_t mcUuid_t
#define CUuuid MCuuid
#define CUdevice MCdevice
#define CUdevice_v1 MCdevice
#define CU_DEVICE_CPU MC_DEVICE_CPU
#define CU_DEVICE_INVALID MC_DEVICE_INVALID

#define CUdevprop_st mcDrvDevprop_st
#define CUdevprop_v1 mcDrvDevprop
#define CUdevprop mcDrvDevprop

#define CUdevice_attribute_enum mcDrvDeviceAttribute_enum
#define CUdevice_attribute mcDrvDeviceAttribute_t
#define CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK                              \
  MC_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK
#define CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X MC_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X
#define CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y MC_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y
#define CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z MC_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z
#define CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X MC_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X
#define CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y MC_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y
#define CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z MC_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z
#define CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK                        \
  MC_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK
// cuda deprecated,use CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK
#define CU_DEVICE_ATTRIBUTE_SHARED_MEMORY_PER_BLOCK                            \
  MC_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK
#define CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY                              \
  MC_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY
#define CU_DEVICE_ATTRIBUTE_WARP_SIZE MC_DEVICE_ATTRIBUTE_WARP_SIZE
#define CU_DEVICE_ATTRIBUTE_MAX_PITCH MC_DEVICE_ATTRIBUTE_MAX_PITCH
#define CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK                            \
  MC_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK
// cuda deprecated,use CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK
#define CU_DEVICE_ATTRIBUTE_REGISTERS_PER_BLOCK                                \
  MC_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK
#define CU_DEVICE_ATTRIBUTE_CLOCK_RATE MC_DEVICE_ATTRIBUTE_CLOCK_RATE
#define CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT                                  \
  MC_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT
// cuda deprecated,use instead CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT
#define CU_DEVICE_ATTRIBUTE_GPU_OVERLAP MC_DEVICE_ATTRIBUTE_GPU_OVERLAP
#define CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT                               \
  MC_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT
#define CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT                                \
  MC_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT
#define CU_DEVICE_ATTRIBUTE_INTEGRATED MC_DEVICE_ATTRIBUTE_INTEGRATED
#define CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY                                \
  MC_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY
#define CU_DEVICE_ATTRIBUTE_COMPUTE_MODE MC_DEVICE_ATTRIBUTE_COMPUTE_MODE
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_WIDTH                            \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_1D_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_WIDTH                            \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_2D_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_HEIGHT                           \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_2D_HEIGHT
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH                            \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_3D_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT                           \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_3D_HEIGHT
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH                            \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_3D_DEPTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_WIDTH \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_2D_LAYERED_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_HEIGHT \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_2D_LAYERED_HEIGHT
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_LAYERS \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_2D_LAYERED_LAYERS
// next 3 type cuda deprecated,use
// CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_XXX
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_WIDTH                      \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_HEIGHT                     \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_NUMSLICES                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_SURFACE_ALIGNMENT MC_DEVICE_ATTRIBUTE_SURFACE_ALIGNMENT
#define CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS                                 \
  MC_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS
#define CU_DEVICE_ATTRIBUTE_ECC_ENABLED MC_DEVICE_ATTRIBUTE_ECC_ENABLED
#define CU_DEVICE_ATTRIBUTE_PCI_BUS_ID MC_DEVICE_ATTRIBUTE_PCI_BUS_ID
#define CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID MC_DEVICE_ATTRIBUTE_PCI_DEVICE_ID
#define CU_DEVICE_ATTRIBUTE_TCC_DRIVER MC_DEVICE_ATTRIBUTE_TCC_DRIVER
#define CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE                                  \
  MC_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE
#define CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH                            \
  MC_DEVICE_ATTRIBUTE_MEMORY_BUS_WIDTH
#define CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE MC_DEVICE_ATTRIBUTE_L2_CACHE_SIZE
#define CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR                     \
  MC_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR
#define CU_DEVICE_ATTRIBUTE_MAX_BLOCKS_PER_MULTIPROCESSOR                                          \
    MC_DEVICE_ATTRIBUTE_MAX_BLOCKS_PER_MULTIPROCESSOR
#define CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT                                 \
  MC_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT
#define CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING                                 \
  MC_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_WIDTH \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_1D_LAYERED_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_LAYERS \
  MC_DEVICE_ATTRIBUTE_MAX_TEXTURE_1D_LAYERED_LAYERS
// cuda deprecated,do not use
#define CU_DEVICE_ATTRIBUTE_CAN_TEX2D_GATHER MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_WIDTH                     \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_HEIGHT                    \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH_ALTERNATE                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT_ALTERNATE                 \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH_ALTERNATE                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID MC_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID
#define CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT                            \
  MC_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_WIDTH                       \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_WIDTH               \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_LAYERS              \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_WIDTH MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_WIDTH MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_HEIGHT MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_WIDTH MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_HEIGHT MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_DEPTH MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_WIDTH                    \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_LAYERS                   \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_WIDTH                    \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_HEIGHT                   \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_LAYERS                   \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_WIDTH                       \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_WIDTH               \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_LAYERS              \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LINEAR_WIDTH                     \
  MC_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LINEAR_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_WIDTH                     \
  MC_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_WIDTH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_HEIGHT                    \
  MC_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_HEIGHT
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_PITCH                     \
  MC_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_PITCH
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_WIDTH                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_HEIGHT                 \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR                           \
  MC_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR
#define CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR                           \
  MC_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR
#define CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_MIPMAPPED_WIDTH                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_STREAM_PRIORITIES_SUPPORTED                        \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_GLOBAL_L1_CACHE_SUPPORTED MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_LOCAL_L1_CACHE_SUPPORTED MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR               \
  MC_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR
#define CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR                   \
  MC_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR
#define CU_DEVICE_ATTRIBUTE_MANAGED_MEMORY MC_DEVICE_ATTRIBUTE_MANAGED_MEMORY
#define CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD                                    \
  MC_DEVICE_ATTRIBUTE_IS_MULTI_GPU_BOARD
#define CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD_GROUP_ID MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_HOST_NATIVE_ATOMIC_SUPPORTED \
  MC_DEVICE_ATTRIBUTE_HOST_NATIVE_ATOMIC_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_SINGLE_TO_DOUBLE_PRECISION_PERF_RATIO              \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS                             \
  MC_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS
#define CU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS                          \
  MC_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS
#define CU_DEVICE_ATTRIBUTE_COMPUTE_PREEMPTION_SUPPORTED \
  MC_DEVICE_ATTRIBUTE_COMPUTE_PREEMPTION_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_CAN_USE_HOST_POINTER_FOR_REGISTERED_MEM            \
  MC_DEVICE_ATTRIBUTE_CAN_USE_HOST_POINTER_FOR_REGISTERD_MEM
#define CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS                             \
  MC_DEVICE_ATTRIBUTE_CAN_USE_STREAM_MEM_OPS
#define CU_DEVICE_ATTRIBUTE_CAN_USE_64_BIT_STREAM_MEM_OPS                      \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_CAN_USE_STREAM_WAIT_VALUE_NOR                      \
  MC_DEVICE_ATTRIBUTE_CAN_USE_STREAM_WAIT_VALUE_NOR
#define CU_DEVICE_ATTRIBUTE_COOPERATIVE_LAUNCH                                 \
  MC_DEVICE_ATTRIBUTE_COOPERATIVE_LAUNCH
#define CU_DEVICE_ATTRIBUTE_COOPERATIVE_MULTI_DEVICE_LAUNCH                    \
  MC_DEVICE_ATTRIBUTE_COOPERATIVE_MULTI_DEVICE_LAUNCH
#define CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK_OPTIN                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_CAN_FLUSH_REMOTE_WRITES                            \
  MC_DEVICE_ATTRIBUTE_CAN_FLUSH_REMOTE_WRITES
#define CU_DEVICE_ATTRIBUTE_HOST_REGISTER_SUPPORTED MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS_USES_HOST_PAGE_TABLES       \
  MC_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS_USES_HOST_PAGE_TABLES
#define CU_DEVICE_ATTRIBUTE_DIRECT_MANAGED_MEM_ACCESS_FROM_HOST                \
  MC_DEVICE_ATTRIBUTE_DIRECT_MANAGED_MEM_ACCESS_FROM_HOST
#define CU_DEVICE_ATTRIBUTE_VIRTUAL_ADDRESS_MANAGEMENT_SUPPORTED               \
  MC_DEVICE_ATTRIBUTE_VIRTUAL_MEMORY_MANAGEMENT_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_VIRTUAL_MEMORY_MANAGEMENT_SUPPORTED                \
  MC_DEVICE_ATTRIBUTE_VIRTUAL_MEMORY_MANAGEMENT_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR_SUPPORTED        \
  MC_DEVICE_ATTRIBUTE_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_HANDLE_TYPE_WIN32_HANDLE_SUPPORTED                 \
  MC_DEVICE_ATTRIBUTE_HANDLE_TYPE_WIN32_HANDLE_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_HANDLE_TYPE_WIN32_KMT_HANDLE_SUPPORTED             \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_GENERIC_COMPRESSION_SUPPORTED                      \
  MC_DEVICE_ATTRIBUTE_GENERIC_COMPRESSION_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_MAX_PERSISTING_L2_CACHE_SIZE                       \
  MC_DEVICE_ATTRIBUTE_MAX_PERSISTING_L2_CACHE_SIZE
#define CU_DEVICE_ATTRIBUTE_MAX_ACCESS_POLICY_WINDOW_SIZE                      \
  MC_DEVICE_ATTRIBUTE_MAX_ACCESS_POLICY_WINDOW_SIZE
#define CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_WITH_CUDA_VMM_SUPPORTED            \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_RESERVED_SHARED_MEMORY_PER_BLOCK                   \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_SPARSE_CUDA_ARRAY_SUPPORTED                        \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_READ_ONLY_HOST_REGISTER_SUPPORTED                  \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_TIMELINE_SEMAPHORE_INTEROP_SUPPORTED               \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MEMORY_POOLS_SUPPORTED                             \
  MC_DEVICE_ATTRIBUTE_MEMORY_POOLS_SUPPORTED
#define CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_SUPPORTED MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_FLUSH_WRITES_OPTIONS               \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_GPU_DIRECT_RDMA_WRITES_ORDERING                    \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MEMPOOL_SUPPORTED_HANDLE_TYPES                     \
  MC_DEVICE_ATTRIBUTE_MEMORY_POOL_SUPPORTED_HANDLE_TYPES
#define CU_DEVICE_ATTRIBUTE_DEFERRED_MAPPING_CUDA_ARRAY_SUPPORTED              \
  MC_DEVICE_ATTRIBUTE_UNKNOW
#define CU_DEVICE_ATTRIBUTE_MAX MC_DEVICE_ATTRIBUTE_MAX

#define CUcomputemode_enum mcDrvComputeMode_enum
#define CUcomputemode mcDrvComputeMode
#define CU_COMPUTEMODE_DEFAULT MC_COMPUTEMODE_DEFAULT
#define CU_COMPUTEMODE_PROHIBITED MC_COMPUTEMODE_PROHIBITED
#define CU_COMPUTEMODE_EXCLUSIVE_PROCESS MC_COMPUTEMODE_EXCLUSIVE_PROCESS

#define CUmem_advise_enum mcDrvMemoryAdvise_enum
#define CU_MEM_ADVISE_SET_READ_MOSTLY MC_MEM_ADVISE_SET_READ_MOSTLY
#define CU_MEM_ADVISE_UNSET_READ_MOSTLY MC_MEM_ADVISE_UNSET_READ_MOSTLY
#define CU_MEM_ADVISE_SET_PREFERRED_LOCATION                                   \
  MC_MEM_ADVISE_SET_PREFERRED_LOCATION
#define CU_MEM_ADVISE_UNSET_PREFERRED_LOCATION                                 \
  MC_MEM_ADVISE_UNSET_PREFERRED_LOCATION
#define CU_MEM_ADVISE_SET_ACCESSED_BY MC_MEM_ADVISE_SET_ACCESSED_BY
#define CU_MEM_ADVISE_UNSET_ACCESSED_BY MC_MEM_ADVISE_UNSET_ACCESSED_BY
#define CUmem_advise mcDrvMemoryAdvise

#define CUjitInputType_enum mcDrvJitInputType_enum
#define CU_JIT_INPUT_CUBIN MC_JIT_INPUT_DEVICE_OBJECT
#define CU_JIT_INPUT_PTX MC_JIT_INPUT_BITCODE
#define CU_JIT_INPUT_FATBINARY MC_JIT_INPUT_FATBINARY
#define CU_JIT_INPUT_OBJECT MC_JIT_INPUT_OBJECT
#define CU_JIT_INPUT_LIBRARY MC_JIT_INPUT_LIBRARY
#define CU_JIT_INPUT_NVVM MC_JIT_INPUT_BITCODE
#define CU_JIT_NUM_INPUT_TYPES MC_JIT_NUM_INPUT_TYPES
#define CUjitInputType mcDrvJitInputType_t

#define CUjit_fallback_enum mcDrvJitFallback_enum
#define CU_PREFER_PTX MC_PREFER_PTX
#define CU_PREFER_BINARY MC_PREFER_BINARY
#define CUjit_fallback mcDrvJitFallback

#define CUjit_cacheMode_enum mcDrvJitCacheMode_enum
#define CU_JIT_CACHE_OPTION_NONE MC_JIT_CACHE_OPTION_NONE
#define CU_JIT_CACHE_OPTION_CG MC_JIT_CACHE_OPTION_CG
#define CU_JIT_CACHE_OPTION_CA MC_JIT_CACHE_OPTION_CA
#define CUjit_cacheMode mcDrvJitCacheMode

#define CUjit_option_enum mcDrvJitOption_enum
#define CUjit_option mcDrvjit_option_t
#define CU_JIT_MAX_REGISTERS MC_JIT_MAX_REGISTERS
#define CU_JIT_THREADS_PER_BLOCK MC_JIT_THREADS_PER_BLOCK
#define CU_JIT_WALL_TIME MC_JIT_WALL_TIME
#define CU_JIT_INFO_LOG_BUFFER MC_JIT_INFO_LOG_BUFFER
#define CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES MC_JIT_INFO_LOG_BUFFER_SIZE_BYTES
#define CU_JIT_ERROR_LOG_BUFFER MC_JIT_ERROR_LOG_BUFFER
#define CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES MC_JIT_ERROR_LOG_BUFFER_SIZE_BYTES
#define CU_JIT_OPTIMIZATION_LEVEL MC_JIT_OPTIMIZATION_LEVEL
#define CU_JIT_TARGET_FROM_CUCONTEXT MC_JIT_TARGET_FROM_CUCONTEXT
#define CU_JIT_TARGET MC_JIT_TARGET
#define CU_JIT_FALLBACK_STRATEGY MC_JIT_FALLBACK_STRATEGY
#define CU_JIT_GENERATE_DEBUG_INFO MC_JIT_GENERATE_DEBUG_INFO
#define CU_JIT_LOG_VERBOSE MC_JIT_LOG_VERBOSE
#define CU_JIT_GENERATE_LINE_INFO MC_JIT_GENERATE_LINE_INFO
#define CU_JIT_CACHE_MODE MC_JIT_CACHE_MODE
#define CU_JIT_NEW_SM3X_OPT MC_JIT_NEW_SM3X_OPT
#define CU_JIT_FAST_COMPILE MC_JIT_FAST_COMPILE
#define CU_JIT_GLOBAL_SYMBOL_NAMES MC_JIT_GLOBAL_SYMBOL_NAMES
#define CU_JIT_GLOBAL_SYMBOL_ADDRESSES MC_JIT_GLOBAL_SYMBOL_ADDRESSES
#define CU_JIT_GLOBAL_SYMBOL_COUNT MC_JIT_GLOBAL_SYMBOL_COUNT
#define CU_JIT_LTO MC_JIT_LTO
#define CU_JIT_FTZ MC_JIT_FTZ
#define CU_JIT_PREC_DIV MC_JIT_PREC_DIV
#define CU_JIT_PREC_SQRT MC_JIT_PREC_SQRT
#define CU_JIT_FMA MC_JIT_FMA
#define CU_JIT_REFERENCED_KERNEL_NAMES MC_JIT_REFERENCED_KERNEL_NAMES
#define CU_JIT_REFERENCED_KERNEL_COUNT MC_JIT_REFERENCED_KERNEL_COUNT
#define CU_JIT_REFERENCED_VARIABLE_NAMES MC_JIT_REFERENCED_VARIABLE_NAMES
#define CU_JIT_REFERENCED_VARIABLE_COUNT MC_JIT_REFERENCED_VARIABLE_COUNT
#define CU_JIT_OPTIMIZE_UNUSED_DEVICE_VARIABLES                                \
  MC_JIT_OPTIMIZE_UNUSED_DEVICE_VARIABLES
#define CU_JIT_NUM_OPTIONS MC_JIT_NUM_OPTIONS

#define CUjit_target_enum mcDrvJitTarget_enum
#define CU_TARGET_COMPUTE_70 WNV_TARGET_SM_70
#define CU_TARGET_COMPUTE_75 WNV_TARGET_SM_75
#define CU_TARGET_COMPUTE_80 WNV_TARGET_SM_80
#define CUjit_target mcDrvJitTarget

#define CUctx_flags_enum mcDrvCtxFlags_enum
#define CU_CTX_SCHED_AUTO MC_CTX_SCHED_AUTO
#define CU_CTX_SCHED_SPIN MC_CTX_SCHED_SPIN
#define CU_CTX_SCHED_YIELD MC_CTX_SCHED_YIELD
#define CU_CTX_SCHED_BLOCKING_SYNC MC_CTX_SCHED_BLOCKING_SYNC
#define CU_CTX_BLOCKING_SYNC MC_CTX_BLOCKING_SYNC
#define CU_CTX_SCHED_MASK MC_CTX_SCHED_MASK
#define CU_CTX_MAP_HOST MC_CTX_MAP_HOST
#define CU_CTX_LMEM_RESIZE_TO_MAX MC_CTX_LMEM_RESIZE_TO_MAX
#define CU_CTX_FLAGS_MASK MC_CTX_FLAGS_MASK
#define CUctx_flags mcDrvCtxFlags

#define CUexecAffinityType_enum mcDrvexecAffinityType_enum
#define CU_EXEC_AFFINITY_TYPE_SM_COUNT MC_EXEC_AFFINITY_TYPE_SM_COUNT
#define CU_EXEC_AFFINITY_TYPE_MAX MC_EXEC_AFFINITY_TYPE_MAX
#define CUexecAffinityType mcDrvexecAffinityType

#define CUfunc_cache_enum mcDrvfuncCache_enum
#define CU_FUNC_CACHE_PREFER_NONE MC_FUNC_CACHE_PREFER_NONE
#define CU_FUNC_CACHE_PREFER_SHARED MC_FUNC_CACHE_PREFER_SHARED
#define CU_FUNC_CACHE_PREFER_L1 MC_FUNC_CACHE_PREFER_L1
#define CU_FUNC_CACHE_PREFER_EQUAL MC_FUNC_CACHE_PREFER_EQUAL
#define CUfunc_cache mcDrvfunc_cache_t

#define CUlimit_enum mcDrvlimit_enum
#define CU_LIMIT_STACK_SIZE MC_LIMIT_STACK_SIZE
#define CU_LIMIT_PRINTF_FIFO_SIZE MC_LIMIT_PRINTF_FIFO_SIZE
#define CU_LIMIT_MALLOC_HEAP_SIZE MC_LIMIT_MALLOC_HEAP_SIZE
#define CU_LIMIT_DEV_RUNTIME_SYNC_DEPTH MC_LIMIT_DEV_RUNTIME_SYNC_DEPTH
#define CU_LIMIT_DEV_RUNTIME_PENDING_LAUNCH_COUNT                              \
  MC_LIMIT_DEV_RUNTIME_PENDING_LAUNCH_COUNT
#define CU_LIMIT_MAX_L2_FETCH_GRANULARITY MC_LIMIT_MAX_L2_FETCH_GRANULARITY
#define CU_LIMIT_PERSISTING_L2_CACHE_SIZE MC_LIMIT_PERSISTING_L2_CACHE_SIZE
#define CU_LIMIT_MAX MC_LIMIT_MAX
#define CUlimit mcDrvlimit_t

#define CUfunction_attribute_enum mcFunctionAttribute_enum
#define CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK                                \
  MC_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK
#define CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES MC_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES
#define CU_FUNC_ATTRIBUTE_CONST_SIZE_BYTES MC_FUNC_ATTRIBUTE_CONST_SIZE_BYTES
#define CU_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES MC_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES
#define CU_FUNC_ATTRIBUTE_NUM_REGS MC_FUNC_ATTRIBUTE_NUM_REGS
#define CU_FUNC_ATTRIBUTE_PTX_VERSION MC_FUNC_ATTRIBUTE_PTX_VERSION
#define CU_FUNC_ATTRIBUTE_BINARY_VERSION MC_FUNC_ATTRIBUTE_BINARY_VERSION
#define CU_FUNC_ATTRIBUTE_CACHE_MODE_CA MC_FUNC_ATTRIBUTE_CACHE_MODE_CA
#define CU_FUNC_ATTRIBUTE_MAX_DYNAMIC_SHARED_SIZE_BYTES                        \
  MC_FUNC_ATTRIBUTE_MAX_DYNAMIC_SHARED_SIZE_BYTES
#define CU_FUNC_ATTRIBUTE_PREFERRED_SHARED_MEMORY_CARVEOUT                     \
  MC_FUNC_ATTRIBUTE_PREFERRED_SHARED_MEMORY_CARVEOUT
#define CU_FUNC_ATTRIBUTE_MAX MC_FUNC_ATTRIBUTE_MAX
#define CUfunction_attribute mcFunction_attribute

#define CUsharedconfig_enum mcDrvsharedconfig_enum
#define CU_SHARED_MEM_CONFIG_DEFAULT_BANK_SIZE                                 \
  MC_SHARED_MEM_CONFIG_DEFAULT_BANK_SIZE
#define CU_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE                               \
  MC_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE
#define CU_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE                              \
  MC_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE
#define CUsharedconfig mcDrvsharedconfig_t

#define CUdevice_P2PAttribute_enum mcDrvDeviceP2PAttribute_enum
#define CUdevice_P2PAttribute mcDrvDeviceP2PAttribute
#define CU_DEVICE_P2P_ATTRIBUTE_PERFORMANCE_RANK                               \
  MC_DEVICE_P2P_ATTRIBUTE_PERFORMANCE_RANK
#define CU_DEVICE_P2P_ATTRIBUTE_ACCESS_SUPPORTED                               \
  MC_DEVICE_P2P_ATTRIBUTE_ACCESS_SUPPORTED
#define CU_DEVICE_P2P_ATTRIBUTE_NATIVE_ATOMIC_SUPPORTED                        \
  MC_DEVICE_P2P_ATTRIBUTE_NATIVE_ATOMIC_SUPPORTED
#define CU_DEVICE_P2P_ATTRIBUTE_ACCESS_ACCESS_SUPPORTED                        \
  MC_DEVICE_P2P_ATTRIBUTE_CUDA_ARRAY_ACCESS_SUPPORTED
#define CU_DEVICE_P2P_ATTRIBUTE_CUDA_ARRAY_ACCESS_SUPPORTED                    \
  MC_DEVICE_P2P_ATTRIBUTE_CUDA_ARRAY_ACCESS_SUPPORTED

/* data type used by cuda rt & driver */
// context
#define CUctx_st MCctx_st
#define CUcontext MCcontext
// stream
#define CUstream_st MCstream_st
#define CUstream MCstream
// event
#define CUevent_st MCevent_st
#define CUevent MCevent
// function
#define CUfunc_st MCfunc_st
#define CUfunction MCfunction
// module
#define CUmod_st MCmod_st
#define CUmodule MCmodule

#define CUuserObject_st UserObject
#define CUarray_st _mcArray
#define CUgraph_st imcGraph
#define CUgraphExec_st GraphExec
#define CUgraphNode_st GraphNode

#define CUlinkState_st imcLinkState_t
#define CUmemPoolHandle_st imcMemPool_t
#define CUextSemaphore_st mcDrvExtSemaphore_st
#define CUexternalSemaphore mcDrvExternalSemaphore

#define CUtexref_st mcDrvTexref_st
#define CUtexref mcDrvTexref

#define CUDA_MEMCPY2D_st mcDrvMemcpy2D_st
#define CUDA_MEMCPY2D_v2 mcDrvMemcpy2D
#define CUDA_MEMCPY2D mcDrvMemcpy2D
#define CUDA_MEMCPY2D_v1_st mcDrvMemcpy2Dv1_st
#define CUDA_MEMCPY2D_v1 mcDrvMemcpy2Dv1

#define CUlinkState mcDrvlinkState_t
#define CUmemoryPool mcDrvMemPool_t

#define CUexecAffinityParam_st mcDrvexecAffinityParam
#define CUexecAffinityParam_v1 mcDrvexecAffinityParam_t
#define CUexecAffinityParam mcDrvexecAffinityParam_t

#define CUexecAffinitySmCount_st mcDrvexecAffinitySmCount
#define CUexecAffinitySmCount_v1 mcDrvexecAffinitySmCount_t
#define CUexecAffinitySmCount mcDrvexecAffinitySmCount_t

#define CUmemAllocationProp_st mcMemAllocationProp_st
#define CUmemAllocationProp_v1 mcDrvMemAllocationProp_t
#define CUmemAllocationProp mcDrvMemAllocationProp_t

#define CUipcEventHandle_st mcIpcEventHandle_st
#define CUipcEventHandle_v1 mcIpcEventHandle_t
#define CUipcEventHandle mcDrvIpcEventHandle_t

#define CUipcMemHandle_st mcIpcMemHandle_st
#define CUipcMemHandle_v1 mcIpcMemHandle_t
#define CUipcMemHandle mcDrvIpcMemHandle_t

/* enum CUmemAllocationType */
#define CUmemAllocationType_enum mcMemAllocationType
#define CU_MEM_ALLOCATION_TYPE_INVALID mcMemAllocationTypeInvalid
#define CU_MEM_ALLOCATION_TYPE_PINNED mcMemAllocationTypePinned
#define CU_MEM_ALLOCATION_TYPE_MAX mcMemAllocationTypeMax
#define CUmemAllocationType mcDrvMemAllocationType_t

/* enum CUmemAllocationGranularity_flags */
#define CUmemAllocationGranularity_flags_enum                                  \
  mcMemAllocationGranularityFlags_enum
#define CU_MEM_ALLOC_GRANULARITY_MINIMUM MC_MEM_ALLOC_GRANULARITY_MINIMUM
#define CU_MEM_ALLOC_GRANULARITY_RECOMMENDED                                   \
  MC_MEM_ALLOC_GRANULARITY_RECOMMENDED
#define CUmemAllocationGranularity_flags mcMemAllocationGranularity_flags

/* struct */
#define CUmemPoolProps_st mcMemPoolProps
#define CUmemPoolProps_v1 mcDrvMemPoolProps_t
#define CUmemPoolProps mcDrvMemPoolProps_t

/* struct */
#define CUarrayMapInfo_st mcDrvArrayMapInfo_st
#define CUarrayMapInfo_v1 mcDrvArrayMapInfo
#define CUarrayMapInfo mcDrvArrayMapInfo

/* struct */
#define CUmemLocation_st mcMemLocation
#define CUmemLocation_v1 mcDrvMemLocation_t
#define CUmemLocation mcDrvMemLocation_t

/* struct */
#define CUmemPoolPtrExportData_st mcMemPoolPtrExportData
#define CUmemPoolPtrExportData_v1 mcDrvMemPoolPtrExportData_t
#define CUmemPoolPtrExportData mcDrvMemPoolPtrExportData_t

/* enum CUmemPool_attribute */
#define CUmemPool_attribute_enum mcMemPoolAttr
#define CU_MEMPOOL_ATTR_REUSE_FOLLOW_EVENT_DEPENDENCIES                        \
  mcMemPoolReuseFollowEventDependencies
#define CU_MEMPOOL_ATTR_REUSE_ALLOW_OPPORTUNISTIC                              \
  mcMemPoolReuseAllowOpportunistic
#define CU_MEMPOOL_ATTR_REUSE_ALLOW_INTERNAL_DEPENDENCIES                      \
  mcMemPoolReuseAllowInternalDependencies
#define CU_MEMPOOL_ATTR_RELEASE_THRESHOLD mcMemPoolAttrReleaseThreshold
#define CU_MEMPOOL_ATTR_RESERVED_MEM_CURRENT mcMemPoolAttrReservedMemCurrent
#define CU_MEMPOOL_ATTR_RESERVED_MEM_HIGH mcMemPoolAttrReservedMemHigh
#define CU_MEMPOOL_ATTR_USED_MEM_CURRENT mcMemPoolAttrUsedMemCurrent
#define CU_MEMPOOL_ATTR_USED_MEM_HIGH mcMemPoolAttrUsedMemHigh
#define CUmemPool_attribute mcDrvMemPoolAttr_t

/* enum CUmemLocationType */
#define CUmemLocationType_enum mcMemLocationType
#define CU_MEM_LOCATION_TYPE_INVALID mcMemLocationTypeInvalid
#define CU_MEM_LOCATION_TYPE_DEVICE mcMemLocationTypeDevice
#define CU_MEM_LOCATION_TYPE_MAX mcMemLocationTypeMax
#define CUmemLocationType mcDrvMemLocationType_t

#define CUmemGenericAllocationHandle_v1 mcDrvMemGenericAllocationHandle_t
#define CUmemGenericAllocationHandle mcDrvMemGenericAllocationHandle_t

#define CU_DEVICE_ATTRIBUTE_MULTICAST_SUPPORTED MC_DEVICE_ATTRIBUTE_MULTICAST_SUPPORTED
#define CU_MULTICAST_GRANULARITY_MINIMUM MC_MULTICAST_GRANULARITY_MINIMUM
#define CU_MULTICAST_GRANULARITY_RECOMMENDED MC_MULTICAST_GRANULARITY_RECOMMENDED
#define CUmulticastGranularity_flags_enum mcMulticastGranularity_flags_enum
#define CUmulticastGranularity_flags mcMulticastGranularity_flags
#define CUmulticastObjectProp_st mcMulticastObjectProp_st
#define CUmulticastObjectProp_v1 mcMulticastObjectProp_v1
#define CUmulticastObjectProp mcMulticastObjectProp

/* struct */
#define CUmemAccessDesc_st mcMemAccessDesc
#define CUmemAccessDesc_v1 mcDrvMemAccessDesc_t
#define CUmemAccessDesc mcDrvMemAccessDesc_t

/* enum CUmemAccess_flags */
#define CUmemAccess_flags_enum mcMemAccessFlags
#define CU_MEM_ACCESS_FLAGS_PROT_NONE mcMemAccessFlagsProtNone
#define CU_MEM_ACCESS_FLAGS_PROT_READ mcMemAccessFlagsProtRead
#define CU_MEM_ACCESS_FLAGS_PROT_READWRITE mcMemAccessFlagsProtReadWrite
#define CU_MEM_ACCESS_FLAGS_PROT_MAX mcMemAccessFlagsProtReadMax
#define CUmemAccess_flags mcDrvMemAccess_flags_t

/* enum CUmemAllocationHandleType */
#define CUmemAllocationHandleType_enum mcMemAllocationHandleType
#define CU_MEM_HANDLE_TYPE_NONE mcMemHandleTypeNone
#define CU_MEM_HANDLE_TYPE_POSIX_FILE_DESCRIPTOR                               \
  mcMemHandleTypePosixFileDescriptor
#define CU_MEM_HANDLE_TYPE_WIN32 mcMemHandleTypeWin32
#define CU_MEM_HANDLE_TYPE_WIN32_KMT mcMemHandleTypeWin32Kmt
#define CU_MEM_HANDLE_TYPE_MAX mcMemHandleTypeMax
#define CUmemAllocationHandleType mcDrvMemAllocationHandleType_t

#define CUipcMem_flags_enum mcDrvIpcMemFlags_enum
#define CU_IPC_MEM_LAZY_ENABLE_PEER_ACCESS MC_IPC_MEM_LAZY_ENABLE_PEER_ACCESS
#define CUipcMem_flags mcDrvIpcMemFlags

#define CUmemAttach_flags_enum mcDrvMemAttachFlags_enum
#define CU_MEM_ATTACH_GLOBAL MC_MEM_ATTACH_GLOBAL
#define CU_MEM_ATTACH_HOST MC_MEM_ATTACH_HOST
#define CU_MEM_ATTACH_SINGLE MC_MEM_ATTACH_SINGLE
#define CUmemAttach_flags mcDrvMemAttachFlags

/* enum CUmem_range_attribute */
#define CUmem_range_attribute_enum mcDrvMemoryAttributeType_enum
#define CUmem_range_attribute mcDrvMemoryAttributeType_t
#define CU_MEM_RANGE_ATTRIBUTE_READ_MOSTLY MC_MEM_RANGE_ATTRIBUTE_READ_MOSTLY
#define CU_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION                              \
  MC_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION
#define CU_MEM_RANGE_ATTRIBUTE_ACCESSED_BY MC_MEM_RANGE_ATTRIBUTE_ACCESSED_BY
#define CU_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION                          \
  MC_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION

// function redefinition
#define cuGetErrorName wcuGetErrorName
#define cuGetErrorString wcuGetErrorString

#define cuInit wcuInit

/* event flags */
#define CUevent_flags_enum mcDrvEventFlagsEnum
#define CU_EVENT_DEFAULT MC_EVENT_DEFAULT
#define CU_EVENT_BLOCKING_SYNC MC_EVENT_BLOCKING_SYNC
#define CU_EVENT_DISABLE_TIMING MC_EVENT_DISABLE_TIMING
#define CU_EVENT_INTERPROCESS MC_EVENT_INTERPROCESS
#define CUevent_flags mcDrvEventFlags

#define CUevent_record_flags_enum mcDrvEventRecordFlagsEnum
#define CU_EVENT_RECORD_DEFAULT MC_EVENT_RECORD_DEFAULT
#define CU_EVENT_RECORD_EXTERNAL MC_EVENT_RECORD_EXTERNAL
#define CUevent_record_flags mcDrvEventRecordFlags

#define CUevent_wait_flags_enum mcDrvEventWaitFlagsEnum
#define CU_EVENT_WAIT_DEFAULT MC_EVENT_WAIT_DEFAULT
#define CU_EVENT_WAIT_EXTERNAL MC_EVENT_WAIT_EXTERNAL
#define CUevent_wait_flags mcDrvEventWaitFlags

/*************Data types used by CUDA driver********************/
#define CU_ARRAY_SPARSE_PROPERTIES_SINGLE_MIPTAIL                              \
  MC_ARRAY_SPARSE_PROPERTIES_SINGLE_MIPTAIL
#define CU_IPC_HANDLE_SIZE MC_IPC_HANDLE_SIZE
#define CU_LAUNCH_PARAM_BUFFER_POINTER MC_LAUNCH_PARAM_BUFFER_POINTER
#define CU_LAUNCH_PARAM_BUFFER_SIZE MC_LAUNCH_PARAM_BUFFER_SIZE
#define CU_LAUNCH_PARAM_END MC_LAUNCH_PARAM_END
#define CU_MEM_CREATE_USAGE_TILE_POOL MC_MEM_CREATE_USAGE_TILE_POOL
#define CU_PARAM_TR_DEFAULT MC_PARAM_TR_DEFAULT
#define CU_TRSA_OVERRIDE_FORMAT MC_TRSA_OVERRIDE_FORMAT
#define CU_TRSF_READ_AS_INTEGER MC_TRSF_READ_AS_INTEGER
#define CU_TRSF_NORMALIZED_COORDINATES MC_TRSF_NORMALIZED_COORDINATES
#define CU_TRSF_SRGB MC_TRSF_SRGB
#define CU_TRSF_DISABLE_TRILINEAR_OPTIMIZATION                                 \
  MC_TRSF_DISABLE_TRILINEAR_OPTIMIZATION
#define CU_TRSF_SEAMLESS_CUBEMAP MC_TRSF_SEAMLESS_CUBEMAP

#define CUDA_ARRAY3D_LAYERED MC_ARRAY3D_LAYERED
#define CUDA_ARRAY3D_2DARRAY MC_ARRAY3D_2DARRAY
#define CUDA_ARRAY3D_SURFACE_LDST MC_ARRAY3D_SURFACE_LDST
#define CUDA_ARRAY3D_CUBEMAP MC_ARRAY3D_CUBEMAP
#define CUDA_ARRAY3D_TEXTURE_GATHER MC_ARRAY3D_TEXTURE_GATHER
#define CUDA_ARRAY3D_DEPTH_TEXTURE MC_ARRAY3D_DEPTH_TEXTURE
#define CUDA_ARRAY3D_COLOR_ATTACHMENT MC_ARRAY3D_COLOR_ATTACHMENT
#define CUDA_ARRAY3D_SPARSE MC_ARRAY3D_SPARSE
#define CUDA_ARRAY3D_DEFERRED_MAPPING MC_ARRAY3D_DEFERRED_MAPPING
#define CUDA_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_PRE_LAUNCH_SYNC                \
  MC_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_PRE_LAUNCH_SYNC
#define CUDA_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_POST_LAUNCH_SYNC               \
  MC_COOPERATIVE_LAUNCH_MULTI_DEVICE_NO_POST_LAUNCH_SYNC
#define CUDA_EXTERNAL_MEMORY_DEDICATED MC_EXTERNAL_MEMORY_DEDICATED
#define CUDA_EXTERNAL_SEMAPHORE_SIGNAL_SKIP_NVSCIBUF_MEMSYNC                   \
  MC_EXTERNAL_SEMAPHORE_SIGNAL_SKIP_NVSCIBUF_MEMSYNC
#define CUDA_EXTERNAL_SEMAPHORE_WAIT_SKIP_NVSCIBUF_MEMSYNC                     \
  MC_EXTERNAL_SEMAPHORE_WAIT_SKIP_NVSCIBUF_MEMSYNC
#define CUDA_NVSCISYNC_ATTR_SIGNAL MC_NVSCISYNC_ATTR_SIGNAL
#define CUDA_NVSCISYNC_ATTR_WAIT MC_NVSCISYNC_ATTR_WAIT

/* enum */
#define CUarraySparseSubresourceType_enum mcDrvArraySparseSubresourceType_enum
#define CU_ARRAY_SPARSE_SUBRESOURCE_TYPE_SPARSE_LEVEL                          \
  MC_ARRAY_SPARSE_SUBRESOURCE_TYPE_SPARSE_LEVEL
#define CU_ARRAY_SPARSE_SUBRESOURCE_TYPE_MIPTAIL                               \
  MC_ARRAY_SPARSE_SUBRESOURCE_TYPE_MIPTAIL
#define CUarraySparseSubresourceType mcDrvArraySparseSubresourceType

#define CUmemAllocationCompType_enum mcDrvMemAllocationCompType_enum
#define CU_MEM_ALLOCATION_COMP_NONE MC_MEM_ALLOCATION_COMP_NONE
#define CU_MEM_ALLOCATION_COMP_GENERIC MC_MEM_ALLOCATION_COMP_GENERIC
#define CUmemAllocationCompType mcDrvMemAllocationCompType

#define CUmemHandleType_enum mcDrvMemHandleType_enum
#define CU_MEM_HANDLE_TYPE_GENERIC MC_MEM_HANDLE_TYPE_GENERIC
#define CUmemHandleType mcDrvMemHandleType

#define CUmemOperationType_enum mcDrvMemOperationType_enum
#define CU_MEM_OPERATION_TYPE_MAP MC_MEM_OPERATION_TYPE_MAP
#define CU_MEM_OPERATION_TYPE_UNMAP MC_MEM_OPERATION_TYPE_UNMAP
#define CUmemOperationType mcDrvMemOperationType

#define CUflushGPUDirectRDMAWritesOptions_enum mcFlushGPUDirectRDMAWritesOptions
#define CU_FLUSH_GPU_DIRECT_RDMA_WRITES_OPTION_HOST                            \
  mcFlushGPUDirectRDMAWritesOptionHost
#define CU_FLUSH_GPU_DIRECT_RDMA_WRITES_OPTION_MEMOPS                          \
  mcFlushGPUDirectRDMAWritesOptionMemOps
#define CUflushGPUDirectRDMAWritesOptions mcDrvFlushGPUDirectRDMAWritesOptions

#define CUGPUDirectRDMAWritesOrdering_enum mcGPUDirectRDMAWritesOrdering_enum
#define CU_GPU_DIRECT_RDMA_WRITES_ORDERING_NONE                                \
  mcGPUDirectRDMAWritesOrderingNone
#define CU_GPU_DIRECT_RDMA_WRITES_ORDERING_OWNER                               \
  mcGPUDirectRDMAWritesOrderingOwner
#define CU_GPU_DIRECT_RDMA_WRITES_ORDERING_ALL_DEVICES                         \
  mcGPUDirectRDMAWritesOrderingAllDevices
#define CUGPUDirectRDMAWritesOrdering mcGPUDirectRDMAWritesOrdering

#define CUflushGPUDirectRDMAWritesScope_enum                                   \
  mcDrvFlushGPUDirectRDMAWritesScope_enum
#define CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_OWNER                               \
  MC_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_OWNER
#define CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_ALL_DEVICES                         \
  MC_FLUSH_GPU_DIRECT_RDMA_WRITES_TO_ALL_DEVICES
#define CUflushGPUDirectRDMAWritesScope mcDrvFlushGPUDirectRDMAWritesScope

#define CUflushGPUDirectRDMAWritesTarget_enum                                  \
  mcDrvFlushGPUDirectRDMAWritesTarget_enum
#define CU_FLUSH_GPU_DIRECT_RDMA_WRITES_TARGET_CURRENT_CTX                     \
  MC_FLUSH_GPU_DIRECT_RDMA_WRITES_TARGET_CURRENT_CTX
#define CUflushGPUDirectRDMAWritesTarget mcDrvFlushGPUDirectRDMAWritesTarget

#define CUgraphDebugDot_flags_enum mcGraphDebugDotFlags_enum
#define CU_GRAPH_DEBUG_DOT_FLAGS_VERBOSE mcGraphDebugDotFlagsVerbose
#define CU_GRAPH_DEBUG_DOT_FLAGS_RUNTIME_TYPES mcGraphDebugDotFlagsRuntimeTypes
#define CU_GRAPH_DEBUG_DOT_FLAGS_KERNEL_NODE_PARAMS                            \
  mcGraphDebugDotFlagsKernelNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_MEMCPY_NODE_PARAMS                            \
  mcGraphDebugDotFlagsMemcpyNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_MEMSET_NODE_PARAMS                            \
  mcGraphDebugDotFlagsMemsetNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_HOST_NODE_PARAMS                              \
  mcGraphDebugDotFlagsHostNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_EVENT_NODE_PARAMS                             \
  mcGraphDebugDotFlagsEventNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_EXT_SEMAS_SIGNAL_NODE_PARAMS                  \
  mcGraphDebugDotFlagsExtSemasSignalNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_EXT_SEMAS_WAIT_NODE_PARAMS                    \
  mcGraphDebugDotFlagsExtSemasWaitNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_KERNEL_NODE_ATTRIBUTES                        \
  mcGraphDebugDotFlagsKernelNodeAttributes
#define CU_GRAPH_DEBUG_DOT_FLAGS_HANDLES mcGraphDebugDotFlagsHandles
#define CU_GRAPH_DEBUG_DOT_FLAGS_MEM_ALLOC_NODE_PARAMS                         \
  mcGraphDebugDotFlagsMemAllocNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_MEM_FREE_NODE_PARAMS                          \
  mcGraphDebugDotFlagsMemFreeNodeParams
#define CU_GRAPH_DEBUG_DOT_FLAGS_BATCH_MEM_OP_NODE_PARAMS                      \
  mcGraphDebugDotFlagsBatchMemOpNodeParams
#define CUgraphDebugDot_flags mcGraphDebugDotFlags

#define CUgraphInstantiate_flags_enum mcGraphInstantiateFlags_enum
#define CUDA_GRAPH_INSTANTIATE_FLAG_AUTO_FREE_ON_LAUNCH                        \
  mcGraphInstantiateFlagAutoFreeOnLaunch
#define CUDA_GRAPH_INSTANTIATE_FLAG_UPLOAD mcGraphInstantiateFlagUpload
#define CUDA_GRAPH_INSTANTIATE_FLAG_DEVICE_LAUNCH                              \
  mcGraphInstantiateFlagDeviceLaunch
#define CUDA_GRAPH_INSTANTIATE_FLAG_USE_NODE_PRIORITY                          \
  mcGraphInstantiateFlagUseNodePriority
#define CUgraphInstantiate_flags mcGraphInstantiateFlags

#define CUgraphInstantiateResult_enum mcGraphInstantiateResult_enum
#define CUDA_GRAPH_INSTANTIATE_SUCCESS mcGraphInstantiateSuccess
#define CUDA_GRAPH_INSTANTIATE_ERROR mcGraphInstantiateError
#define CUDA_GRAPH_INSTANTIATE_INVALID_STRUCTURE                               \
  mcGraphInstantiateInvalidStructure
#define CUDA_GRAPH_INSTANTIATE_NODE_OPERATION_NOT_SUPPORTED                    \
  mcGraphInstantiateNodeOperationNotSupported
#define CUDA_GRAPH_INSTANTIATE_MULTIPLE_CTXS_NOT_SUPPORTED                     \
  mcGraphInstantiateMultipleDevicesNotSupported
#define CUgraphInstantiateResult mcGraphInstantiateResult

#define CUexternalSemaphoreHandleType_enum mcExternalSemaphoreHandleType
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD                            \
  mcExternalSemaphoreHandleTypeOpaqueFd
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32                         \
  mcExternalSemaphoreHandleTypeOpaqueWin32
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT                     \
  mcExternalSemaphoreHandleTypeOpaqueWin32Kmt
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D12_FENCE                          \
  mcExternalSemaphoreHandleTypeD3D12Fence
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_FENCE                          \
  mcExternalSemaphoreHandleTypeD3D11Fence
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_NVSCISYNC                            \
  mcExternalSemaphoreHandleTypeNvSciSync
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_KEYED_MUTEX                    \
  mcExternalSemaphoreHandleTypeKeyedMutex
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_D3D11_KEYED_MUTEX_KMT                \
  mcExternalSemaphoreHandleTypeKeyedMutexKmt
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_TIMELINE_SEMAPHORE_FD                \
  mcExternalSemaphoreHandleTypeTimelineSemaphoreFd
#define CU_EXTERNAL_SEMAPHORE_HANDLE_TYPE_TIMELINE_SEMAPHORE_WIN32             \
  mcExternalSemaphoreHandleTypeTimelineSemaphoreWin32
#define CUexternalSemaphoreHandleType mcDrvExternalSemaphoreHandleType

#define CUpointer_attribute_enum mcDrvPointerAttributeType_enum
#define CU_POINTER_ATTRIBUTE_CONTEXT MC_POINTER_ATTRIBUTE_CONTEXT
#define CU_POINTER_ATTRIBUTE_MEMORY_TYPE MC_POINTER_ATTRIBUTE_MEMORY_TYPE
#define CU_POINTER_ATTRIBUTE_DEVICE_POINTER MC_POINTER_ATTRIBUTE_DEVICE_POINTER
#define CU_POINTER_ATTRIBUTE_HOST_POINTER MC_POINTER_ATTRIBUTE_HOST_POINTER
#define CU_POINTER_ATTRIBUTE_P2P_TOKENS MC_POINTER_ATTRIBUTE_P2P_TOKENS
#define CU_POINTER_ATTRIBUTE_SYNC_MEMOPS MC_POINTER_ATTRIBUTE_SYNC_MEMOPS
#define CU_POINTER_ATTRIBUTE_BUFFER_ID MC_POINTER_ATTRIBUTE_BUFFER_ID
#define CU_POINTER_ATTRIBUTE_IS_MANAGED MC_POINTER_ATTRIBUTE_IS_MANAGED
#define CU_POINTER_ATTRIBUTE_DEVICE_ORDINAL MC_POINTER_ATTRIBUTE_DEVICE_ORDINAL
#define CU_POINTER_ATTRIBUTE_IS_LEGACY_CUDA_IPC_CAPABLE                        \
  MC_POINTER_ATTRIBUTE_IS_LEGACY_CUDA_IPC_CAPABLE
#define CU_POINTER_ATTRIBUTE_RANGE_START_ADDR                                  \
  MC_POINTER_ATTRIBUTE_RANGE_START_ADDR
#define CU_POINTER_ATTRIBUTE_RANGE_SIZE MC_POINTER_ATTRIBUTE_RANGE_SIZE
#define CU_POINTER_ATTRIBUTE_MAPPED MC_POINTER_ATTRIBUTE_MAPPED
#define CU_POINTER_ATTRIBUTE_ALLOWED_HANDLE_TYPES                              \
  MC_POINTER_ATTRIBUTE_ALLOWED_HANDLE_TYPES
#define CU_POINTER_ATTRIBUTE_IS_GPU_DIRECT_RDMA_CAPABLE                        \
  MC_POINTER_ATTRIBUTE_IS_GPU_DIRECT_RDMA_CAPABLE
#define CU_POINTER_ATTRIBUTE_ACCESS_FLAGS MC_POINTER_ATTRIBUTE_ACCESS_FLAGS
#define CU_POINTER_ATTRIBUTE_MEMPOOL_HANDLE MC_POINTER_ATTRIBUTE_MEMPOOL_HANDLE
#define CUpointer_attribute mcDrvPointerAttributeType

#define CUshared_carveout_enum mcSharedCarveout
#define CU_SHAREDMEM_CARVEOUT_DEFAULT mcSharedmemCarveoutDefault
#define CU_SHAREDMEM_CARVEOUT_MAX_SHARED mcSharedmemCarveoutMaxShared
#define CU_SHAREDMEM_CARVEOUT_MAX_L1 mcSharedmemCarveoutMaxL1
#define CUshared_carveout mcDrvSharedCarveout_t

/*enum*/
#define CUstreamBatchMemOpType_enum mcStreamBatchMemOpType_enum
#define CU_STREAM_MEM_OP_WAIT_VALUE_32 MC_STREAM_MEM_OP_WAIT_VALUE_32
#define CU_STREAM_MEM_OP_WRITE_VALUE_32 MC_STREAM_MEM_OP_WRITE_VALUE_32
#define CU_STREAM_MEM_OP_WAIT_VALUE_64 MC_STREAM_MEM_OP_WAIT_VALUE_64
#define CU_STREAM_MEM_OP_WRITE_VALUE_64 MC_STREAM_MEM_OP_WRITE_VALUE_64
#define CU_STREAM_MEM_OP_FLUSH_REMOTE_WRITES                                   \
  MC_STREAM_MEM_OP_FLUSH_REMOTE_WRITES
#define CUstreamBatchMemOpType mcStreamBatchMemOpType

#define CUstreamWaitValue_flags_enum mcStreamWaitValue_flags_enum
#define CU_STREAM_WAIT_VALUE_GEQ MC_STREAM_WAIT_VALUE_GEQ
#define CU_STREAM_WAIT_VALUE_EQ MC_STREAM_WAIT_VALUE_EQ
#define CU_STREAM_WAIT_VALUE_AND MC_STREAM_WAIT_VALUE_AND
#define CU_STREAM_WAIT_VALUE_NOR MC_STREAM_WAIT_VALUE_NOR
#define CU_STREAM_WAIT_VALUE_FLUSH MC_STREAM_WAIT_VALUE_FLUSH
#define CUstreamWaitValue_flags mcStreamWaitValue_flags

#define CUstreamWriteValue_flags_enum mcStreamWriteValue_flags_enum
#define CU_STREAM_WRITE_VALUE_DEFAULT MC_STREAM_WRITE_VALUE_DEFAULT
#define CU_STREAM_WRITE_VALUE_NO_MEMORY_BARRIER                                \
  MC_STREAM_WRITE_VALUE_NO_MEMORY_BARRIER
#define CUstreamWriteValue_flags mcStreamWriteValue_flags

#define CUstreamCaptureMode_enum mcStreamCaptureMode_enum
#define CU_STREAM_CAPTURE_MODE_GLOBAL mcStreamCaptureModeGlobal
#define CU_STREAM_CAPTURE_MODE_THREAD_LOCAL mcStreamCaptureModeThreadLocal
#define CU_STREAM_CAPTURE_MODE_RELAXED mcStreamCaptureModeRelaxed
#define CUstreamCaptureMode mcStreamCaptureMode

#define CUstreamCaptureStatus_enum mcStreamCaptureStatus_enum
#define CUstreamCaptureStatus mcStreamCaptureStatus
#define CU_STREAM_CAPTURE_STATUS_NONE mcStreamCaptureStatusNone
#define CU_STREAM_CAPTURE_STATUS_ACTIVE mcStreamCaptureStatusActive
#define CU_STREAM_CAPTURE_STATUS_INVALIDATED mcStreamCaptureStatusInvalidated

#define CUuserObject_flags_enum mcUserObjectFlags
#define CU_USER_OBJECT_NO_DESTRUCTOR_SYNC mcUserObjectNoDestructorSync
#define CUuserObject_flags mvDrvUserObjectFlags

#define CUuserObjectRetain_flags_enum mcUserObjectRetainFlags
#define CU_GRAPH_USER_OBJECT_MOVE mcGraphUserObjectMove
#define CUuserObjectRetain_flags mcDrvUserObjectRetainFlags

#define cuCtxCreate_v2 wcuCtxCreate
#define cuCtxDestroy_v2 wcuCtxDestroy
#define cuCtxPopCurrent_v2 wcuCtxPopCurrent
#define cuCtxPushCurrent_v2 wcuCtxPushCurrent
#define cuDevicePrimaryCtxRelease_v2 wcuDevicePrimaryCtxRelease
#define cuDevicePrimaryCtxReset_v2 wcuDevicePrimaryCtxReset
#define cuDevicePrimaryCtxSetFlags_v2 wcuDevicePrimaryCtxSetFlags
#define cuDeviceTotalMem_v2 wcuDeviceTotalMem
#define cuIpcOpenMemHandle_v2 wcuIpcOpenMemHandle
#define cuLinkAddData_v2 wcuLinkAddData
#define cuLinkAddFile_v2 wcuLinkAddFile
#define cuLinkCreate_v2 wcuLinkCreate
#define cuMemAlloc_v2 wcuMemAlloc
#define cuMemAllocHost_v2 wcuMemAllocHost
#define cuMemAllocPitch_v2 wcuMemAllocPitch
#define cuMemcpyHtoD_v2 wcuMemcpyHtoD
#define cuMemcpyDtoH_v2 wcuMemcpyDtoH
#define cuMemcpyDtoD_v2 wcuMemcpyDtoD
#define cuMemcpyDtoA_v2 wcuMemcpyDtoA
#define cuMemcpyAtoD_v2 wcuMemcpyAtoD
#define cuMemcpyHtoA_v2 wcuMemcpyHtoA
#define cuMemcpyAtoH_v2 wcuMemcpyAtoH
#define cuMemcpyAtoA_v2 wcuMemcpyAtoA
#define cuMemcpyHtoAAsync_v2 wcuMemcpyHtoAAsync
#define cuMemcpyAtoHAsync_v2 wcuMemcpyAtoHAsync
#define cuMemcpy2D_v2 wcuMemcpy2D
#define cuMemcpy2DUnaligned_v2 wcuMemcpy2DUnaligned
#define cuMemcpy3D_v2 wcuMemcpy3D
#define cuMemcpyHtoDAsync_v2 wcuMemcpyHtoDAsync
#define cuMemcpyDtoHAsync_v2 wcuMemcpyDtoHAsync
#define cuMemcpyDtoDAsync_v2 wcuMemcpyDtoDAsync
#define cuMemcpy2DAsync_v2 wcuMemcpy2DAsync
#define cuMemcpy3DAsync_v2 wcuMemcpy3DAsync
#define cuMemsetD8_v2 wcuMemsetD8
#define cuMemsetD16_v2 wcuMemsetD16
#define cuMemsetD32_v2 wcuMemsetD32
#define cuMemsetD2D8_v2 wcuMemsetD2D8
#define cuMemsetD2D16_v2 wcuMemsetD2D16
#define cuMemsetD2D32_v2 wcuMemsetD2D32
#define cuArrayCreate_v2 wcuArrayCreate
#define cuArrayGetDescriptor_v2 wcuArrayGetDescriptor
#define cuArray3DCreate_v2 wcuArray3DCreate
#define cuArray3DGetDescriptor_v2 wcuArray3DGetDescriptor
#define cuMemFree_v2 wcuMemFree
#define cuMemHostGetDevicePointer_v2 wcuMemHostGetDevicePointer
#define cuMemHostRegister_v2 wcuMemHostRegister
#define cuMemGetAddressRange_v2 wcuMemGetAddressRange
#define cuMemGetInfo_v2 wcuMemGetInfo
#define cuModuleGetGlobal_v2 wcuModuleGetGlobal
#define cuStreamBeginCapture_v2 wcuStreamBeginCapture
#define cuStreamDestroy_v2 wcuStreamDestroy
#define cuEventDestroy_v2 wcuEventDestroy
#define cuGraphInstantiate_v2 wcuGraphInstantiate
#define cuTexRefSetAddress_v2 wcuTexRefSetAddress
#define cuTexRefSetAddress2D_v3 wcuTexRefSetAddress2D
#define cuTexRefGetAddress_v2 wcuTexRefGetAddress

#define cuGraphicsResourceSetMapFlags_v2 wcuGraphicsResourceSetMapFlags
#define cuGraphicsResourceGetMappedPointer_v2                                  \
  wcuGraphicsResourceGetMappedPointer

/**************Execution Control Driver API******************/
#define cuFuncGetAttribute wcuFuncGetAttribute
#define cuLaunchKernel wcuLaunchKernel
#define cuFuncGetModule wcuFuncGetModule
#define cuFuncSetAttribute wcuFuncSetAttribute
#define cuFuncSetCacheConfig wcuFuncSetCacheConfig
#define cuFuncSetSharedMemConfig wcuFuncSetSharedMemConfig
#define cuLaunchCooperativeKernel wcuLaunchCooperativeKernel
#define cuLaunchHostFunc wcuLaunchHostFunc

#define cuLaunchCooperativeKernelMultiDevice                                   \
  wcuLaunchCooperativeKernelMultiDevice

/* deprecated */
#define cuFuncSetBlockShape wcuFuncSetBlockShape
#define cuFuncSetSharedSize wcuFuncSetSharedSize
#define cuLaunch wcuLaunch
#define cuLaunchGrid wcuLaunchGrid
#define cuLaunchGridAsync wcuLaunchGridAsync
#define cuParamSetf wcuParamSetf
#define cuParamSeti wcuParamSeti
#define cuParamSetSize wcuParamSetSize
#define cuParamSetTexRef wcuParamSetTexRef
#define cuParamSetv wcuParamSetv

/**************Execution Control drive type******************/
#define CUhostFn mcHostFn_t

/*stream flags*/
#define CUstream_flags_enum mcDrvStreamFlags_enum
#define CU_STREAM_DEFAULT MC_STREAM_DEFAULT
#define CU_STREAM_NON_BLOCKING MC_STREAM_NON_BLOCKING
#define CUstream_flags mcDrvStreamFlags

#define CU_STREAM_LEGACY mcStreamAllThreads
#define CU_STREAM_PER_THREAD mcStreamPerThread

/*stream Driver types*/
#define CUstreamCallback mcDrvStreamCallback_t

#define CUstreamAttrValue_union mcStreamAttrValue
#define CUstreamAttrValue_v1 mcDrvStreamAttrValue
#define CUstreamAttrValue mcDrvStreamAttrValue

#define CUstreamAttrID_enum mcStreamAttrID_enum
#define CU_STREAM_ATTRIBUTE_ACCESS_POLICY_WINDOW                               \
  mcStreamAttributeAccessPolicyWindow
#define CU_STREAM_ATTRIBUTE_SYNCHRONIZATION_POLICY                             \
  mcStreamAttributeSynchronizationPolicy
#define CUstreamAttrID mcStreamAttrID

#define CUsynchronizationPolicy_enum mcSynchronizationPolicy
#define CU_SYNC_POLICY_AUTO mcSyncPolicyAuto
#define CU_SYNC_POLICY_SPIN mcSyncPolicySpin
#define CU_SYNC_POLICY_YIELD mcSyncPolicyYield
#define CU_SYNC_POLICY_BLOCKING_SYNC mcSyncPolicyBlockingSync
#define CUsynchronizationPolicy mcDrvSynchronizationPolicy

#define CUaccessProperty_enum mcAccessProperty_enum
#define CUaccessProperty mcAccessProperty
#define CU_ACCESS_PROPERTY_NORMAL mcAccessPropertyNormal
#define CU_ACCESS_PROPERTY_STREAMING mcAccessPropertyStreaming
#define CU_ACCESS_PROPERTY_PERSISTING mcAccessPropertyPersisting

/******************Stream Driver API*************************/
#define cuStreamBeginCapture wcuStreamBeginCapture
#define cuStreamCreate wcuStreamCreate
#define cuStreamCreateWithPriority wcuStreamCreateWithPriority
#define cuStreamDestroy wcuStreamDestroy
#define cuStreamEndCapture wcuStreamEndCapture
#define cuStreamGetCaptureInfo wcuStreamGetCaptureInfo
#define cuStreamGetCaptureInfo_v2 wcuStreamGetCaptureInfo_v2
#define cuStreamGetFlags wcuStreamGetFlags
#define cuStreamGetPriority wcuStreamGetPriority
#define cuStreamIsCapturing wcuStreamIsCapturing
#define cuStreamQuery wcuStreamQuery
#define cuStreamSynchronize wcuStreamSynchronize
#define cuStreamWaitEvent wcuStreamWaitEvent
#define cuStreamAddCallback wcuStreamAddCallback
#define cuStreamAttachMemAsync wcuStreamAttachMemAsync
#define cuStreamCopyAttributes wcuStreamCopyAttributes
#define cuStreamSetAttribute wcuStreamSetAttribute
#define cuStreamGetAttribute wcuStreamGetAttribute
#define cuStreamGetCtx wcuStreamGetCtx
#define cuStreamUpdateCaptureDependencies wcuStreamUpdateCaptureDependencies
#define cuThreadExchangeStreamCaptureMode wcuThreadExchangeStreamCaptureMode

/**************Profiler Control Driver Types******************/
#define CUoutput_mode_enum mcOutputMode
#define CUoutput_mode mcOutputMode_t
#define CU_OUT_KEY_VALUE_PAIR mcKeyValuePair
#define CU_OUT_CSV mcCSV

/**************Profiler Control Driver API******************/
#define cuProfilerInitialize wcuProfilerInitialize
#define cuProfilerStart wcuProfilerStart
#define cuProfilerStop wcuProfilerStop

/******************Device Driver API*************************/
#define cuDeviceGetAttribute wcuDeviceGetAttribute
#define cuDeviceGetUuid wcuDeviceGetUuid
#define cuDeviceGetUuid_v2 wcuDeviceGetUuid_v2
#define cuDeviceGetCount wcuDeviceGetCount
#define cuDeviceGetByPCIBusId wcuDeviceGetByPCIBusId
#define cuDeviceGetExecAffinitySupport wcuDeviceGetExecAffinitySupport
#define cuDeviceGetPCIBusId wcuDeviceGetPCIBusId
#define cuDeviceGet wcuDeviceGet
#define cuDeviceGetName wcuDeviceGetName
#define cuDeviceTotalMem wcuDeviceTotalMem
#define cuDeviceGetLuid wcuDeviceGetLuid
#define cuDeviceGetP2PAttribute wcuDeviceGetP2PAttribute
#define cuFlushGPUDirectRDMAWrites wcuFlushGPUDirectRDMAWrites
#define cuDeviceGetDefaultMemPool wcuDeviceGetDefaultMemPool
#define cuDeviceGetMemPool wcuDeviceGetMemPool
#define cuDeviceSetMemPool wcuDeviceSetMemPool
#define cuDeviceGetNvSciSyncAttributes wcuDeviceGetNvSciSyncAttributes
#define cuDeviceGetTexture1DLinearMaxWidth wcuDeviceGetTexture1DLinearMaxWidth

/* Deprecated APIs */
#define cuDeviceComputeCapability wcuDeviceComputeCapability
#define cuDeviceGetProperties wcuDeviceGetProperties

/******************Event Driver API*************************/
#define cuEventCreate wcuEventCreate
#define cuEventDestroy wcuEventDestroy
#define cuEventElapsedTime wcuEventElapsedTime
#define cuEventQuery wcuEventQuery
#define cuEventRecord wcuEventRecord
#define cuEventRecordWithFlags wcuEventRecordWithFlags
#define cuEventSynchronize wcuEventSynchronize
#define cuIpcGetEventHandle wcuIpcGetEventHandle
#define cuIpcOpenEventHandle wcuIpcOpenEventHandle

/******************External Resource Interoperability Driver
 * API*************************/
#define cuDestroyExternalMemory wcuDestroyExternalMemory
#define cuDestroyExternalSemaphore wcuDestroyExternalSemaphore
#define cuExternalMemoryGetMappedBuffer wcuExternalMemoryGetMappedBuffer
#define cuExternalMemoryGetMappedMipmappedArray                                \
  wcuExternalMemoryGetMappedMipmappedArray
#define cuImportExternalMemory wcuImportExternalMemory
#define cuImportExternalSemaphore wcuImportExternalSemaphore
#define cuSignalExternalSemaphoresAsync wcuSignalExternalSemaphoresAsync
#define cuWaitExternalSemaphoresAsync wcuWaitExternalSemaphoresAsync

/******************Context Driver API*************************/
#define cuCtxCreate wcuCtxCreate
#define cuCtxCreate_v3 wcuCtxCreate_v3
#define cuCtxDestroy wcuCtxDestroy
#define cuCtxGetApiVersion wcuCtxGetApiVersion
#define cuCtxGetCacheConfig wcuCtxGetCacheConfig
#define cuCtxGetCurrent wcuCtxGetCurrent
#define cuCtxGetDevice wcuCtxGetDevice
#define cuCtxGetExecAffinity wcuCtxGetExecAffinity
#define cuCtxGetFlags wcuCtxGetFlags
#define cuCtxGetLimit wcuCtxGetLimit
#define cuCtxGetSharedMemConfig wcuCtxGetSharedMemConfig
#define cuCtxGetStreamPriorityRange wcuCtxGetStreamPriorityRange
#define cuCtxPopCurrent wcuCtxPopCurrent
#define cuCtxPushCurrent wcuCtxPushCurrent
#define cuCtxResetPersistingL2Cache wcuCtxResetPersistingL2Cache
#define cuCtxSetCacheConfig wcuCtxSetCacheConfig
#define cuCtxSetCurrent wcuCtxSetCurrent
#define cuCtxSetLimit wcuCtxSetLimit
#define cuCtxSetSharedMemConfig wcuCtxSetSharedMemConfig
#define cuCtxSynchronize wcuCtxSynchronize

/* Deprecated APIs */
#define cuCtxAttach wcuCtxAttach
#define cuCtxDetach wcuCtxDetach

/******************Primary Context Driver API*************************/
#define cuDevicePrimaryCtxGetState wcuDevicePrimaryCtxGetState
#define cuDevicePrimaryCtxRelease wcuDevicePrimaryCtxRelease
#define cuDevicePrimaryCtxRetain wcuDevicePrimaryCtxRetain
#define cuDevicePrimaryCtxReset wcuDevicePrimaryCtxReset
#define cuDevicePrimaryCtxSetFlags wcuDevicePrimaryCtxSetFlags

/******************Error Handling Driver API*************************/
#define cuGetErrorName wcuGetErrorName
#define cuGetErrorString wcuGetErrorString

/******************Module Driver API*************************/
#define cuModuleLoad wcuModuleLoad
#define cuModuleLoadData wcuModuleLoadData
#define cuModuleLoadDataEx wcuModuleLoadDataEx
#define cuModuleUnload wcuModuleUnload
#define cuModuleGetFunction wcuModuleGetFunction
#define cuLinkCreate wcuLinkCreate
#define cuLinkAddData wcuLinkAddData
#define cuLinkComplete wcuLinkComplete
#define cuLinkAddFile wcuLinkAddFile
#define cuLinkDestroy wcuLinkDestroy
#define cuModuleGetGlobal wcuModuleGetGlobal
#define cuModuleGetSurfRef wcuModuleGetSurfRef
#define cuModuleGetTexRef wcuModuleGetTexRef
#define cuModuleLoadFatBinary wcuModuleLoadFatBinary

/**********Peer Context Memory Access Driver API************/
#define cuDeviceCanAccessPeer wcuDeviceCanAccessPeer
#define cuCtxEnablePeerAccess wcuCtxEnablePeerAccess
#define cuCtxDisablePeerAccess wcuCtxDisablePeerAccess

/******************Memory Driver API************************/
/* memory flags */
#define CU_MEMHOSTALLOC_PORTABLE mcMallocHostPortable
#define CU_MEMHOSTALLOC_DEVICEMAP mcMallocHostMapped
#define CU_MEMHOSTALLOC_WRITECOMBINED mcMallocHostWriteCombined
#define CU_MEMHOSTREGISTER_PORTABLE mcHostRegisterDefault
#define CU_MEMHOSTREGISTER_DEVICEMAP mcHostRegisterPortable
#define CU_MEMHOSTREGISTER_IOMEMORY mcHostRegisterMapped
/*define as mcHostRegisterDefault temporarily */
#define CU_MEMHOSTREGISTER_READ_ONLY mcHostRegisterDefault

#define CUmemorytype_enum mcMemoryType_enum
#define CUmemorytype mcMemoryType
#define CU_MEMORYTYPE_HOST mcMemoryTypeHost
#define CU_MEMORYTYPE_DEVICE mcMemoryTypeDevice
#define CU_MEMORYTYPE_ARRAY mcMemoryTypeArray
#define CU_MEMORYTYPE_UNIFIED mcMemoryTypeUnified

#define cuArray3DCreate wcuArray3DCreate
#define cuArray3DGetDescriptor wcuArray3DGetDescriptor
#define cuArrayCreate wcuArrayCreate
#define cuArrayDestroy wcuArrayDestroy
#define cuArrayGetDescriptor wcuArrayGetDescriptor
#define cuArrayGetMemoryRequirements wcuArrayGetMemoryRequirements
#define cuArrayGetPlane wcuArrayGetPlane
#define cuArrayGetSparseProperties wcuArrayGetSparseProperties

#define cuIpcCloseMemHandle wcuIpcCloseMemHandle
#define cuIpcGetMemHandle wcuIpcGetMemHandle
#define cuIpcOpenMemHandle wcuIpcOpenMemHandle
#define cuMemAlloc wcuMemAlloc
#define cuMemAllocHost wcuMemAllocHost
#define cuMemAllocManaged wcuMemAllocManaged
#define cuMemAllocPitch wcuMemAllocPitch
#define cuMemFree wcuMemFree
#define cuMemFreeHost wcuMemFreeHost
#define cuMemHostAlloc wcuMemHostAlloc
#define cuMemHostGetDevicePointer wcuMemHostGetDevicePointer
#define cuMemHostGetFlags wcuMemHostGetFlags
#define cuMemHostRegister wcuMemHostRegister
#define cuMemHostUnregister wcuMemHostUnregister
#define cuMemcpy wcuMemcpy
#define cuMemcpy2D wcuMemcpy2D
#define cuMemcpy2DUnaligned wcuMemcpy2DUnaligned
#define cuMemcpy2DAsync wcuMemcpy2DAsync
#define cuMemcpy3D wcuMemcpy3D
#define cuMemcpy3DAsync wcuMemcpy3DAsync
#define cuMemcpy3DPeer wcuMemcpy3DPeer
#define cuMemcpy3DPeerAsync wcuMemcpy3DPeerAsync
#define cuMemcpyAsync wcuMemcpyAsync
#define cuMemGetInfo wcuMemGetInfo
#define cuMemGetAddressRange wcuMemGetAddressRange
#define cuMemsetD16 wcuMemsetD16
#define cuMemsetD16Async wcuMemsetD16Async
#define cuMemsetD32 wcuMemsetD32
#define cuMemsetD32Async wcuMemsetD32Async
#define cuMemsetD8 wcuMemsetD8
#define cuMemsetD8Async wcuMemsetD8Async
#define cuMemsetD2D8 wcuMemsetD2D8
#define cuMemsetD2D8Async wcuMemsetD2D8Async
#define cuMemsetD2D16 wcuMemsetD2D16
#define cuMemsetD2D16Async wcuMemsetD2D16Async
#define cuMemsetD2D32 wcuMemsetD2D32
#define cuMemsetD2D32Async wcuMemsetD2D32Async
#define cuMemcpyAtoA wcuMemcpyAtoA
#define cuMemcpyAtoD wcuMemcpyAtoD
#define cuMemcpyDtoA wcuMemcpyDtoA
#define cuMemcpyHtoA wcuMemcpyHtoA
#define cuMemcpyAtoH wcuMemcpyAtoH
#define cuMemcpyAtoHAsync wcuMemcpyAtoHAsync
#define cuMemcpyHtoAAsync wcuMemcpyHtoAAsync
#define cuMemcpyDtoD wcuMemcpyDtoD
#define cuMemcpyDtoDAsync wcuMemcpyDtoDAsync
#define cuMemcpyDtoH wcuMemcpyDtoH
#define cuMemcpyDtoHAsync wcuMemcpyDtoHAsync
#define cuMemcpyHtoD wcuMemcpyHtoD
#define cuMemcpyHtoDAsync wcuMemcpyHtoDAsync
#define cuMemcpyPeer wcuMemcpyPeer
#define cuMemcpyPeerAsync wcuMemcpyPeerAsync

#define cuMipmappedArrayCreate wcuMipmappedArrayCreate
#define cuMipmappedArrayDestroy wcuMipmappedArrayDestroy
#define cuMipmappedArrayGetLevel wcuMipmappedArrayGetLevel
#define cuMipmappedArrayGetMemoryRequirements                                  \
  wcuMipmappedArrayGetMemoryRequirements
#define cuMipmappedArrayGetSparseProperties wcuMipmappedArrayGetSparseProperties

/*********** Virtual Memory Management Driver API **********/
#define cuMemAddressFree wcuMemAddressFree
#define cuMemAddressReserve wcuMemAddressReserve
#define cuMemCreate wcuMemCreate
#define cuMemExportToShareableHandle wcuMemExportToShareableHandle
#define cuMemGetAccess wcuMemGetAccess
#define cuMemGetAllocationGranularity wcuMemGetAllocationGranularity
#define cuMemGetAllocationPropertiesFromHandle                                 \
  wcuMemGetAllocationPropertiesFromHandle
#define cuMemImportFromShareableHandle wcuMemImportFromShareableHandle
#define cuMemMap wcuMemMap
#define cuMemMapArrayAsync wcuMemMapArrayAsync
#define cuMemRelease wcuMemRelease
#define cuMemRetainAllocationHandle wcuMemRetainAllocationHandle
#define cuMemSetAccess wcuMemSetAccess
#define cuMemUnmap wcuMemUnmap

/*********** Multicast Object Management Driver API **********/
#define cuMulticastCreate wcuMulticastCreate
#define cuMulticastAddDevice wcuMulticastAddDevice
#define cuMulticastBindMem wcuMulticastBindMem
#define cuMulticastBindAddr wcuMulticastBindAddr
#define cuMulticastUnbind wcuMulticastUnbind
#define cuMulticastGetGranularity wcuMulticastGetGranularity

/************Unified Addressing Driver API******************/
#define cuMemAdvise wcuMemAdvise
#define cuPointerGetAttribute wcuPointerGetAttribute
#define cuPointerGetAttributes wcuPointerGetAttributes
#define cuMemPrefetchAsync wcuMemPrefetchAsync
#define cuMemRangeGetAttribute wcuMemRangeGetAttribute
#define cuMemRangeGetAttributes wcuMemRangeGetAttributes
#define cuPointerSetAttribute wcuPointerSetAttribute

/*************Stream Ordered Memory Allocator Driver API********************/
#define cuMemAllocAsync wcuMemAllocAsync
#define cuMemAllocFromPoolAsync wcuMemAllocFromPoolAsync
#define cuMemFreeAsync wcuMemFreeAsync
#define cuMemPoolCreate wcuMemPoolCreate
#define cuMemPoolDestroy wcuMemPoolDestroy
#define cuMemPoolExportPointer wcuMemPoolExportPointer
#define cuMemPoolExportToShareableHandle wcuMemPoolExportToShareableHandle
#define cuMemPoolImportFromShareableHandle wcuMemPoolImportFromShareableHandle
#define cuMemPoolImportPointer wcuMemPoolImportPointer

#define cuMemPoolGetAccess wcuMemPoolGetAccess
#define cuMemPoolGetAttribute wcuMemPoolGetAttribute
#define cuMemPoolSetAccess wcuMemPoolSetAccess
#define cuMemPoolSetAttribute wcuMemPoolSetAttribute
#define cuMemPoolTrimTo wcuMemPoolTrimTo
/*Stream Memory Opertions type*/
#define CUstreamBatchMemOpParams_union mcStreamBatchMemOpParams_union
#define CUstreamBatchMemOpParams_v1 mcStreamBatchMemOpParams_v1
#define CUstreamBatchMemOpParams mcDrvStreamBatchMemOpParams

#define CUstreamMemOpWaitValueParams_st mcStreamMemOpWaitValueParams_st
#define CUstreamMemOpWriteValueParams_st mcStreamMemOpWriteValueParams_st
#define CUstreamMemOpFlushRemoteWritesParams_st                                \
  mcStreamMemOpFlushRemoteWritesParams_st
#define CUstreamMemOpMemoryBarrierParams_st mcStreamMemOpMemoryBarrierParams_st

/**************Stream Memory Opertions**************/
#define cuStreamBatchMemOp wcuStreamBatchMemOp
#define cuStreamWaitValue32 wcuStreamWaitValue32
#define cuStreamWaitValue64 wcuStreamWaitValue64
#define cuStreamWriteValue32 wcuStreamWriteValue32
#define cuStreamWriteValue64 wcuStreamWriteValue64

/**************Graphics Driver API******************/
#define cuGraphicsMapResources wcuGraphicsMapResources
#define cuGraphicsResourceGetMappedMipmappedArray                              \
  wcuGraphicsResourceGetMappedMipmappedArray
#define cuGraphicsResourceGetMappedPointer wcuGraphicsResourceGetMappedPointer
#define cuGraphicsResourceSetMapFlags wcuGraphicsResourceSetMapFlags
#define cuGraphicsSubResourceGetMappedArray wcuGraphicsSubResourceGetMappedArray
#define cuGraphicsUnmapResources wcuGraphicsUnmapResources
#define cuGraphicsUnregisterResource wcuGraphicsUnregisterResource

#define CUmipmappedArray_st mcDrvMipmappedArray_st
#define CUgraphicsResource_st mcDrvGraphicsResource_st
#define CUmipmappedArray mcDrvMipmappedArray
#define CUgraphicsResource mcDrvGraphicsResource

#define CUgraphicsRegisterFlags_enum mcDrvGraphicsRegisterFlags_enum
#define CU_GRAPHICS_REGISTER_FLAGS_NONE MC_GRAPHICS_REGISTER_FLAGS_NONE
#define CU_GRAPHICS_REGISTER_FLAGS_READ_ONLY                                   \
  MC_GRAPHICS_REGISTER_FLAGS_READ_ONLY
#define CU_GRAPHICS_REGISTER_FLAGS_WRITE_DISCARD                               \
  MC_GRAPHICS_REGISTER_FLAGS_WRITE_DISCARD
#define CU_GRAPHICS_REGISTER_FLAGS_SURFACE_LDST                                \
  MC_GRAPHICS_REGISTER_FLAGS_SURFACE_LDST
#define CU_GRAPHICS_REGISTER_FLAGS_TEXTURE_GATHER                              \
  MC_GRAPHICS_REGISTER_FLAGS_TEXTURE_GATHER
#define CUgraphicsRegisterFlags mcDrvGraphicsRegisterFlags

#define CUgraphicsMapResourceFlags_enum mcDrvGraphicsMapResourceFlags_enum
#define CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE MC_GRAPHICS_MAP_RESOURCE_FLAGS_NONE
#define CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY                               \
  MC_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY
#define CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD                           \
  MC_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD
#define CUgraphicsMapResourceFlags mcDrvGraphicsMapResourceFlags

#define CUarray_cubemap_face_enum mcDrvArrayCubemapFace_enum
#define CU_CUBEMAP_FACE_POSITIVE_X MC_CUBEMAP_FACE_POSITIVE_X
#define CU_CUBEMAP_FACE_NEGATIVE_X MC_CUBEMAP_FACE_NEGATIVE_X
#define CU_CUBEMAP_FACE_POSITIVE_Y MC_CUBEMAP_FACE_POSITIVE_Y
#define CU_CUBEMAP_FACE_NEGATIVE_Y MC_CUBEMAP_FACE_NEGATIVE_Y
#define CU_CUBEMAP_FACE_POSITIVE_Z MC_CUBEMAP_FACE_POSITIVE_Z
#define CU_CUBEMAP_FACE_NEGATIVE_Z MC_CUBEMAP_FACE_NEGATIVE_Z
#define CUarray_cubemap_face mcDrvArrayCubemapFace

/**************Texture reference Driver API******************/
#define cuTexRefSetArray wcuTexRefSetArray
#define cuTexRefSetMipmappedArray wcuTexRefSetMipmappedArray
#define cuTexRefSetAddress wcuTexRefSetAddress
#define cuTexRefSetAddress2D wcuTexRefSetAddress2D
#define cuTexRefSetFormat wcuTexRefSetFormat
#define cuTexRefSetAddressMode wcuTexRefSetAddressMode
#define cuTexRefSetFilterMode wcuTexRefSetFilterMode
#define cuTexRefSetMipmapFilterMode wcuTexRefSetMipmapFilterMode
#define cuTexRefSetMipmapLevelBias wcuTexRefSetMipmapLevelBias
#define cuTexRefSetMipmapLevelClamp wcuTexRefSetMipmapLevelClamp
#define cuTexRefSetMaxAnisotropy wcuTexRefSetMaxAnisotropy
#define cuTexRefSetBorderColor wcuTexRefSetBorderColor
#define cuTexRefSetFlags wcuTexRefSetFlags
#define cuTexRefGetAddress wcuTexRefGetAddress
#define cuTexRefGetArray wcuTexRefGetArray
#define cuTexRefGetMipmappedArray wcuTexRefGetMipmappedArray
#define cuTexRefGetAddressMode wcuTexRefGetAddressMode
#define cuTexRefGetFilterMode wcuTexRefGetFilterMode
#define cuTexRefGetMipmapFilterMode wcuTexRefGetMipmapFilterMode
#define cuTexRefGetMipmapLevelBias wcuTexRefGetMipmapLevelBias
#define cuTexRefGetMipmapLevelClamp wcuTexRefGetMipmapLevelClamp
#define cuTexRefGetMaxAnisotropy wcuTexRefGetMaxAnisotropy
#define cuTexRefGetBorderColor wcuTexRefGetBorderColor
#define cuTexRefGetFlags wcuTexRefGetFlags
#define cuTexRefGetFormat wcuTexRefGetFormat
#define cuTexRefCreate wcuTexRefCreate
#define cuTexRefDestroy wcuTexRefDestroy

#define CUarray_format_enum mcArrayFormat_enum
#define CUarray_format mcArray_Format
#define CU_AD_FORMAT_UNSIGNED_INT8 MC_AD_FORMAT_UNSIGNED_INT8
#define CU_AD_FORMAT_UNSIGNED_INT16 MC_AD_FORMAT_UNSIGNED_INT16
#define CU_AD_FORMAT_UNSIGNED_INT32 MC_AD_FORMAT_UNSIGNED_INT32
#define CU_AD_FORMAT_SIGNED_INT8 MC_AD_FORMAT_SIGNED_INT8
#define CU_AD_FORMAT_SIGNED_INT16 MC_AD_FORMAT_SIGNED_INT16
#define CU_AD_FORMAT_SIGNED_INT32 MC_AD_FORMAT_SIGNED_INT32
#define CU_AD_FORMAT_HALF MC_AD_FORMAT_HALF
#define CU_AD_FORMAT_FLOAT MC_AD_FORMAT_FLOAT
#define CU_AD_FORMAT_NV12 MC_AD_FORMAT_NV12
#define CU_AD_FORMAT_UNORM_INT8X1 MC_AD_FORMAT_UNORM_INT8X1
#define CU_AD_FORMAT_UNORM_INT8X2 MC_AD_FORMAT_UNORM_INT8X2
#define CU_AD_FORMAT_UNORM_INT8X4 MC_AD_FORMAT_UNORM_INT8X4
#define CU_AD_FORMAT_UNORM_INT16X1 MC_AD_FORMAT_UNORM_INT16X1
#define CU_AD_FORMAT_UNORM_INT16X2 MC_AD_FORMAT_UNORM_INT16X2
#define CU_AD_FORMAT_UNORM_INT16X4 MC_AD_FORMAT_UNORM_INT16X4
#define CU_AD_FORMAT_SNORM_INT8X1 MC_AD_FORMAT_SNORM_INT8X1
#define CU_AD_FORMAT_SNORM_INT8X2 MC_AD_FORMAT_SNORM_INT8X2
#define CU_AD_FORMAT_SNORM_INT8X4 MC_AD_FORMAT_SNORM_INT8X4
#define CU_AD_FORMAT_SNORM_INT16X1 MC_AD_FORMAT_SNORM_INT16X1
#define CU_AD_FORMAT_SNORM_INT16X2 MC_AD_FORMAT_SNORM_INT16X2
#define CU_AD_FORMAT_SNORM_INT16X4 MC_AD_FORMAT_SNORM_INT16X4
#define CU_AD_FORMAT_BC1_UNORM MC_AD_FORMAT_BC1_UNORM
#define CU_AD_FORMAT_BC1_UNORM_SRGB MC_AD_FORMAT_BC1_UNORM_SRGB
#define CU_AD_FORMAT_BC2_UNORM MC_AD_FORMAT_BC2_UNORM
#define CU_AD_FORMAT_BC2_UNORM_SRGB MC_AD_FORMAT_BC2_UNORM_SRGB
#define CU_AD_FORMAT_BC3_UNORM MC_AD_FORMAT_BC3_UNORM
#define CU_AD_FORMAT_BC3_UNORM_SRGB MC_AD_FORMAT_BC3_UNORM_SRGB
#define CU_AD_FORMAT_BC4_UNORM MC_AD_FORMAT_BC4_UNORM
#define CU_AD_FORMAT_BC4_SNORM MC_AD_FORMAT_BC4_SNORM
#define CU_AD_FORMAT_BC5_UNORM MC_AD_FORMAT_BC5_UNORM
#define CU_AD_FORMAT_BC5_SNORM MC_AD_FORMAT_BC5_SNORM
#define CU_AD_FORMAT_BC6H_UF16 MC_AD_FORMAT_BC6H_UF16
#define CU_AD_FORMAT_BC6H_SF16 MC_AD_FORMAT_BC6H_SF16
#define CU_AD_FORMAT_BC7_UNORM MC_AD_FORMAT_BC7_UNORM
#define CU_AD_FORMAT_BC7_UNORM_SRGB MC_AD_FORMAT_BC7_UNORM_SRGB

#define CUaddress_mode_enum mcDrvAddressMode_enum
#define CU_TR_ADDRESS_MODE_WRAP MC_TR_ADDRESS_MODE_WRAP
#define CU_TR_ADDRESS_MODE_CLAMP MC_TR_ADDRESS_MODE_CLAMP
#define CU_TR_ADDRESS_MODE_MIRROR MC_TR_ADDRESS_MODE_MIRROR
#define CU_TR_ADDRESS_MODE_BORDER MC_TR_ADDRESS_MODE_BORDER
#define CUaddress_mode mcDrvAddress_mode

#define CUfilter_mode_enum mcDrvFilterMode_enum
#define CU_TR_FILTER_MODE_POINT MC_TR_FILTER_MODE_POINT
#define CU_TR_FILTER_MODE_LINEAR MC_TR_FILTER_MODE_LINEAR
#define CUfilter_mode mcDrvFilter_mode

/**************Texture objcet driver API******************/
#define cuTexObjectCreate wcuTexObjectCreate
#define cuTexObjectDestroy wcuTexObjectDestroy
#define cuTexObjectGetResourceDesc wcuTexObjectGetResourceDesc
#define cuTexObjectGetTextureDesc wcuTexObjectGetTextureDesc
#define cuTexObjectGetResourceViewDesc wcuTexObjectGetResourceViewDesc

#define CUtexObject_v1 mcDrvTexObject
#define CUtexObject mcDrvTexObject

#define CUextMemory_st mcDrvExtMemory_st
#define CUexternalMemory mcDrvExternalMemory

#define CUresourcetype_enum mcResourceType
#define CU_RESOURCE_TYPE_ARRAY MC_RESOURCE_TYPE_ARRAY
#define CU_RESOURCE_TYPE_MIPMAPPED_ARRAY MC_RESOURCE_TYPE_MIPMAPPED_ARRAY
#define CU_RESOURCE_TYPE_LINEAR MC_RESOURCE_TYPE_LINEAR
#define CU_RESOURCE_TYPE_PITCH2D MC_RESOURCE_TYPE_PITCH2D
#define CUresourcetype mcDrvResourceType_t

#define CUresourceViewFormat_enum mcDrvResourceViewFormat_enum
#define CU_RES_VIEW_FORMAT_NONE MC_RES_VIEW_FORMAT_NONE
#define CU_RES_VIEW_FORMAT_UINT_1X8 MC_RES_VIEW_FORMAT_UINT_1X8
#define CU_RES_VIEW_FORMAT_UINT_2X8 MC_RES_VIEW_FORMAT_UINT_2X8
#define CU_RES_VIEW_FORMAT_UINT_4X8 MC_RES_VIEW_FORMAT_UINT_4X8
#define CU_RES_VIEW_FORMAT_SINT_1X8 MC_RES_VIEW_FORMAT_SINT_1X8
#define CU_RES_VIEW_FORMAT_SINT_2X8 MC_RES_VIEW_FORMAT_SINT_2X8
#define CU_RES_VIEW_FORMAT_SINT_4X8 MC_RES_VIEW_FORMAT_SINT_4X8
#define CU_RES_VIEW_FORMAT_UINT_1X16 MC_RES_VIEW_FORMAT_UINT_1X16
#define CU_RES_VIEW_FORMAT_UINT_2X16 MC_RES_VIEW_FORMAT_UINT_2X16
#define CU_RES_VIEW_FORMAT_UINT_4X16 MC_RES_VIEW_FORMAT_UINT_4X16
#define CU_RES_VIEW_FORMAT_SINT_1X16 MC_RES_VIEW_FORMAT_SINT_1X16
#define CU_RES_VIEW_FORMAT_SINT_2X16 MC_RES_VIEW_FORMAT_SINT_2X16
#define CU_RES_VIEW_FORMAT_SINT_4X16 MC_RES_VIEW_FORMAT_SINT_4X16
#define CU_RES_VIEW_FORMAT_UINT_1X32 MC_RES_VIEW_FORMAT_UINT_1X32
#define CU_RES_VIEW_FORMAT_UINT_2X32 MC_RES_VIEW_FORMAT_UINT_2X32
#define CU_RES_VIEW_FORMAT_UINT_4X32 MC_RES_VIEW_FORMAT_UINT_4X32
#define CU_RES_VIEW_FORMAT_SINT_1X32 MC_RES_VIEW_FORMAT_SINT_1X32
#define CU_RES_VIEW_FORMAT_SINT_2X32 MC_RES_VIEW_FORMAT_SINT_2X32
#define CU_RES_VIEW_FORMAT_SINT_4X32 MC_RES_VIEW_FORMAT_SINT_4X32
#define CU_RES_VIEW_FORMAT_FLOAT_1X16 MC_RES_VIEW_FORMAT_FLOAT_1X16
#define CU_RES_VIEW_FORMAT_FLOAT_2X16 MC_RES_VIEW_FORMAT_FLOAT_2X16
#define CU_RES_VIEW_FORMAT_FLOAT_4X16 MC_RES_VIEW_FORMAT_FLOAT_4X16
#define CU_RES_VIEW_FORMAT_FLOAT_1X32 MC_RES_VIEW_FORMAT_FLOAT_1X32
#define CU_RES_VIEW_FORMAT_FLOAT_2X32 MC_RES_VIEW_FORMAT_FLOAT_2X32
#define CU_RES_VIEW_FORMAT_FLOAT_4X32 MC_RES_VIEW_FORMAT_FLOAT_4X32
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC1 MC_RES_VIEW_FORMAT_UNSIGNED_BC1
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC2 MC_RES_VIEW_FORMAT_UNSIGNED_BC2
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC3 MC_RES_VIEW_FORMAT_UNSIGNED_BC3
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC4 MC_RES_VIEW_FORMAT_UNSIGNED_BC4
#define CU_RES_VIEW_FORMAT_SIGNED_BC4 MC_RES_VIEW_FORMAT_SIGNED_BC4
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC5 MC_RES_VIEW_FORMAT_UNSIGNED_BC5
#define CU_RES_VIEW_FORMAT_SIGNED_BC5 MC_RES_VIEW_FORMAT_SIGNED_BC5
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC6H MC_RES_VIEW_FORMAT_UNSIGNED_BC6H
#define CU_RES_VIEW_FORMAT_SIGNED_BC6H MC_RES_VIEW_FORMAT_SIGNED_BC6H
#define CU_RES_VIEW_FORMAT_UNSIGNED_BC7 MC_RES_VIEW_FORMAT_UNSIGNED_BC7
#define CUresourceViewFormat mcDrvResourceViewFormat

/* struct */
#define CUDA_RESOURCE_VIEW_DESC_st mcDrvResourceViewDesc_st
#define CUDA_RESOURCE_VIEW_DESC_v1 mcDrvResourceViewDesc
#define CUDA_RESOURCE_VIEW_DESC mcDrvResourceViewDesc

#define CUDA_POINTER_ATTRIBUTE_P2P_TOKENS_st mcDrvPointerAttributeP2PTokens_st
#define CUDA_POINTER_ATTRIBUTE_P2P_TOKENS_v1 mcDrvPointerAttributeP2PTokens
#define CUDA_POINTER_ATTRIBUTE_P2P_TOKENS mcDrvPointerAttributeP2PTokens

#define CUDA_POINTER_ATTRIBUTE_ACCESS_FLAGS_enum                               \
  mcDrvPointerAttributeAccessFlags_enum
#define CU_POINTER_ATTRIBUTE_ACCESS_FLAG_NONE                                  \
  MC_POINTER_ATTRIBUTE_ACCESS_FLAG_NONE
#define CU_POINTER_ATTRIBUTE_ACCESS_FLAG_READ                                  \
  MC_POINTER_ATTRIBUTE_ACCESS_FLAG_READ
#define CU_POINTER_ATTRIBUTE_ACCESS_FLAG_READWRITE                             \
  MC_POINTER_ATTRIBUTE_ACCESS_FLAG_READWRITE
#define CUDA_POINTER_ATTRIBUTE_ACCESS_FLAGS mcDrvPointerAttributeAccessFlags

#define CUDA_LAUNCH_PARAMS_st mcDrvLaunchParams_st
#define CUDA_LAUNCH_PARAMS_v1 mcDrvLaunchParams_v1
#define CUDA_LAUNCH_PARAMS mcDrvLaunchParams

#define CUexternalMemoryHandleType_enum mcDrvExternalMemoryHandleType_enum
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD                               \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32                            \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT                        \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP                              \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_HEAP
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE                          \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_D3D12_RESOURCE
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE                          \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE_KMT                      \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_D3D11_RESOURCE_KMT
#define CU_EXTERNAL_MEMORY_HANDLE_TYPE_NVSCIBUF                                \
  MC_EXTERNAL_MEMORY_HANDLE_TYPE_NVSCIBUF
#define CUexternalMemoryHandleType mcDrvExternalMemoryHandleType

/**************Surface  Driver API******************/
/* Surface Reference Management [Deprecated] */
#define cuSurfRefSetArray wcuSurfRefSetArray
#define cuSurfRefGetArray wcuSurfRefGetArray

#define CUsurfref_st mcDrvSurfref_st
#define CUsurfref mcDrvSurfref

/* Surface Object Management */
#define CUsurfObject_v1 mcDrvSurfObject
#define CUsurfObject mcDrvSurfObject

#define cuSurfObjectCreate wcuSurfObjectCreate
#define cuSurfObjectDestroy wcuSurfObjectDestroy
#define cuSurfObjectGetResourceDesc wcuSurfObjectGetResourceDesc

/**************Occupancy  Driver API******************/
#define cuOccupancyAvailableDynamicSMemPerBlock                                \
  wcuOccupancyAvailableDynamicSMemPerBlock
#define cuOccupancyMaxPotentialBlockSize wcuOccupancyMaxPotentialBlockSize
#define cuOccupancyMaxPotentialBlockSizeWithFlags                              \
  wcuOccupancyMaxPotentialBlockSizeWithFlags
#define cuOccupancyMaxActiveBlocksPerMultiprocessor                            \
  wcuOccupancyMaxActiveBlocksPerMultiprocessor
#define cuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags                   \
  wcuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags

#define CUoccupancy_flags_enum mcDrvOccupancyFlagsEnum
#define CU_OCCUPANCY_DEFAULT MC_OCCUPANCY_DEFAULT
#define CU_OCCUPANCY_DISABLE_CACHING_OVERRIDE                                  \
  MC_OCCUPANCY_DISABLE_CACHING_OVERRIDE
#define CUoccupancy_flags mcDrvOccupancyFlags

#define CUoccupancyB2DSize mcOccupancyB2DSize

#define CUstreamUpdateCaptureDependencies_flags_enum                           \
  mcStreamUpdateCaptureDependenciesFlags_enum
#define CU_STREAM_ADD_CAPTURE_DEPENDENCIES mcStreamAddCaptureDependencies
#define CU_STREAM_SET_CAPTURE_DEPENDENCIES mcStreamSetCaptureDependencies
#define CUstreamUpdateCaptureDependencies_flags                                \
  mcStreamUpdateCaptureDependenciesFlags

/************Driver Entry Point Access******************/
#define CUdriverProcAddress_flags_enum mcDrvDriverProcAddressFlags_enum
#define CUdriverProcAddress_flags mcDrvDriverProcAddressFlags
#define CU_GET_PROC_ADDRESS_DEFAULT MC_GET_PROC_ADDRESS_DEFAULT
#define CU_GET_PROC_ADDRESS_LEGACY_STREAM MC_GET_PROC_ADDRESS_LEGACY_STREAM
#define CU_GET_PROC_ADDRESS_PER_THREAD_DEFAULT_STREAM                          \
  MC_GET_PROC_ADDRESS_PER_THREAD_DEFAULT_STREAM

#define cuGetProcAddress wcuGetProcAddress
#define cuGetExportTable wcuGetExportTable

/************Driver Version Management******************/
#define cuDriverGetVersion wcuDriverGetVersion

/*************graph Driver API********************/
#define CUgraphNode mcDrvGraphNode
#define CUgraph mcDrvGraph
#define CUgraphExec mcDrvGraphExec

#define CUDA_HOST_NODE_PARAMS_st mcDrvHostNodeParams_st
#define CUDA_HOST_NODE_PARAMS_v1 mcDrvHostNodeParams
#define CUDA_HOST_NODE_PARAMS mcDrvHostNodeParams

#define CUDA_KERNEL_NODE_PARAMS_st mcDrvKernelNodeParams_st
#define CUDA_KERNEL_NODE_PARAMS_v1 mcDrvKernelNodeParams
#define CUDA_KERNEL_NODE_PARAMS mcDrvKernelNodeParams

#define CUDA_MEM_ALLOC_NODE_PARAMS_st mcDrvMemAllocNodeParams_st
#define CUDA_MEM_ALLOC_NODE_PARAMS mcDrvMemAllocNodeParams

#define CUDA_BATCH_MEM_OP_NODE_PARAMS_st mcBatchMemOpNodeParams_st
#define CUDA_BATCH_MEM_OP_NODE_PARAMS_v1 mcDrvBatchMemOpNodeParams
#define CUDA_BATCH_MEM_OP_NODE_PARAMS mcDrvBatchMemOpNodeParams

#define CUDA_MEMCPY3D_st mcDrvMemcpy3D_st
#define CUDA_MEMCPY3D_v2 mcDrvMemcpy3D
#define CUDA_MEMCPY3D mcDrvMemcpy3D
#define CUDA_MEMCPY3D_v1_st mcDrvMemcpy3Dv1_st
#define CUDA_MEMCPY3D_v1 mcDrvMemcpy3Dv1

#define CUDA_MEMCPY3D_PEER_st mcDrvMemcpy3DPeer_st
#define CUDA_MEMCPY3D_PEER_v1 mcDrvMemcpy3DPeer
#define CUDA_MEMCPY3D_PEER mcDrvMemcpy3DPeer

#define CUDA_ARRAY_DESCRIPTOR_st mcDrvArrayDescriptor_st
#define CUDA_ARRAY_DESCRIPTOR_v2 mcDrvArrayDescriptor
#define CUDA_ARRAY_DESCRIPTOR mcDrvArrayDescriptor
#define CUDA_ARRAY_DESCRIPTOR_v1_st mcDrvArrayDescriptorv1_st
#define CUDA_ARRAY_DESCRIPTOR_v1 mcDrvArrayDescriptorv1

#define CUDA_ARRAY3D_DESCRIPTOR_st mcDrvArray3DDescriptor_st
#define CUDA_ARRAY3D_DESCRIPTOR_v2 mcDrvArray3DDescriptor
#define CUDA_ARRAY3D_DESCRIPTOR mcDrvArray3DDescriptor
#define CUDA_ARRAY3D_DESCRIPTOR_v1_st mcDrvArray3DDescriptorv1_st
#define CUDA_ARRAY3D_DESCRIPTOR_v1 mcDrvArray3DDescriptorv1

#define CUDA_ARRAY_SPARSE_PROPERTIES_st mcDrvArraySparseProperties_st
#define CUDA_ARRAY_SPARSE_PROPERTIES_v1 mcDrvArraySparseProperties
#define CUDA_ARRAY_SPARSE_PROPERTIES mcDrvArraySparseProperties

#define CUDA_ARRAY_MEMORY_REQUIREMENTS_st mcDrvArrayMemoryRequirements_st
#define CUDA_ARRAY_MEMORY_REQUIREMENTS_v1 mcDrvArrayMemoryRequirements
#define CUDA_ARRAY_MEMORY_REQUIREMENTS mcDrvArrayMemoryRequirements

#define CUDA_RESOURCE_DESC_st mcDrvResourceDesc_st
#define CUDA_RESOURCE_DESC_v1 mcDrvResourceDesc
#define CUDA_RESOURCE_DESC mcDrvResourceDesc

#define CUDA_TEXTURE_DESC_st mcDrvTextureDesc_st
#define CUDA_TEXTURE_DESC_v1 mcDrvTextureDesc
#define CUDA_TEXTURE_DESC mcDrvTextureDesc

#define CUDA_MEMSET_NODE_PARAMS_st mcDrvMemsetNodeParams_st
#define CUDA_MEMSET_NODE_PARAMS_v1 mcDrvMemsetNodeParams
#define CUDA_MEMSET_NODE_PARAMS mcDrvMemsetNodeParams

#define CUarray MCarray

#define CUgraphMem_attribute_enum mcGraphMemAttributeType_enum
#define CU_GRAPH_MEM_ATTR_USED_MEM_CURRENT mcGraphMemAttrUsedMemCurrent
#define CU_GRAPH_MEM_ATTR_USED_MEM_HIGH mcGraphMemAttrUsedMemHigh
#define CU_GRAPH_MEM_ATTR_RESERVED_MEM_CURRENT mcGraphMemAttrReservedMemCurrent
#define CU_GRAPH_MEM_ATTR_RESERVED_MEM_HIGH mcGraphMemAttrReservedMemHigh
#define CUgraphMem_attribute mcGraphMemAttributeType

#define CUDA_EXT_SEM_SIGNAL_NODE_PARAMS_st mcDrvExtSemSignalNodeParams_st
#define CUDA_EXT_SEM_SIGNAL_NODE_PARAMS_v1 mcDrvExtSemSignalNodeParams
#define CUDA_EXT_SEM_SIGNAL_NODE_PARAMS mcDrvExtSemSignalNodeParams

#define CUDA_EXTERNAL_MEMORY_HANDLE_DESC_st mcDrvExternalMemoryHandleDesc_st
#define CUDA_EXTERNAL_MEMORY_HANDLE_DESC_v1 mcDrvExternalMemoryHandleDesc
#define CUDA_EXTERNAL_MEMORY_HANDLE_DESC mcDrvExternalMemoryHandleDesc

#define CUDA_EXTERNAL_MEMORY_BUFFER_DESC_st mcDrvExternalMemoryBufferDesc_st
#define CUDA_EXTERNAL_MEMORY_BUFFER_DESC_v1 mcDrvExternalMemoryBufferDesc
#define CUDA_EXTERNAL_MEMORY_BUFFER_DESC mcDrvExternalMemoryBufferDesc

#define CUDA_EXTERNAL_MEMORY_MIPMAPPED_ARRAY_DESC_st                           \
  mcDrvExternalMemoryMipmappedArrayDesc_st
#define CUDA_EXTERNAL_MEMORY_MIPMAPPED_ARRAY_DESC_v1                           \
  mcDrvExternalMemoryMipmappedArrayDesc
#define CUDA_EXTERNAL_MEMORY_MIPMAPPED_ARRAY_DESC                              \
  mcDrvExternalMemoryMipmappedArrayDesc

#define CUDA_EXTERNAL_SEMAPHORE_HANDLE_DESC_st                                 \
  mcDrvExternalSemaphorHandleDesc_st
#define CUDA_EXTERNAL_SEMAPHORE_HANDLE_DESC_v1 mcDrvExternalSemaphorHandleDesc
#define CUDA_EXTERNAL_SEMAPHORE_HANDLE_DESC mcDrvExternalSemaphorHandleDesc

#define CUDA_EXTERNAL_SEMAPHORE_SIGNAL_PARAMS_st                               \
  mcDrvExternalSemaphoreSignalParams_st
#define CUDA_EXTERNAL_SEMAPHORE_SIGNAL_PARAMS_v1                               \
  mcDrvExternalSemaphoreSignalParams
#define CUDA_EXTERNAL_SEMAPHORE_SIGNAL_PARAMS mcDrvExternalSemaphoreSignalParams

#define CUDA_EXT_SEM_WAIT_NODE_PARAMS_st mcDrvExtSemWaitNodeParams_st
#define CUDA_EXT_SEM_WAIT_NODE_PARAMS_v1 mcDrvExtSemWaitNodeParams
#define CUDA_EXT_SEM_WAIT_NODE_PARAMS mcDrvExtSemWaitNodeParams

#define CUDA_EXTERNAL_SEMAPHORE_WAIT_PARAMS_st                                 \
  mcDrvExternalSemaphoreWaitParams_st
#define CUDA_EXTERNAL_SEMAPHORE_WAIT_PARAMS_v1 mcDrvExternalSemaphoreWaitParams
#define CUDA_EXTERNAL_SEMAPHORE_WAIT_PARAMS mcDrvExternalSemaphoreWaitParams

#define CUgraphNodeType_enum mcDrvGraphNodeType_enum
#define CUgraphNodeType mcDrvGraphNodeType
#define CU_GRAPH_NODE_TYPE_KERNEL MC_GRAPH_NODE_TYPE_KERNEL
#define CU_GRAPH_NODE_TYPE_MEMCPY MC_GRAPH_NODE_TYPE_MEMCPY
#define CU_GRAPH_NODE_TYPE_MEMSET MC_GRAPH_NODE_TYPE_MEMSET
#define CU_GRAPH_NODE_TYPE_HOST MC_GRAPH_NODE_TYPE_HOST
#define CU_GRAPH_NODE_TYPE_GRAPH MC_GRAPH_NODE_TYPE_GRAPH
#define CU_GRAPH_NODE_TYPE_EMPTY MC_GRAPH_NODE_TYPE_EMPTY
#define CU_GRAPH_NODE_TYPE_WAIT_EVENT MC_GRAPH_NODE_TYPE_WAIT_EVENT
#define CU_GRAPH_NODE_TYPE_EVENT_RECORD MC_GRAPH_NODE_TYPE_EVENT_RECORD
#define CU_GRAPH_NODE_TYPE_EXT_SEMAS_SIGNAL MC_GRAPH_NODE_TYPE_EXT_SEMAS_SIGNAL
#define CU_GRAPH_NODE_TYPE_EXT_SEMAS_WAIT MC_GRAPH_NODE_TYPE_EXT_SEMAS_WAIT
#define CU_GRAPH_NODE_TYPE_MEM_ALLOC MC_GRAPH_NODE_TYPE_MEM_ALLOC
#define CU_GRAPH_NODE_TYPE_MEM_FREE MC_GRAPH_NODE_TYPE_MEM_FREE
#define CU_GRAPH_NODE_TYPE_BATCH_MEM_OP MC_GRAPH_NODE_TYPE_BATCH_MEM_OP

#define CUgraphExecUpdateResult_enum mcGraphExecUpdateResult_enum
#define CU_GRAPH_EXEC_UPDATE_SUCCESS mcGraphExecUpdateSuccess
#define CU_GRAPH_EXEC_UPDATE_ERROR mcGraphExecUpdateError
#define CU_GRAPH_EXEC_UPDATE_ERROR_TOPOLOGY_CHANGED                            \
  mcGraphExecUpdateErrorTopologyChanged
#define CU_GRAPH_EXEC_UPDATE_ERROR_NODE_TYPE_CHANGED                           \
  mcGraphExecUpdateErrorNodeTypeChanged
#define CU_GRAPH_EXEC_UPDATE_ERROR_FUNCTION_CHANGED                            \
  mcGraphExecUpdateErrorFunctionChanged
#define CU_GRAPH_EXEC_UPDATE_ERROR_PARAMETERS_CHANGED                          \
  mcGraphExecUpdateErrorParametersChanged
#define CU_GRAPH_EXEC_UPDATE_ERROR_NOT_SUPPORTED                               \
  mcGraphExecUpdateErrorNotSupported
#define CU_GRAPH_EXEC_UPDATE_ERROR_UNSUPPORTED_FUNCTION_CHANGE                 \
  mcGraphExecUpdateErrorUnsupportedFunctionChange
#define CU_GRAPH_EXEC_UPDATE_ERROR_ATTRIBUTES_CHANGED                          \
  mcGraphExecUpdateErrorAttributesChanged
#define CUgraphExecUpdateResult mcGraphExecUpdateResult

#define CUkernelNodeAttrID_enum mcDrvkernelNodeAttrID_enum
#define CUkernelNodeAttrID mcDrvkernelNodeAttrID
#define CU_KERNEL_NODE_ATTRIBUTE_ACCESS_POLICY_WINDOW                          \
  MC_KERNEL_NODE_ATTRIBUTE_ACCESS_POLICY_WINDOW
#define CU_KERNEL_NODE_ATTRIBUTE_COOPERATIVE                                   \
  MC_KERNEL_NODE_ATTRIBUTE_COOPERATIVE
#define CU_KERNEL_NODE_ATTRIBUTE_PRIORITY MC_KERNEL_NODE_ATTRIBUTE_PRIORITY

#define CUkernelNodeAttrValue_union mcDrvKernelNodeAttrValue_st
#define CUkernelNodeAttrValue_v1 mcDrvKernelNodeAttrValue
#define CUkernelNodeAttrValue mcDrvKernelNodeAttrValue

#define CUaccessPolicyWindow_st mcAccessPolicywindow
#define CUaccessPolicyWindow_v1 mcDrvAccessPolicyWindow
#define CUaccessPolicyWindow mcDrvAccessPolicyWindow

#define CUgraphExecUpdateResultInfo_st mcDrvGraphExecUpdateResultInfo_st
#define CUgraphExecUpdateResultInfo mcDrvGraphExecUpdateResultInfo
#define CUgraphExecUpdateResultInfo_v1 mcDrvGraphExecUpdateResultInfo

#define CUDA_GRAPH_INSTANTIATE_PARAMS_st mcDrvGraphInstantiateParams_st
#define CUDA_GRAPH_INSTANTIATE_PARAMS mcDrvGraphInstantiateParams
#define CUDA_GRAPH_INSTANTIATE_PARAMS_v1 mcDrvGraphInstantiateParams

#define CUuserObject mcDrvUserObject

#define cuDeviceGetGraphMemAttribute wcuDeviceGetGraphMemAttribute
#define cuDeviceGraphMemTrim wcuDeviceGraphMemTrim
#define cuDeviceSetGraphMemAttribute wcuDeviceSetGraphMemAttribute
#define cuGraphAddChildGraphNode wcuGraphAddChildGraphNode
#define cuGraphAddDependencies wcuGraphAddDependencies
#define cuGraphAddEmptyNode wcuGraphAddEmptyNode
#define cuGraphAddEventRecordNode wcuGraphAddEventRecordNode
#define cuGraphAddEventWaitNode wcuGraphAddEventWaitNode
#define cuGraphAddExternalSemaphoresSignalNode                                 \
  wcuGraphAddExternalSemaphoresSignalNode
#define cuGraphAddExternalSemaphoresWaitNode                                   \
  wcuGraphAddExternalSemaphoresWaitNode
#define cuGraphAddHostNode wcuGraphAddHostNode
#define cuGraphAddKernelNode wcuGraphAddKernelNode
#define cuGraphAddMemAllocNode wcuGraphAddMemAllocNode
#define cuGraphAddMemFreeNode wcuGraphAddMemFreeNode
#define cuGraphAddMemcpyNode wcuGraphAddMemcpyNode
#define cuGraphAddMemsetNode wcuGraphAddMemsetNode
#define cuGraphChildGraphNodeGetGraph wcuGraphChildGraphNodeGetGraph
#define cuGraphClone wcuGraphClone
#define cuGraphCreate wcuGraphCreate
#define cuGraphDebugDotPrint wcuGraphDebugDotPrint
#define cuGraphDestroy wcuGraphDestroy
#define cuGraphDestroyNode wcuGraphDestroyNode
#define cuGraphEventRecordNodeGetEvent wcuGraphEventRecordNodeGetEvent
#define cuGraphEventRecordNodeSetEvent wcuGraphEventRecordNodeSetEvent
#define cuGraphEventWaitNodeGetEvent wcuGraphEventWaitNodeGetEvent
#define cuGraphEventWaitNodeSetEvent wcuGraphEventWaitNodeSetEvent
#define cuGraphExecChildGraphNodeSetParams wcuGraphExecChildGraphNodeSetParams
#define cuGraphExecDestroy wcuGraphExecDestroy
#define cuGraphExecEventRecordNodeSetEvent wcuGraphExecEventRecordNodeSetEvent
#define cuGraphExecEventWaitNodeSetEvent wcuGraphExecEventWaitNodeSetEvent
#define cuGraphExecExternalSemaphoresSignalNodeSetParams                       \
  wcuGraphExecExternalSemaphoresSignalNodeSetParams
#define cuGraphExecExternalSemaphoresWaitNodeSetParams                         \
  wcuGraphExecExternalSemaphoresWaitNodeSetParams
#define cuGraphExecHostNodeSetParams wcuGraphExecHostNodeSetParams
#define cuGraphExecKernelNodeSetParams wcuGraphExecKernelNodeSetParams
#define cuGraphExecMemcpyNodeSetParams wcuGraphExecMemcpyNodeSetParams
#define cuGraphExecMemsetNodeSetParams wcuGraphExecMemsetNodeSetParams
#define cuGraphExecUpdate wcuGraphExewcupdate
#define cuGraphExternalSemaphoresSignalNodeGetParams                           \
  wcuGraphExternalSemaphoresSignalNodeGetParams
#define cuGraphExternalSemaphoresSignalNodeSetParams                           \
  wcuGraphExternalSemaphoresSignalNodeSetParams
#define cuGraphExternalSemaphoresWaitNodeGetParams                             \
  wcuGraphExternalSemaphoresWaitNodeGetParams
#define cuGraphExternalSemaphoresWaitNodeSetParams                             \
  wcuGraphExternalSemaphoresWaitNodeSetParams
#define cuGraphGetEdges wcuGraphGetEdges
#define cuGraphGetNodes wcuGraphGetNodes
#define cuGraphGetRootNodes wcuGraphGetRootNodes
#define cuGraphHostNodeGetParams wcuGraphHostNodeGetParams
#define cuGraphHostNodeSetParams wcuGraphHostNodeSetParams
#define cuGraphInstantiate wcuGraphInstantiate
#define cuGraphInstantiateWithFlags wcuGraphInstantiateWithFlags
#define cuGraphInstantiateWithParams wcuGraphInstantiateWithParams
#define cuGraphExecGetFlags wcuGraphExecGetFlags
#define cuGraphKernelNodeCopyAttributes wcuGraphKernelNodeCopyAttributes
#define cuGraphKernelNodeGetAttribute wcuGraphKernelNodeGetAttribute
#define cuGraphKernelNodeGetParams wcuGraphKernelNodeGetParams
#define cuGraphKernelNodeSetAttribute wcuGraphKernelNodeSetAttribute
#define cuGraphKernelNodeSetParams wcuGraphKernelNodeSetParams
#define cuGraphLaunch wcuGraphLaunch
#define cuGraphMemAllocNodeGetParams wcuGraphMemAllocNodeGetParams
#define cuGraphMemFreeNodeGetParams wcuGraphMemFreeNodeGetParams
#define cuGraphMemcpyNodeGetParams wcuGraphMemcpyNodeGetParams
#define cuGraphMemcpyNodeSetParams wcuGraphMemcpyNodeSetParams
#define cuGraphMemsetNodeGetParams wcuGraphMemsetNodeGetParams
#define cuGraphMemsetNodeSetParams wcuGraphMemsetNodeSetParams
#define cuGraphNodeFindInClone wcuGraphNodeFindInClone
#define cuGraphNodeGetDependencies wcuGraphNodeGetDependencies
#define cuGraphNodeGetDependentNodes wcuGraphNodeGetDependentNodes
#define cuGraphNodeGetEnabled wcuGraphNodeGetEnabled
#define cuGraphNodeGetType wcuGraphNodeGetType
#define cuGraphNodeSetEnabled wcuGraphNodeSetEnabled
#define cuGraphReleaseUserObject wcuGraphReleaseUserObject
#define cuGraphRemoveDependencies wcuGraphRemoveDependencies
#define cuGraphRetainUserObject wcuGraphRetainUserObject
#define cuGraphUpload wcuGraphUpload
#define cuUserObjectCreate wcuUserObjectCreate
#define cuUserObjectRelease wcuUserObjectRelease
#define cuUserObjectRetain wcuUserObjectRetain
#define cuGraphAddBatchMemOpNode wcuGraphAddBatchMemOpNode
#define cuGraphBatchMemOpNodeGetParams wcuGraphBatchMemOpNodeGetParams
#define cuGraphBatchMemOpNodeSetParams wcuGraphBatchMemOpNodeSetParams
#define cuGraphExecBatchMemOpNodeSetParams wcuGraphExecBatchMemOpNodeSetParams

/***********cudalibxt**************/

#define _CUDA_LIB_XT_H_ _MACA_LIB_XT_H_
#define MAX_CUDA_DESCRIPTOR_GPUS MAX_MACA_DESCRIPTOR_GPUS
#define CUDA_XT_DESCRIPTOR_VERSION MACA_XT_DESCRIPTOR_VERSION
#define cudaXtCopyType_t mcXtCopyType_t
#define cudaLibXtCopyType mcLibXtCopyType
#define cudaXtDesc_t mcXtDesc_t
#define cudaXtDesc mcXtDesc
#define cudaLibXtDesc_t mcLibXtDesc_t
#define cudaLibXtDesc mcLibXtDesc

/********device functions**********/

#define __nv_aligned_device_malloc __mc_aligned_device_malloc
#define nvstd mcstd
#define cudaStreamWaitEvent_ptsz wcudaStreamWaitEvent_ptsz
#define cudaEventRecord_ptsz wcudaEventRecord_ptsz
#define cudaEventRecordWithFlags_ptsz wcudaEventRecordWithFlags_ptsz
#define cudaMemcpyAsync_ptsz wcudaMemcpyAsync_ptsz
#define cudaMemcpy2DAsync_ptsz wcudaMemcpy2DAsync_ptsz
#define cudaMemcpy3DAsync_ptsz wcudaMemcpy3DAsync_ptsz
#define cudaMemsetAsync_ptsz wcudaMemsetAsync_ptsz
#define cudaMemset2DAsync_ptsz wcudaMemset2DAsync_ptsz
#define cudaMemset3DAsync_ptsz wcudaMemset3DAsync_ptsz
#define cudaGetParameterBuffer wcudaGetParameterBuffer
#define cudaGetParameterBufferV2 wcudaGetParameterBufferV2
#define cudaLaunchDevice_ptsz wcudaLaunchDevice_ptsz
#define cudaLaunchDeviceV2_ptsz wcudaLaunchDeviceV2_ptsz
#define cudaLaunchDevice wcudaLaunchDevice
#define cudaLaunchDeviceV2 wcudaLaunchDeviceV2
#define cudaCGSynchronize wcudaCGSynchronize
#define cudaCGSynchronizeGrid wcudaCGSynchronizeGrid
#define cudaCGGetSize wcudaCGGetSize
#define cudaCGGetRank wcudaCGGetRank
#define cudaCGGetIntrinsicHandle wcudaCGGetIntrinsicHandle

/********complex number**********/
#define cuFloatComplex mcFloatComplex
#define cuCrealf mcCrealf
#define cuCimagf mcCimagf
#define make_cuFloatComplex make_mcFloatComplex
#define cuConjf mcConjf
#define cuCaddf mcCaddf
#define cuCsubf mcCsubf
#define cuCmulf mcCmulf
#define cuCdivf mcCdivf
#define cuCabsf mcCabsf
#define cuDoubleComplex mcDoubleComplex
#define cuCreal mcCreal
#define cuCimag mcCimag
#define make_cuDoubleComplex make_mcDoubleComplex
#define cuConj mcConj
#define cuCadd mcCadd
#define cuCsub mcCsub
#define cuCmul mcCmul
#define cuCdiv mcCdiv
#define cuCabs mcCabs
#define cuComplex mcComplex
#define make_cuComplex make_mcComplex
#define cuComplexFloatToDouble mcComplexFloatToDouble
#define cuComplexDoubleToFloat mcComplexDoubleToFloat
#define cuCfmaf mcCfmaf
#define cuCfma mcCfma

/********C++ namespace**********/
#define nvcuda mxmaca

/***********vdpau***************/

#define cudaVDPAUGetDevice wcudaVDPAUGetDevice
#define cudaVDPAUSetVDPAUDevice wcudaVDPAUSetVDPAUDevice
#define cudaGraphicsVDPAURegisterOutputSurface                                 \
  wcudaGraphicsVDPAURegisterOutputSurface

#define cuVDPAUGetDevice wcuVDPAUGetDevice
#define cuVDPAUCtxCreate wcuVDPAUCtxCreate
#define cuGraphicsVDPAURegisterVideoSurface wcuGraphicsVDPAURegisterVideoSurface
#define cuGraphicsVDPAURegisterOutputSurface                                   \
  wcuGraphicsVDPAURegisterOutputSurface
#define PFN_cuVDPAUGetDevice PFN_mcVDPAUGetDevice
#define PFN_cuVDPAUCtxCreate PFN_mcVDPAUCtxCreate
#define PFN_cuGraphicsVDPAURegisterVideoSurface                                \
  PFN_mcGraphicsVDPAURegisterVideoSurface
#define PFN_cuGraphicsVDPAURegisterOutputSurface                               \
  PFN_mcGraphicsVDPAURegisterOutputSurface

/*************EGL*************/
#define CUeglFrameType_enum mcDrveglFrameType_enum
#define CUeglFrameType mcDrveglFrameType
#define CU_EGL_FRAME_TYPE_ARRAY MC_EGL_FRAME_TYPE_ARRAY
#define CU_EGL_FRAME_TYPE_PITCH MC_EGL_FRAME_TYPE_PITCH

#define CUDA_EGL_INFINITE_TIMEOUT MC_EGL_INFINITE_TIMEOUT

#define CUeglResourceLocationFlags_enum mcDrveglResourceLocationFlags_enum
#define CUeglResourceLocationFlags mcDrveglResourceLocationFlags
#define CU_EGL_RESOURCE_LOCATION_SYSMEM MC_EGL_RESOURCE_LOCATION_SYSMEM
#define CU_EGL_RESOURCE_LOCATION_VIDMEM MC_EGL_RESOURCE_LOCATION_VIDMEM

#define CUeglColorFormat_enum mcDrveglColorFormat_enum
#define CUeglColorFormat mcDrveglColorFormat
#define CU_EGL_COLOR_FORMAT_YUV420_PLANAR MC_EGL_COLOR_FORMAT_YUV420_PLANAR
#define CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_YUV422_PLANAR MC_EGL_COLOR_FORMAT_YUV422_PLANAR
#define CU_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_RGB MC_EGL_COLOR_FORMAT_RGB
#define CU_EGL_COLOR_FORMAT_BGR MC_EGL_COLOR_FORMAT_BGR
#define CU_EGL_COLOR_FORMAT_ARGB MC_EGL_COLOR_FORMAT_ARGB
#define CU_EGL_COLOR_FORMAT_RGBA MC_EGL_COLOR_FORMAT_RGBA
#define CU_EGL_COLOR_FORMAT_L MC_EGL_COLOR_FORMAT_L
#define CU_EGL_COLOR_FORMAT_R MC_EGL_COLOR_FORMAT_R
#define CU_EGL_COLOR_FORMAT_YUV444_PLANAR MC_EGL_COLOR_FORMAT_YUV444_PLANAR
#define MC_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_YUYV_422 MC_EGL_COLOR_FORMAT_YUYV_422
#define CU_EGL_COLOR_FORMAT_UYVY_422 MC_EGL_COLOR_FORMAT_UYVY_422
#define CU_EGL_COLOR_FORMAT_ABGR MC_EGL_COLOR_FORMAT_ABGR
#define CU_EGL_COLOR_FORMAT_BGRA MC_EGL_COLOR_FORMAT_BGRA
#define CU_EGL_COLOR_FORMAT_A MC_EGL_COLOR_FORMAT_A
#define CU_EGL_COLOR_FORMAT_RG MC_EGL_COLOR_FORMAT_RG
#define CU_EGL_COLOR_FORMAT_AYUV MC_EGL_COLOR_FORMAT_AYUV
#define CU_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR                                  \
  MC_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR                           \
  MC_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR                           \
  MC_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR                           \
  MC_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR                           \
  MC_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_VYUY_ER MC_EGL_COLOR_FORMAT_VYUY_ER
#define CU_EGL_COLOR_FORMAT_UYVY_ER MC_EGL_COLOR_FORMAT_UYVY_ER
#define CU_EGL_COLOR_FORMAT_YUYV_ER MC_EGL_COLOR_FORMAT_YUYV_ER
#define CU_EGL_COLOR_FORMAT_YVYU_ER MC_EGL_COLOR_FORMAT_YVYU_ER
#define CU_EGL_COLOR_FORMAT_YUV_ER MC_EGL_COLOR_FORMAT_YUV_ER
#define CU_EGL_COLOR_FORMAT_YUVA_ER MC_EGL_COLOR_FORMAT_YUVA_ER
#define CU_EGL_COLOR_FORMAT_AYUV_ER MC_EGL_COLOR_FORMAT_AYUV_ER
#define CU_EGL_COLOR_FORMAT_YUV444_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YUV444_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YUV422_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YUV422_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YUV420_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YUV420_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YUV444_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YUV422_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU444_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YVU444_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU422_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YVU422_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU420_PLANAR_ER                                   \
  MC_EGL_COLOR_FORMAT_YVU420_PLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YVU444_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YVU422_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_ER                               \
  MC_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_BAYER_RGGB MC_EGL_COLOR_FORMAT_BAYER_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER_BGGR MC_EGL_COLOR_FORMAT_BAYER_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER_GRBG MC_EGL_COLOR_FORMAT_BAYER_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER_GBRG MC_EGL_COLOR_FORMAT_BAYER_GBRG
#define CU_EGL_COLOR_FORMAT_BAYER10_RGGB MC_EGL_COLOR_FORMAT_BAYER10_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER10_BGGR MC_EGL_COLOR_FORMAT_BAYER10_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER10_GRBG MC_EGL_COLOR_FORMAT_BAYER10_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER10_GBRG MC_EGL_COLOR_FORMAT_BAYER10_GBRG
#define CU_EGL_COLOR_FORMAT_BAYER12_RGGB MC_EGL_COLOR_FORMAT_BAYER12_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER12_BGGR MC_EGL_COLOR_FORMAT_BAYER12_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER12_GRBG MC_EGL_COLOR_FORMAT_BAYER12_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER12_GBRG MC_EGL_COLOR_FORMAT_BAYER12_GBRG
#define CU_EGL_COLOR_FORMAT_BAYER14_RGGB MC_EGL_COLOR_FORMAT_BAYER14_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER14_BGGR MC_EGL_COLOR_FORMAT_BAYER14_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER14_GRBG MC_EGL_COLOR_FORMAT_BAYER14_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER14_GBRG MC_EGL_COLOR_FORMAT_BAYER14_GBRG
#define CU_EGL_COLOR_FORMAT_BAYER20_RGGB MC_EGL_COLOR_FORMAT_BAYER20_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER20_BGGR MC_EGL_COLOR_FORMAT_BAYER20_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER20_GRBG MC_EGL_COLOR_FORMAT_BAYER20_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER20_GBRG MC_EGL_COLOR_FORMAT_BAYER20_GBRG
#define CU_EGL_COLOR_FORMAT_YVU444_PLANAR MC_EGL_COLOR_FORMAT_YVU444_PLANAR
#define CU_EGL_COLOR_FORMAT_YVU422_PLANAR MC_EGL_COLOR_FORMAT_YVU422_PLANAR
#define CU_EGL_COLOR_FORMAT_YVU420_PLANAR MC_EGL_COLOR_FORMAT_YVU420_PLANAR
#define CU_EGL_COLOR_FORMAT_BAYER_ISP_RGGB MC_EGL_COLOR_FORMAT_BAYER_ISP_RGGB
#define CU_EGL_COLOR_FORMAT_BAYER_ISP_BGGR MC_EGL_COLOR_FORMAT_BAYER_ISP_BGGR
#define CU_EGL_COLOR_FORMAT_BAYER_ISP_GRBG MC_EGL_COLOR_FORMAT_BAYER_ISP_GRBG
#define CU_EGL_COLOR_FORMAT_BAYER_ISP_GBRG MC_EGL_COLOR_FORMAT_BAYER_ISP_GBRG
#define CU_EGL_COLOR_FORMAT_BAYER_BCCR MC_EGL_COLOR_FORMAT_BAYER_BCCR
#define CU_EGL_COLOR_FORMAT_BAYER_RCCB MC_EGL_COLOR_FORMAT_BAYER_RCCB
#define CU_EGL_COLOR_FORMAT_BAYER_CRBC MC_EGL_COLOR_FORMAT_BAYER_CRBC
#define CU_EGL_COLOR_FORMAT_BAYER_CBRC MC_EGL_COLOR_FORMAT_BAYER_CBRC
#define CU_EGL_COLOR_FORMAT_BAYER10_CCCC MC_EGL_COLOR_FORMAT_BAYER10_CCCC
#define CU_EGL_COLOR_FORMAT_BAYER12_BCCR MC_EGL_COLOR_FORMAT_BAYER12_BCCR
#define CU_EGL_COLOR_FORMAT_BAYER12_RCCB MC_EGL_COLOR_FORMAT_BAYER12_RCCB
#define CU_EGL_COLOR_FORMAT_BAYER12_CRBC MC_EGL_COLOR_FORMAT_BAYER12_CRBC
#define CU_EGL_COLOR_FORMAT_BAYER12_CBRC MC_EGL_COLOR_FORMAT_BAYER12_CBRC
#define CU_EGL_COLOR_FORMAT_BAYER12_CCCC MC_EGL_COLOR_FORMAT_BAYER12_CCCC
#define CU_EGL_COLOR_FORMAT_Y MC_EGL_COLOR_FORMAT_Y
#define CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_2020                             \
  MC_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_2020
#define CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_2020                             \
  MC_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_2020
#define CU_EGL_COLOR_FORMAT_YUV420_PLANAR_2020                                 \
  MC_EGL_COLOR_FORMAT_YUV420_PLANAR_2020
#define CU_EGL_COLOR_FORMAT_YVU420_PLANAR_2020                                 \
  MC_EGL_COLOR_FORMAT_YVU420_PLANAR_2020
#define CU_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_709                              \
  MC_EGL_COLOR_FORMAT_YUV420_SEMIPLANAR_709
#define CU_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_709                              \
  MC_EGL_COLOR_FORMAT_YVU420_SEMIPLANAR_709
#define CU_EGL_COLOR_FORMAT_YUV420_PLANAR_709                                  \
  MC_EGL_COLOR_FORMAT_YUV420_PLANAR_709
#define CU_EGL_COLOR_FORMAT_YVU420_PLANAR_709                                  \
  MC_EGL_COLOR_FORMAT_YVU420_PLANAR_709
#define CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_709                       \
  MC_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_709
#define CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_2020                      \
  MC_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_2020
#define CU_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR_2020                      \
  MC_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR_2020
#define CU_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR                           \
  MC_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR
#define CU_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR_709                       \
  MC_EGL_COLOR_FORMAT_Y10V10U10_422_SEMIPLANAR_709
#define CU_EGL_COLOR_FORMAT_Y_ER MC_EGL_COLOR_FORMAT_Y_ER
#define CU_EGL_COLOR_FORMAT_Y_709_ER MC_EGL_COLOR_FORMAT_Y_709_ER
#define CU_EGL_COLOR_FORMAT_Y10_ER MC_EGL_COLOR_FORMAT_Y10_ER
#define CU_EGL_COLOR_FORMAT_Y10_709_ER MC_EGL_COLOR_FORMAT_Y10_709_ER
#define CU_EGL_COLOR_FORMAT_Y12_ER MC_EGL_COLOR_FORMAT_Y12_ER
#define CU_EGL_COLOR_FORMAT_Y12_709_ER MC_EGL_COLOR_FORMAT_Y12_709_ER
#define CU_EGL_COLOR_FORMAT_YUVA MC_EGL_COLOR_FORMAT_YUVA
#define CU_EGL_COLOR_FORMAT_YUV MC_EGL_COLOR_FORMAT_YUV
#define CU_EGL_COLOR_FORMAT_YVYU MC_EGL_COLOR_FORMAT_YVYU
#define CU_EGL_COLOR_FORMAT_VYUY MC_EGL_COLOR_FORMAT_VYUY
#define CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_ER                        \
  MC_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_709_ER                    \
  MC_EGL_COLOR_FORMAT_Y10V10U10_420_SEMIPLANAR_709_ER
#define CU_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR_ER                        \
  MC_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR_709_ER                    \
  MC_EGL_COLOR_FORMAT_Y10V10U10_444_SEMIPLANAR_709_ER
#define CU_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR_ER                        \
  MC_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR_709_ER                    \
  MC_EGL_COLOR_FORMAT_Y12V12U12_420_SEMIPLANAR_709_ER
#define CU_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR_ER                        \
  MC_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR_ER
#define CU_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR_709_ER                    \
  MC_EGL_COLOR_FORMAT_Y12V12U12_444_SEMIPLANAR_709_ER
#define CU_EGL_COLOR_FORMAT_MAX MC_EGL_COLOR_FORMAT_MAX

#define CUeglFrame_st mcDrveglFrame_st
#define CUeglFrame_v1 mcDrveglFrame_v1
#define CUeglFrame mcDrveglFrame

#define CUeglStreamConnection_st mcDrveglStreamConnection_st
#define CUeglStreamConnection mcDrveglStreamConnection

#define cuGraphicsEGLRegisterImage wcuGraphicsEGLRegisterImage
#define cuEGLStreamConsumerConnect wcuEGLStreamConsumerConnect
#define cuEGLStreamConsumerConnectWithFlags wcuEGLStreamConsumerConnectWithFlags
#define cuEGLStreamConsumerDisconnect wcuEGLStreamConsumerDisconnect
#define cuEGLStreamConsumerReleaseFrame wcuEGLStreamConsumerReleaseFrame
#define cuEGLStreamConsumerAcquireFrame wcuEGLStreamConsumerAcquireFrame
#define cuEGLStreamProducerConnect wcuEGLStreamProducerConnect
#define cuEGLStreamProducerDisconnect wcuEGLStreamProducerDisconnect
#define cuEGLStreamProducerPresentFrame wcuEGLStreamProducerPresentFrame
#define cuEGLStreamProducerReturnFrame wcuEGLStreamProducerReturnFrame
#define cuGraphicsResourceGetMappedEglFrame wcuGraphicsResourceGetMappedEglFrame
#define cuEventCreateFromEGLSync wcuEventCreateFromEGLSync

#define PFN_cuGraphicsEGLRegisterImage PFN_wcuGraphicsEGLRegisterImage
#define PFN_cuEGLStreamConsumerConnect PFN_wcuEGLStreamConsumerConnect
#define PFN_cuEGLStreamConsumerConnectWithFlags                                \
  PFN_wcuEGLStreamConsumerConnectWithFlags
#define PFN_cuEGLStreamConsumerDisconnect PFN_wcuEGLStreamConsumerDisconnect
#define PFN_cuEGLStreamConsumerAcquireFrame PFN_wcuEGLStreamConsumerAcquireFrame
#define PFN_cuEGLStreamConsumerReleaseFrame PFN_wcuEGLStreamConsumerReleaseFrame
#define PFN_cuEGLStreamProducerConnect PFN_wcuEGLStreamProducerConnect
#define PFN_cuEGLStreamProducerDisconnect PFN_wcuEGLStreamProducerDisconnect
#define PFN_cuEGLStreamProducerPresentFrame PFN_wcuEGLStreamProducerPresentFrame
#define PFN_cuEGLStreamProducerReturnFrame PFN_wcuEGLStreamProducerReturnFrame
#define PFN_cuGraphicsResourceGetMappedEglFrame                                \
  PFN_wcuGraphicsResourceGetMappedEglFrame
#define PFN_cuEventCreateFromEGLSync PFN_wcuEventCreateFromEGLSync

#define cudaEglFrameType_enum mcEglFrameType_enum
#define cudaEglFrameType mcEglFrameType
#define cudaEglFrameTypeArray mcEglFrameTypeArray
#define cudaEglFrameTypePitch mcEglFrameTypePitch

#define cudaEglResourceLocationFlags_enum mcEglResourceLocationFlags_enum
#define cudaEglResourceLocationFlags mcEglResourceLocationFlags
#define cudaEglResourceLocationSysmem mcEglResourceLocationSysmem
#define cudaEglResourceLocationVidmem mcEglResourceLocationVidmem

#define cudaEglColorFormat_enum mcEglColorFormat_enum
#define cudaEglColorFormat mcEglColorFormat
#define cudaEglColorFormatYUV420Planar mcEglColorFormatYUV420Planar
#define cudaEglColorFormatYUV420SemiPlanar mcEglColorFormatYUV420SemiPlanar
#define cudaEglColorFormatYUV422Planar mcEglColorFormatYUV422Planar
#define cudaEglColorFormatYUV422SemiPlanar mcEglColorFormatYUV422SemiPlanar
#define cudaEglColorFormatARGB mcEglColorFormatARGB
#define cudaEglColorFormatRGBA mcEglColorFormatRGBA
#define cudaEglColorFormatL mcEglColorFormatL
#define cudaEglColorFormatR mcEglColorFormatR
#define cudaEglColorFormatYUV444Planar mcEglColorFormatYUV444Planar
#define cudaEglColorFormatYUV444SemiPlanar mcEglColorFormatYUV444SemiPlanar
#define cudaEglColorFormatYUYV422 mcEglColorFormatYUYV422
#define cudaEglColorFormatUYVY422 mcEglColorFormatUYVY422
#define cudaEglColorFormatABGR mcEglColorFormatABGR
#define cudaEglColorFormatBGRA mcEglColorFormatBGRA
#define cudaEglColorFormatA mcEglColorFormatA
#define cudaEglColorFormatRG mcEglColorFormatRG
#define cudaEglColorFormatAYUV mcEglColorFormatAYUV
#define cudaEglColorFormatYVU444SemiPlanar mcEglColorFormatYVU444SemiPlanar
#define cudaEglColorFormatYVU422SemiPlanar mcEglColorFormatYVU422SemiPlanar
#define cudaEglColorFormatYVU420SemiPlanar mcEglColorFormatYVU420SemiPlanar
#define cudaEglColorFormatY10V10U10_444SemiPlanar                              \
  mcEglColorFormatY10V10U10_444SemiPlanar
#define cudaEglColorFormatY10V10U10_420SemiPlanar                              \
  mcEglColorFormatY10V10U10_420SemiPlanar
#define cudaEglColorFormatY12V12U12_444SemiPlanar                              \
  mcEglColorFormatY12V12U12_444SemiPlanar
#define cudaEglColorFormatY12V12U12_420SemiPlanar                              \
  mcEglColorFormatY12V12U12_420SemiPlanar
#define cudaEglColorFormatVYUY_ER mcEglColorFormatVYUY_ER
#define cudaEglColorFormatUYVY_ER mcEglColorFormatUYVY_ER
#define cudaEglColorFormatYUYV_ER mcEglColorFormatYUYV_ER
#define cudaEglColorFormatYVYU_ER mcEglColorFormatYVYU_ER
#define cudaEglColorFormatYUVA_ER mcEglColorFormatYUVA_ER
#define cudaEglColorFormatAYUV_ER mcEglColorFormatAYUV_ER
#define cudaEglColorFormatYUV444Planar_ER mcEglColorFormatYUV444Planar_ER
#define cudaEglColorFormatYUV422Planar_ER mcEglColorFormatYUV422Planar_ER
#define cudaEglColorFormatYUV420Planar_ER mcEglColorFormatYUV420Planar_ER
#define cudaEglColorFormatYUV444SemiPlanar_ER                                  \
  mcEglColorFormatYUV444SemiPlanar_ER
#define cudaEglColorFormatYUV422SemiPlanar_ER                                  \
  mcEglColorFormatYUV422SemiPlanar_ER
#define cudaEglColorFormatYUV420SemiPlanar_ER                                  \
  mcEglColorFormatYUV420SemiPlanar_ER
#define cudaEglColorFormatYVU444Planar_ER mcEglColorFormatYVU444Planar_ER
#define cudaEglColorFormatYVU422Planar_ER mcEglColorFormatYVU422Planar_ER
#define cudaEglColorFormatYVU420Planar_ER mcEglColorFormatYVU420Planar_ER
#define cudaEglColorFormatYVU444SemiPlanar_ER                                  \
  mcEglColorFormatYVU444SemiPlanar_ER
#define cudaEglColorFormatYVU422SemiPlanar_ER                                  \
  mcEglColorFormatYVU422SemiPlanar_ER
#define cudaEglColorFormatYVU420SemiPlanar_ER                                  \
  mcEglColorFormatYVU420SemiPlanar_ER
#define cudaEglColorFormatBayerRGGB mcEglColorFormatBayerRGGB
#define cudaEglColorFormatBayerBGGR mcEglColorFormatBayerBGGR
#define cudaEglColorFormatBayerGRBG mcEglColorFormatBayerGRBG
#define cudaEglColorFormatBayerGBRG mcEglColorFormatBayerGBRG
#define cudaEglColorFormatBayer10RGGB mcEglColorFormatBayer10RGGB
#define cudaEglColorFormatBayer10BGGR mcEglColorFormatBayer10BGGR
#define cudaEglColorFormatBayer10GRBG mcEglColorFormatBayer10GRBG
#define cudaEglColorFormatBayer10GBRG mcEglColorFormatBayer10GBRG
#define cudaEglColorFormatBayer12RGGB mcEglColorFormatBayer12RGGB
#define cudaEglColorFormatBayer12BGGR mcEglColorFormatBayer12BGGR
#define cudaEglColorFormatBayer12GRBG mcEglColorFormatBayer12GRBG
#define cudaEglColorFormatBayer12GBRG mcEglColorFormatBayer12GBRG
#define cudaEglColorFormatBayer14RGGB mcEglColorFormatBayer14RGGB
#define cudaEglColorFormatBayer14BGGR mcEglColorFormatBayer14BGGR
#define cudaEglColorFormatBayer14GRBG mcEglColorFormatBayer14GRBG
#define cudaEglColorFormatBayer14GBRG mcEglColorFormatBayer14GBRG
#define cudaEglColorFormatBayer20RGGB mcEglColorFormatBayer20RGGB
#define cudaEglColorFormatBayer20BGGR mcEglColorFormatBayer20BGGR
#define cudaEglColorFormatBayer20GRBG mcEglColorFormatBayer20GRBG
#define cudaEglColorFormatBayer20GBRG mcEglColorFormatBayer20GBRG
#define cudaEglColorFormatYVU444Planar mcEglColorFormatYVU444Planar
#define cudaEglColorFormatYVU422Planar mcEglColorFormatYVU422Planar
#define cudaEglColorFormatYVU420Planar mcEglColorFormatYVU420Planar
#define cudaEglColorFormatBayerIspRGGB mcEglColorFormatBayerIspRGGB
#define cudaEglColorFormatBayerIspBGGR mcEglColorFormatBayerIspBGGR
#define cudaEglColorFormatBayerIspGRBG mcEglColorFormatBayerIspGRBG
#define cudaEglColorFormatBayerIspGBRG mcEglColorFormatBayerIspGBRG
#define cudaEglColorFormatBayerBCCR mcEglColorFormatBayerBCCR
#define cudaEglColorFormatBayerRCCB mcEglColorFormatBayerRCCB
#define cudaEglColorFormatBayerCRBC mcEglColorFormatBayerCRBC
#define cudaEglColorFormatBayerCBRC mcEglColorFormatBayerCBRC
#define cudaEglColorFormatBayer10CCCC mcEglColorFormatBayer10CCCC
#define cudaEglColorFormatBayer12BCCR mcEglColorFormatBayer12BCCR
#define cudaEglColorFormatBayer12RCCB mcEglColorFormatBayer12RCCB
#define cudaEglColorFormatBayer12CRBC mcEglColorFormatBayer12CRBC
#define cudaEglColorFormatBayer12CBRC mcEglColorFormatBayer12CBRC
#define cudaEglColorFormatBayer12CCCC mcEglColorFormatBayer12CCCC
#define cudaEglColorFormatY mcEglColorFormatY
#define cudaEglColorFormatYUV420SemiPlanar_2020                                \
  mcEglColorFormatYUV420SemiPlanar_2020
#define cudaEglColorFormatYVU420SemiPlanar_2020                                \
  mcEglColorFormatYVU420SemiPlanar_2020
#define cudaEglColorFormatYUV420Planar_2020 mcEglColorFormatYUV420Planar_2020
#define cudaEglColorFormatYVU420Planar_2020 mcEglColorFormatYVU420Planar_2020
#define cudaEglColorFormatYUV420SemiPlanar_709                                 \
  mcEglColorFormatYUV420SemiPlanar_709
#define cudaEglColorFormatYVU420SemiPlanar_709                                 \
  mcEglColorFormatYVU420SemiPlanar_709
#define cudaEglColorFormatYUV420Planar_709 mcEglColorFormatYUV420Planar_709
#define cudaEglColorFormatYVU420Planar_709 mcEglColorFormatYVU420Planar_709
#define cudaEglColorFormatY10V10U10_420SemiPlanar_709                          \
  mcEglColorFormatY10V10U10_420SemiPlanar_709
#define cudaEglColorFormatY10V10U10_420SemiPlanar_2020                         \
  mcEglColorFormatY10V10U10_420SemiPlanar_2020
#define cudaEglColorFormatY10V10U10_422SemiPlanar_2020                         \
  mcEglColorFormatY10V10U10_422SemiPlanar_2020
#define cudaEglColorFormatY10V10U10_422SemiPlanar                              \
  mcEglColorFormatY10V10U10_422SemiPlanar
#define cudaEglColorFormatY10V10U10_422SemiPlanar_709                          \
  mcEglColorFormatY10V10U10_422SemiPlanar_709
#define cudaEglColorFormatY_ER mcEglColorFormatY_ER
#define cudaEglColorFormatY_709_ER mcEglColorFormatY_ER
#define cudaEglColorFormatY10_ER mcEglColorFormatY10_ER
#define cudaEglColorFormatY10_709_ER mcEglColorFormatY10_709_ER
#define cudaEglColorFormatY12_ER mcEglColorFormatY12_ER
#define cudaEglColorFormatY12_709_ER mcEglColorFormatY12_709_ER
#define cudaEglColorFormatYUVA mcEglColorFormatYUVA
#define cudaEglColorFormatYVYU mcEglColorFormatYVYU
#define cudaEglColorFormatVYUY mcEglColorFormatVYUY
#define cudaEglColorFormatY10V10U10_420SemiPlanar_ER                           \
  mcEglColorFormatY10V10U10_420SemiPlanar_ER
#define cudaEglColorFormatY10V10U10_420SemiPlanar_709_ER                       \
  mcEglColorFormatY10V10U10_420SemiPlanar_709_ER
#define cudaEglColorFormatY10V10U10_444SemiPlanar_ER                           \
  mcEglColorFormatY10V10U10_444SemiPlanar_ER
#define cudaEglColorFormatY10V10U10_444SemiPlanar_709_ER                       \
  mcEglColorFormatY10V10U10_444SemiPlanar_709_ER
#define cudaEglColorFormatY12V12U12_420SemiPlanar_ER                           \
  mcEglColorFormatY12V12U12_420SemiPlanar_ER
#define cudaEglColorFormatY12V12U12_420SemiPlanar_709_ER                       \
  mcEglColorFormatY12V12U12_420SemiPlanar_709_ER
#define cudaEglColorFormatY12V12U12_444SemiPlanar_ER                           \
  mcEglColorFormatY12V12U12_444SemiPlanar_ER
#define cudaEglColorFormatY12V12U12_444SemiPlanar_709_ER                       \
  mcEglColorFormatY12V12U12_444SemiPlanar_709_ER

#define cudaEglPlaneDesc_st mcEglPlaneDesc_st
#define cudaEglPlaneDesc mcEglPlaneDesc
#define cudaEglFrame_st mcEglFrame_st
#define cudaEglFrame mcEglFrame_st
#define cudaEglStreamConnection mcEglStreamConnection

#define cudaGraphicsEGLRegisterImage wcudaGraphicsEGLRegisterImage
#define cudaEGLStreamConsumerConnect wcudaEGLStreamConsumerConnect
#define cudaEGLStreamConsumerConnectWithFlags                                  \
  wcudaEGLStreamConsumerConnectWithFlags
#define cudaEGLStreamConsumerDisconnect wcudaEGLStreamConsumerDisconnect
#define cudaEGLStreamConsumerAcquireFrame wcudaEGLStreamConsumerAcquireFrame
#define cudaEGLStreamConsumerReleaseFrame wcudaEGLStreamConsumerReleaseFrame
#define cudaEGLStreamProducerConnect wcudaEGLStreamProducerConnect
#define cudaEGLStreamProducerDisconnect wcudaEGLStreamProducerDisconnect
#define cudaEGLStreamProducerPresentFrame wcudaEGLStreamProducerPresentFrame
#define cudaEGLStreamProducerReturnFrame wcudaEGLStreamProducerReturnFrame
#define cudaGraphicsResourceGetMappedEglFrame                                  \
  wcudaGraphicsResourceGetMappedEglFrame
#define cudaEventCreateFromEGLSync wcudaEventCreateFromEGLSync

/*******************OpenGL********************/
#define cuGLCtxCreate wcuGLCtxCreate
#define cuGLCtxCreate_v2 wcuGLCtxCreate_v2
#define cuGLMapBufferObject wcuGLMapBufferObject
#define cuGLMapBufferObjectAsync wcuGLMapBufferObjectAsync
#define cuGLGetDevices wcuGLGetDevices
#define cuGLGetDevices_v2 wcuGLGetDevices_v2
#define cuGraphicsGLRegisterBuffer wcuGraphicsGLRegisterBuffer
#define cuGraphicsGLRegisterImage wcuGraphicsGLRegisterImage
#define cuGLMapBufferObject_v2 wcuGLMapBufferObject_v2
#define cuGLMapBufferObjectAsync_v2 wcuGLMapBufferObjectAsync_v2
#define cuGLInit wcuGLInit
#define cuGLRegisterBufferObject wcuGLRegisterBufferObject
#define cuGLUnmapBufferObject wcuGLUnmapBufferObject
#define cuGLUnregisterBufferObject wcuGLUnregisterBufferObject
#define cuGLSetBufferObjectMapFlags wcuGLSetBufferObjectMapFlags
#define cuGLUnmapBufferObjectAsync wcuGLUnmapBufferObjectAsync

#define CUGLDeviceList_enum mcDrvGLDeviceList_enum
#define CUGLDeviceList mcDrvGLDeviceList
#define CU_GL_DEVICE_LIST_ALL MC_GL_DEVICE_LIST_ALL
#define CU_GL_DEVICE_LIST_CURRENT_FRAME MC_GL_DEVICE_LIST_CURRENT_FRAME
#define CU_GL_DEVICE_LIST_NEXT_FRAME MC_GL_DEVICE_LIST_NEXT_FRAME
#define CUGLmap_flags_enum mcDrvGLmap_flags_enum
#define CUGLmap_flags mcDrvGLmap_flags
#define CU_GL_MAP_RESOURCE_FLAGS_NONE MC_GL_MAP_RESOURCE_FLAGS_NONE
#define CU_GL_MAP_RESOURCE_FLAGS_READ_ONLY MC_GL_MAP_RESOURCE_FLAGS_READ_ONLY
#define CU_GL_MAP_RESOURCE_FLAGS_WRITE_DISCARD                                 \
  MC_GL_MAP_RESOURCE_FLAGS_WRITE_DISCARD

#define cudaGLDeviceList mcGLDeviceList
#define cudaGLDeviceListAll mcGLDeviceListAll
#define cudaGLDeviceListCurrentFrame mcGLDeviceListCurrentFrame
#define cudaGLDeviceListNextFrame mcGLDeviceListNextFrame
#define cudaGLMapFlags mcGLMapFlags
#define cudaGLMapFlagsNone mcGLMapFlagsNone
#define cudaGLMapFlagsReadOnly mcGLMapFlagsReadOnly
#define cudaGLMapFlagsWriteDiscard mcGLMapFlagsWriteDiscard

#define cudaGLGetDevices wcudaGLGetDevices
#define cudaGraphicsGLRegisterImage wcudaGraphicsGLRegisterImage
#define cudaGraphicsGLRegisterBuffer wcudaGraphicsGLRegisterBuffer
#define cudaWGLGetDevice wcudaWGLGetDevice
#define cudaGLSetGLDevice wcudaGLSetGLDevice
#define cudaGLRegisterBufferObject wcudaGLRegisterBufferObject
#define cudaGLMapBufferObject wcudaGLMapBufferObject
#define cudaGLUnmapBufferObject wcudaGLUnmapBufferObject
#define cudaGLUnregisterBufferObject wcudaGLUnregisterBufferObject
#define cudaGLSetBufferObjectMapFlags wcudaGLSetBufferObjectMapFlags
#define cudaGLMapBufferObjectAsync wcudaGLMapBufferObjectAsync
#define cudaGLUnmapBufferObjectAsync wcudaGLUnmapBufferObjectAsync

#define __cu_demangle __mc_demangle

#endif
