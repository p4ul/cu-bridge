#ifndef __CUDA_DRIVER_WRAPPER_H__
#define __CUDA_DRIVER_WRAPPER_H__

#include "cuda_benchmark_targets.h"
#include "cuda_driver_types.h"
#include "cuda_wrapper_internal_types.h"
#include "mc_runtime_types.h"
#ifdef __cplusplus
#include <cstdlib>
#include <cstring>
#else
#include <stdlib.h>
#include <string.h>
#endif

#ifdef __cplusplus
#define __dparm(x) = x
#define __dparm_deprecated(x) [[deprecated(#x)]]
#else
#define __dparm(x)
#define __dparm_deprecated(x)
#endif

extern enum wcudaNvArchType __nv_arch_type_internal__;
extern int __wcuda_version_internal__;

#ifdef __cplusplus
extern "C" {
#endif
/**
 *  @defgroup wcuDriver CudaDriver Wrapper API
 *  @{
 *  This section describes functions of CUDA Driver Wrapper API.
 *
 */

/**
 *  @defgroup Error Error Handling
 *  @{
 *  This section describes the Error Handling functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t mcErrorTomcDrvError(mcError_t mcError);

mcDrvError_t wcuGetErrorName(mcDrvError_t error, const char **pStr);

mcDrvError_t wcuGetErrorString(mcDrvError_t error, const char **pStr);

/**
 * @} Error
 */

/**
 *  @defgroup Initialization Initialization
 *  @{
 *  This section describes the Initialization functions of CUDA Driver Wrapper API.
 *
 */

mcDrvError_t wcuInit(unsigned int flags);

/**
 * @} Initialization
 */

/**
 *  @defgroup Version Version Management
 *  @{
 *  This section describes the Version Management functions of CUDA Driver Wrapper API.
 *
 */

mcDrvError_t wcuDriverGetVersion(int *driverVersion);

/**
 * @} Version
 */

/**
 *  @defgroup Device Device Management
 *  @{
 *  This section describes the Device Management functions of CUDA Driver Wrapper API.
 *
 */

mcDrvError_t wcuDeviceGetUuid(mcDrvUuid_t *uuid, mcDrvDevice_t device);
mcDrvError_t wcuDeviceGetUuid_v2(mcDrvUuid_t *uuid, mcDrvDevice_t device);
mcDrvError_t wcuDeviceGetCount(int *count);
mcDrvError_t wcuDeviceGetByPCIBusId(mcDrvDevice_t *device, const char *pciBusId);
mcDrvError_t wcuDeviceGetExecAffinitySupport(int *pi, mcDrvexecAffinityType type, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetPCIBusId(char *pciBusId, int len, mcDrvDevice_t device);
mcDrvError_t wcuDeviceGet(mcDrvDevice_t *device, int ordinal);
mcDrvError_t wcuDeviceGetName(char *name, int len, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceTotalMem(size_t *bytes, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetDefaultMemPool(mcDrvMemPool_t *poolOut, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetMemPool(mcDrvMemPool_t *poolOut, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceSetMemPool(mcDrvDevice_t dev, mcDrvMemPool_t poolOut);
mcDrvError_t wcuDeviceGetLuid(char *luid, unsigned int *deviceNodeMask, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetP2PAttribute(int *value, mcDrvDeviceP2PAttribute attrib,
                                      mcDrvDevice_t srcDevice, mcDrvDevice_t dstDevice);
mcDrvError_t wcuFlushGPUDirectRDMAWrites(mcDrvFlushGPUDirectRDMAWritesTarget target,
                                         mcDrvFlushGPUDirectRDMAWritesScope scope);
mcDrvError_t wcuDeviceGetNvSciSyncAttributes(void *mcSciSyncAttrList, mcDrvDevice_t dev, int flags);
mcDrvError_t wcuDeviceGetTexture1DLinearMaxWidth(size_t *maxWidthInElements, mcArray_Format format,
                                                 unsigned numChannels, mcDrvDevice_t dev);

/* deprecated APIs */
mcDrvError_t wcuDeviceComputeCapability(int *major, int *minor, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetProperties(mcDrvDevprop *prop, mcDrvDevice_t dev);
mcDrvError_t wcuDeviceGetAttributeInit(int *pi, mcDrvDeviceAttribute_t drv_attr,
                                       mcDrvDevice_t device);
mcDrvError_t wcuDeviceGetAttribute(int *pi, mcDrvDeviceAttribute_t drv_attr, mcDrvDevice_t device);

/**
 * @} Device
 */

/**
 *  @defgroup Primary Primary Context Managent
 *  @{
 *  This section describes the Primary Context Management functions of CUDA Driver Wrapper API.
 *
 */

/**
 * @} Primary
 */
mcDrvError_t wcuDevicePrimaryCtxGetState(mcDrvDevice_t dev, unsigned int *flags, int *active);
mcDrvError_t wcuDevicePrimaryCtxRelease(mcDrvDevice_t dev);
mcDrvError_t wcuDevicePrimaryCtxRetain(mcDrvContext_t *pctx, mcDrvDevice_t dev);
mcDrvError_t wcuDevicePrimaryCtxSetFlags(mcDrvDevice_t dev, unsigned int flags);
mcDrvError_t wcuDevicePrimaryCtxReset(mcDrvDevice_t dev);

/**
 *  @defgroup Context Context Managent
 *  @{
 *  This section describes the Context Management functions of CUDA Driver Wrapper API.
 *
 */

/**
 * @} Context
 */
mcDrvError_t wcuCtxCreate(mcDrvContext_t *pctx, unsigned int flags, mcDrvDevice_t dev);
mcDrvError_t wcuCtxCreate_v3(mcDrvContext_t *pctx, mcDrvexecAffinityParam_t *paramsArray,
                             int numParams, unsigned int flags, mcDrvDevice_t dev);
mcDrvError_t wcuCtxDestroy(mcDrvContext_t ctx);
mcDrvError_t wcuCtxGetApiVersion(mcDrvContext_t ctx, unsigned int *version);
mcDrvError_t wcuCtxGetCacheConfig(mcDrvfunc_cache_t *pconfig);
mcDrvError_t wcuCtxGetCurrent(mcDrvContext_t *pctx);
mcDrvError_t wcuCtxGetDevice(mcDrvDevice_t *device);
mcDrvError_t wcuCtxGetExecAffinity(mcDrvexecAffinityParam_t *pExecAffinity,
                                   mcDrvexecAffinityType type);
mcDrvError_t wcuCtxGetFlags(unsigned int *flags);
mcDrvError_t wcuCtxGetLimit(size_t *pvalue, mcDrvlimit_t limit);
mcDrvError_t wcuCtxGetSharedMemConfig(mcDrvsharedconfig_t *pConfig);
mcDrvError_t wcuCtxGetStreamPriorityRange(int *leastPriority, int *greatestPriority);
mcDrvError_t wcuCtxPopCurrent(mcDrvContext_t *pctx);
mcDrvError_t wcuCtxPushCurrent(mcDrvContext_t pctx);
mcDrvError_t wcuCtxResetPersistingL2Cache(void);
mcDrvError_t wcuCtxSetCacheConfig(mcDrvfunc_cache_t config);
mcDrvError_t wcuCtxSetCurrent(mcDrvContext_t ctx);
mcDrvError_t wcuCtxSetLimit(mcDrvlimit_t limit, size_t value);
mcDrvError_t wcuCtxSetSharedMemConfig(mcDrvsharedconfig_t config);
mcDrvError_t wcuCtxSynchronize(void);

/* deprecated APIs */
mcDrvError_t wcuCtxAttach(mcDrvContext_t *pctx, unsigned int flags);
mcDrvError_t wcuCtxDetach(mcDrvContext_t ctx);

/**
 *  @defgroup Module Module Managent
 *  @{
 *  This section describes the Module Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuModuleLoad(mcDrvModule_t *module, const char *fname);
mcDrvError_t wcuModuleLoadData(mcDrvModule_t *module, const void *image);
mcDrvError_t wcuModuleLoadDataEx(mcDrvModule_t *module, const void *image, unsigned int numOptions,
                                 mcDrvjit_option_t *options, void **optionValues);
mcDrvError_t wcuModuleUnload(mcDrvModule_t hmod);
mcDrvError_t wcuModuleGetFunction(mcDrvFunction_t *hfunc, mcDrvModule_t hmod, const char *name);
mcDrvError_t wcuLinkCreate(unsigned int numOptions, mcDrvjit_option_t *options, void **optionValues,
                           mcDrvlinkState_t *stateOut);
mcDrvError_t wcuLinkAddData(mcDrvlinkState_t state, mcDrvJitInputType_t type, void *data,
                            size_t size, const char *name, unsigned int numOptions,
                            mcDrvjit_option_t *options, void **optionValues);
mcDrvError_t wcuLinkComplete(mcDrvlinkState_t state, void **binOut, size_t *sizeOut);
mcDrvError_t wcuLinkAddFile(mcDrvlinkState_t state, mcDrvJitInputType_t type, const char *path,
                            unsigned int numOptions, mcDrvjit_option_t *options,
                            void **optionValues);
mcDrvError_t wcuLinkDestroy(mcDrvlinkState_t state);
mcDrvError_t wcuModuleGetGlobal(mcDrvDeviceptr_t *dptr, size_t *bytes, mcDrvModule_t hmod,
                                const char *name);
mcDrvError_t wcuModuleGetSurfRef(mcDrvSurfref *pSurfRef, mcDrvModule_t hmod, const char *name);
mcDrvError_t wcuModuleGetTexRef(mcDrvTexref *pTexRef, mcDrvModule_t hmod, const char *name);
mcDrvError_t wcuModuleLoadFatBinary(mcDrvModule_t *module, const void *fatCubin);

/**
 * @} Module
 */

/**
 *  @defgroup Memory Memory Management
 *  @{
 *  This section describes the Memory Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuArray3DCreate(MCarray *pHandle, const mcDrvArray3DDescriptor *pAllocateArray);
mcDrvError_t wcuArray3DGetDescriptor(mcDrvArray3DDescriptor *pArrayDescriptor, MCarray hArray);
mcDrvError_t wcuArrayCreate(MCarray *pHandle, const mcDrvArrayDescriptor *pAllocateArray);
mcDrvError_t wcuArrayDestroy(MCarray hArray);
mcDrvError_t wcuArrayGetDescriptor(mcDrvArrayDescriptor *pArrayDescriptor, MCarray hArray);
mcDrvError_t wcuArrayGetMemoryRequirements(mcDrvArrayMemoryRequirements *memoryRequirements,
                                           MCarray array, mcDrvDevice_t device);
mcDrvError_t wcuArrayGetPlane(MCarray *pPlaneArray, MCarray hArray,
                              unsigned int planeIdx);
mcDrvError_t wcuArrayGetSparseProperties(mcDrvArraySparseProperties *sparseProperties,
                                         MCarray array);

mcDrvError_t wcuIpcCloseMemHandle(mcDrvDeviceptr_t dptr);
mcDrvError_t wcuIpcGetMemHandle(mcDrvIpcMemHandle_t *pHandle, mcDrvDeviceptr_t dptr);
mcDrvError_t wcuIpcOpenMemHandle(mcDrvDeviceptr_t *pdptr, mcDrvIpcMemHandle_t handle,
                                 unsigned int Flags);

mcDrvError_t wcuMemAlloc(mcDrvDeviceptr_t *ptr, size_t sizeBytes);
mcDrvError_t wcuMemAllocHost(void **pHost, size_t sizeBytes);
mcDrvError_t wcuMemAllocManaged(mcDrvDeviceptr_t *dptr, size_t sizeBytes, unsigned int flags);
mcDrvError_t wcuMemAllocPitch(mcDrvDeviceptr_t *dptr, size_t *pPitch, size_t widthInBytes,
                              size_t height, unsigned int elementSizeBytes);
mcDrvError_t wcuMemFree(mcDrvDeviceptr_t ptr);
mcDrvError_t wcuMemFreeHost(void *ptr);
mcDrvError_t wcuMemHostAlloc(void **pHost, size_t size, unsigned int flags);
mcDrvError_t wcuMemHostGetDevicePointer(mcDrvDeviceptr_t *pDevice, void *pHost, unsigned int flags);
mcDrvError_t wcuMemHostGetFlags(unsigned int *flagsPtr, void *hostPtr);
mcDrvError_t wcuMemHostRegister(void *ptr, size_t size, unsigned int flags);
mcDrvError_t wcuMemHostUnregister(void *ptr);
mcDrvError_t wcuMemcpy(mcDrvDeviceptr_t dst, mcDrvDeviceptr_t src, size_t sizeBytes);
mcDrvError_t wcuMemcpy2D(const mcDrvMemcpy2D *pCopy);
mcDrvError_t wcuMemcpy2DUnaligned(const mcDrvMemcpy2D *pCopy);
mcDrvError_t wcuMemcpy2DAsync(const mcDrvMemcpy2D *pCopy, mcDrvStream_t hStream);
mcDrvError_t wcuMemcpy3D(const mcDrvMemcpy3D *pCopy);
mcDrvError_t wcuMemcpy3DAsync(const mcDrvMemcpy3D *pCopy, mcDrvStream_t hStream);
mcDrvError_t wcuMemcpy3DPeer(const mcDrvMemcpy3DPeer *pCopy);
mcDrvError_t wcuMemcpy3DPeerAsync(const mcDrvMemcpy3DPeer *pCopy, mcDrvStream_t hStream);
mcDrvError_t wcuMemcpyAsync(mcDrvDeviceptr_t dst, mcDrvDeviceptr_t src, size_t ByteCount,
                            mcDrvStream_t hStream);
mcDrvError_t wcuMemGetInfo(size_t *free, size_t *total);
mcDrvError_t wcuMemGetAddressRange(mcDrvDeviceptr_t *pbase, size_t *psize, mcDrvDeviceptr_t dptr);
mcDrvError_t wcuMemsetD16(mcDrvDeviceptr_t dstDevice, unsigned short us, size_t N);
mcDrvError_t wcuMemsetD16Async(mcDrvDeviceptr_t dstDevice, unsigned short us, size_t N,
                               mcDrvStream_t hStream);
mcDrvError_t wcuMemsetD32(mcDrvDeviceptr_t dstDevice, unsigned int ui, size_t N);
mcDrvError_t wcuMemsetD32Async(mcDrvDeviceptr_t dstDevice, unsigned int ui, size_t N,
                               mcDrvStream_t hStream);
mcDrvError_t wcuMemsetD8Async(mcDrvDeviceptr_t dstDevice, unsigned char uc, size_t N,
                              mcDrvStream_t hStream);
mcDrvError_t wcuMemsetD8(mcDrvDeviceptr_t dstDevice, unsigned char uc, size_t N);
mcDrvError_t wcuMemsetD2D8(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned char uc,
                           size_t Width, size_t Height);
mcDrvError_t wcuMemsetD2D8Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned char uc,
                                size_t Width, size_t Height, mcDrvStream_t hStream);
mcDrvError_t wcuMemsetD2D16(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned short us,
                            size_t Width, size_t Height);
mcDrvError_t wcuMemsetD2D16Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned short us,
                                 size_t Width, size_t Height, mcDrvStream_t hStream);
mcDrvError_t wcuMemsetD2D32(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned int ui,
                            size_t Width, size_t Height);
mcDrvError_t wcuMemsetD2D32Async(mcDrvDeviceptr_t dstDevice, size_t dstPitch, unsigned int ui,
                                 size_t Width, size_t Height, mcDrvStream_t hStream);

mcDrvError_t wcuMemcpyDtoA(MCarray dstArray, size_t dstOffset, mcDrvDeviceptr_t srcDevice,
                           size_t ByteCount);
mcDrvError_t wcuMemcpyAtoD(mcDrvDeviceptr_t dstDevice, MCarray srcArray, size_t srcOffset,
                           size_t ByteCount);
mcDrvError_t wcuMemcpyHtoA(MCarray dstArray, size_t dstOffset, const void *srcHost,
                           size_t ByteCount);
mcDrvError_t wcuMemcpyAtoH(void *dstHost, MCarray srcArray, size_t srcOffset,
                           size_t ByteCount);
mcDrvError_t wcuMemcpyAtoA(MCarray dstArray, size_t dstOffset, MCarray srcArray,
                           size_t srcOffset, size_t ByteCount);
mcDrvError_t wcuMemcpyHtoAAsync(MCarray dstArray, size_t dstOffset, const void *srcHost,
                                size_t ByteCount, mcDrvStream_t hStream);
mcDrvError_t wcuMemcpyAtoHAsync(void *dstHost, MCarray srcArray, size_t srcOffset,
                                size_t ByteCount, mcDrvStream_t hStream);

mcDrvError_t wcuMemcpyDtoD(mcDrvDeviceptr_t dstDevice, mcDrvDeviceptr_t srcDevice,
                           size_t byteCount);
mcDrvError_t wcuMemcpyDtoDAsync(mcDrvDeviceptr_t dstDevice, mcDrvDeviceptr_t srcDevice,
                                size_t byteCount, mcDrvStream_t hStream);
mcDrvError_t wcuMemcpyDtoH(void *dstHost, mcDrvDeviceptr_t srcDevice, size_t byteCount);
mcDrvError_t wcuMemcpyDtoHAsync(void *dstHost, mcDrvDeviceptr_t srcDevice, size_t byteCount,
                                mcDrvStream_t hStream);
mcDrvError_t wcuMemcpyHtoD(mcDrvDeviceptr_t dstDevice, const void *srcHost, size_t byteCount);
mcDrvError_t wcuMemcpyHtoDAsync(mcDrvDeviceptr_t dstDevice, const void *srcHost, size_t byteCount,
                                mcDrvStream_t hStream);
mcDrvError_t wcuMemcpyPeer(mcDrvDeviceptr_t dstDevice, mcDrvContext_t dstContext,
                           mcDrvDeviceptr_t srcDevice, mcDrvContext_t srcContext, size_t byteCount);
mcDrvError_t wcuMemcpyPeerAsync(mcDrvDeviceptr_t dstDevice, mcDrvContext_t dstContext,
                                mcDrvDeviceptr_t srcDevice, mcDrvContext_t srcContext,
                                size_t byteCount, mcDrvStream_t hStream);

mcDrvError_t wcuMipmappedArrayCreate(mcDrvMipmappedArray *pHandle,
                                     const mcDrvArray3DDescriptor *pMipmappedArrayDesc,
                                     unsigned int numMipmapLevels);
mcDrvError_t wcuMipmappedArrayDestroy(mcDrvMipmappedArray hMipmappedArray);
mcDrvError_t wcuMipmappedArrayGetLevel(MCarray *pLevelArray,
                                       mcDrvMipmappedArray hMipmappedArray, unsigned int level);
mcDrvError_t
wcuMipmappedArrayGetMemoryRequirements(mcDrvArrayMemoryRequirements *memoryRequirements,
                                       mcDrvMipmappedArray mipmap, mcDrvDevice_t device);
mcDrvError_t wcuMipmappedArrayGetSparseProperties(mcDrvArraySparseProperties *sparseProperties,
                                                  mcDrvMipmappedArray mipmap);

/**
 * @} Memory
 */

/**
 *  @defgroup Virtual Memory Management
 *  @{
 *  This section describes the Virtual Memory Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuMemAddressFree(mcDrvDeviceptr_t ptr, size_t size);
mcDrvError_t wcuMemAddressReserve(mcDrvDeviceptr_t *ptr, size_t size, size_t alignment,
                                  mcDrvDeviceptr_t addr, unsigned long long flags);
mcDrvError_t wcuMemMap(mcDrvDeviceptr_t ptr, size_t size, size_t offset,
                       mcDrvMemGenericAllocationHandle_t handle, unsigned long long flags);
mcDrvError_t wcuMemMapArrayAsync(mcDrvArrayMapInfo *mapInfoList, unsigned int count,
                                 mcDrvStream_t hStream);
mcDrvError_t wcuMemUnmap(mcDrvDeviceptr_t ptr, size_t size);
mcDrvError_t wcuMemSetAccess(mcDrvDeviceptr_t ptr, size_t size, const mcDrvMemAccessDesc_t *desc,
                             size_t count);
mcDrvError_t wcuMemGetAccess(unsigned long long *flags, const mcDrvMemLocation_t *location,
                             mcDrvDeviceptr_t ptr);
mcDrvError_t wcuMemExportToShareableHandle(void *shareableHandle,
                                           mcDrvMemGenericAllocationHandle_t handle,
                                           mcDrvMemAllocationHandleType_t handleType,
                                           unsigned long long flags);
mcDrvError_t wcuMemImportFromShareableHandle(mcDrvMemGenericAllocationHandle_t *handle,
                                             void *osHandle,
                                             mcDrvMemAllocationHandleType_t shHandleType);
mcDrvError_t wcuMemGetAllocationGranularity(size_t *granularity,
                                            const mcDrvMemAllocationProp_t *prop,
                                            mcDrvMemAllocationGranularity_flags_t option);
mcDrvError_t wcuMemGetAllocationPropertiesFromHandle(mcDrvMemAllocationProp_t *prop,
                                                     mcDrvMemGenericAllocationHandle_t handle);
mcDrvError_t wcuMemRetainAllocationHandle(mcDrvMemGenericAllocationHandle_t *handle, void *addr);
mcDrvError_t wcuMemCreate(mcDrvMemGenericAllocationHandle_t *handle, size_t size,
                          const mcDrvMemAllocationProp_t *prop, unsigned long long flags);
mcDrvError_t wcuMemRelease(mcDrvMemGenericAllocationHandle_t handle);

/**
 * @} Memory
 */

/**
 *  @defgroup Multicast Object Management
 *  @{
 *  This section describes the Multicast Object Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuMulticastCreate(mcDrvMemGenericAllocationHandle_t *mcHandle,
                                const mcDrvMulticastObjectProp *prop);
mcDrvError_t wcuMulticastAddDevice(mcDrvMemGenericAllocationHandle_t mcHandle, mcDrvDevice_t dev);
mcDrvError_t wcuMulticastBindMem(mcDrvMemGenericAllocationHandle_t mcHandle, size_t mcOffset,
                                 mcDrvMemGenericAllocationHandle_t memHandle, size_t memOffset,
                                 size_t size, unsigned long long flags);
mcDrvError_t wcuMulticastBindAddr(mcDrvMemGenericAllocationHandle_t mcHandle, size_t mcOffset,
                                  mcDrvDeviceptr_t memptr, size_t size, unsigned long long flags);
mcDrvError_t wcuMulticastUnbind(mcDrvMemGenericAllocationHandle_t mcHandle, mcDrvDevice_t dev,
                                size_t mcOffset, size_t size);
mcDrvError_t wcuMulticastGetGranularity(size_t *granularity, const mcDrvMulticastObjectProp *prop,
                                        mcDrvMulticastGranularity_flags option);

/**
 * @} Memory
 */

/**
 *  @defgroup Stream Stream Management
 *  @{
 *  This section describes the Stream Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuStreamBeginCapture(mcDrvStream_t hStream, mcStreamCaptureMode mode);
mcDrvError_t wcuStreamCreate(mcDrvStream_t *phStream, unsigned int Flags);
mcDrvError_t wcuStreamCreateWithPriority(mcDrvStream_t *phStream, unsigned int flags, int priority);
mcDrvError_t wcuStreamDestroy(mcDrvStream_t hStream);
mcDrvError_t wcuStreamEndCapture(mcDrvStream_t hStream, mcDrvGraph *phGraph);
mcDrvError_t wcuStreamGetCaptureInfo(mcDrvStream_t hStream,
                                     mcStreamCaptureStatus *captureStatus_out, mcuint64_t *id_out);
mcDrvError_t wcuStreamGetCaptureInfo_v2(mcDrvStream_t hStream,
                                        mcStreamCaptureStatus *captureStatus_out,
                                        mcuint64_t *id_out, mcDrvGraph *graph_out,
                                        const mcDrvGraphNode **dependencies_out,
                                        size_t *numDependencies_out);
mcDrvError_t wcuStreamGetFlags(mcDrvStream_t hStream, unsigned int *flags);
mcDrvError_t wcuStreamGetPriority(mcDrvStream_t hStream, int *priority);
mcDrvError_t wcuStreamIsCapturing(mcDrvStream_t hStream, mcStreamCaptureStatus *captureStatus);
mcDrvError_t wcuStreamQuery(mcDrvStream_t hStream);
mcDrvError_t wcuStreamSynchronize(mcDrvStream_t hStream);
mcDrvError_t wcuStreamWaitEvent(mcDrvStream_t hStream, mcDrvEvent_t hEvent, unsigned int Flags);
mcDrvError_t wcuStreamAddCallback(mcDrvStream_t hStream, mcDrvStreamCallback_t callback,
                                  void *userData, unsigned int flags);
mcDrvError_t wcuStreamAttachMemAsync(mcDrvStream_t hStream, mcDrvDeviceptr_t dptr, size_t length,
                                     unsigned int flags);
mcDrvError_t wcuStreamCopyAttributes(mcDrvStream_t dst, mcDrvStream_t src);
mcDrvError_t wcuStreamSetAttribute(mcDrvStream_t hStream, mcDrvStreamAttrID attr,
                                   const mcDrvStreamAttrValue *value);
mcDrvError_t wcuStreamGetAttribute(mcDrvStream_t hStream, mcDrvStreamAttrID attr,
                                   mcDrvStreamAttrValue *value_out);
mcDrvError_t wcuStreamGetCtx(mcDrvStream_t hStream, mcDrvContext_t *pctx);
mcDrvError_t wcuStreamUpdateCaptureDependencies(mcDrvStream_t hStream, mcDrvGraphNode *dependencies,
                                                size_t numDependencies, unsigned int flags);
mcDrvError_t wcuThreadExchangeStreamCaptureMode(mcStreamCaptureMode *mode);

/**
 * @} Stream
 */

/**
 *  @defgroup Unified Unified Addressing
 *  @{
 *  This section describes the Unified Addressing functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuMemAdvise(mcDrvDeviceptr_t devPtr, size_t count, mcDrvMemoryAdvise advice,
                          mcDrvDevice_t device);
mcDrvError_t wcuPointerGetAttribute(void *data, mcDrvPointerAttributeType attribute,
                                    mcDrvDeviceptr_t ptr);
mcDrvError_t wcuPointerGetAttributes(unsigned int numAttributes,
                                     mcDrvPointerAttributeType *attributes, void **data,
                                     mcDrvDeviceptr_t ptr);
mcDrvError_t wcuMemPrefetchAsync(mcDrvDeviceptr_t devPtr, size_t count, mcDrvDevice_t dstDevice,
                                 mcDrvStream_t hStream);
mcDrvError_t wcuMemRangeGetAttribute(void *data, size_t dataSize,
                                     mcDrvMemoryAttributeType_t attribute, mcDrvDeviceptr_t devPtr,
                                     size_t count);
mcDrvError_t wcuMemRangeGetAttributes(void **data, size_t *dataSizes,
                                      mcDrvMemoryAttributeType_t *attributes, size_t numAttributes,
                                      mcDrvDeviceptr_t devPtr, size_t count);
mcDrvError_t wcuPointerSetAttribute(const void *value, mcDrvPointerAttributeType attribute,
                                    mcDrvDeviceptr_t ptr);
/**
 * @} Unified
 */

/**
 *  @defgroup Event Event Management
 *  @{
 *  This section describes the Event Management functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuIpcGetEventHandle(mcDrvIpcEventHandle_t *pHandle, mcDrvEvent_t event);
mcDrvError_t wcuIpcOpenEventHandle(mcDrvEvent_t *phEvent, mcDrvIpcEventHandle_t handle);
mcDrvError_t wcuEventCreate(mcDrvEvent_t *event, unsigned int flags);
mcDrvError_t wcuEventDestroy(mcDrvEvent_t event);
mcDrvError_t wcuEventElapsedTime(float *ms, mcDrvEvent_t start, mcDrvEvent_t end);
mcDrvError_t wcuEventQuery(mcDrvEvent_t event);
mcDrvError_t wcuEventRecord(mcDrvEvent_t event, mcDrvStream_t stream);
mcDrvError_t wcuEventRecordWithFlags(mcDrvEvent_t hEvent, mcDrvStream_t hStream,
                                     unsigned int flags);
mcDrvError_t wcuEventSynchronize(mcDrvEvent_t event);

/**
 * @} Event
 */

/**
 *  @defgroup External External Resource Interoperability
 *  @{
 *  This section describes the External Resource Interoperability functions of CUDA Driver Wrapper
 * API.
 *
 */
mcDrvError_t wcuDestroyExternalMemory(mcDrvExternalMemory extMem);
mcDrvError_t wcuDestroyExternalSemaphore(mcDrvExternalSemaphore extSem);
mcDrvError_t wcuExternalMemoryGetMappedBuffer(mcDrvDeviceptr_t *devPtr, mcDrvExternalMemory extMem,
                                              const mcDrvExternalMemoryBufferDesc *bufferDesc);
mcDrvError_t
wcuExternalMemoryGetMappedMipmappedArray(mcDrvMipmappedArray *mipmap, mcDrvExternalMemory extMem,
                                         const mcDrvExternalMemoryMipmappedArrayDesc *mipmapDesc);
mcDrvError_t wcuImportExternalMemory(mcDrvExternalMemory *extMem_out,
                                     const mcDrvExternalMemoryHandleDesc *memHandleDesc);
mcDrvError_t wcuImportExternalSemaphore(mcDrvExternalSemaphore *extSem_out,
                                        const mcDrvExternalSemaphorHandleDesc *semHandleDesc);
mcDrvError_t wcuSignalExternalSemaphoresAsync(const mcDrvExternalSemaphore *extSemArray,
                                              const mcDrvExternalSemaphoreSignalParams *paramsArray,
                                              unsigned int numExtSems, mcDrvStream_t stream);
mcDrvError_t wcuWaitExternalSemaphoresAsync(const mcDrvExternalSemaphore *extSemArray,
                                            const mcDrvExternalSemaphoreWaitParams *paramsArray,
                                            unsigned int numExtSems, mcDrvStream_t stream);

/**
 * @} External
 */

/**
 *  @defgroup Execution Execution Control
 *  @{
 *  This section describes the Execution Control functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuFuncGetAttribute(int *value, mcDrvFunction_attribute attrib, mcDrvFunction_t hfunc);
mcDrvError_t wcuLaunchKernel(mcDrvFunction_t f, unsigned int gridDimX, unsigned int gridDimY,
                             unsigned int gridDimZ, unsigned int blockDimX, unsigned int blockDimY,
                             unsigned int blockDimZ, unsigned int sharedMemBytes,
                             mcDrvStream_t stream, void **kernelParams, void **extra);
mcDrvError_t wcuFuncGetModule(mcDrvModule_t *hmod, mcDrvFunction_t hfunc);
mcDrvError_t wcuFuncSetAttribute(mcDrvFunction_t hfunc, mcDrvFunction_attribute attrib, int value);
mcDrvError_t wcuFuncSetCacheConfig(mcDrvFunction_t hfunc, mcDrvfunc_cache_t config);
mcDrvError_t wcuFuncSetSharedMemConfig(mcDrvFunction_t hfunc, mcDrvsharedconfig_t config);
mcDrvError_t wcuLaunchCooperativeKernel(mcDrvFunction_t f, unsigned int gridDimX,
                                        unsigned int gridDimY, unsigned int gridDimZ,
                                        unsigned int blockDimX, unsigned int blockDimY,
                                        unsigned int blockDimZ, unsigned int sharedMemBytes,
                                        mcDrvStream_t stream, void **kernelParams);
mcDrvError_t wcuLaunchHostFunc(mcDrvStream_t hStream, mcHostFn_t fn, void *userData);
mcDrvError_t wcuLaunchCooperativeKernelMultiDevice(mcDrvLaunchParams *launchParamsList,
                                                   unsigned int numDevices, unsigned int flags);

/* deprecated APIs */
mcDrvError_t wcuFuncSetBlockShape(mcDrvFunction_t hfunc, int x, int y, int z);
mcDrvError_t wcuFuncSetSharedSize(mcDrvFunction_t hfunc, unsigned int bytes);
mcDrvError_t wcuLaunch(mcDrvFunction_t f);
mcDrvError_t wcuLaunchGrid(mcDrvFunction_t f, int grid_width, int grid_height);
mcDrvError_t wcuLaunchGridAsync(mcDrvFunction_t f, int grid_width, int grid_height,
                                mcDrvStream_t hStream);
mcDrvError_t wcuParamSetf(mcDrvFunction_t hfunc, int offset, float value);
mcDrvError_t wcuParamSeti(mcDrvFunction_t hfunc, int offset, unsigned int value);
mcDrvError_t wcuParamSetSize(mcDrvFunction_t hfunc, unsigned int numbytes);
mcDrvError_t wcuParamSetTexRef(mcDrvFunction_t hfunc, int texunit, mcDrvTexref hTexRef);
mcDrvError_t wcuParamSetv(mcDrvFunction_t hfunc, int offset, void *ptr, unsigned int numbytes);

/**
 * @} Execution
 */

/**
 *  @defgroup Occupancy Occupancy
 *  @{
 *  This section describes the Occupancy functions of CUDA Driver Wrapper API.
 *
 */

/**
 * @} Occupancy
 */

/**
 *  @defgroup Peer Peer Device Memory Access
 *  @{
 *  This section describes the Peer Device Memory Access functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuDeviceCanAccessPeer(int *canAccessPeer, mcDrvDevice_t dev, mcDrvDevice_t peerDev);
mcDrvError_t wcuCtxEnablePeerAccess(mcDrvContext_t peerContext, unsigned int Flags);
mcDrvError_t wcuCtxDisablePeerAccess(mcDrvContext_t peerContext);

/**
 * @} Peer
 */

/**
 *  @defgroup Profiler Profiler Control
 *  @{
 *  This section describes the Profiler Control functions of CUDA Driver Wrapper API.
 *
 */
mcDrvError_t wcuProfilerInitialize(const char *configFile, const char *outputFile,
                                   enum mcOutputMode outputMode);
mcDrvError_t wcuProfilerStart(void);
mcDrvError_t wcuProfilerStop(void);

/**
 * @} Profiler
 */

/**
 *  @defgroup Driver Driver Entry Point Access
 *  @{
 *  This section describes the driver entry point access functions of the low-level MACA driver
 * application programming interface.
 *
 */

mcDrvError_t wcuGetProcAddress(const char *symbol, void **pfn, int mcVersion, uint64_t flags);

mcDrvError_t wcuGetExportTable(const void **ppExportTable, const mcDrvUuid_t *pExportTableId);
/**
 * @} Driver
 */

/**
 *  @defgroup SOMA Stream Ordered Memory Allocator
 *  @{
 *  This section describes the Stream Ordered Memory Allocator functions of CUDA Driver
 * Wrapper API.
 *
 */

mcDrvError_t wcuMemAllocAsync(mcDrvDeviceptr_t *dptr, size_t bytesize, mcDrvStream_t hStream);

mcDrvError_t wcuMemAllocFromPoolAsync(mcDrvDeviceptr_t *dptr, size_t bytesize, mcDrvMemPool_t pool,
                                      mcDrvStream_t hStream);

mcDrvError_t wcuMemFreeAsync(mcDrvDeviceptr_t dptr, mcDrvStream_t hStream);

mcDrvError_t wcuMemPoolCreate(mcDrvMemPool_t *pool, const mcDrvMemPoolProps_t *poolProps);
mcDrvError_t wcuMemPoolDestroy(mcDrvMemPool_t pool);
mcDrvError_t wcuMemPoolExportPointer(mcDrvMemPoolPtrExportData_t *shareData_out,
                                     mcDrvDeviceptr_t ptr);
mcDrvError_t wcuMemPoolExportToShareableHandle(void *handle_out, mcDrvMemPool_t pool,
                                               mcDrvMemAllocationHandleType_t handleType,
                                               unsigned long long flags);
mcDrvError_t wcuMemPoolImportFromShareableHandle(mcDrvMemPool_t *pool_out, void *handle,
                                                 mcDrvMemAllocationHandleType_t handleType,
                                                 unsigned long long flags);
mcDrvError_t wcuMemPoolImportPointer(mcDrvDeviceptr_t *ptr_out, mcDrvMemPool_t pool,
                                     mcDrvMemPoolPtrExportData_t *shareData);

mcDrvError_t wcuMemPoolGetAccess(mcDrvMemAccess_flags_t *flags, mcDrvMemPool_t memPool,
                                 mcDrvMemLocation_t *location);
mcDrvError_t wcuMemPoolGetAttribute(mcDrvMemPool_t pool, mcDrvMemPoolAttr_t attr, void *value);
mcDrvError_t wcuMemPoolSetAccess(mcDrvMemPool_t pool, const mcDrvMemAccessDesc_t *map,
                                 size_t count);
mcDrvError_t wcuMemPoolSetAttribute(mcDrvMemPool_t pool, mcDrvMemPoolAttr_t attr, void *value);
mcDrvError_t wcuMemPoolTrimTo(mcDrvMemPool_t pool, size_t minBytesToKeep);
/**
 * @} SOMA
 */

/**
 * @} wcuDriver
 */

/**
 * @} Stream Memory Opertions
 */
mcDrvError_t wcuStreamBatchMemOp(mcDrvStream_t stream, unsigned int count,
                                 mcDrvStreamBatchMemOpParams *paramArray, unsigned int flags);
mcDrvError_t wcuStreamWaitValue32(mcDrvStream_t stream, mcDrvDeviceptr_t addr, uint32_t value,
                                  unsigned int flags);
mcDrvError_t wcuStreamWaitValue64(mcDrvStream_t stream, mcDrvDeviceptr_t addr, uint64_t value,
                                  unsigned int flags);
mcDrvError_t wcuStreamWriteValue32(mcDrvStream_t stream, mcDrvDeviceptr_t addr, uint32_t value,
                                   unsigned int flags);
mcDrvError_t wcuStreamWriteValue64(mcDrvStream_t stream, mcDrvDeviceptr_t addr, uint64_t value,
                                   unsigned int flags);

/**
 * @} Occupancy Funtions
 */
mcDrvError_t wcuOccupancyAvailableDynamicSMemPerBlock(size_t *dynamicSmemSize, mcDrvFunction_t func,
                                                      int numBlocks, int blockSize);
mcDrvError_t wcuOccupancyMaxActiveBlocksPerMultiprocessor(int *numBlocks, mcDrvFunction_t func,
                                                          int blockSize, size_t dynamicSMemSize);
mcDrvError_t wcuOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(int *numBlocks,
                                                                   mcDrvFunction_t func,
                                                                   int blockSize,
                                                                   size_t dynamicSMemSize,
                                                                   unsigned int flags);
mcDrvError_t wcuOccupancyMaxPotentialBlockSize(int *minGridSize, int *blockSize,
                                               mcDrvFunction_t func,
                                               mcOccupancyB2DSize blockSizeToDynamicSMemSize,
                                               size_t dynamicSMemSize, int blockSizeLimit);
mcDrvError_t
wcuOccupancyMaxPotentialBlockSizeWithFlags(int *minGridSize, int *blockSize, mcDrvFunction_t func,
                                           mcOccupancyB2DSize blockSizeToDynamicSMemSize,
                                           size_t dynamicSMemSize, int blockSizeLimit,
                                           unsigned int flags);

/**
 * @} graph Funtions
 */

__dparm_deprecated(cuDeviceGetGraphMemAttribute to be supported) mcDrvError_t
    wcuDeviceGetGraphMemAttribute(mcDrvDevice_t device, mcDrvGraphMem_attribute attr, void *value);

__dparm_deprecated(cuDeviceGraphMemTrim to be supported) mcDrvError_t
    wcuDeviceGraphMemTrim(mcDrvDevice_t device);

__dparm_deprecated(cuDeviceSetGraphMemAttribute to be supported) mcDrvError_t
    wcuDeviceSetGraphMemAttribute(mcDrvDevice_t device, mcDrvGraphMem_attribute attr, void *value);

mcDrvError_t wcuGraphAddChildGraphNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                       const mcDrvGraphNode *dependencies, size_t numDependencies,
                                       mcDrvGraph childGraph);

mcDrvError_t wcuGraphAddDependencies(mcDrvGraph hGraph, const mcDrvGraphNode *from,
                                     const mcDrvGraphNode *to, size_t numDependencies);

mcDrvError_t wcuGraphAddEmptyNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                  const mcDrvGraphNode *dependencies, size_t numDependencies);

mcDrvError_t wcuGraphAddEventRecordNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                        const mcDrvGraphNode *dependencies, size_t numDependencies,
                                        mcDrvEvent_t event);

mcDrvError_t wcuGraphAddEventWaitNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                      const mcDrvGraphNode *dependencies, size_t numDependencies,
                                      mcDrvEvent_t event);

__dparm_deprecated(cuGraphAddExternalSemaphoresSignalNode to be supported) mcDrvError_t
    wcuGraphAddExternalSemaphoresSignalNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                            const mcDrvGraphNode *dependencies,
                                            size_t numDependencies,
                                            const mcDrvExtSemSignalNodeParams *nodeParams);

__dparm_deprecated(cuGraphAddExternalSemaphoresWaitNode to be supported) mcDrvError_t
    wcuGraphAddExternalSemaphoresWaitNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                          const mcDrvGraphNode *dependencies,
                                          size_t numDependencies,
                                          const mcDrvExtSemWaitNodeParams *nodeParams);

mcDrvError_t wcuGraphAddHostNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                 const mcDrvGraphNode *dependencies, size_t numDependencies,
                                 const mcDrvHostNodeParams *nodeParams);

mcDrvError_t wcuGraphAddKernelNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                   const mcDrvGraphNode *dependencies, size_t numDependencies,
                                   const mcDrvKernelNodeParams *nodeParams);

mcDrvError_t wcuGraphAddMemAllocNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                     const mcDrvGraphNode *dependencies, size_t numDependencies,
                                     mcDrvMemAllocNodeParams *nodeParams);

mcDrvError_t wcuGraphAddMemFreeNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                    const mcDrvGraphNode *dependencies, size_t numDependencies,
                                    mcDrvDeviceptr_t dptr);

mcDrvError_t wcuGraphAddMemcpyNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                   const mcDrvGraphNode *dependencies, size_t numDependencies,
                                   const mcDrvMemcpy3D *copyParams, mcDrvContext_t ctx);

mcDrvError_t wcuGraphAddMemsetNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                   const mcDrvGraphNode *dependencies, size_t numDependencies,
                                   const mcDrvMemsetNodeParams *memsetParams, mcDrvContext_t ctx);

mcDrvError_t wcuGraphChildGraphNodeGetGraph(mcDrvGraphNode hNode, mcDrvGraph *phGraph);

mcDrvError_t wcuGraphClone(mcDrvGraph *phGraphClone, mcDrvGraph originalGraph);

mcDrvError_t wcuGraphCreate(mcDrvGraph *phGraph, unsigned int flags);

mcDrvError_t wcuGraphDebugDotPrint(mcDrvGraph hGraph, const char *path, unsigned int flags);

mcDrvError_t wcuGraphDestroy(mcDrvGraph hGraph);

mcDrvError_t wcuGraphDestroyNode(mcDrvGraphNode hNode);

mcDrvError_t wcuGraphEventRecordNodeGetEvent(mcDrvGraphNode hNode, mcDrvEvent_t *event_out);

mcDrvError_t wcuGraphEventRecordNodeSetEvent(mcDrvGraphNode hNode, mcDrvEvent_t event);

mcDrvError_t wcuGraphEventWaitNodeGetEvent(mcDrvGraphNode hNode, mcDrvEvent_t *event_out);

mcDrvError_t wcuGraphEventWaitNodeSetEvent(mcDrvGraphNode hNode, mcDrvEvent_t event);

mcDrvError_t wcuGraphExecChildGraphNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                                 mcDrvGraph childGraph);

mcDrvError_t wcuGraphExecDestroy(mcDrvGraphExec hGraphExec);

mcDrvError_t wcuGraphExecEventRecordNodeSetEvent(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                                 mcDrvEvent_t event);

mcDrvError_t wcuGraphExecEventWaitNodeSetEvent(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                               mcDrvEvent_t event);

__dparm_deprecated(cuGraphExecExternalSemaphoresSignalNodeSetParams to be supported) mcDrvError_t
    wcuGraphExecExternalSemaphoresSignalNodeSetParams(
        mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
        const mcDrvExtSemSignalNodeParams *nodeParams);

__dparm_deprecated(cuGraphExecExternalSemaphoresWaitNodeSetParams to be supported) mcDrvError_t
    wcuGraphExecExternalSemaphoresWaitNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                                    const mcDrvExtSemWaitNodeParams *nodeParams);

mcDrvError_t wcuGraphExecHostNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                           const mcDrvHostNodeParams *nodeParams);

__dparm_deprecated(cuGraphExecKernelNodeSetParams to be supported) mcDrvError_t
    wcuGraphExecKernelNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                    const mcDrvKernelNodeParams *nodeParams);

mcDrvError_t wcuGraphExecMemcpyNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                             const mcDrvMemcpy3D *copyParams, mcDrvContext_t ctx);

mcDrvError_t wcuGraphExecMemsetNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                             const mcDrvMemsetNodeParams *memsetParams,
                                             mcDrvContext_t ctx);

mcDrvError_t wcuGraphExecUpdate(mcDrvGraphExec hGraphExec, mcDrvGraph hGraph,
                                mcDrvGraphNode *hErrorNode_out,
                                mcGraphExecUpdateResult *updateResult_out);

__dparm_deprecated(cuGraphExternalSemaphoresSignalNodeGetParams to be supported) mcDrvError_t
    wcuGraphExternalSemaphoresSignalNodeGetParams(mcDrvGraphNode hNode,
                                                  mcDrvExtSemSignalNodeParams *params_out);

__dparm_deprecated(cuGraphExternalSemaphoresSignalNodeSetParams to be supported) mcDrvError_t
    wcuGraphExternalSemaphoresSignalNodeSetParams(mcDrvGraphNode hNode,
                                                  const mcDrvExtSemSignalNodeParams *nodeParams);

__dparm_deprecated(cuGraphExternalSemaphoresWaitNodeGetParams to be supported) mcDrvError_t
    wcuGraphExternalSemaphoresWaitNodeGetParams(mcDrvGraphNode hNode,
                                                mcDrvExtSemWaitNodeParams *params_out);

__dparm_deprecated(cuGraphExternalSemaphoresWaitNodeSetParams to be supported) mcDrvError_t
    wcuGraphExternalSemaphoresWaitNodeSetParams(mcDrvGraphNode hNode,
                                                const mcDrvExtSemWaitNodeParams *nodeParams);

mcDrvError_t wcuGraphGetEdges(mcDrvGraph hGraph, mcDrvGraphNode *from, mcDrvGraphNode *to,
                              size_t *numEdges);

mcDrvError_t wcuGraphGetNodes(mcDrvGraph hGraph, mcDrvGraphNode *nodes, size_t *numNodes);

mcDrvError_t wcuGraphGetRootNodes(mcDrvGraph hGraph, mcDrvGraphNode *rootNodes,
                                  size_t *numRootNodes);

mcDrvError_t wcuGraphHostNodeGetParams(mcDrvGraphNode hNode, mcDrvHostNodeParams *nodeParams);

mcDrvError_t wcuGraphHostNodeSetParams(mcDrvGraphNode hNode, const mcDrvHostNodeParams *nodeParams);

mcDrvError_t wcuGraphInstantiate(mcDrvGraphExec *phGraphExec, mcDrvGraph hGraph,
                                 mcDrvGraphNode *phErrorNode, char *logBuffer, size_t bufferSize);

mcDrvError_t wcuGraphInstantiateWithFlags(mcDrvGraphExec *phGraphExec, mcDrvGraph hGraph,
                                          unsigned long long flags);

mcDrvError_t wcuGraphInstantiateWithParams(mcDrvGraphExec *phGraphExec, mcDrvGraph hGraph,
                                           mcDrvGraphInstantiateParams *instantiateParams);

mcDrvError_t wcuGraphExecGetFlags(mcDrvGraphExec hGraphExec, mcuint64_t *flags);

__dparm_deprecated(cuGraphKernelNodeCopyAttributes to be supported) mcDrvError_t
    wcuGraphKernelNodeCopyAttributes(mcDrvGraphNode dst, mcDrvGraphNode src);

__dparm_deprecated(cuGraphKernelNodeGetAttribute to be supported) mcDrvError_t
    wcuGraphKernelNodeGetAttribute(mcDrvGraphNode hNode, mcDrvkernelNodeAttrID attr,
                                   mcDrvKernelNodeAttrValue *value_out);

__dparm_deprecated(cuGraphKernelNodeGetParams to be supported) mcDrvError_t
    wcuGraphKernelNodeGetParams(mcDrvGraphNode hNode, mcDrvKernelNodeParams *nodeParams);

__dparm_deprecated(cuGraphKernelNodeSetAttribute to be supported) mcDrvError_t
    wcuGraphKernelNodeSetAttribute(mcDrvGraphNode hNode, mcDrvkernelNodeAttrID attr,
                                   const mcDrvKernelNodeAttrValue *value);

__dparm_deprecated(cuGraphKernelNodeSetParams to be supported) mcDrvError_t
    wcuGraphKernelNodeSetParams(mcDrvGraphNode hNode, const mcDrvKernelNodeParams *nodeParams);

mcDrvError_t wcuGraphLaunch(mcDrvGraphExec hGraphExec, mcDrvStream_t hStream);

mcDrvError_t wcuGraphMemAllocNodeGetParams(mcDrvGraphNode hNode,
                                           mcDrvMemAllocNodeParams *params_out);

mcDrvError_t wcuGraphMemFreeNodeGetParams(mcDrvGraphNode hNode, mcDrvDeviceptr_t *dptr_out);

mcDrvError_t wcuGraphMemcpyNodeGetParams(mcDrvGraphNode hNode, mcDrvMemcpy3D *nodeParams);

mcDrvError_t wcuGraphMemcpyNodeSetParams(mcDrvGraphNode hNode, const mcDrvMemcpy3D *nodeParams);

mcDrvError_t wcuGraphMemsetNodeGetParams(mcDrvGraphNode hNode, mcDrvMemsetNodeParams *nodeParams);

mcDrvError_t wcuGraphMemsetNodeSetParams(mcDrvGraphNode hNode,
                                         const mcDrvMemsetNodeParams *nodeParams);

mcDrvError_t wcuGraphNodeFindInClone(mcDrvGraphNode *phNode, mcDrvGraphNode hOriginalNode,
                                     mcDrvGraph hClonedGraph);

mcDrvError_t wcuGraphNodeGetDependencies(mcDrvGraphNode hNode, mcDrvGraphNode *dependencies,
                                         size_t *numDependencies);

mcDrvError_t wcuGraphNodeGetDependentNodes(mcDrvGraphNode hNode, mcDrvGraphNode *dependentNodes,
                                           size_t *numDependentNodes);

mcDrvError_t wcuGraphNodeGetEnabled(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                    unsigned int *isEnabled);

mcDrvError_t wcuGraphNodeGetType(mcDrvGraphNode hNode, mcDrvGraphNodeType *type);

mcDrvError_t wcuGraphNodeSetEnabled(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                    unsigned int isEnabled);

mcDrvError_t wcuGraphReleaseUserObject(mcDrvGraph graph, mcDrvUserObject object,
                                       unsigned int count);

mcDrvError_t wcuGraphRemoveDependencies(mcDrvGraph hGraph, const mcDrvGraphNode *from,
                                        const mcDrvGraphNode *to, size_t numDependencies);

mcDrvError_t wcuGraphRetainUserObject(mcDrvGraph graph, mcDrvUserObject object, unsigned int count,
                                      unsigned int flags);

mcDrvError_t wcuGraphUpload(mcDrvGraphExec hGraphExec, mcDrvStream_t hStream);

mcDrvError_t wcuUserObjectCreate(mcDrvUserObject *object_out, void *ptr, mcHostFn_t destroy,
                                 unsigned int initialRefcount, unsigned int flags);

mcDrvError_t wcuUserObjectRelease(mcDrvUserObject object, unsigned int count);

mcDrvError_t wcuUserObjectRetain(mcDrvUserObject object, unsigned int count);

mcDrvError_t wcuGraphAddBatchMemOpNode(mcDrvGraphNode *phGraphNode, mcDrvGraph hGraph,
                                       const mcDrvGraphNode *dependencies, size_t numDependencies,
                                       const mcDrvBatchMemOpNodeParams *nodeParams);
mcDrvError_t wcuGraphBatchMemOpNodeGetParams(mcDrvGraphNode hNode,
                                             mcDrvBatchMemOpNodeParams *nodeParams_out);
mcDrvError_t wcuGraphBatchMemOpNodeSetParams(mcDrvGraphNode hNode,
                                             const mcDrvBatchMemOpNodeParams *nodeParams);
mcDrvError_t wcuGraphExecBatchMemOpNodeSetParams(mcDrvGraphExec hGraphExec, mcDrvGraphNode hNode,
                                                 const mcDrvBatchMemOpNodeParams *nodeParams);

/**
 * @defgroup GRAPHICS Graphics Interoperability
 * This section describes the graphics interoperability functions of the
 * low-level MACA driver application programming interface.
 * @{
 */
__dparm_deprecated(cuGraphicsMapResources not supported) mcDrvError_t
    wcuGraphicsMapResources(unsigned int count, mcDrvGraphicsResource *resources,
                            mcDrvStream_t hStream);

__dparm_deprecated(cuGraphicsResourceGetMappedMipmappedArray not supported) mcDrvError_t
    wcuGraphicsResourceGetMappedMipmappedArray(mcDrvMipmappedArray *pMipmappedArray,
                                               mcDrvGraphicsResource resource);

__dparm_deprecated(cuGraphicsResourceGetMappedPointer) mcDrvError_t
    wcuGraphicsResourceGetMappedPointer(mcDrvDeviceptr_t *pDevPtr, size_t *pSize,
                                        mcDrvGraphicsResource resource);

__dparm_deprecated(cuGraphicsResourceSetMapFlags not supported) mcDrvError_t
    wcuGraphicsResourceSetMapFlags(mcDrvGraphicsResource resource, unsigned int flags);

__dparm_deprecated(cuGraphicsSubResourceGetMappedArray not supported) mcDrvError_t
    wcuGraphicsSubResourceGetMappedArray(MCarray *pArray, mcDrvGraphicsResource resource,
                                         unsigned int arrayIndex, unsigned int mipLevel);

__dparm_deprecated(cuGraphicsUnmapResources not supported) mcDrvError_t
    wcuGraphicsUnmapResources(unsigned int count, mcDrvGraphicsResource *resources,
                              mcDrvStream_t hStream);

__dparm_deprecated(cuGraphicsUnregisterResource not supported) mcDrvError_t
    wcuGraphicsUnregisterResource(mcDrvGraphicsResource resource);

/**
 * @} GRAPHICS
 */

/**
 * @defgroup Texture Reference Management [DEPRECATED]
 *
 * This section describes the deprecated texture reference management
 * functions of the low-level MACA driver application programming interface.
 *
 * @{
 */

__dparm_deprecated(cuTexRefSetArray not supported) mcDrvError_t
    wcuTexRefSetArray(mcDrvTexref hTexRef, MCarray hArray, unsigned int Flags);

__dparm_deprecated(cuTexRefSetMipmappedArray not supported) mcDrvError_t
    wcuTexRefSetMipmappedArray(mcDrvTexref hTexRef, mcDrvMipmappedArray hMipmappedArray,
                               unsigned int Flags);

__dparm_deprecated(cuTexRefSetAddress not supported) mcDrvError_t
    wcuTexRefSetAddress(size_t *ByteOffset, mcDrvTexref hTexRef, mcDrvDeviceptr_t dptr,
                        size_t bytes);

__dparm_deprecated(cuTexRefSetAddress2D not supported) mcDrvError_t
    wcuTexRefSetAddress2D(mcDrvTexref hTexRef, const mcDrvArrayDescriptor *desc,
                          mcDrvDeviceptr_t dptr, size_t Pitch);

__dparm_deprecated(cuTexRefSetFormat not supported) mcDrvError_t
    wcuTexRefSetFormat(mcDrvTexref hTexRef, mcArray_Format fmt, int NumPackedComponents);

__dparm_deprecated(cuTexRefSetAddressMode not supported) mcDrvError_t
    wcuTexRefSetAddressMode(mcDrvTexref hTexRef, int dim, mcDrvAddress_mode am);

__dparm_deprecated(cuTexRefSetFilterMode not supported) mcDrvError_t
    wcuTexRefSetFilterMode(mcDrvTexref hTexRef, mcDrvFilter_mode fm);

__dparm_deprecated(cuTexRefSetMipmapFilterMode not supported) mcDrvError_t
    wcuTexRefSetMipmapFilterMode(mcDrvTexref hTexRef, mcDrvFilter_mode fm);

__dparm_deprecated(cuTexRefSetMipmapLevelBias not supported) mcDrvError_t
    wcuTexRefSetMipmapLevelBias(mcDrvTexref hTexRef, float bias);

__dparm_deprecated(cuTexRefSetMipmapLevelClamp not supported) mcDrvError_t
    wcuTexRefSetMipmapLevelClamp(mcDrvTexref hTexRef, float minMipmapLevelClamp,
                                 float maxMipmapLevelClamp);

__dparm_deprecated(cuTexRefSetMaxAnisotropy not supported) mcDrvError_t
    wcuTexRefSetMaxAnisotropy(mcDrvTexref hTexRef, unsigned int maxAniso);

__dparm_deprecated(cuTexRefSetBorderColor not supported) mcDrvError_t
    wcuTexRefSetBorderColor(mcDrvTexref hTexRef, float *pBorderColor);

__dparm_deprecated(cuTexRefSetFlags not supported) mcDrvError_t
    wcuTexRefSetFlags(mcDrvTexref hTexRef, unsigned int Flags);

__dparm_deprecated(cuTexRefGetAddress not supported) mcDrvError_t
    wcuTexRefGetAddress(mcDrvDeviceptr_t *pdptr, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetArray not supported) mcDrvError_t
    wcuTexRefGetArray(MCarray *phArray, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetMipmappedArray not supported) mcDrvError_t
    wcuTexRefGetMipmappedArray(mcDrvMipmappedArray *phMipmappedArray, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetAddressMode not supported) mcDrvError_t
    wcuTexRefGetAddressMode(mcDrvAddress_mode *pam, mcDrvTexref hTexRef, int dim);

__dparm_deprecated(cuTexRefGetFilterMode not supported) mcDrvError_t
    wcuTexRefGetFilterMode(mcDrvFilter_mode *pfm, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetMipmapFilterMode not supported) mcDrvError_t
    wcuTexRefGetMipmapFilterMode(mcDrvFilter_mode *pfm, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetMipmapLevelBias not supported) mcDrvError_t
    wcuTexRefGetMipmapLevelBias(float *pbias, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetMipmapLevelClamp not supported) mcDrvError_t
    wcuTexRefGetMipmapLevelClamp(float *pminMipmapLevelClamp, float *pmaxMipmapLevelClamp,
                                 mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetMaxAnisotropy not supported) mcDrvError_t
    wcuTexRefGetMaxAnisotropy(int *pmaxAniso, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetBorderColor not supported) mcDrvError_t
    wcuTexRefGetBorderColor(float *pBorderColor, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetFlags not supported) mcDrvError_t
    wcuTexRefGetFlags(unsigned int *pFlags, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefGetFormat not supported) mcDrvError_t
    wcuTexRefGetFormat(mcArray_Format *pFormat, int *pNumChannels, mcDrvTexref hTexRef);

__dparm_deprecated(cuTexRefCreate not supported) mcDrvError_t wcuTexRefCreate(mcDrvTexref *pTexRef);

__dparm_deprecated(cuTexRefDestroy not supported) mcDrvError_t
    wcuTexRefDestroy(mcDrvTexref hTexRef);

/**
 * @} TEXREF_DEPRECATED
 */

/**
 * @defgroup Texture Object Management
 *
 * This section describes the texture object management functions of the
 * low-level MACA driver application programming interface.
 *
 * @{
 */

mcDrvError_t wcuTexObjectCreate(mcDrvTexObject *pTexObject, const mcDrvResourceDesc *pResDesc,
                                const mcDrvTextureDesc *pTexDesc,
                                const mcDrvResourceViewDesc *pResViewDesc);

mcDrvError_t wcuTexObjectDestroy(mcDrvTexObject texObject);

mcDrvError_t wcuTexObjectGetResourceDesc(mcDrvResourceDesc *pResDesc, mcDrvTexObject texObject);

mcDrvError_t wcuTexObjectGetTextureDesc(mcDrvTextureDesc *pTexDesc, mcDrvTexObject texObject);

mcDrvError_t wcuTexObjectGetResourceViewDesc(mcDrvResourceViewDesc *pResViewDesc,
                                             mcDrvTexObject texObject);

/**
 * @} TEXOBJECT
 */

/**
 * @defgroup Surface Reference Management [DEPRECATED]
 *
 * This section describes the surface reference management functions of the
 * low-level MACA driver application programming interface.
 *
 * @{
 */

__dparm_deprecated(cuSurfRefSetArray not supported) mcDrvError_t
    wcuSurfRefSetArray(mcDrvSurfref hSurfRef, MCarray hArray, unsigned int Flags);

__dparm_deprecated(cuSurfRefGetArray not supported) mcDrvError_t
    wcuSurfRefGetArray(MCarray *phArray, mcDrvSurfref hSurfRef);

/**
 * @} SURFREF_DEPRECATED
 */

/**
 * @defgroup Surface Surface Object Management
 *
 * This section describes the surface object management functions of the
 * low-level MACA driver application programming interface.
 *
 * @{
 */
mcDrvError_t wcuSurfObjectCreate(mcDrvSurfObject *pSurfObject, const mcDrvResourceDesc *pResDesc);
mcDrvError_t wcuSurfObjectDestroy(mcDrvSurfObject surfObject);
mcDrvError_t wcuSurfObjectGetResourceDesc(mcDrvResourceDesc *pResDesc, mcDrvSurfObject surfObject);

#ifdef __cplusplus
} /* extern "C" */
#endif
/**
 * @} Surface
 */

/**
 * @defgroup cuda driver template C++ wrapper
 *
 * Perform automatic type conversion to eliminate need for excessive typecasting (ie void**)
 *
 * __MC_DISABLE_CPP_FUNCTIONS__ macro can be defined to suppress these
 * wrappers. It is useful for applications which need to obtain decltypes of
 * CUDA driver APIs.
 *
 */
#if defined(__cplusplus) && !defined(__MC_DISABLE_CPP_FUNCTIONS__)
#include "cuda_driver_template_wrapper.h"
#endif

/**
 * @} wcuDriver
 */

#endif
