#ifndef __CUDA_EGL_WRAPPER_TYPEDEF_H__
#define __CUDA_EGL_WRAPPER_TYPEDEF_H__

#include "mcEGLTypedefs.h"

typedef mcDrvError_t (*PFN_wcuGraphicsEGLRegisterImage)(mcDrvGraphicsResource *pMacaResource,
                                                        EGLImageKHR image, unsigned int flags);
typedef mcDrvError_t (*PFN_wcuEGLStreamConsumerConnect)(mcDrveglStreamConnection *conn,
                                                        EGLStreamKHR stream);
typedef mcDrvError_t (*PFN_wcuEGLStreamConsumerConnectWithFlags)(mcDrveglStreamConnection *conn,
                                                                 EGLStreamKHR stream,
                                                                 unsigned int flags);
typedef mcDrvError_t (*PFN_wcuEGLStreamConsumerDisconnect)(mcDrveglStreamConnection *conn);
typedef mcDrvError_t (*PFN_wcuEGLStreamConsumerAcquireFrame)(mcDrveglStreamConnection *conn,
                                                             mcDrvGraphicsResource *pMacaResource,
                                                             mcDrvStream_t *pStream,
                                                             unsigned int timeout);
typedef mcDrvError_t (*PFN_wcuEGLStreamConsumerReleaseFrame)(mcDrveglStreamConnection *conn,
                                                             mcDrvGraphicsResource pMacaResource,
                                                             mcDrvStream_t *pStream);
typedef mcDrvError_t (*PFN_wcuEGLStreamProducerConnect)(mcDrveglStreamConnection *conn,
                                                        EGLStreamKHR stream, EGLint width,
                                                        EGLint height);
typedef mcDrvError_t (*PFN_wcuEGLStreamProducerDisconnect)(mcDrveglStreamConnection *conn);
typedef mcDrvError_t (*PFN_wcuEGLStreamProducerPresentFrame)(mcDrveglStreamConnection *conn,
                                                             mcDrveglFrame_v1 eglframe,
                                                             mcDrvStream_t *pStream);
typedef mcDrvError_t (*PFN_wcuEGLStreamProducerReturnFrame)(mcDrveglStreamConnection *conn,
                                                            mcDrveglFrame_v1 *eglframe,
                                                            mcDrvStream_t *pStream);
typedef mcDrvError_t (*PFN_wcuGraphicsResourceGetMappedEglFrame)(mcDrveglFrame_v1 *eglFrame,
                                                                 mcDrvGraphicsResource resource,
                                                                 unsigned int index,
                                                                 unsigned int mipLevel);
typedef mcDrvError_t (*PFN_wcuEventCreateFromEGLSync)(mcDrvEvent_t *phEvent, EGLSyncKHR eglSync,
                                                      unsigned int flags);

#endif