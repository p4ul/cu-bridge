#ifndef __CUDA_TO_MACA_RTC_ADAPTOR_H__
#define __CUDA_TO_MACA_RTC_ADAPTOR_H__

/***********NVRTC**************/

#define nvrtcGetErrorString       wnvrtcGetErrorString
#define nvrtcGetNumSupportedArchs wnvrtcGetNumSupportedArchs
#define nvrtcGetSupportedArchs    wnvrtcGetSupportedArchs
#define nvrtcVersion              wnvrtcVersion
#define nvrtcAddNameExpression    wnvrtcAddNameExpression
#define nvrtcCompileProgram       wnvrtcCompileProgram
#define nvrtcCreateProgram        wnvrtcCreateProgram
#define nvrtcDestroyProgram       wnvrtcDestroyProgram
#define nvrtcGetLoweredName       wnvrtcGetLoweredName
#define nvrtcGetPTX               wnvrtcGetPTX
#define nvrtcGetPTXSize           wnvrtcGetPTXSize
#define nvrtcGetCUBIN             wnvrtcGetCUBIN
#define nvrtcGetCUBINSize         wnvrtcGetCUBINSize
#define nvrtcGetNVVM              wnvrtcGetNVVM
#define nvrtcGetNVVMSize          wnvrtcGetNVVMSize
#define nvrtcGetProgramLog        wnvrtcGetProgramLog
#define nvrtcGetTypeName          wnvrtcGetTypeName
#define nvrtcGetProgramLogSize    wnvrtcGetProgramLogSize

#define nvrtcResult   mcrtcResult
#define _nvrtcProgram _mcrtcProgram
#define nvrtcProgram  mcrtcProgram

#define NVRTC_SUCCESS                         MCRTC_SUCCESS
#define NVRTC_ERROR_OUT_OF_MEMORY             MCRTC_ERROR_OUT_OF_MEMORY
#define NVRTC_ERROR_PROGRAM_CREATION_FAILURE  MCRTC_ERROR_PROGRAM_CREATION_FAILURE
#define NVRTC_ERROR_INVALID_INPUT             MCRTC_ERROR_INVALID_INPUT
#define NVRTC_ERROR_INVALID_PROGRAM           MCRTC_ERROR_INVALID_PROGRAM
#define NVRTC_ERROR_INVALID_OPTION            MCRTC_ERROR_INVALID_OPTION
#define NVRTC_ERROR_COMPILATION               MCRTC_ERROR_COMPILATION
#define NVRTC_ERROR_BUILTIN_OPERATION_FAILURE MCRTC_ERROR_BUILTIN_OPERATION_FAILURE
#define NVRTC_ERROR_NO_NAME_EXPRESSIONS_AFTER_COMPILATION                                          \
    MCRTC_ERROR_NO_NAME_EXPRESSIONS_AFTER_COMPILATION
#define NVRTC_ERROR_NO_LOWERED_NAMES_BEFORE_COMPILATION                                            \
    MCRTC_ERROR_NO_LOWERED_NAMES_BEFORE_COMPILATION
#define NVRTC_ERROR_NAME_EXPRESSION_NOT_VALID MCRTC_ERROR_NAME_EXPRESSION_NOT_VALID
#define NVRTC_ERROR_INTERNAL_ERROR            MCRTC_ERROR_INTERNAL_ERROR

#endif
