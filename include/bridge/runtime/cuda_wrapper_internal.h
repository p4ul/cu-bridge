#ifndef __CUDA_WRAPPER_INTERNAL_H__
#define __CUDA_WRAPPER_INTERNAL_H__

extern enum wcudaNvArchType __nv_arch_type_internal__;
extern int __wcuda_version_internal__;

#if defined(NV_ARCH_V100)
__attribute__((constructor(103), weak)) void init_nv_arch_V100()
{
    __nv_arch_type_internal__ = __NV_ARCH_V100;
}
#endif
#if defined(NV_ARCH_T4)
__attribute__((constructor(101), weak)) void init_nv_arch_T4()
{
    __nv_arch_type_internal__ = __NV_ARCH_T4;
}
#endif
#if defined(NV_ARCH_A100)
__attribute__((constructor(102), weak)) void init_nv_arch_A100()
{
    __nv_arch_type_internal__ = __NV_ARCH_A100;
}
#endif

#if defined(WCUDA_VERSION)
__attribute__((constructor, weak)) void init_wcuda_version()
{
    __wcuda_version_internal__ = WCUDA_VERSION;
}
#endif

#endif //__CUDA_WRAPPER_INTERNAL_H__