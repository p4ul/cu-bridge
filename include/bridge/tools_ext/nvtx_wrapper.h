#ifndef __NVTX_WRAPPER_H__
#define __NVTX_WRAPPER_H__

#include "nvtx_to_mctx_adaptor.h"

#include <stddef.h>

#include "mcToolsExt.h"
#include "mcToolsExtSync.h"
#include "mcToolsExtPayload.h"
#include "mc_runtime_types.h"

#include <CL/cl.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* ====================== mcToolsExt.h ============================== */
void wnvtxInitialize_v3(const void *reserved);

int wnvtxInitialize_v2(const mctxInitializationAttributes_t *initAttrib);

void wnvtxDomainMarkEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib);

void wnvtxMarkEx(const mctxEventAttributes_t *eventAttrib);
void wnvtxMarkA(const char *message);
void wnvtxMarkW(const wchar_t *message);

mctxRangeId_t wnvtxDomainRangeStartEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib);

mctxRangeId_t wnvtxRangeStartEx(const mctxEventAttributes_t *eventAttrib);
mctxRangeId_t wnvtxRangeStartA(const char *message);
mctxRangeId_t wnvtxRangeStartW(const wchar_t *message);

void wnvtxDomainRangeEnd(mctxDomainHandle_t domain, mctxRangeId_t id);

void wnvtxRangeEnd(mctxRangeId_t id);

int wnvtxDomainRangePushEx(mctxDomainHandle_t domain, const mctxEventAttributes_t *eventAttrib);

int wnvtxRangePushEx(const mctxEventAttributes_t *eventAttrib);
int wnvtxRangePushA(const char *message);
int wnvtxRangePushW(const wchar_t *message);

int wnvtxDomainRangePop(mctxDomainHandle_t domain);

int wnvtxRangePop(void);

mctxResourceHandle_t wnvtxDomainResourceCreate(mctxDomainHandle_t domain, mctxResourceAttributes_t *attribs);

void wnvtxDomainResourceDestroy(mctxResourceHandle_t resource);

void wnvtxDomainNameCategoryA(mctxDomainHandle_t domain, uint32_t category, const char *name);
void wnvtxDomainNameCategoryW(mctxDomainHandle_t domain, uint32_t category, const wchar_t *name);

void wnvtxNameCategoryA(uint32_t category, const char *name);
void wnvtxNameCategoryW(uint32_t category, const wchar_t *name);

void wnvtxNameOsThreadA(uint32_t threadId, const char *name);
void wnvtxNameOsThreadW(uint32_t threadId, const wchar_t *name);

mctxStringHandle_t wnvtxDomainRegisterStringA(mctxDomainHandle_t domain, const char *string);
mctxStringHandle_t wnvtxDomainRegisterStringW(mctxDomainHandle_t domain, const wchar_t *string);

mctxDomainHandle_t wnvtxDomainCreateA(const char *name);
mctxDomainHandle_t wnvtxDomainCreateW(const wchar_t *name);

void wnvtxDomainDestroy(mctxDomainHandle_t domain);

/* ====================== mcToolsExt.h end ============================== */

/* ====================== mcToolsExtCuda.h ================================ */
void wnvtxNameCuDeviceA(mcDevice_t device, const char *name);
void wnvtxNameCuDeviceW(mcDevice_t device, const wchar_t *name);

void wnvtxNameCuContextA(mcCtx_t context, const char *name);
void wnvtxNameCuContextW(mcCtx_t context, const wchar_t *name);

void wnvtxNameCuStreamA(mcStream_t stream, const char *name);
void wnvtxNameCuStreamW(mcStream_t stream, const wchar_t *name);

void wnvtxNameCuEventA(mcEvent_t event, const char *name);
void wnvtxNameCuEventW(mcEvent_t event, const wchar_t *name);

/* ====================== mcToolsExtCuda.h end ============================ */

/* ====================== mcToolsExtCudaRt.h ============================== */
void wnvtxNameCudaDeviceA(int device, const char *name);
void wnvtxNameCudaDeviceW(int device, const wchar_t *name);

void wnvtxNameCudaStreamA(mcStream_t stream, const char *name);
void wnvtxNameCudaStreamW(mcStream_t stream, const wchar_t *name);

void wnvtxNameCudaEventA(mcEvent_t event, const char *name);
void wnvtxNameCudaEventW(mcEvent_t event, const wchar_t *name);

/* ====================== mcToolsExtCudaRt.h end ============================== */

/* ====================== mcToolsExtOpenCL.h ============================== */
void wnvtxNameClDeviceA(cl_device_id device, const char *name);
void wnvtxNameClDeviceW(cl_device_id device, const wchar_t *name);

/* Annotates an OpenCL context. */
void wnvtxNameClContextA(cl_context context, const char *name);
void wnvtxNameClContextW(cl_context context, const wchar_t *name);

void wnvtxNameClCommandQueueA(cl_command_queue command_queue, const char *name);
void wnvtxNameClCommandQueueW(cl_command_queue command_queue, const wchar_t *name);

void wnvtxNameClMemObjectA(cl_mem memobj, const char *name);
void wnvtxNameClMemObjectW(cl_mem memobj, const wchar_t *name);

void wnvtxNameClSamplerA(cl_sampler sampler, const char *name);
void wnvtxNameClSamplerW(cl_sampler sampler, const wchar_t *name);

void wnvtxNameClProgramA(cl_program program, const char *name);
void wnvtxNameClProgramW(cl_program program, const wchar_t *name);

void wnvtxNameClEventA(cl_event evnt, const char *name);
void wnvtxNameClEventW(cl_event evnt, const wchar_t *name);

/* ====================== mcToolsExtOpenCL.h end ============================== */

/* ====================== mcToolsExtSync.h ============================== */
mctxSyncUser_t wnvtxDomainSyncUserCreate(mctxDomainHandle_t domain, const mctxSyncUserAttributes_t *attribs);

void wnvtxDomainSyncUserDestroy(mctxSyncUser_t handle);

void wnvtxDomainSyncUserAcquireStart(mctxSyncUser_t handle);

void wnvtxDomainSyncUserAcquireFailed(mctxSyncUser_t handle);

void wnvtxDomainSyncUserAcquireSuccess(mctxSyncUser_t handle);

void wnvtxDomainSyncUserReleasing(mctxSyncUser_t handle);

/* ====================== mcToolsExtSync.h end ============================== */

/* ====================== mcToolsExtPayload.h ============================== */
uint64_t wnvtxPayloadSchemaRegister(mctxDomainHandle_t domain, const mctxPayloadSchemaAttr_t *attr);

uint64_t wnvtxPayloadEnumRegister(mctxDomainHandle_t domain, const mctxPayloadEnumAttr_t *attr);

/* ====================== mcToolsExtPayload.h end ============================== */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __NVTX_WRAPPER_H__ */