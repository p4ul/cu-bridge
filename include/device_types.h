#ifndef __DEVICE_TYPES_H__
#define __DEVICE_TYPES_H__

#include <common/__clang_macac_math.h>

#define cudaRoundMode mcRoundMode
#define cudaRoundNearest mcRoundNearest
#define cudaRoundZero mcRoundZero
#define cudaRoundPosInf mcRoundPosInf
#define cudaRoundMinInf mcRoundMinInf

#endif /* __DEVICE_TYPES_H__ */