#ifndef CUTLASS_WRAPPER_H_
#define CUTLASS_WRAPPER_H_

#define ENABLE_CUDA_TO_MACA_ADAPTOR
#include "bridge/runtime/cuda_runtime_wrapper.h"
#include "bridge/runtime/cuda_to_maca_mcr_adaptor.h"
#include "common/__clang_maca_common_define.h"

#define cutlass mctlass
#define CUTLASS MCTLASS
#define nvcuda mxmaca
#define __nv_bfloat16 maca_bfloat16

#define CUTLASS_HOST_DEVICE MCTLASS_HOST_DEVICE
#define CUTLASS_DEVICE MCTLASS_DEVICE
#define CUTLASS_PRAGMA_UNROLL MCTLASS_PRAGMA_UNROLL
#define CUTLASS_PRAGMA_NO_UNROLL MCTLASS_PRAGMA_NO_UNROLL
#define CUTLASS_GEMM_LOOP MCTLASS_GEMM_LOOP
#define CUTLASS_TRACE_HOST MCTLASS_TRACE_HOST
#define cutlassGetStatusString mctlassGetStatusString

#endif //CUTLASS_WRAPPER_H_
