/** \file nvToolsExt.h
 */
#include "mcToolsExt.h"
#include "bridge/tools_ext/nvtx_wrapper.h"

#if defined(NVTX_VERSION) && NVTX_VERSION < 3
#error                                                                                             \
    "Trying to #include NVTX version 3 in a source file where an older NVTX version has already been included.  If you are not directly using NVTX (the NVIDIA Tools Extension library), you are getting this error because libraries you are using have included different versions of NVTX.  Suggested solutions are: (1) reorder #includes so the newest NVTX version is included first, (2) avoid using the conflicting libraries in the same .c/.cpp file, or (3) update the library using the older NVTX version to use the newer version instead."
#endif

/* Header guard */
#if !defined(NVTX_VERSION)
#define NVTX_VERSION 3

#if defined(_MSC_VER)
#define NVTX_API           __stdcall
#define NVTX_INLINE_STATIC __inline static
#else /*defined(__GNUC__)*/
#define NVTX_API
#define NVTX_INLINE_STATIC inline static
#endif /* Platform */

#if defined(NVTX_NO_IMPL)
#define NVTX_DECLSPEC
#elif defined(NVTX_EXPORT_API)
#if !defined(NVTX_DECLSPEC)
#define NVTX_DECLSPEC
#endif
#else
#define NVTX_DECLSPEC NVTX_INLINE_STATIC
#endif

#ifndef NVTX_STDINT_TYPES_ALREADY_DEFINED
#include <stdint.h>
#endif

#define nvtxInitialize wnvtxInitialize_v3

#ifdef UNICODE
#define nvtxMark         wnvtxMarkW
#define nvtxRangeStart   wnvtxRangeStartW
#define nvtxRangePush    wnvtxRangePushW
#define nvtxNameCategory wnvtxNameCategoryW
#define nvtxNameOsThread wnvtxNameOsThreadW
/* NVTX_VERSION_2 */
#define nvtxDomainCreate         wnvtxDomainCreateW
#define nvtxDomainRegisterString wnvtxDomainRegisterStringW
#define nvtxDomainNameCategory   wnvtxDomainNameCategoryW
#else
#define nvtxMark                 wnvtxMarkA
#define nvtxRangeStart           wnvtxRangeStartA
#define nvtxRangePush            wnvtxRangePushA
#define nvtxNameCategory         wnvtxNameCategoryA
#define nvtxNameOsThread         wnvtxNameOsThreadA
/* NVTX_VERSION_2 */
#define nvtxDomainCreate         wnvtxDomainCreateA
#define nvtxDomainRegisterString wnvtxDomainRegisterStringA
#define nvtxDomainNameCategory   wnvtxDomainNameCategoryA
#endif

#endif /* !defined(NVTX_VERSION) */
