#ifndef NVTOOLSEXT_CUDA_V3
#define NVTOOLSEXT_CUDA_V3

#include "mcToolsExtMaca.h"
#include "bridge/tools_ext/nvtx_wrapper.h"

#ifdef UNICODE
#define nvtxNameCuDevice  wnvtxNameCuDeviceW
#define nvtxNameCuContext wnvtxNameCuContextW
#define nvtxNameCuStream  wnvtxNameCuStreamW
#define nvtxNameCuEvent   wnvtxNameCuEventW
#else
#define nvtxNameCuDevice  wnvtxNameCuDeviceA
#define nvtxNameCuContext wnvtxNameCuContextA
#define nvtxNameCuStream  wnvtxNameCuStreamA
#define nvtxNameCuEvent   wnvtxNameCuEventA
#endif

#endif /* NVTOOLSEXT_CUDA_V3 */