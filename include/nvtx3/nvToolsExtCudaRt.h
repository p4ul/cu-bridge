#ifndef NVTOOLSEXT_CUDART_V3
#define NVTOOLSEXT_CUDART_V3

#include "mcToolsExtMaca.h"
#include "bridge/tools_ext/nvtx_wrapper.h"

#ifdef UNICODE
#define nvtxNameCudaDevice wnvtxNameCudaDeviceW
#define nvtxNameCudaStream wnvtxNameCudaStreamW
#define nvtxNameCudaEvent  wnvtxNameCudaEventW
#else
#define nvtxNameCudaDevice wnvtxNameCudaDeviceA
#define nvtxNameCudaStream wnvtxNameCudaStreamA
#define nvtxNameCudaEvent  wnvtxNameCudaEventA
#endif

#endif /* NVTOOLSEXT_CUDART_V3 */