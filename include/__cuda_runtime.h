#if !defined(__CUDA_RUNTIME_H__)
#define __CUDA_RUNTIME_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
#include <fstream>
#endif

#include <string.h>
#include <strings.h>

#ifndef STRCASECMP
#define STRCASECMP strcasecmp
#endif
#ifndef STRNCASECMP
#define STRNCASECMP strncasecmp
#endif
#ifndef STRCPY
#define STRCPY(sFilePath, nLength, sPath) strcpy(sFilePath, sPath)
#endif

int getCmdLineArgumentInt(const int argc, const char **argv,
                          const char *string_ref);

#define __DRIVER_TYPES_H__
#define ENABLE_CUDA_TO_MACA_ADAPTOR

#include "bridge/runtime/cuda_driver_wrapper.h"
#include "bridge/runtime/cuda_runtime_wrapper.h"
#include "bridge/runtime/cuda_to_maca_mcr_adaptor.h"
#include "bridge/runtime/cuda_wrapper_internal.h"
#include <math.h>
#include <mc_runtime.h>
#include <stdlib.h>

#ifdef __cplusplus
#include <algorithm>
#endif

#ifdef __CUDACC_EXTENDED_LAMBDA__
#include <functional>
#include <utility>
#endif

#include "crt/host_defines.h"
#include "library_types.h"

#endif /*__CUDA_RUNTIME_H__ */