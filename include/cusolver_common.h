#ifndef MCSOLVER_CUSOLVER_COMMON_H_
#define MCSOLVER_CUSOLVER_COMMON_H_

#define ENABLE_CUDA_TO_MACA_ADAPTOR

#include <__cuda_runtime.h>
#include <bridge/solver/cusolver_common_wrapper.h>

#endif  // MCSOLVER_CUSOLVER_COMMON_H_
