#if !defined(__CUDA_BF16_HPP__)
#define __CUDA_BF16_HPP__

#ifdef CUDA_NO_BFLOAT16
#define MACA_NO_BFLOAT16
#endif

#include "maca_bfloat16.h"

#if defined(__cplusplus) && !defined(CUDA_NO_BFLOAT16)
#ifndef __CUDA_BF16_TYPES_EXIST__

typedef maca_bfloat16 __nv_bfloat16;
typedef struct __maca_bfloat162 __nv_bfloat162;
typedef struct __maca_bfloat162_raw_st __nv_bfloat162_raw;
typedef struct __maca_bfloat16_raw_st __nv_bfloat16_raw;
typedef maca_bfloat16 nv_bfloat16;
typedef struct __maca_bfloat162 nv_bfloat162;
#define make_bfloat162 make_maca_bfloat162

#define __BFLOAT16_TO_US(var) *(reinterpret_cast<unsigned short *>(&(var)))
#define __BFLOAT16_TO_CUS(var) *(reinterpret_cast<const unsigned short *>(&(var)))

#define __CUDA_BF16_TYPES_EXIST__
#endif /* __CUDA_BF16_TYPES_EXIST__ */

#endif
#endif /* __CUDA_BF16_HPP__ */