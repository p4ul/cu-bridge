#if !defined(__CUDA_FP8_HPP__)
#define __CUDA_FP8_HPP__

#if defined(__CUDACC__)
#ifndef __MACACC__
#define __MACACC__
#endif
#endif

#include "cuda_bf16.h"
#include "cuda_fp16.h"
#include "vector_types.h"

#include "maca_fp8.h"

#define __nv_cvt_double_to_fp8         __maca_cvt_double_to_fp8
#define __nv_cvt_double2_to_fp8x2      __maca_cvt_double2_to_fp8x2
#define __nv_cvt_float_to_fp8          __maca_cvt_float_to_fp8
#define __nv_cvt_float2_to_fp8x2       __maca_cvt_float2_to_fp8x2
#define __nv_cvt_halfraw_to_fp8        __maca_cvt_halfraw_to_fp8
#define __nv_cvt_halfraw2_to_fp8x2     __maca_cvt_halfraw2_to_fp8x2
#define __nv_cvt_bfloat16raw_to_fp8    __maca_cvt_bfloat16raw_to_fp8
#define __nv_cvt_bfloat16raw2_to_fp8x2 __maca_cvt_bfloat16raw2_to_fp8x2
#define __nv_cvt_fp8_to_halfraw        __maca_cvt_fp8_to_halfraw
#define __nv_cvt_fp8x2_to_halfraw2     __maca_cvt_fp8x2_to_halfraw2

#define __NV_NOSAT     __MACA_NOSAT
#define __NV_SATFINITE __MACA_SATFINITE

#define __NV_E4M3 __MACA_E4M3
#define __NV_E5M2 __MACA_E5M2

typedef unsigned char __nv_fp8_storage_t;
typedef unsigned short int __nv_fp8x2_storage_t;
typedef unsigned int __nv_fp8x4_storage_t;
typedef enum __maca_saturation_t __nv_saturation_t;
typedef enum __maca_fp8_interpretation_t __nv_fp8_interpretation_t;

typedef struct __maca_fp8_e5m2 __nv_fp8_e5m2;
typedef struct __maca_fp8x2_e5m2 __nv_fp8x2_e5m2;
typedef struct __maca_fp8x4_e5m2 __nv_fp8x4_e5m2;

typedef struct __maca_fp8_e4m3 __nv_fp8_e4m3;
typedef struct __maca_fp8x2_e4m3 __nv_fp8x2_e4m3;
typedef struct __maca_fp8x4_e4m3 __nv_fp8x4_e4m3;

#endif