#ifndef _HELPER_CUDA_DRVAPI_H
#define _HELPER_CUDA_DRVAPI_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <cstring>
#include <sstream>

#include <helper_string.h>
#include <__cuda_runtime.h>
#include <samples_cuda_to_maca_adaptor.h>

#ifdef __cuda_cuda_h__

#ifndef checkCudaErrors
#define checkCudaErrors(err) __wcheckCudaErrors(err, __FILE__, __LINE__)

inline void __wcheckCudaErrors(mcDrvError_t err, const char *file, const int line)
{
    if (MC_SUCCESS != err)
    {
        const char *errorStr = NULL;
        wcuGetErrorString(err, &errorStr);
        fprintf(stderr,
                "checkCudaErrors() Driver API error = %04d \"%s\" from file <%s>, "
                "line %i.\n",
                err, errorStr, file, line);
        exit(EXIT_FAILURE);
    }
}
#endif

template <class T>
void wgetCudaAttribute(T *attribute, mcDrvDeviceAttribute_t device_attribute,
                       int device)
{
    checkCudaErrors(wcuDeviceGetAttribute(attribute, device_attribute, device));
}
#endif

/* TODO:mcDeviceGetCount is NOT READY */
inline int gpuDeviceInitDRV(int ARGC, const char **ARGV)
{
    (void)ARGC;
    (void)ARGV;
    exit(EXIT_FAILURE);
}
/* TODO:mcDeviceGetCount is NOT READY */
inline int gpuGetMaxGflopsDeviceIdDRV(void)
{
    exit(EXIT_FAILURE);
}
/* TODO:mcDeviceGetCount is NOT READY */
inline mcDrvDevice_t wfindCudaDeviceDRV(int argc, const char **argv)
{
    (void)argc;
    (void)argv;
    return MC_ERROR_UNKNOWN;
}
/* TODO:mcDeviceGetCount is NOT READY */
inline mcDrvDevice_t findIntegratedGPUDrv(void)
{
    return MC_ERROR_UNKNOWN;
}
/* TODO:NOT SUPPORT */
bool inline wcheckCudaCapabilitiesDRV(int major_version, int minor_version,
                                      int devID)
{
    (void)major_version;
    (void)devID;
    (void)minor_version;
    exit(EXIT_FAILURE);
}
/* TODO:NOT READY */
bool inline findFatbinPath(const char *module_file, std::string &module_path, char **argv, std::ostringstream &ostrm)
{
    (void)module_file;
    (void)module_path;
    (void)argv;
    (void)ostrm;
    exit(EXIT_FAILURE);
}

#endif