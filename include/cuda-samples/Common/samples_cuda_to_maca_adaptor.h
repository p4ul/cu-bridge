#ifndef __SAMPLES_CUDA_TO_MACA_ADAPTOR_H__
#define __SAMPLES_CUDA_TO_MACA_ADAPTOR_H__

/*****************namespace**************************/
#define nv mc

/*****************math constants**************************/
#define NV_PI MC_PI

/*****************multithreading**************************/
#define CUTThread mcThread_t
#define CUT_THREADROUTINE mcThreadRoutine_t
#define CUT_THREADPROC MC_THREADPROC
#define CUT_THREADEND MC_THREADEND
#define cutStartThread mcStartThread
#define cutEndThread mcEndThread
#define cutDestroyThread mcDestroyThread
#define cutWaitForThreads mcWaitForThreads

/*****************helper_cuda**************************/
#ifdef __HELPER_MACA_H_
#define checkCudaErrors checkMacaErrors
#endif
#define getLastCudaError getLastMacaError
#define printLastCudaError printLastMacaError
#define findCudaDevice findMacaDevice
#define checkCudaCapabilities checkMacaCapabilities

/****************helper_cuda_drvapi******************/
#define getCudaAttribute wgetCudaAttribute
#define findCudaDeviceDRV wfindCudaDeviceDRV
#define checkCudaCapabilitiesDRV wcheckCudaCapabilitiesDRV

/****************drvapi_error_string*****************/
#define s_CudaErrorStr wCudaErrorStr
#define sCudaDrvErrorString wCudaDrvErrorString
#define getCudaDrvErrorString wgetCudaDrvErrorString

/****************nvrtc_helper*****************/
#define NVRTC_SAFE_CALL MCRTC_SAFE_CALL
#define compileFileToCUBIN compileFileToBitcode
#define loadCUBIN loadCode

#endif /* __SAMPLES_CUDA_TO_MACA_ADAPTOR_H__ */