#ifndef __cuda_cuda_h__
#define __cuda_cuda_h__

#include<__cuda_runtime.h>

#if defined CUDA_VERSION
#define WCUDA_VERSION CUDA_VERSION
#else
#define CUDA_VERSION WCUDA_VERSION
#endif

#ifdef _WIN32
#define CUDAAPI __stdcall
#else
#define CUDAAPI
#endif

#ifdef _WIN32
#define CUDA_CB __stdcall
#else
#define CUDA_CB MC_CB
#endif

#endif /* __cuda_cuda_h__ */