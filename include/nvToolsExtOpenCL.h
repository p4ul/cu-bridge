#ifndef NVTOOLSEXT_OPENCL_H_
#define NVTOOLSEXT_OPENCL_H_

#include "bridge/tools_ext/nvtx_wrapper.h"

#ifdef UNICODE
#define nvtxNameClDevice       wnvtxNameClDeviceW
#define nvtxNameClContext      wnvtxNameClContextW
#define nvtxNameClCommandQueue wnvtxNameClCommandQueueW
#define nvtxNameClMemObject    wnvtxNameClMemObjectW
#define nvtxNameClSampler      wnvtxNameClSamplerW
#define nvtxNameClProgram      wnvtxNameClProgramW
#define nvtxNameClEvent        wnvtxNameClEventW
#else
#define nvtxNameClDevice       wnvtxNameClDeviceA
#define nvtxNameClContext      wnvtxNameClContextA
#define nvtxNameClCommandQueue wnvtxNameClCommandQueueA
#define nvtxNameClMemObject    wnvtxNameClMemObjectA
#define nvtxNameClSampler      wnvtxNameClSamplerA
#define nvtxNameClProgram      wnvtxNameClProgramA
#define nvtxNameClEvent        wnvtxNameClEventA
#endif

#endif /* NVTOOLSEXT_OPENCL_H_ */