#if !defined(__DEVICE_ATOMIC_FUNCTIONS_HPP__)
#define __DEVICE_ATOMIC_FUNCTIONS_HPP__

#if defined(__MACACC_RTC__)
#define __DEVICE_ATOMIC_FUNCTIONS_DECL__ __device__
#else /* __MACACC_RTC__ */
#define __DEVICE_ATOMIC_FUNCTIONS_DECL__ static __inline__ __device__
#endif /* __MACACC_RTC__ */

#if defined(__cplusplus) && defined(__MACACC__)

#include "__clang_maca_atomic_functions.h"
#include "__clang_maca_device_functions.h"

__DEVICE_ATOMIC_FUNCTIONS_DECL__ bool any(bool cond)
{
    return (bool)__any((int)cond);
}

__DEVICE_ATOMIC_FUNCTIONS_DECL__ bool all(bool cond)
{
    return (bool)__all((int)cond);
}

#endif /*__cplusplus && __MACACC__*/

#endif /*__DEVICE_ATOMIC_FUNCTIONS_HPP__*/