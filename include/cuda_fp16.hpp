#ifndef __CUDA_FP16_HPP__
#define __CUDA_FP16_HPP__

#ifdef __CUDA_NO_HALF_CONVERSIONS__
#define __MACA_NO_HALF_CONVERSIONS__
#endif

#ifdef __CUDA_NO_HALF_OPERATORS__
#define __MACA_NO_HALF_OPERATORS__
#endif

#ifdef __CUDA_NO_HALF2_OPERATORS__
#define __MACA_NO_HALF2_OPERATORS__
#endif

#if defined(CUDA_NO_HALF)
#define MACA_NO_HALF
#endif

#include <maca_fp16.h>

#if defined(__cplusplus) && !defined(CUDA_NO_HALF)
#ifndef __CUDA_FP16_TYPES_EXIST__
#define __CUDA_FP16_TYPES_EXIST__
typedef __half __nv_half;
typedef __half2 __nv_half2;
typedef __half_raw __nv_half_raw;
typedef __half2_raw __nv_half2_raw;
typedef __half nv_half;
typedef __half2 nv_half2;
#endif /* __CUDA_FP16_TYPES_EXIST__ */

#endif
#endif /* __CUDA_FP16_HPP__*/
