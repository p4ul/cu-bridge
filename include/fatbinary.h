#ifndef __FATBINARY_H__
#define __FATBINARY_H__

typedef struct fatBinaryHeader * computeFatBinaryFormat_t;
typedef const struct fatBinaryHeader * computeFatBinaryFormat_ct;

#if defined(__GNUC__)
#define fatbinary_ALIGN_(n) __attribute__((aligned(n)))
#elif defined(_WIN32)
#define fatbinary_ALIGN_(n) __declspec(align(n))
#else
#error !! UNSUPPORTED COMPILER !!
#endif

/* Magic numbers */
#define FATBIN_MAGIC  0x4C435F5FU
#define FATBIN_MAGIC1 0x5F474E41U
#define FATBIN_MAGIC2 0x4C46464FU
#define FATBIN_MAGIC3 0x5F44414FU
#define FATBIN_MAGIC4 0x444E5542U
#define FATBIN_MAGIC5 0x5F5F454CU

struct fatbinary_ALIGN_(8) fatBinaryHeader
{
  unsigned int           magic;
  unsigned int           magic1;
  unsigned int           magic2;
  unsigned int           magic3;
  unsigned int           magic4;
  unsigned int           magic5;
};

typedef enum {
  FATBIN_KIND_PTX      = 0x0001,
  FATBIN_KIND_ELF      = 0x0002,
  FATBIN_KIND_OLDCUBIN = 0x0004,
  FATBIN_KIND_NVVM     = 0x0008,
  FATBIN_KIND_MERCURY  = 0x0010,
} fatBinaryCodeKind;

#endif /* __FATBINARY_H__ */