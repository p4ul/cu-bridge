#pragma once
#include "vector_types.h"


#if !defined(__STORAGE__)

#if defined(__CUDACC_RTC__)
#define __STORAGE__ \
        extern const __device__
#else /* !__CUDACC_RTC__ */
#define __STORAGE__ \
        extern const
#endif /* __CUDACC_RTC__ */

#endif /* __STORAGE__ */

#if !defined(__MACACC__)
#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

uint3 __device_builtin__ __STORAGE__ threadIdx;
uint3 __device_builtin__ __STORAGE__ blockIdx;
dim3 __device_builtin__ __STORAGE__ blockDim;
dim3 __device_builtin__ __STORAGE__ gridDim;
int __device_builtin__ __STORAGE__ warpSize;

#undef __STORAGE__

#if defined(__cplusplus)
}
#endif /* __cplusplus */
#endif /*__MACACC__*/

#if !defined(__cudaGet_threadIdx)

#define __cudaGet_threadIdx() \
        threadIdx

#endif /* __cudaGet_threadIdx */

#if !defined(__cudaGet_blockIdx)

#define __cudaGet_blockIdx() \
        blockIdx

#endif /* __cudaGet_blockIdx */

#if !defined(__cudaGet_blockDim)

#define __cudaGet_blockDim() \
        blockDim

#endif /* __cudaGet_blockDim */

#if !defined(__cudaGet_gridDim)

#define __cudaGet_gridDim() \
        gridDim

#endif /* __cudaGet_gridDim */

#if !defined(__cudaGet_warpSize)

#define __cudaGet_warpSize() \
        warpSize

#endif /* __cudaGet_warpSize */
