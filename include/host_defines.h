#if !defined(__CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__)
#if defined(_MSC_VER)
#pragma message("host_defines.h is deprecated and it's enough if cuda_runtime.h is included already")
#else
#warning "host_defines.h is deprecated and it's enough if cuda_runtime.h is included already"
#endif
#define __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
#endif

#include "crt/host_defines.h"

#undef __CUDA_INCLUDE_COMPILER_INTERNAL_HEADERS__
