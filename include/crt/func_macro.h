#if !defined(__FUNC_MACRO_H__)
#define __FUNC_MACRO_H__

#if defined(__GNUC__)

#define __func__(decl) \
    inline decl

#define __device_func__(decl) \
    static __attribute__((__unused__)) decl

#endif /* __GNUC__ */

#endif /* __FUNC_MACRO_H__ */
