#pragma once
#if !defined(__CUDA_INTERNAL_COMPILATION__)

#define __CUDA_INTERNAL_COMPILATION__
#define __text__
#define __surf__
#define __name__shadow_var(c, cpp) \
#c
#define __name__text_var(c, cpp) \
#cpp
#define __host__shadow_var(c, cpp) \
        cpp
#define __text_var(c, cpp) \
        cpp
#define __device_fun(fun) \
#fun
#define __device_var(var) \
#var
#define __device__text_var(c, cpp) \
#c
#define __device__shadow_var(c, cpp) \
#c

#if defined(_WIN32) && !defined(_WIN64)

#define __pad__(f) \
        f

#else /* _WIN32 && !_WIN64 */

#define __pad__(f)

#endif /* _WIN32 && !_WIN64 */

#include "builtin_types.h"
#include "storage_class.h"

#endif /* !__CUDA_INTERNAL_COMPILATION__ */
