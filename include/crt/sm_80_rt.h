#if !defined(__SM_80_RT_H__)
#define __SM_80_RT_H__

#if defined(__MACACC_RTC__)
/*Todo __host__ attribute function is not created. */
#endif /* __MACACC_RTC__ */

#include "__clang_maca_device_functions.h"

#if defined(__cplusplus) && defined(__CUDACC__)

extern "C"
{
    inline __device__ void *__nv_associate_access_property(const void *ptr,
                                                           unsigned long long property)
    {
        /*Todo: associate_access_property_impl need compiler to support.*/
        return nullptr;
    }

    inline __device__ void __nv_memcpy_async_shared_global_4(void *dst,
                                                             const void *src,
                                                             unsigned src_size)
    {
        /*Todo: memcpy_async_shared_global_4_impl need compiler to support.*/
    }

    inline __device__ void __nv_memcpy_async_shared_global_8(void *dst,
                                                             const void *src,
                                                             unsigned src_size)
    {
        /*Todo: memcpy_async_shared_global_8_impl need compiler to support.*/
    }

    inline __device__ void __nv_memcpy_async_shared_global_16(void *dst,
                                                              const void *src,
                                                              unsigned src_size)
    {
        /*Todo: memcpy_async_shared_global_16_impl need compiler to support.*/
    }
}

#endif /* __cplusplus && __CUDACC__ */

#endif /* __SM_80_RT_H__ */