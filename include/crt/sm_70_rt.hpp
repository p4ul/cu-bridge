#if !defined(__SM_70_RT_HPP__)
#define __SM_70_RT_HPP__

#if defined(__MACACC_RTC__)
/*Todo __host__ __device__ attribute function is not create*/
#else // !__MACACC_RTC__
#include "__clang_maca_device_functions.h"
#include "__clang_maca_atomic_functions.h"
#endif //__MACACC_RTC__

#endif /* !__SM_70_RT_HPP__ */