#if !defined(__MACA_MMA_H__)
#define __MACA_MMA_H__

#if defined(__cplusplus) && defined(__CUDACC__)
#include "__clang_maca_mma_functions.h"
#endif /* __cplusplus && __CUDACC__ */

#endif /* !__MACA_MMA_H__ */
