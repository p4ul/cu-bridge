#ifndef __CUDA_VDPAU_INTEROP_H__
#define __CUDA_VDPAU_INTEROP_H__

#include <__cuda_runtime.h>
#include <bridge/video/cuda_vdpau_interop_wrapper.h>

#endif /* __CUDA_VDPAU_INTEROP_H__ */