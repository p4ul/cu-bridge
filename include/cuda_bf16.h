#ifndef __CUDA_BF16_H__
#define __CUDA_BF16_H__

#ifdef __CUDA_NO_BFLOAT16_CONVERSIONS__
#define __MACA_NO_BFLOAT16_CONVERSIONS__
#endif

#ifdef __CUDA_NO_BFLOAT16_OPERATORS__
#define __MACA_NO_BFLOAT16_OPERATORS__
#endif

#ifdef __CUDA_NO_BFLOAT162_OPERATORS__
#define __MACA_NO_BFLOAT162_OPERATORS__
#endif

#ifdef CUDA_NO_BFLOAT16
#define MACA_NO_BFLOAT16
#endif

#include <maca_bfloat16.h>

#if defined(__cplusplus) && !defined(CUDA_NO_BFLOAT16)
#ifndef __CUDA_BF16_TYPES_EXIST__
typedef maca_bfloat16 __nv_bfloat16;
typedef struct __maca_bfloat162 __nv_bfloat162;
typedef __maca_bfloat162_raw __nv_bfloat162_raw;
typedef __maca_bfloat16_raw __nv_bfloat16_raw;
typedef maca_bfloat16 nv_bfloat16;
typedef struct __maca_bfloat162 nv_bfloat162;
#define make_bfloat162 make_maca_bfloat162

#define __CUDA_BF16_TYPES_EXIST__
#endif /* __CUDA_BF16_TYPES_EXIST__ */
#endif

#endif /*__CUDA_BF16_H__*/