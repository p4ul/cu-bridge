#ifndef _COOPERATIVE_GROUPS_MEMCPY_ASYNC
#define _COOPERATIVE_GROUPS_MEMCPY_ASYNC

#include "../cooperative_groups.h"
#include "details/info.h"

#ifdef _CG_CPP11_FEATURES
#include "details/async.h"
#else
#error This file requires compiler support for the ISO C++ 2011 standard. This support must be enabled with the \
         -std=c++11 compiler option.
#endif

#endif // _COOPERATIVE_GROUPS_MEMCPY_ASYNC