/*Todo: include maca async header file */
#ifndef _CG_ASYNC_H
#define _CG_ASYNC_H
#include "maca_async.h"
#include "info.h"

_CG_BEGIN_NAMESPACE namespace details
{
    /* Group wait for prior Nth stage of memcpy_async to complete. */
    template <unsigned int Stage, class TyGroup>
    _CG_STATIC_QUALIFIER void wait_prior(const TyGroup &group)
    {
        /*Todo: need compiler to create __pipeline_wait_prior(stage) api*/
    }
}
_CG_END_NAMESPACE

#endif //_CG_ASYNC_H