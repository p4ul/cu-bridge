
#ifndef _COOPERATIVE_GROUPS_HELPERS_H_
#define _COOPERATIVE_GROUPS_HELPERS_H_
#include "info.h"
#include "maca_cooperative_groups.h"

_CG_BEGIN_NAMESPACE

namespace details
{
    namespace tile
    {
#ifdef _CG_CPP11_FEATURES
        namespace shfl
        {
            template <unsigned int count, bool intSized = (count <= sizeof(int))>
            struct recursive_sliced_shuffle_helper;

            template <unsigned int count>
            struct recursive_sliced_shuffle_helper<count, true>
            {
                /*Todo: nedd compiler to create this api.*/
            };

            template <unsigned int count>
            struct recursive_sliced_shuffle_helper<count, false>
            {
                /*Todo: nedd compiler to create this api.*/
                template <typename TyFn>
                _CG_QUALIFIER void invoke_shuffle(const TyFn &shfl)
                {
                    /*Todo: nedd compiler to create this api.*/
                }
            };
        }
        struct _memory_shuffle
        {
            /*Todo: need compiler to create this struct.*/
        };
        struct _intrinsic_compat_shuffle
        {
            /*Todo: need compiler to create this struct.*/
        };
        struct _native_shuffle
        {
            /*Todo: need compiler to create this struct.*/
        };

#endif // _CG_CPP11_FEATURES
    }
};

_CG_END_NAMESPACE
#endif // _COOPERATIVE_GROUPS_HELPERS_H_