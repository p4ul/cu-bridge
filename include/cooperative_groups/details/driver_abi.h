#ifndef _CG_DRIVER_API_H
#define _CG_DRIVER_API_H

#include "info.h"

_CG_BEGIN_NAMESPACE

namespace details
{
    template <unsigned int RegId>
    _CG_QUALIFIER unsigned int load_env_reg()
    {
        // Abort by default
        _CG_ABORT();
        return 0;
    }

    template <unsigned int HiReg, unsigned int LoReg>
    _CG_QUALIFIER unsigned long long load_env_reg64()
    {
        /*Todo: need compielr to create this api.*/
        return 0;
    }

    struct grid_workspace
    {
        unsigned int wsSize;
        unsigned int barrier;
    };

    _CG_QUALIFIER grid_workspace *get_grid_workspace()
    {
        unsigned long long gridWsAbiAddress = load_env_reg64<1, 2>();
        // Interpret the address from envreg 1 and 2 as the driver's grid workspace
        return (reinterpret_cast<grid_workspace *>(gridWsAbiAddress));
    }
}
_CG_END_NAMESPACE

#endif //_CG_DRIVER_API_H