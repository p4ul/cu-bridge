#ifndef __LIBRARY_TYPES_H__
#define __LIBRARY_TYPES_H__

#include <common/mc_library_types.h>

#define CUDA_R_16F MACA_R_16F
#define CUDA_C_16F MACA_C_16F
#define CUDA_R_16BF MACA_R_16BF
#define CUDA_C_16BF MACA_C_16BF
#define CUDA_R_32F MACA_R_32F
#define CUDA_C_32F MACA_C_32F
#define CUDA_R_64F MACA_R_64F
#define CUDA_C_64F MACA_C_64F
#define CUDA_R_4I MACA_R_4I
#define CUDA_C_4I MACA_C_4I
#define CUDA_R_4U MACA_R_4U
#define CUDA_C_4U MACA_C_4U
#define CUDA_R_8I MACA_R_8I
#define CUDA_C_8I MACA_C_8I
#define CUDA_R_8U MACA_R_8U
#define CUDA_C_8U MACA_C_8U
#define CUDA_R_16I MACA_R_16I
#define CUDA_C_16I MACA_C_16I
#define CUDA_R_16U MACA_R_16U
#define CUDA_C_16U MACA_C_16U
#define CUDA_R_32I MACA_R_32I
#define CUDA_C_32I MACA_C_32I
#define CUDA_R_32U MACA_R_32U
#define CUDA_C_32U MACA_C_32U
#define CUDA_R_64I MACA_R_64I
#define CUDA_C_64I MACA_C_64I
#define CUDA_R_64U MACA_R_64U
#define CUDA_C_64U MACA_C_64U

#define cudaDataType macaDataType
#define cudaDataType_t macaDataType_t

#endif /*__LIBRARY_TYPES_H__*/