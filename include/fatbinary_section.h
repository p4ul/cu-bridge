#ifndef __FATBINARY_SECTION_H__
#define __FATBINARY_SECTION_H__

#ifdef __cplusplus
extern "C" {
#endif

#define FATBINC_MAGIC   0x4D414341
#define FATBINC_VERSION 1
#define FATBINC_LINK_VERSION 1

typedef struct {
  int magic;
  int version;
  const unsigned long long* data;
  void *filename_or_fatbins;
} __fatBinC_Wrapper_t;

#define FATBIN_CONTROL_SECTION_NAME     ".mcFatBinSegment"
#define FATBIN_DATA_SECTION_NAME        ".mc_fatbin"

#define FATBIN_PRELINK_DATA_SECTION_NAME "__nv_relfatbin"

#ifdef __cplusplus
}
#endif

#endif /* __FATBINARY_SECTION_H__ */